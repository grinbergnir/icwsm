<?php $section = "About"; $subsection = "Previous Conferences"; include("../../header.php"); ?>

  <h2 class="pageTitle">Previous Conferences</h2>
         
  <ul>
  
  <li><a href="http://icwsm.org/2007">ICWSM-07: Boulder, Colorado, USA</a></li>
  
  <li><a href="http://icwsm.org/2008">ICWSM-08: Seattle, Washington, USA</a>&nbsp;-&nbsp;<a href="http://videolectures.net/icwsm08_seattle/">Videos</a></li>

  <li><a href="http://icwsm.org/2009">ICWSM-09: San Jose, California, USA</a>&nbsp;-&nbsp;<a href="http://videolectures.net/icwsm09_sanjose/">Videos</a></li>

  <li><a href="http://icwsm.org/2010">ICWSM-10: Washington, DC, USA</a>&nbsp;-&nbsp;<a href="http://videolectures.net/icwsm2010_washington/">Videos</a></li>

  <li><a href="http://icwsm.org/2011">ICWSM-11: Barcelona, Spain</a>&nbsp;-&nbsp;<a href="http://videolectures.net/icwsm2011_barcelona/">Videos</a></li>

  <li><a href="http://icwsm.org/2012">ICWSM-12: Dublin, Ireland</a>&nbsp;-&nbsp;<a href="http://videolectures.net/icwsm2012_dublin/">Videos</a></li>

    <li><a href="http://icwsm.org/2013">ICWSM-13: Boston, MA</a>&nbsp;-&nbsp;<a href="http://videolectures.net/icwsm2013_boston/">Videos</a></li>

    <li><a href="http://icwsm.org/2014">ICWSM-14: Ann Arbor, MI</a></li>

    <li><a href="http://icwsm.org/2015">ICWSM-15: Oxford, UK</a></li>

    <li><a href="http://icwsm.org/2016">ICWSM-16: Cologne, Germany</a></li>

    <li>  <a href="https://scholar.google.com/citations?hl=en&vq=eng_databasesinformationsystems&view_op=list_hcore&venue=Yqny2grKwjoJ.2015">Top ICWSM papers by citation</a></li>
  </ul>

  
<?php include("../../footer.php"); ?>