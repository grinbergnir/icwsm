<?php $section = "About"; $subsection = "Future Conferences"; include("../../header.php"); ?>

  <h2 class="pageTitle">Future Conferences</h2>
         

<h3>Request for Proposals to Host ICWSM-18</h3>

<p>The Association for the Advancement of Artificial Intelligence is currently seeking proposals for a host city for the Twelfth International AAAI Conference on Weblogs and Social Media (ICWSM-18). The conference is typically held Monday - Thursday during the timeframe of mid-May through mid-June. Proposals will be reviewed by the ICWSM Steering Committee, the AAAI Conference Committee, and the AAAI Executive Director. 
<br>
<br>
Preliminary proposals of no more than 1-3 pages are requested by the following dates:
<br>

</p>


<p>Proposals should reflect the following priorities that AAAI usually places on venue selection:</p>

<ol>
<li>Active local research community</li>
<li>Accessibility and attractiveness for national and international attendees</li>
<li>Student accommodations/services available</li>
<li>Cohesive and adequate meeting space with optimal technical infrastructure</li>
<li>Affordability for attendees and conference organizers</li>
</ol>

<p>Proposals should include the following information:</p>

<ol>
<li>Biosketches of one to three local organizers, including a suggestion for the Conference General Chair. The local organizers would work with AAAI to help raise funds to support special activities at the conference, and would serve as liaisons with local organizations, such as universities, research laboratories, and cultural organizations. Please designate a primary contact.</li>
<li>Sponsorship potential. Include a list of local public and private organizations that might be able to support the conference, and an estimate of the anticipated amount of support possible.</li>
<li>Information about the host city. Why would this be a good site for the ICWSM conference? What venues, such as hotels, conference centers, or universities, might be appropriate venues for the conference, which attracts on the order of 300-350 participants? If available, provide a broad view of typical local costs relative to other cities. (Please do not contact conference centers, hotels, or other venues on AAAI's behalf.)</li>
<li>Potential for co-location of other conferences or events. Do you know of other conferences that would consider co-locations if your proposed city is chosen?</li>
</ol>

<p>Strong proposals will be further developed in cooperation with the ICWSM Steering Committee and the AAAI Executive Director. For further information, please contact Carol Hamilton at <a href="mailto:hamilton@aaai.org">hamilton@aaai.org</a>. Proposals should be submitted in PDF format via email attachment by the deadline listed above. Proposals can also be mailed to:</p>

<p>Carol Hamilton<br>
AAAI<br>
2275 East Bayshore Road, Suite 160<br>
Palo Alto, CA 94303</p>

<p>Thank you for your support of ICWSM-17!</p>

<?php include("../../footer.php"); ?>