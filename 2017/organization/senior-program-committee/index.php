<?php $section = "Organisation"; $subsection = "Senior Program Committee"; include("../../header.php"); ?>

<br>  <h2 class="pageTitle">Senior Program Committee</h2>

<h3>Senior Program Committee Members</h3>
<ul>
<li>Alan Mislove (Northeastern University, USA)</li>
<li>Alice Oh (KAIST, Korea)</li>
<li>Andreas Kaltenbrunner (Barcelona Media, Spain)</li>
<li>Ben Zhao (UC Santa Barbara, USA)</li>
<li>Bernardo Huberman (HP Laboratories, USA)</li>
<li>Bernie Hogan (University of Oxford, United Kingdom)</li>
<li>Bogdan State (Stanford University, USA)</li>
<li>Brian Keegan (University of Colorado Boulder, USA)</li>
<li>Bruno Gonçalves (New York University, USA)</li>
<li>Carlos Castillo (Eurecat, Spain)</li>
<li>Catalina Toma (University of Wisconsin-Madison, USA)</li>
<li>Ceren Budak (University of Michigan School of Information, USA)</li>
<li>Christo Wilson (Northeastern University, USA)</li>
<li>Cuihua Shen (University of California, Davis, USA)</li>
<li>Daniel Archambault (Swansea University, United Kingdom)</li>
<li>Daniel Romero (University of Michigan, USA)</li>
<li>David Garcia (ETH Zurich, Switzerland)</li>
<li>David Liben-Nowell (Carleton College, USA)</li>
<li>Deen Freelon (American University, USA)</li>
<li>Emilio Ferrara (University of Southern California, Information Sciences Institute, USA)</li>
<li>Emre Kiciman (Microsoft Research, USA)</li>
<li>Eytan Adar (University of Washington, USA)</li>
<li>Fabricio Benevenuto (Federal University of Minas Gerais (UFMG), Brazil)</li>
<li>Francesco Bonchi (Yahoo! Research, Spain)</li>
<li>Ingmar Weber (Qatar Computing Research Institute, Qatar)</li>
<li>Jahna Otterbacher (Open University of Cyprus, Cyprus)</li>
<li>Javier Borge-Holthoefer (Internet Interdisciplinary Institute, Spain)</li>
<li>Jeff Hancock (Cornell University, USA)</li>
<li>Jessica Vitak (University of Maryland, USA)</li>
<li>Jie Tang (Tsinghua University, China)</li>
<li>Jimmy Lin (University of Waterloo, Canada)</li>
<li>John Breslin (NUI Galway, Ireland)</li>
<li>Kate Starbird (University of Washington, HCDE, USA)</li>
<li>Katherine Ognyanova (Rutgers University, USA)</li>
<li>Kenneth Anderson (University of Colorado at Boulder, USA)</li>
<li>Licia Capra (University College London, United Kingdom)</li>
<li>Luca Maria Aiello (Bell Labs, United Kingdom)</li>
<li>Meeyoung Cha (KAIST & Facebook, Korea)</li>
<li>Milena Tsvetkova (University of Oxford, United Kingdom)</li>
<li>Mirco Musolesi (University College London, United Kingdom)</li>
<li>Nicholas Diakopoulos (University of Maryland, USA)</li>
<li>Nishanth Sastry (King's College London, United Kingdom)</li>
<li>Nuria Oliver (Telefonica I+D, Spain)</li>
<li>Oren Tsur (Harvard | Northeastern University, USA)</li>
<li>Paul Resnick (University of Michigan, USA)</li>
<li>Peter Rentfrow (Cambridge University, United Kingdom)</li>
<li>Renaud Lambiotte (University of Namur, Belgium)</li>
<li>Ricardo Baeza-Yates (NTENT, USA)</li>
<li>Robert West (EPFL, Switzerland)</li>
<li>Roberta Sinatra (CNS and Math Department, Central European University, Hungary, Hungary)</li>
<li>Rossano Schifanella (University of Turin, Italy)</li>
<li>Rosta Farzan (University of Pittsburgh, USA)</li>
<li>Ruth Garcia Gavilanes (Oxford Internet Institute, United Kingdom)</li>
<li>Saeideh Bakhshi (Georgia Institute of Technology, USA)</li>
<li>Shaomei Wu (Facebook Inc., USA)</li>
<li>Steffen Staab (Institut WeST, University Koblenz-Landau and WAIS, University of Southampton, Germany)</li>
<li>Sune Lehmann (Technical University of Denmark, Denmark)</li>
<li>Taha Yasseri (Oxford Internet Institute, University of Oxford, United Kingdom)</li>
<li>Tanushree Mitra (Georgia Institute of Technology, USA)</li>
<li>Uichin Lee (KAIST, Korea)</li>
<li>Virgilio Almeida (UFMG, Brazil)</li>
<li>Xing Xie (Microsoft Research, China)</li>
<li>Yong-Yeol Ahn (Indiana University Bloomington, USA)</li>
<li>Yu-Ru Lin (University of Pittsburgh, USA)</li>
</ul>

<?php include("../../footer.php"); ?>?