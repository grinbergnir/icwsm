<?php $section = "Organisation"; $subsection = "Senior Program Committee"; include("../../header.php"); ?>

<h2 class="pageTitle">ICWSM-16 Senior Program Committee</h2>


<h3>Senior Program Committee Members</h3>
<ul>
<li> Lada Adamic (University of Michigan, USA) </li>
<li> Eytan Adar (University of Washington, USA) </li>
<li> Yong-Yeol Ahn (Indiana University Bloomington, USA) </li>
<li> Luca Maria Aiello (Yahoo Labs, UK) </li>
<li> Virgilio Almeida (UFMG, Brazil) </li>
<li> Ricardo Baeza-Yates (Yahoo Labs, USA) </li>
<li> Saeideh Bakhshi (Yahoo Labs, USA) </li>
<li> Eytan Bakshy (Facebook, USA) </li>
<li> Andrea Baronchelli (City University London, UK) </li>
<li> Fabricio Benevenuto (Federal University of Minas Gerais (UFMG, Brazil) </li>
<li> Johan Bollen (Indiana University, USA) </li>
<li> Francesco Bonchi (ISI Foundation, Italy) </li>
<li> John Breslin NUI Galway, Ireland) </li>
<li> Moira Burke (Facebook, USA) </li>
<li> Carlos Castillo (Sapienza University of Rome, Italy) </li>
<li> Meeyoung Cha (KAIST, South Korea) </li>
<li> Coye Cheshire (UC Berkeley, USA) </li>
<li> Munmun De Choudhury (Georgia Institute of Technology, USA) </li>
<li> Nicholas Diakopoulos (University of Maryland, USA) </li>
<li> Rosta Farzan (University of Pittsburgh, USA) </li>
<li> Emilio Ferrara (University of Southern California, Information Sciences Institute, USA) </li>
<li> David Garcia (ETH Zurich, Switzerland) </li>
<li> Bruno Gon&ccedil;alves (Aix-Marseille Université, France) </li>
<li> Sandra Gonzalez-Bailon (University of Pennsylvania, USA) </li>
<li> Jeff Hancock (Stanford University, USA) </li>
<li> Brent Hecht (University of Minnesota, USA) </li>
<li> Bernie Hogan (University of Oxford, UK) </li>
<li> Bernardo Huberman (HP Laboratories, USA) </li>
<li> David Jurgens (Stanford University, Canada) </li>
<li> Andreas Kaltenbrunner (Barcelona Media </li>
<li> Brian Keegan (Northeastern University, USA) </li>
<li> Emre Kiciman (Microsoft Research, USA) </li>
<li> Renaud Lambiotte (University of Namur, Belgium) </li>
<li> Uichin Lee (KAIST, Korea) </li>
<li> Sune Lehmann (Technical University of Denmark, Denmark) </li>
<li> Kristina Lerman (University of Southern California, USA) </li>
<li> Ee-Peng Lim Singapore Management University, Singapore) </li>
<li> Jimmy Lin (University of Waterloo, Canada) </li>
<li> Yu-Ru Lin (University of Pittsburgh, USA) </li>
<li> Kurt Luther (Virginia Tech, USA) </li>
<li> Michael M&auml;s (ETH Zurich, Switzerland) </li>
<li> Cecilia Mascolo (University of Cambridge, USA) </li>
<li> Winter Mason (Facebook, USA) </li>
<li> Alan Mislove (Northeastern University, USA) </li>
<li> Tanushree Mitra (Georgia Institute of Technology, USA) </li>
<li> Mirco Musolesi (University College London, UK) </li>
<li> Katherine Ognyanova (Rutgers University, USA) </li>
<li> Alice Oh (KAIST, South Korea) </li>
<li> Nuria Oliver (Telefonica Research, Spain) </li>
<li> Jahna Otterbacher (Open University of Cyprus) </li>
<li> J&uuml;rgen Pfeffer (Carnegie Mellon University, USA) </li>
<li> Daniele Quercia (Yahoo Labs, USA) </li>
<li> Paul Resnick (University of Michigan, USA) </li>
<li> Daniel Romero (University of Michigan, USA) </li>
<li> Christian Sandvig (University of Michigan, USA) </li>
<li> Nishanth Sastry (King's College London, UK) </li>
<li> Rossano Schifanella (University of Turin, Italy) </li>
<li> Lauren Scissors (Facebook, USA) </li>
<li> Steffen Staab (University of Koblenz-Landau, Germay) </li>
<li> Kate Starbird (University of Washington, HCDE, USA) </li>
<li> Bogdan State (Stanford University, USA) </li>
<li> Jie Tang (Tsinghua University, China) </li>
<li> Sean Taylor (NYU Stern School, USA) </li>
<li> Loren Terveen (University of Minnesota, USA) </li>
<li> Catalina Toma (University of Wisconsin-Madison, USA) </li>
<li> Oren Tsur (Harvard | Northeastern University, USA) </li>
<li> Johan Ugander (Stanford University, USA) </li>
<li> Jessica Vitak (University of Maryland, USA) </li>
<li> Ingmar Weber (Qatar Computing Research Institute, USA) </li>
<li> Robert West (Stanford University, USA) </li>
<li> Christo Wilson (Northeastern University, USA) </li>
<li> Shaomei Wu (Facebook, USA) </li>
<li> Xing Xie (Microsoft Research, China) </li>
<li> Sarita Yardi Schoenebeck (University of Michigan, USA) </li>
<li> Taha Yasseri (University of Oxford, UK) </li>
<li> Ben Zhao (UC Santa Barbara, USA) </li>
</ul>


<!--<h3>Program Committee Members</h3>
<ul>

</ul>
-->
<?php include("../../footer.php"); ?>
