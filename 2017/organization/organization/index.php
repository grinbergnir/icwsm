<?php $section = "Organisation"; $subsection = "Organisation"; include("../../header.php"); ?>

  <h2 class="pageTitle">Organization</h2>

  <ul id="organisationList">


<li class="">
      <a href="http://www.derekruths.com"><img src="/2017/images/organisation/derek_thumb.jpg" class="project-thumb" alt=""></a>
      <h3 class=""><a href="http://www.derekruths.com">Derek Ruths</a></h3>
      <h5>McGill University</h5>   
      <p class="">General Chair</p>
    </li>
<li class="">
      <a href="https://research.facebook.com/winter-mason"><img src="/2017/images/organisation/winter_thumb.jpg" class="project-thumb" alt=""></a>
      <h3 class=""><a href="https://research.facebook.com/winter-mason">Winter Mason</a></h3>
      <h5>Facebook Inc.</h5>   
      <p class="">Program Chair</p>
    </li>
<li class="">
      <a href="http://www.tiara.org/index.html"><img src="/2017/images/organisation/alice_thumb.jpg" class="project-thumb" alt=""></a>
      <h3 class=""><a href="http://www.tiara.org/index.html">Alice Marwick</a></h3>
      <h5>Data & Society Research Institute</h5>   
      <p class="">Program Chair</p>
    </li>
<li class="">
      <a href="http://dimenet.asc.upenn.edu/people/sgonzalezbailon/"><img src="/2017/images/organisation/sandra_thumb.jpg" class="project-thumb" alt=""></a>
      <h3 class=""><a href="http://dimenet.asc.upenn.edu/people/sgonzalezbailon/">Sandra Gonz&aacute;lez-Bail&oacute;n</a></h3>
      <h5>University of Pennsylvania</h5>   
      <p class="">Program Chair</p>
    </li>
<li class="">
      <a href="http://www.munmund.net"><img src="/2017/images/organisation/munmun_thumb.jpg" class="project-thumb" alt=""></a>
      <h3 class=""><a href="http://www.munmund.net">Munmun De Choudhury</a></h3>
      <h5>Georgia Tech</h5>   
      <p class="">Workshop Chair</p>
    </li>
<li class="">
      <a href="http://www.pfeffer.at"><img src="/2017/images/organisation/jurgen_thumb.jpg" class="project-thumb" alt=""></a>
      <h3 class=""><a href="http://www.pfeffer.at">J&uuml;rgen Pfeffer</a></h3>
      <h5>Technical University of Munich</h5>   
      <p class="">Workshop Chair</p>
    </li>
<li class="">
      <a href="http://www.brianckeegan.com"><img src="/2017/images/organisation/brian_thumb.jpg" class="project-thumb" alt=""></a>
      <h3 class=""><a href="http://www.brianckeegan.com">Brian Keegan</a></h3>
      <h5>University of Colorado</h5>   
      <p class="">Workshop Chair</p>
    </li>
<li class="">
      <a href="http://yardi.people.si.umich.edu/"><img src="/2017/images/organisation/sarita_thumb.jpg" class="project-thumb" alt=""></a>
      <h3 class=""><a href="http://yardi.people.si.umich.edu/">Sarita Schoenebeck</a></h3>
      <h5>University of Michigan</h5>   
      <p class="">Tutorial Chairs</p>
    </li>
<li class="">
      <a href="http://www.vanessafriasmartinez.org/"><img src="/2017/images/organisation/vanessa_thumb.jpg" class="project-thumb" alt=""></a>
      <h3 class=""><a href="http://www.vanessafriasmartinez.org/">Vanessa Frias-Martinez</a></h3>
      <h5>University of Maryland</h5>   
      <p class="">Tutorial Chairs</p>
    </li>
<li class="">
      <a href="http://www.claudiawagner.info/"><img src="/2017/images/organisation/claudia_thumb.jpg" class="project-thumb" alt=""></a>
      <h3 class=""><a href="http://www.claudiawagner.info/">Claudia Wagner</a></h3>
      <h5>University of Koblenz-Landau</h5>   
      <p class="">Data Chairs</p>
    </li>
<li class="">
      <a href="http://cs.stanford.edu/~jurgens/"><img src="/2017/images/organisation/david_thumb.jpg" class="project-thumb" alt=""></a>
      <h3 class=""><a href="http://cs.stanford.edu/~jurgens/">David Jurgens</a></h3>
      <h5>Stanford University</h5>   
      <p class="">Data Chairs</p>
    </li>
<li class="">
      <a href="http://homepages.dcc.ufmg.br/~fabricio/"><img src="/2017/images/organisation/fabricio_thumb.png" class="project-thumb" alt=""></a>
      <h3 class=""><a href="http://homepages.dcc.ufmg.br/~fabricio/">Fabricio Benevenuto</a></h3>
      <h5>Federal University of Minas Gerais</h5>   
      <p class="">Data Chairs</p>
    </li>
  <li class="">
      <a href="https://www.hcde.washington.edu/Munson"><img src="/2017/images/organisation/sean_thumb.jpg" class="project-thumb" alt=""></a>
      <h3 class=""><a href="https://www.hcde.washington.edu/Munson">Sean Munson</a></h3>
      <h5>University of Washington</h5>   
      <p class="">Social Media & Publicity Chair</p>
    </li>
  <li class="">
      <a href="http://brenthecht.com/"><img src="/2017/images/organisation/brent_thumb.png" class="project-thumb" alt=""></a>
      <h3 class=""><a href="http://brenthecht.com/">Brent Hecht</a></h3>
      <h5>Northwestern University</h5>   
      <p class="">Sponsorships Chair</p>
    </li>
  <li class="">
      <a href="http://nirg.net/"><img src="/2017/images/organisation/nir_thumb.jpg" class="project-thumb" alt=""></a>
      <h3 class=""><a href="http://nirg.net/">Nir Grinberg</a></h3>
      <h5>Cornell University</h5>   
      <p class="">Web Chair</p>
    </li>
  <li class="">
      <a href="http://cgi.cs.mcgill.ca/~enewel3/"><img src="/2017/images/organisation/edward_thumb.jpg" class="project-thumb" alt=""></a>
      <h3 class=""><a href="http://cgi.cs.mcgill.ca/~enewel3/">Edward Newell</a></h3>
      <h5>McGill University</h5>   
      <p class="">Local Chair</p>
    </li>
</ul>

<?php include("../../footer.php"); ?>