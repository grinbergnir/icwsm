<?php $section = "Organisation"; $subsection = "Steering Committee"; include("../../header.php"); ?>

<br>  <h2 class="pageTitle">Steering Committee</h2>

<ul>
<li>John Breslin</li>
<li>Nicole Ellison</li>
<li>Bernie Hogan</li>
<li>Emre Kiciman</li>
<li>Paul Resnick</li>
<li>Ian Soboroff</li>
<li>Eytan Adar</li>
<li>Munmun De Choudhury</li>
<li>Daniele Quercia</li>
<li>Meeyoung Cha</li>
<li>Cecilia Mascolo</li>
<li>Christian Sandvig</li>
<li>Krishna Gummadi</li>
<li>Markus Strohmaier</li>
<li>Claudia Wagner</li>
<li>Eric Gilbert</li>
<li>Michael Macy</li>
</ul>
    
<?php include("../../footer.php"); ?>