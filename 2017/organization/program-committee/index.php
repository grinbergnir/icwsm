<?php $section = "Organisation"; $subsection = "Program Committee"; include("../../header.php"); ?>

<br>  <h2 class="pageTitle">Program Committee</h2>

<br><br>
<h3>Program Committee Members</h3>
<ul>
<li>Aaron Clauset (University of Colorado Boulder, USA)</li>
<li>Adriano Veloso (UFMG, Brazil)</li>
<li>Afra Mashhadi (Bell Labs, Ireland)</li>
<li>Alessandro Epasto (Google Research, USA)</li>
<li>Alessandro Flammini (Indiana U., USA)</li>
<li>Alex Dow (Facebook, USA)</li>
<li>Alex Hanna (University of Toronto, Canada)</li>
<li>Alexander Peysakhovich (Facebook, USA)</li>
<li>Alexandra Olteanu (EPFL, Switzerland)</li>
<li>Amit Sheth (Kno.e.sis Center, Wright State University, USA)</li>
<li>Amy Zhang (Cambridge University, United Kingdom)</li>
<li>Anastasios Noulas (Lancaster University, United Kingdom)</li>
<li>Andreas Hotho (University of Wuerzburg, Germany)</li>
<li>Andrew Mao (Microsoft Research, USA)</li>
<li>Anne Oeldorf-Hirsch (University of Connecticut, USA)</li>
<li>Antti Ukkonen (Finnish Institute of Occupational Health, Finland)</li>
<li>Aris Anagnostopoulos (Sapienza University of Rome, Italy)</li>
<li>Arkadiusz Stopczynski (Technical University of Denmark, Denmark)</li>
<li>Arnd Christian K&ouml;nig (Microsoft Research, USA)</li>
<li>Ashton Anderson (Microsoft Research, USA)</li>
<li>Aziz Mohaisen (SUNY Buffalo, USA)</li>
<li>Bimal Viswanath (MPI-SWS, Germany)</li>
<li>Bing Liu (University of Illinois at Chicago, USA)</li>
<li>Blaine Robbins (New York University Abu Dhabi, USA)</li>
<li>Bongwon Suh (Seoul National University, Korea)</li>
<li>Brian Eoff (Spring, USA)</li>
<li>Carsten Ke&szlig;ler (Hunter College, City University of New York, USA)</li>
<li>Cecilia Mascolo (University of Cambridge, United Kingdom)</li>
<li>Changtao Zhong (King's College London, United Kingdom)</li>
<li>Charalampos Tsourakakis (Harvard University, USA)</li>
<li>Chaya Hiruncharoenvate (Georgia Institute of Technology, USA)</li>
<li>Cheng Li (University of Michigan, USA)</li>
<li>Cheng-Te Li (Research Center for IT Innovation, Academia Sinica, Taiwan)</li>
<li>Chengkai Li (University of Texas at Arlington, USA)</li>
<li>Christoph Kling (GESIS, Leibniz Institute for the Social Sciences, Germany)</li>
<li>Christoph Trattner (KMI, TU-Graz, Austria)</li>
<li>Christopher Homan (Rochester Institute of Technology, USA)</li>
<li>Cindy Chung (Intel, USA)</li>
<li>Ciro Cattuto (ISI Foundation, Italy)</li>
<li>Claudia Hauff (Delft University of Technology, Netherlands)</li>
<li>Claudia Lopez (Universidad Tecnica Federico Santa Maria, Chile)</li>
<li>Claudia M&uuml;ller-Birn (Freie Universit&auml;t Berlin, Germany)</li>
<li>Clay Fink (Johns Hopkins University Applied Physics Laboratory, USA)</li>
<li>Cody Buntain (University of Maryland, USA)</li>
<li>Conor Hayes (Insight Centre for Data Analytics, NUI Galway, Ireland)</li>
<li>Cristian Lumezanu (NEC Laboratories, USA)</li>
<li>Dan Pelleg (Yahoo!, Israel)</li>
<li>Daniel Gatica-Perez (Idiap and EPFL, Switzerland)</li>
<li>David Laniado (Eurecat, Spain)</li>
<li>David Markowitz (Stanford University, USA)</li>
<li>Defu Lian (University of Electronic Science and Technology of China, China)</li>
<li>Derek Greene (University College Dublin, Ireland)</li>
<li>Desislava Hristova (University of Cambridge, United Kingdom)</li>
<li>Dietmar Jannach (TU Dortmund, Germany)</li>
<li>Dmytro Karamshuk (King's College London, United Kingdom)</li>
<li>Doug Downey (Northwestern University, USA)</li>
<li>Drew Margolin (Cornell University, USA)</li>
<li>Edward Platt (University of Michigan School of Information, USA)</li>
<li>Ee-Peng Lim (Singapore Management University, Singapore)</li>
<li>Elizabeth M. Daly (IBM Research, Ireland)</li>
<li>Emilio Zagheni (University of Washington, USA)</li>
<li>Emma Spiro (University of Washington, USA)</li>
<li>Esma Aimeur (University of Montreal, Canada)</li>
<li>Eunhee Rhim (Samsung Electronics, Korea)</li>
<li>Evaggelia Pitoura (Univ. of Ioannina, Greece)</li>
<li>Evimaria Terzi (Boston University, USA)</li>
<li>Fabian Fl&ouml;ck (GESIS Cologne, Germany)</li>
<li>Fariba Karimi (GESIS, Germany)</li>
<li>Farshad Kooti (Facebook, USA)</li>
<li>Feida Zhu (Singapore Management University, Singapore)</li>
<li>Florent Garcin (Artificial Intelligence Lab, EPFL, Switzerland)</li>
<li>Francesco Gullo (UniCredit R&amp;D, Italy)</li>
<li>Frank Schweitzer (ETH Zurich, Switzerland)</li>
<li>Fred Morstatter (Arizona State University, USA)</li>
<li>Freddy Chong Tat Chua (Hewlett Packard Labs, USA)</li>
<li>Gang Wang (Virginia Tech, USA)</li>
<li>Gareth Tyson (Queen Mary, University of London, United Kingdom)</li>
<li>Geli Fei (University of Illinois at Chicago, USA)</li>
<li>George Berry (Cornell University, Department of Sociology, USA)</li>
<li>George Valkanas (Department of Informatics and Telecommunications, University of Athens, Greece)</li>
<li>Gerard de Melo (Rutgers University, USA)</li>
<li>Giancarlo Ruffo (Universita' di Torino, Italy)</li>
<li>Gianluca Demartini (University of Sheffield, United Kingdom)</li>
<li>Gianmarco De Francisci Morales (Qatar Computing Research Institute, Qatar)</li>
<li>Hady Lauw (Singapore Management University, Singapore)</li>
<li>Haiko Lietz (GESIS - Leibniz Institute for the Social Sciences, Germany)</li>
<li>Hamed Haddadi (Queen Mary University of London, United Kingdom)</li>
<li>Hanghang Tong (City College, CUNY, USA)</li>
<li>Hao Ma (Microsoft Research, USA)</li>
<li>Heather Ford (University of Leeds, United Kingdom)</li>
<li>Hemant Purohit (George Mason University, USA)</li>
<li>Hengshu Zhu (Baidu Research-Big Data Lab, China)</li>
<li>Hirozumi Yamaguchi (Osaka University, Japan)</li>
<li>Ho-Jin Choi (KAIST, Korea)</li>
<li>Hoh Kim (THE LAB h, Korea)</li>
<li>Hsun-Ping Hsieh (Department of Electrical Engineering, National Cheng Kung University, Taiwan)</li>
<li>Huan Liu (Professor, Arizona State University, USA)</li>
<li>Huawei Shen (Chinese Academy of Sciences, China)</li>
<li>Hui Fang (University of Delaware, USA)</li>
<li>Huiji Gao (Arizona State University, USA)</li>
<li>Hyejin Youn (MIT, USA)</li>
<li>Iadh Ounis (University of Glasgow, United Kingdom)</li>
<li>Ian Soboroff (NIST, USA)</li>
<li>Ilias Leontiadis (Telefonica Research, Spain)</li>
<li>Ingo Scholtes (ETH Zurich, Switzerland)</li>
<li>Inseok Hwang (IBM Research, USA)</li>
<li>Jae-Gil Lee (KAIST, Korea)</li>
<li>Jaewoo Kang (Korea University, Korea)</li>
<li>Jaime Arguello (University of North Carolina at Chapel Hill, USA)</li>
<li>James Caverlee (Texas A&amp;M University, USA)</li>
<li>Jana Diesner (University of Illinois at Urbana-Champaign, USA)</li>
<li>Jesse Chandler (Mathematica Policy Research, USA)</li>
<li>Jianyong Wang (Tsinghua University, China)</li>
<li>Jiliang Tang (Michigan State University, USA)</li>
<li>Jisun An (Qatar Computing Research Institute, Qatar)</li>
<li>Joel Nishimura (Arizona State University, USA)</li>
<li>Jong Gun Lee (UN, Indonesia)</li>
<li>Juhi Kulshrestha (MPI-SWS, Germany)</li>
<li>Jussara Almeida (UFMG, Brazil)</li>
<li>Justin Cranshaw (Microsoft Research, USA)</li>
<li>Juyong Park (Graduate School of Culture Technology, Korea Advanced Institute of Science and Technology, Korea)</li>
<li>Karthik Subbian (Facebook, USA)</li>
<li>Kartik Talamadupula (IBM Research, USA)</li>
<li>Kate Niederhoffer (dachis group, USA)</li>
<li>Katrin Weller (GESIS - Leibniz Institute for the Social Sciences, Germany)</li>
<li>Kav&eacute; Salamatian (Universit&eacute; de Savoie, France)</li>
<li>Kiran Lakkaraju (Sandia National Labs, USA)</li>
<li>Komal Kapoor (University of Minnesota Twin Cities, USA)</li>
<li>Konstantinos Pelechrinis (University of Pittsburgh, USA)</li>
<li>Kristina Lerman (University of Southern California, USA)</li>
<li>Kuan-Ta Chen (Academia Sinica, Taiwan)</li>
<li>Kunwoo Park (Divison of Web Science, KAIST, Korea)</li>
<li>Kyumin Lee (Utah State University, USA)</li>
<li>Lei Zhang (ADOBE SYSTEMS INC, USA)</li>
<li>Leto Peel (Universite catholique de Louvain, Belgium)</li>
<li>Lexing Xie (Australian National University, Australia)</li>
<li>Li Chen (Hong Kong Baptist University, Hong Kong)</li>
<li>Libby Hemphill (Illinois Institute of Technology, USA)</li>
<li>Liz Liddy (Center for Natural Language Processing, Syracuse University, USA)</li>
<li>Lu Chen (LinkedIn, USA)</li>
<li>Lu Wang (Northeastern University, USA)</li>
<li>Ludovico Boratto (Universit&agrave; di Cagliari, Italy)</li>
<li>Manuel Gomez Rodriguez (MPI for Software Systems, Germany)</li>
<li>Marco Brambilla (Politecnico di Milano, Italy)</li>
<li>Marco Pellegrini (Institute for Informatics and Telematics of C.N.R., Italy)</li>
<li>Margaret Roberts (UCSD, USA)</li>
<li>Marina Litvak (BGU, Israel)</li>
<li>Marios Belk (Dept. of Computer Science, University of Cyprus, Cyprus)</li>
<li>Marko Skoric (City University of Hong Kong, Hong Kong)</li>
<li>Masahiro Kimura (Ryukoku University, Japan)</li>
<li>Matteo Magnani (Uppsala University, Sweden)</li>
<li>Matteo Manca (Eurecat (Technological Center of Catalunya), Spain)</li>
<li>Mauro Sozio (T&eacute;l&eacute;com ParisTech, France)</li>
<li>Michael Gamon (Microsoft Research, USA)</li>
<li>Michael Mathioudakis (&nbsp;, &nbsp;)</li>
<li>Michael Paul (University of Colorado Boulder, USA)</li>
<li>Michael Szell (Hungarian Academy of Sciences, Hungary)</li>
<li>Michalis Vazirgiannis (Department of Informatics, AUEB, Greece)</li>
<li>Michele Coscia (Center for International Development - Harvard University, USA)</li>
<li>Miriam Redi (Yahoo, Spain)</li>
<li>Mohammad Ali Abbasi (Arizona State University, USA)</li>
<li>Motahhare Eslami (University of Illinois at Urbana-Champaign, USA)</li>
<li>Mouna Kacimi (Free University of Bozen-Bolzano, Italy)</li>
<li>Muhammad Imran (Qatar Computing Research Institute, Qatar)</li>
<li>Myle Ott (Facebook, USA)</li>
<li>Nathan Hodas (PNNL, USA)</li>
<li>Nazanin Andalibi (Drexel University, USA)</li>
<li>Neel Sundaresan (eBay, USA)</li>
<li>Nelly Litvak (University of Twente, Netherlands)</li>
<li>Nicholas Lane (University College London and Bell Labs, United Kingdom)</li>
<li>Nicolas Kourtellis (Telefonica Research, Spain)</li>
<li>Niloufar Salehi (Stanford University, USA)</li>
<li>Nish Parikh (eBay Research Labs, USA)</li>
<li>Norbert Fuhr (University of Duisburg-Essen, Germany)</li>
<li>Oliver Haimson (UC Irvine, USA)</li>
<li>Omar Alonso (Microsoft, USA)</li>
<li>Onur Varol (Indiana University, USA)</li>
<li>Pablo Arag&oacute;n (Universitat Pompeu Fabra, Spain)</li>
<li>Pablo Barbera (New York University, USA)</li>
<li>Padmini Srinivasan (The University of Iowa, USA)</li>
<li>Pan Hui (Hong Kong University of Science and Technology, Hong Kong)</li>
<li>Panayiotis Tsaparas (University of Ioannina, Greece)</li>
<li>Paolo Parigi (Stanford University, USA)</li>
<li>Patty Kostkova (University College London, United Kingdom)</li>
<li>Pedro Olmo Vaz de Melo (UFMG, Brazil)</li>
<li>Peter Burnap (Cardiff University, United Kingdom)</li>
<li>Philipp Singer (GESIS - Leibniz Institute for the Social Sciences, Germany)</li>
<li>Pinar Ozturk (Stevens Institute of Technology, USA)</li>
<li>Piotr Sapiezynski (Technical University of Denmark, Denmark)</li>
<li>Ponnurangam Kumaraguru (IIITD, India)</li>
<li>Pranam Kolari (Yahoo! Labs, USA)</li>
<li>Pritam Gundecha (IBM Research, USA)</li>
<li>Qingbo Hu (LinkedIn, USA)</li>
<li>Reza Farahbakhsh (Institut Mines-T&eacute;l&eacute;com, T&eacute;l&eacute;com SudParis, France)</li>
<li>Richard Mccreadie (University of Glasgow, United Kingdom)</li>
<li>Robert J&auml;schke (L3S Research Center, Germany)</li>
<li>Rok Sosic (Computer Science Department, Stanford University, USA)</li>
<li>Rui Zhang (IBM Research - Almaden, USA)</li>
<li>Rumi Ghosh (USC, USA)</li>
<li>Ruogu Kang (Facebook, USA)</li>
<li>Ryota Kobayashi (National Institute of Informatics, Japan)</li>
<li>Sabrina Gaito (Universit&agrave; degli Studi di Milano, Italy)</li>
<li>Salvatore Scellato (Google, United Kingdom)</li>
<li>Samuel Carton (University of Michigan School of Information, USA)</li>
<li>Sang-Wook Kim (Hanyang University, Korea)</li>
<li>Sanjay Kairam (Stanford University, USA)</li>
<li>Sauvik Das (Carnegie Mellon University, USA)</li>
<li>Sean Goggins (University of Missouri, USA)</li>
<li>Seungwon Eugene Jeong (Facebook, USA)</li>
<li>Shawn Walker (University of Washington, USA)</li>
<li>Shenghua Bao (IBM Watson Group, USA)</li>
<li>Shruti Sannon (Cornell University, USA)</li>
<li>Shuang-Hong Yang (Operator Inc, USA)</li>
<li>Sofiane Abbar (QCRI, Qatar)</li>
<li>Song Feng (IBM, USA)</li>
<li>Souneil Park (Telefonica Research, Spain)</li>
<li>Srinath Srinivasa (International Institute of Information Technology, Bangalore, India)</li>
<li>Stephan Raaijmakers (TNO, The Netherlands, Netherlands)</li>
<li>Stratis Ioannidis (Northeastern University, USA)</li>
<li>Styliani Kleanthous (University of Cyprus, Cyprus)</li>
<li>Suzy Moat (University of Warwick, United Kingdom)</li>
<li>Taejoong Chung (SNU MMLAB, Korea)</li>
<li>Tawfiq Ammari (University of Michigan, USA)</li>
<li>Tetyana Lokot (Dublin City University, Ireland)</li>
<li>Thomas Feliciani (University of Groningen, Netherlands)</li>
<li>Thomas Maillart (UC Berkeley, USA)</li>
<li>Thomas U. Grund (Link&ouml;ping University, Sweden)</li>
<li>Tim Althoff (Stanford University, USA)</li>
<li>Tim Weninger (University of Notre Dame, USA)</li>
<li>Tobias Preis (University of Warwick, United Kingdom)</li>
<li>Tuan-Anh Hoang (L3S Research Center, Germany)</li>
<li>Umashanthi Pavalanathan (Georgia Institute of Technology, USA)</li>
<li>Vicen&ccedil; G&oacute;mez (Universitat Pompeu Fabra, Spain)</li>
<li>Vincent W. Zheng (Advanced Digital Sciences Center, Singapore)</li>
<li>Wai Lam (The Chinese University of Hong Kong, Hong Kong)</li>
<li>Walid Magdy (University of Edinburgh, United Kingdom)</li>
<li>Wang-Chien Lee (the Pennsylvania State University, USA)</li>
<li>Weike Pan (Shenzhen University, China)</li>
<li>William Hamilton (Stanford University, USA)</li>
<li>Xia Hu (Texas A&amp;M University, USA)</li>
<li>Xin Huang (University of British Columbia, Canada)</li>
<li>Xin Rong (University of Michigan, USA)</li>
<li>Xingquan Zhu (Florida Atlantic University, USA)</li>
<li>Xinru Page (Bentley University, USA)</li>
<li>Yang Yang (Tsinghua University, China)</li>
<li>Yelena Mejova (Qatar Computing Research Institute, Qatar)</li>
<li>Yi-Shin Chen (National Tsing Hua University, Taiwan)</li>
<li>Yong Li (Tsinghua, China)</li>
<li>Yong Zheng (Illinois Institute of Technology, USA)</li>
<li>Younghoon Kim (Hanyang University, Korea)</li>
<li>Yuheng Hu (University of Illinois at Chicago, USA)</li>
<li>Yutaka Matsuo (University of Tokyo, Japan)</li>
<li>Zhe Zhao (University of Michigan, Ann Arbor, USA)</li>
<li>&Aacute;ngel Cuevas (Universidad Carlos III de Madrid, Spain)</li>
</ul>

<?php include("../../footer.php"); ?>?