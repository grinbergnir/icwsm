<?php $section = "Organisation"; $subsection = "Program Committee"; include("../../header.php"); ?>

<h2 class="pageTitle">ICWSM-16 Program Committee</h2>


<h3>Program Committee Members</h3>

<ul>

<li> Sofiane Abbar (QCRI, Qatar) </li>
<li> Esma Aimeur (University of Montreal, Canada) </li>
<li> Talayeh Aledavood (Finland) </li>
<li> Jussara Almeida (UFMG, Brazil) </li>
<li> Omar Alonso (Microsoft, USA) </li>
<li> Tim Althoff (Stanford University, USA) </li>
<li> Giambattista Amati (Fondazione Ugo Bordoni, Italy) </li>
<li> Tawfiq Ammari (University of Michigan, USA) </li>
<li> Jisun An (Qatar Computing Research Institute, Qatar) </li>
<li> Nazanin Andalibi (Drexel University, USA) </li>
<li> Ashton Anderson (Stanford University, USA) </li>
<li> Pablo Arag&oacute;n (Universitat Pompeu Fabra, Spain) </li>
<li> Daniel Archambault (Swansea University, UK) </li>
<li> Sitaram Asur </li>
<li> Mossaab Bagdouri (University of Maryland, USA) </li>
<li> Natalie Bazarova (Cornell University, USA) </li>
<li> Marios Belk (University of Cyprus, Cyprus) </li>
<li> George Berry </li>
<li> Cornelia Brantner (TU Dresden, Germany) </li>
<li> Ceren Budak (University of Michigan School of Information, USA) </li>
<li> Cody Buntain (University of Maryland, USA) </li>
<li> Peter Burnap (Cardiff University, UK) </li>
<li> Licia Capra (University College London, UK) </li>
<li> James Caverlee (Texas A&M University, USA) </li>
<li> Kuan-Ta Chen (Academia Sinica, Taiwan) </li>
<li> Lu Chen (Kno.e.sis Center, Wright State University, USA) </li>
<li> Yi-Shin Chen (National Tsing Hua University, Taiwan) </li>
<li> Justin Cheng (Stanford University, USA) </li>
<li> Ed H. Chi (Google, Inc., USA) </li>
<li> Ho-Jin Choi (KAIST, The Republic of Korea) </li>
<li> Jaegul Choo (Korea University, The Republic of Korea) </li>
<li> Dimitris Christopoulos (MODUL University Vienna, Austria) </li>
<li> Freddy Chong Tat Chua (Hewlett Packard Labs, USA) </li>
<li> Cindy Chung (Intel Corporation, USA) </li>
<li> Aaron Clauset (University of Colorado Boulder, USA) </li>
<li> Michele Coscia (National Research Council, Pisa, Italy) </li>
<li> Justin Cranshaw (Microsoft Research, USA) </li>
<li> Rub&eacute;n Cuevas (Universidad Carlos III de Madrid, Spain) </li>
<li> &Aacute;ngel Cuevas (Universidad Carlos III de Madrid, Spain) </li>
<li> Elizabeth M. Daly (IBM Research, Ireland) </li>
<li> Sauvik Das (Carnegie Mellon University, USA) </li>
<li> Jana Diesner (University of Illinois at Urbana-Champaign, USA) </li>
<li> Karthik Dinakar (MIT, USA) </li>
<li> Alex Dow (Facebook, USA) </li>
<li> Brian Eoff (Texas A&M University, USA) </li>
<li> Motahhare Eslami (University of Illinois at Urbana-Champaign, USA) </li>
<li> Hui Fang (University of Delaware, USA) </li>
<li> Reza Farahbakhsh (Institut Mines-T&eacute;l&eacute;com, T&eacute;l&eacute;com SudParis, France) </li>
<li> Thomas Feliciani (University of Groningen, Netherlands) </li>
<li> Song Feng (IBM, USA) </li>
<li> Clay Fink (Johns Hopkins University Applied Physics Laboratory, USA) </li>
<li> Alessandro Flammini (Indiana U., USA) </li>
<li> Fabian Fl&ouml;ck (GESIS Cologne, Germany) </li>
<li> Vanessa Frias-Martinez (University of Maryland, USA) </li>
<li> Sabrina Gaito (Università degli Studi di Milano, Italy) </li>
<li> Aram Galstyan (USC/ISI, USA) </li>
<li> Ruth Garcia (Oxford University, UK) </li>
<li> Daniel Gatica-Perez (Idiap and EPFL, Switzerland) </li>
<li> Kimberly Glasgow </li>
<li> Sean Goggins (University of Missouri, USA) </li>
<li> Manuel Gomez Rodriguez (MPI for Software Systems, Germany) </li>
<li> Derek Greene (University College Dublin, Ireland) </li>
<li> Catherine Grevet (Georgia Institute of Technology, USA) </li>
<li> Nir Grinberg (Cornell University, USA) </li>
<li> Thomas U. Grund (Link&ouml;ping University, Sweden) </li>
<li> Vicen&ccedil; G&oacute;mez (Universitat Pompeu Fabra, Spain) </li>
<li> Hamed Haddadi (Queen Mary University of London, UK) </li>
<li> Scott Hale (Oxford Internet Institute, University of Oxford, UK) </li>
<li> Williamleif Hamilton (USA) </li>
<li> Alexander Hanna (University of Wisconsin-Madison, USA) </li>
<li> Aniko Hannak (Northeaster University, USA) </li>
<li> Claudia Hauff (Delft University of Technology, Netherlands) </li>
<li> Conor Hayes (Insight Centre for Data Analytics, NUI Galway, Ireland) </li>
<li> Libby Hemphill (USA) </li>
<li> Chaya Hiruncharoenvate (Georgia Institute of Technology, USA) </li>
<li> Tuan Anh Hoang (Singapore Management University, Singapore) </li>
<li> Nathan Hodas (Pacific Northwest National Laboratory, USA) </li>
<li> Andreas Hotho (University of Wuerzburg, Germany) </li>
<li> Desislava Hristova (University of Cambridge, UK) </li>
<li> Hsun-Ping Hsieh (National Taiwan University, Taiwan) </li>
<li> Yuheng Hu (Arizona State University, USA) </li>
<li> Muzammil Hussain (University of Michigan, USA) </li>
<li> Muhammad Imran (Qatar Computing Research Institute, Qatar) </li>
<li> Abigail Jacobs (University of Colorado, USA) </li>
<li> Eugene Jeong </li>
<li> Mouna Kacimi (Free University of Bozen-Bolzano, Italy) </li>
<li> Ruogu Kang </li>
<li> Komal Kapoor </li>
<li> Dmytro Karamshuk (King's College London, UK) </li>
<li> Fariba Karimi (GESIS, Germany) </li>
<li> Carsten Ke&#xdf;ler (Hunter College, City University of New York, USA) </li>
<li> Hoh Kim (THE LAB h, The Republic of Korea) </li>
<li> Younghoon Kim (Hanyang University, The Republic of Korea) </li>
<li> Styliani Kleanthous (University of Cyprus, Cyprus) </li>
<li> Christoph Kling </li>
<li> Isabel Kloumann (Cornell University, USA) </li>
<li> Ryota Kobayashi (National Institute of Informatics, Japan) </li>
<li> Pranam Kolari (Yahoo! Labs, USA) </li>
<li> Farshad Kooti (USC Information Sciences Institute, USA) </li>
<li> Nicolas Kourtellis (Telefonica Research, Spain) </li>
<li> Ponnurangam Kumaraguru (IIITD, India) </li>
<li> J&eacute;r&ocirc;me Kunegis (University of Koblenz-Landau, Germany) </li>
<li> Nicholas Lane (Bell Labs, UK) </li>
<li> David Laniado (Barcelona Media - Innovation Centre, Spain) </li>
<li> Jae-Gil Lee (KAIST, The Republic of Korea) </li>
<li> Jong Gun Lee (UN, Indonesia) </li>
<li> Kyumin Lee (Utah State University, USA) </li>
<li> Ilias Leontiadis (Telefonica Research, Spain) </li>
<li> Cheng-Te Li (Research Center for IT Innovation, Academia Sinica, Taiwan) </li>
<li> Yong Li (Tsinghua, China) </li>
<li> Defu Lian (University of Electronic Science and Technology of China, China) </li>
<li> Haiko Lietz (GESIS - Leibniz Institute for the Social Sciences, Germany) </li>
<li> Marina Litvak (BGU, Israel) </li>
<li> Huan Liu (Arizona State University, USA) </li>
<li> Tetyana Lokot (University of Maryland, USA) </li>
<li> Claudia Lopez (University of Pittsburgh, USA) </li>
<li> Gilad Lotan (SocialFlow Inc, USA) </li>
<li> Giovanni Luca Ciampaglia </li>
<li> Diana Maclean </li>
<li> Walid Magdy (Qatar Computing Research Institute, Qatar) </li>
<li> Matteo Magnani (Uppsala University, Sweden) </li>
<li> Thomas Maillart (ETH Zurich, USA) </li>
<li> Matteo Manca (Universit&agrave; degli studi di Cagliari, Italy) </li>
<li> Drew Margolin (Cornell University, USA) </li>
<li> David Markowitz (Stanford University, USA) </li>
<li> Afra Mashhadi (Bell Labs, Ireland) </li>
<li> Yelena Mejova (Qatar Computing Research Institute, Qatar) </li>
<li> Panagiotis Metaxas (Wellesley College, USA) </li>
<li> Suzy Moat (University of Warwick, UK) </li>
<li> Enys Mones (Technical University of Denmark, Denmark) </li>
<li> Fred Morstatter (Arizona State University, USA) </li>
<li> Joel Nishimura (Arizona State University, USA) </li>
<li> Anastasios Noulas </li>
<li> Alexandra Olteanu (EPFL, Switzerland) </li>
<li> Miles Osborne (Edinburgh University, UK) </li>
<li> Iadh Ounis (University of Glasgow, UK) </li>
<li> Han Woo Park (YeungNam University, The Republic of Korea) </li>
<li> Juyong Park (Korea Advanced Institute of Science and Technology, The Republic of Korea) </li>
<li> Denis Parra (Pontificia Universidad Catolica de Chile, Chile) </li>
<li> Sharoda Paul (GE Global Research, USA) </li>
<li> Umashanthi Pavalanathan (Georgia Tech, USA) </li>
<li> Leto Peel (Universite catholique de Louvain, Belgium) </li>
<li> Konstantinos Pelechrinis (University of Pittsburgh, USA) </li>
<li> Edward Platt (University of Michigan School of Information, USA) </li>
<li> Barbara Poblete (University of Chile, Chile) </li>
<li> Tobias Preis (University of Warwick, UK) </li>
<li> Hemant Purohit (George Mason University, USA) </li>
<li> Giovanni Quattrone (University College of London, UK) </li>
<li> Stephan Raaijmakers (Healy Health Analytics, Netherlands) </li>
<li> Miriam Redi (Yahoo, Spain) </li>
<li> Roi Reichart (Technion IIT, Israel) </li>
<li> Eunhee Rhim (Samsung Electronics, The Republic of Korea) </li>
<li> Andrew Roback </li>
<li> Blaine Robbins </li>
<li> Margaret Roberts (UCSD, USA) </li>
<li> James Rosenquist (Harvard Medical School, USA) </li>
<li> Giancarlo Ruffo (Universit&agrave; di Torino, Italy) </li>
<li> Diego Saez-Trumper (Universitat Pompeu Fabra, Spain) </li>
<li> Alessandra Sala (Bell Labs Ireland, Ireland) </li>
<li> Kav&eacute; Salamatian (Universit&eacute; de Savoie, France) </li>
<li> Niloufar Salehi (Stanford University, USA) </li>
<li> Piotr Sapiezynski (Technical University of Denmark, Denmark) </li>
<li> Salvatore Scellato (Google, UK) </li>
<li> Ingo Scholtes (ETH Zurich, Switzerland) </li>
<li> Axel Schulz (DB Mobility Logistics AG, Germany) </li>
<li> Frank Schweitzer (ETH Zurich, Switzerland) </li>
<li> Huawei Shen (Chinese Academy of Sciences, China) </li>
<li> Roberta Sinatra (Northeastern University, USA) </li>
<li> Philipp Singer (Knowledge Management Institute, Austria) </li>
<li> Marko Skoric (City University of Hong Kong, Hong Kong) </li>
<li> Rok Sosic (Stanford University, USA) </li>
<li> Mauro Sozio (T&eacute;l&eacute;com ParisTech, France) </li>
<li> Emma Spiro (University of Washington, USA) </li>
<li> Srinath Srinivasa (International Institute of Information Technology, Bangalore, India) </li>
<li> Greg Stoddard (Northwestern University, USA) </li>
<li> Arek Stopczynski </li>
<li> Michael Szell (Northeastern University, USA) </li>
<li> Chenhao Tan (Cornell University, USA) </li>
<li> Jiliang Tang (Arizona State University, USA) </li>
<li> Yla Tausczik </li>
<li> Loren Terveen (USA) </li>
<li> Bart Thomee (Yahoo Labs, USA) </li>
<li> Christoph Trattner (KMI, TU-Graz, Austria) </li>
<li> Milena Tsvetkova (University of Oxford, UK) </li>
<li> Gareth Tyson (Queen Mary, University of London, UK) </li>
<li> George Valkanas (University of Athens, Greece) </li>
<li> Pedro Olmo Vaz de Melo (UFMG, Brazil) </li>
<li> Adriano Veloso (UFMG, Brazil) </li>
<li> Bimal Viswanath (Nokia Bell Labs, Stuttgart) </li>
<li> Yana Volkovich (Eurecat, Spain) </li>
<li> Gang Wang (University of California, Santa Barbara, USA) </li>
<li> Mirjam Wattenhofer (Google Switzerland GmbH, Switzerland) </li>
<li> Matthew Williams (University of Birmingham, UK) </li>
<li> Fabian Winter </li>
<li> Donghee Yvette Wohn (Michigan State University, USA) </li>
<li> Stefan Wojcik (Northeastern University/Harvard IQSS, USA) </li>
<li> Hirozumi Yamaguchi (Osaka University, Japan) </li>
<li> Xiaoran Yan (ISI-USC, USA) </li>
<li> Yang Yang (Tsinghua University, China) </li>
<li> Emine Yilmaz (University College London, UK) </li>
<li> Hyejin Youn (University of Oxford, UK) </li>
<li> Nicholas Jing Yuan (Microsoft Research, China) </li>
<li> Emilio Zagheni (University of Washington, USA) </li>
<li> Amy Zhang (Cambridge University, UK) </li>
<li> Jing Zhang </li>
<li> Changtao Zhong (King's College London, UK) </li>
<li> Hengshu Zhu (Baidu Research-Big Data Lab, China) </li>
</ul>



<!--<h3>Program Committee Members</h3>
<ul>

</ul>
-->
<?php include("../../footer.php"); ?>
