<?php $section = "Attending"; $subsection = "Registration"; include("../../header.php"); ?>

<h2 class="pageTitle">Registration</h2>

<p><b> Online registration is available at <a href='https://www.regonline.com/icwsm17'>https://www.regonline.com/icwsm17</a>. </b> </p>

<p>The ICWSM-17 technical conference registration fee includes admission to all technical sessions, invited talks, lunch each day of the main conference, the opening reception, the poster/demo reception, and access to the electronic version of the ICWSM-17 Conference Proceedings.</p>

<h3>Registration Fees (all in USD)</h3>

<table border=1 cellpadding=0 cellspacing=0 width=600 style='border-collapse:
 collapse;table-layout:fixed'>
 <tr height=45>
  <td height=28 width=110></td>
  <td width=110 align="center"><b>Early <br> (by March 31)</b></td>
  <td width=110 align="center"><b>Late <br> (by April 21)</b></td>
  <td width=110 align="center"><b>Onsite <br> (after April 21)</b></td>
 </tr>
 <tr height=28>
  <td height=28 width=110 align="center"><b>AAAI Member</b></td>
  <td width=110 align="center">575$</td>
  <td width=110 align="center">615$</td>
  <td width=110 align="center">675$</td>
 </tr>
 <tr height=28>
  <td height=28 width=110 align="center"><b>AAAI Student Member</b></td>
  <td width=110 align="center">335$</td>
  <td width=110 align="center">370$</td>
  <td width=110 align="center">415$</td>
 </tr>
 <tr height=28>
  <td height=28 width=110 align="center"><b> Nonmember </b></td>
  <td width=110 align="center">680$</td>
  <td width=110 align="center">720$</td>
  <td width=110 align="center">775$</td>
 </tr>
 <tr height=28>
  <td height=28 width=110 align="center"><b>Student Nonmember</b></td>
  <td width=110 align="center">385$</td>
  <td width=110 align="center">420$</td>
  <td width=110 align="center">465$</td>
 </tr>
<!-- <tr height=28>
  <td height=28 width=110 align="center"><b>Regular Silver*</b></td>
  <td width=110 align="center">599$</td>
  <td width=110 align="center">629$</td>
  <td width=110 align="center">679$</td>
 </tr>
 <tr height=28>
  <td height=28 width=110 align="center"><b>Student Silver*</b></td>
  <td width=110 align="center">355$</td>
  <td width=110 align="center">390$</td>
  <td width=110 align="center">435$</td>
 </tr>
 <tr height=28>
  <td height=28 width=110 align="center"><b>Platinum**</b></td>
  <td width=110 align="center">645$</td>
  <td width=110 align="center">675$</td>
  <td width=110 align="center">725$</td>
 </tr>-->
</table>
<br>
<!--<p>*Includes discounted conference registration, plus a one-year online new membership in AAAI.<br>
**Includes discounted conference registration, plus a one-year new or renewal membership in AAAI.</p>-->


<br>
<h4>Silver Membership</h4>

<p>Includes discounted conference registration, plus a one-year online new membership in AAAI. </p>

<table border=1 cellpadding=0 cellspacing=0 width=600 style='border-collapse:
 collapse;table-layout:fixed'>
 <tr height=45>
  <td height=28 width=110></td>
  <td width=110 align="center"><b>Early <br> (by March 31)</b></td>
  <td width=110 align="center"><b>Late <br> (by April 21)</b></td>
  <td width=110 align="center"><b>Onsite <br> (after April 21)</b></td>
 </tr>
<tr height=28>
  <td height=28 width=110 align="center"><b>Regular Silver</b></td>
  <td width=110 align="center">674$</td>
  <td width=110 align="center">714$</td>
  <td width=110 align="center">774$</td>
</tr>
<tr height=28>
  <td height=28 width=110 align="center"><b>Student Silver</b></td>
  <td width=110 align="center">384$</td>
  <td width=110 align="center">419$</td>
  <td width=110 align="center">464$</td>
 </tr>
</table>
<br>
<br>

<h4>Workshop/Tutorial Day Fee</h4>

<p>ICWSM-17 workshops and tutorials will be held May 15, just prior to the technical conference. The Workshop/Tutorial Day fee includes participation in any combination of workshops and/or tutorials on May 15. Participants should not sign up for concurrent events, so please consult the schedule carefully before making your selections. There is no additional fee for Workshop 9, the Science Slam.
<br>

Tutorial Information: <a href="http://www.icwsm.org/2017/program/tutorial/">http://www.icwsm.org/2017/program/tutorial/</a> <br>
Workshop Information: <a href="http://www.icwsm.org/2017/program/workshop/">http://www.icwsm.org/2017/program/workshop/</a> <br>
<br>
<b>Workshop / Tutorial fees:</b>
<br>
Regular    115$
<br>
Student    85$
<br>

<!--For complete workshop information, please see the <a href="http://www.icwsm.org/2016/program/workshop/">Workshop page</a>.

<br>
<br>
<b>Full Day:</b>
<br>
W2: Digital Placemaking: Augmenting Physical Places with Contextual Social Data
<br>
Germaine Halegoua, Raz Schwartz and Ed Manley
<br>
<br>
W4: Religion on Social Media
<br>
Yelena Mejova, Ingmar Weber, Lu Chen and Adam Okulicz-Kozaryn
<br>
<br>
W6: Standards and Practices in Large-Scale Social Media Research
<br>
Derek Ruths and Juergen Pfeffer
<br>
<br>
W7: Wikipedia, a Social Pedia: Research Challenges and Opportunities
<br>
Robert West, Leila Zia and Jure Leskovec
<br>
<br>
<b>Half Day:</b>
<br>
<br>
W1: Auditing Algorithms From the Outside: Methods and Implications
<br>
Mike Ananny, Karrie Karahalios, Christian Sandvig and Christo Wilson
<br>
<br>
W3: Modeling and Mining Temporal Interactions
<br>
Bruno Gon?alves, M?rton Karsai and Nicola Perra
<br>
<br>
<b>Evening:</b>
<br>
<br>
W5: The ICWSM Science Slam
<br>
David Garcia, Ingmar Weber and Aniko Hannak
</p>-->

<h4>Opening Reception Guest</h4>
<p>The ICWSM-17 Opening Reception will be held on Tuesday, May 16 in the Salon urbain at the Place des Arts. Hearty Hors d'oeuvres, wine, beer, and soft drinks will be available. Your admittance to the Opening Reception is included in your conference registration. Guests are welcome to attend for an additional fee. 
<br>
<br>
Opening Reception Guest 60.00$</p>

<h4>Poster/Demo Session Guest</h4>
<p> The ICWSM-17 evening poster/demo session will be held Wednesday, May 17. A light supper and one complimentary beverage will be provided. A cash bar will also be available. Your admittance to the Poster/Demo Session is included in your conference registration. Guests are welcome to attend for an additional fee. 
<br>
<br>
Poster/Demo Session Guest 45.00$</p>



<h4>ICWSM-17 Hard Copy Proceedings</h4>
<p>Preregistrants may reserve a hard copy of the ICWSM-17 proceedings, which will be mailed after the conference. The preconference discounted price will be determined in early summer. Shipping and handling will be added. Individuals who select this option will be contacted regarding the final cost after the conference.</p>

<h4>Registration / Proof of Student Status</h4>
<p>To register online, please complete the form at <a href='https://www.regonline.com/icwsm17'>https://www.regonline.com/icwsm17</a>. If you qualify for student rates, please submit your proof of student status to AAAI by email attachment to <a href="mailto:icwsm17@aaai.org">icwsm17@aaai.org</a> or by fax to +1-650-321-4457.</p>

<h4>Refund Requests</h4>
<p>The deadline for refund requests is April 28, 2017. All refund requests must be made in writing to AAAI at <a href="mailto:icwsm17@aaai.org">icwsm17@aaai.org</a>. A $100.00 processing fee will be assessed for all refunds.</p>

<h4>Visa Information</h4>
<p>Letters of invitation can be requested by accepted ICWSM-17 authors or registrants with a completed registration with payment. If you are attending ICWSM-17 and require a letter of invitation, please send the following information to <a href="mailto:icwsm17@aaai.org">icwsm17@aaai.org</a>:
<ul>
<li>First/Given Name:
</li>
<li>Family/Last Name:
</li>
<li>Position:
</li>
<li>Organization:
</li>
<li>Department:
</li>
<li>City:
</li>
<li>Zip/Postal Code:
</li>
<li>Country:
</li>
<li>Email:
</li>
<li>Are you an author of a paper?
</li>
<li>Paper title:
</li>
<li>If not accepted author, Registration Confirmation ID#:
</li>
<li>Email address where the letter should be sent (if different from above):
</li>
</ul>
</p>

<h4>Onsite Registration</h4>
<p>Preregistration is required. Please complete the ICWSM-17 online registration and payment form (<a href='https://www.regonline.com/icwsm17'>https://www.regonline.com/icwsm17</a>) before proceeding to onsite registration. Onsite registration will be held in the foyer of the Grand Salon Opera Ballroom on level four of the Hyatt Regency Montreal, 1255 Jeanne-Mance, Montreal, Quebec, Canada, H5B 1E5, Tel: +1 514 982 1234 (<a href='https://montreal.regency.hyatt.com/en/hotel/home.html'>https://montreal.regency.hyatt.com/en/hotel/home.html</a>). All attendees must pick up their registration packets for admittance to programs. </p>

<p>Registration hours are scheduled for Monday, May 15, 8:00am - 2:00pm, Tuesday and Wednesday, May 16-17, 8:30am - 5:00pm, and Thursday, May 18, 8:30am - 11:00am. These hours are subject to change depending on the final program.</p>

<?php include("../../footer.php"); ?>?