<?php $section = "Attending"; $subsection = "Social Event"; include("../../header.php"); ?>

  <h2 class="pageTitle">Social Event</h2>

<h3>Opening Reception at the Guinness Storehouse</h3>

<p><img src="/2012/images/social/bar_gunness.jpg" alt="Gravity Bar at the Guinness Storehouse Exterior"></p>

<h4>6th June 2012 from 7:30 PM to 11:30 PM</h4>

<p>The opening reception for ICWSM-12 will take place on the evening of the 6th June 2012 at the <a href="http://www.guinness-storehouse.com/en/WhatsInside.aspx">Guinness Storehouse</a>. Specifically, the seventh floor "Gravity Bar" will be the venue.</p>

<p>The Gravity Bar, symbolizing the "head of the pint," hovers above the roof of the Guinness Storehouse, offering uninterrupted 360 degree views across the city and as far as the Dublin Mountains. An informal dinner menu (small bowls) and one complimentary beverage will be served. A full no-host bar will also be available. Admittance to the ICWSM-12 opening reception is included in the ICWSM-12 technical program registration fee. ICWSM-12 registrants must wear their registration badges at all times. Please be sure to check in when you arrive at Guinness to receive your complimentary beverage ticket. Guest tickets may be purchased for $85 USD.</p>

<p>ICWSM-12 has arranged for complimentary round-trip coach transportation to the Guinness Storehouse. Buses will begin departing at 6:30 PM from the Trinity Biomedical Sciences Institute, and the last bus will leave the Guinness Storehouse at 11:30 PM to return to the Trinity Biomedical Sciences Institute.</p>

<p><img src="/2012/images/social/bar_gunness_interior.jpg" alt="Gravity Bar Interior"></p>

<p>The Storehouse is an industrial building, originally built in 1904 in the style of the Chicago School of Architecture, with massive steel beams providing the support for the entire structure of the building. The Storehouse housed the fermentation of Guinness beer until 1988. In November 2000, the Storehouse opened its doors to the public and has quickly become Ireland's number one tourist attraction.</p>

<?php include("../../footer.php"); ?>