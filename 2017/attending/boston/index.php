<?php $section = "Attending"; $subsection = "Boston"; include("../../header.php"); ?>

  <h2 class="pageTitle">Boston</h2>

  <p><img src="/2013/images/boston/1.jpg" alt="Ha'penny Bridge">Dublin is one of the top city break destinations in Europe and for atmosphere, night life, visitor attractions, activities and shopping, Dublin has very few rivals. Steeped in history and buzzing with youthful energy, the Irish capital is at the very heart of Irish culture.</p>

  <p>Dublin is one of Europe's oldest cities and as well as retaining its historical and cultural charms, Dublin offers trendy bars, elegant restaurants and stylish, cosmopolitan shops and hotels. There's never been a better time to visit Dublin, a city that is rivalling Europe's elite for hip appeal.</p>

  <p>With over fifty golf courses within an hour of Dublin, three racecourses and excellent fishing facilities, the sports enthusiast is well taken care of. Or enjoy a literary pub crawl around the city, experience a Dublin Bay sea thrill, a boat trip on the River Liffey or enjoy a traditional Irish music session in one of Dublin's famous Irish pubs. The options are endless and the welcome is always warm.</p>

  <h4>LOCATION</h4>

  <p><img src="/2012/images/dublin/3.jpg" alt="St Patricks Parade">Ireland is an island in North West Europe. Its nearest neighbour is Britain to the east, separated from it by the Irish Sea. To the north, west and south is the Atlantic Ocean. Ireland is made up of four provinces which are broken down into thirty-two counties. Six of these counties are in Northern Ireland and twenty-six are in the Republic. Dublin is the capital of the Republic and is Ireland's largest city. It is located on the east coast of Ireland, overlooking Dublin Bay. The Liffey is the main river running through the city. Dublin is split into four administrative units. These are Dublin City, Fingal, D�n Laoghaire-Rathdown and South Dublin.</p>

  <h4>USEFUL INFORMATION</h4>

  <p><strong>Climate</strong> Dublin's climate is mild and very changeable. It is influenced by the Gulf Stream. Winter temperatures average 4�C-7�C (39�-44�F) and in summer between 16�C-20�C (60�F-67�F). There are about eighteen hours of daylight daily during July and August, only getting truly dark after 23.00. In April, one can expect an average temperature range from 4�C-13�C (39�-55�F) with a 1/3 chance of rain on each day (average of 11 wet days in the month of April).</p>

  <p><strong>Population</strong> The population of Dublin City and County is almost 1.4 million. English is the spoken language throughout Ireland. Irish (or Gaelic) is the original native language of Ireland. Place names and signage are usually in both languages.</p>

  <p><strong>Currency</strong> The Euro is the only currency that is officially recognised as legal tender in the Republic of Ireland. Each Euro has 100 cent.</p>

  <p><strong>Annual public holidays</strong> - New Year's Day - January 1st - St. Patrick's Day - March 17th - Good Friday - although not a public holiday, is observed as such for many businesses in Dublin - Easter Monday - Monday following Easter Sunday - May Holiday - first Monday in May - June Holiday - first Monday in June (first day of ICWSM) - August Holiday - first Monday in August - October Holiday - last Monday in October - Christmas Day - December 25th - St. Stephens Day - December 26th</p>

  <p><strong>General info</strong> Dublin Tourism Information and Reservation Centres can be found at Dublin Airport, D�n Laoghaire Ferry Port, Suffolk Street and O'Connell Street. Call into one of their offices and let the experts guide you around Dublin. For more information see <a href='http://visitdublin.com'>visitdublin.com</a>.</p>

  <p><strong>Dublin Bus</strong> <img src="/2012/images/dublin/2.jpg" alt="Molly Malone Statue">Dublin Bus operates the city's bus routes. Depending on the location, buses run from 06.30-23.30. A late service called Nitelink operates after these hours Monday-Saturday on certain routes. Dublin Bus operates an exact fare policy so it is advisable to have correct change ready when boarding the bus. Fares are dependent on age and destination. For details on all routes, timetables and special value tickets available, enquire at any Dublin Tourism Office or directly at Dublin Bus, 59 Upper O'Connell Street, Dublin 1 or <a href="http://www.dublinbus.ie">www.dublinbus.ie</a>.</p>

  <p><strong>Bus �ireann</strong> is Ireland's national bus company. It provides bus services throughout the country with the exception of Dublin City. The majority of buses depart Dublin from the central bus station (Bus�ras) on Store Street in the city centre. For more information, timetables and services see <a href='http://www.buseireann.ie'>www.buseireann.ie</a>.</p>

  <p><strong>Train</strong> Iarnr�d �ireann (Irish Rail) operates the nationwide rail network. The DART (Dublin Area Rapid Transit) is Dublin's main train service and stretches the full length of Dublin Bay, from Malahide in North County Dublin to Greystones in County Wicklow. Suburban Rail covers commuter routes to and from Dublin and InterCity trains service major cities and towns around the country. Details on all routes, timetables and special value tickets are available at <a href='http://www.irishrail.ie'>www.irishrail.ie</a>.</p>

  <p><strong>Tram</strong> The Dublin tram system is called the LUAS. It is a state of the art light rail transit system with high capacity, high frequency and high speed services. The Luas runs on two tramlines: - The Green Line - connecting Sandyford to St. Stephen's Green - journey time approximately twenty-two minutes. - The Red Line - connecting Tallaght to Connolly train station - journey time approximately forty-six minutes. The two lines do not connect. Details on routes, timetables and tickets are available at <a href='http://www.luas.ie'>www.luas.ie</a>.</p>

  <p><strong>Taxis</strong> Dublin taxis have no distinctive colour but are easily identified by their official roof signs. Three of the main taxi ranks in the city centre are located on O'Connell Street, College Green and at St. Stephen's Green. It is also possible to hail taxis on the street and numerous private taxi companies also operate throughout Dublin City and County.</p>

  <p><strong>Car hire</strong> There is a wide choice of Irish and international car rental companies represented in Dublin. Bookings can be made online before arrival at <a href='http://www.visitdublin.com'>visitdublin.com</a>. Driving is on the left side of the road; Ireland uses the metric system. Safety belts must be worn at all times by front and back seat passengers. Older road signs give distances in miles but newer signs use kilometres. Speed limits are as follows: - Motorways - 120km/h - National roads (primary and secondary) - 100km/h - Non-national roads (regional and local) - 80km/h - Built-up areas - 50km/h</p>

  <p><strong>Telephone</strong> The international code for Ireland is 00 353. The local area code for Dublin is 01. In an emergency, call 999 or 112. These numbers will alert the appropriate service - fire, police (Garda�), lifeboat, coastal, mountain and cave rescue services. The numbers for local and national directory enquiries are 11811 or 11850 and for international enquiries 11818 or 11860.</p>

  <p><strong>Tourist assistance service</strong> is on Harcourt Street, Dublin 2 and can be contacted on +353 1 478 5295, <a href="http://www.itas.ie">www.itas.ie</a>. Internet and WiFi access is available at venues throughout Dublin.</p>

  <p><strong>Electrical current</strong> 220 / 240 volts (50 cycles) is the standard electrical current throughout Ireland. Plugs are 3-pin flat. Adapters are therefore required for 2-pin appliances. At ICWSM we will provide a limited number of (non-switching) universal power points that will work with most international plugs.</p>

  <p><strong>Accommodation</strong> Dublin provides visitors with a wide range of accommodation types, from hotels, guesthouses, Irish homes and self-catering to campus, specialist accommodation, caravan and camping and hostels. For reservations call into one of Dublin Tourism's four Information and Reservation Centres, see <a href='http://www.visitdublin.com'>visitdublin.com</a>, check out the <a href='/2012/attending/accommodation/'>ICWSM accommodation page</a> or book online using your preferred service.</p>

  <p><strong>Post offices</strong> <img src="/2012/images/dublin/4.jpg" alt="The Spire">Most post offices are open Monday to Friday 09.00- 17.30 and some open on Saturdays from 09.00- 13.00. The General Post Office on O'Connell Street in the city centre is open Monday to Saturday 08.00-20.00. Postal codes in Dublin are presented as 'Dublin plus number' - for example Dublin 1, Dublin 2. Odd numbers refer to areas north of the Liffey and even ones to areas south of the Liffey. They spread out numerically from the city centre.</p>

  <p><strong>Banks</strong> In general, banks open Monday to Friday, 10.00-16.00, with late opening on Thursdays until 17.00. All major credit cards are widely accepted. Foreign exchange facilities are available in the Dublin Tourism Centre on Suffolk Street.</p>

  <p><strong>ITAS</strong> The Irish Tourist Assistance Service is on Harcourt Street, Dublin 2 and can be contacted on +353 1 478 5295, <a href='http://www.itas.ie'>www.itas.ie</a>.</p>

  <p><strong>Business hours</strong> Most businesses are open Monday to Friday 09.00-17.30. Most shops in Dublin are open from 09.00 until 18.00 Monday-Saturday, sometimes later, and 12.00 until 18.00 on Sundays. Shops stay open late on Thursday evenings in the city centre and late on Friday evenings in the suburbs.</p>

  <p><strong>Tipping</strong> In restaurants a customary service charge may be added to a bill. If a service charge is included, tipping is not necessary, unless the service received is exemplary. If a service charge was not included, a normal tip is between 10%-15%. With other services from hairdressers to taxi drivers, it is generally not necessary to tip although staff will always appreciate acknowledgment of service.</p>

  <p><strong>Smoking</strong> Dublin was Europe's first smoke free capital city! Since 29th March 2004 smoking is banned in all enclosed places of work in Ireland. This includes banks, office blocks, public buildings, pubs, nightclubs, restaurants and caf�s. The primary purpose of the prohibition is to give protection to workers and the public who are exposed to harmful environmental tobacco smoke. Exceptions include hotel, guesthouse and B&amp;B bedrooms and private accommodation. Most pubs and some restaurants have installed or extended existing beer-gardens or patios to cater for those who smoke.</p>

  <p>For more information please check <a href='http://visitdublin.com'>visitdublin.com</a>.</p>

<?php include("../../footer.php"); ?>
