<?php $section = "Attending"; $subsection = "Accommodation"; include("../../header.php"); ?>

  <h2 class="pageTitle">Accommodation</h2>

  <p>AAAI has reserved a block of rooms at the conference venue hotel, the Hyatt Regency Montreal, at reduced conference rates. Space is limited so please be sure to reserve well in advance of the cut-off date of April 14.
<!--(Information about student accommodations is available by writing to <a href="mailto:aaai16@aaai.org">aaai16@aaai.org</a>. Please attach proof of full-time status.)-->
</p>


<h3>ICWSM-17 Conference Venue/Hotel</h3>

<h4>Hyatt Regency Montreal</h4>
<p>1255 Jeanne-Mance<br>
Montreal, Quebec, Canada, H5B 1E5<br>
Tel: +1 514 982 1234<br>
Website: <a href='https://montreal.regency.hyatt.com/en/hotel/home.html'>https://montreal.regency.hyatt.com/en/hotel/home.html</a>
</p>

<p>
<b>ICWSM-17 Reservations</b>: <a href='https://aws.passkey.com/e/48961536'>https://aws.passkey.com/e/48961536</a><br>
Conference Rates available: Sunday, May 14 - Thursday, May 18, 2017<br>
Standard guest room (single or double) per night: $209.00 CAD<br>
Triple Occupancy per night: $234.00 CAD <br>
Quad Occupancy per night: $259.00 CAD<br>
Check-in Time: 4:00pm<br>
Check-out Time: 11:00am<br>
Cut-off date for conference rate reservations: 5:00 PM local hotel time, Friday, April 14, 2017.
</p>

<h4>Accommodations</h4>
<p>The discounted room rates include basic wireless internet access. High speed internet access is available for an additional CAD $14.95 per day. A 5% federal goods and services tax (GST), a 9.97% provincial sales tax (PST), and a 3.5% lodging tax are applicable to room rates. <p>
 
<h4>Late Departures / Cancellations</h4>
<p>The hotel accommodates late departures upon request and availability. Guests checking out after the standard 11:00am departure time and before 5:00pm will be charged 50% of one night's room rate plus tax. Guests departing after 5:00pm will be charged an additional night. Cancellation of an individual reservation will be accepted NO LATER THAN 72 HOURS PRIOR TO ARRIVAL. Reservations cancelled after this deadline will be charged for one night's stay.</p>
 
<h4>Disclaimer</h4>
<p>In offering Hyatt Regency Montreal, the Montreal Trudeau International Airport, and all other service providers (hereinafter referred to as "Supplier(s)" for the Eleventh International AAAI Conference on Web and Social Media (ICWSM-17), AAAI acts only in the capacity of agent for the Suppliers that are the providers of the service. Because AAAI has no control over the personnel, equipment or operations or providers of accommodations or other services included as part of the ICWSM-17 program, AAAI assumes no responsibility for and will not be liable for any personal delay, inconveniences or other damage suffered by conference participants which may arise by reason of (1) any wrongful or negligent acts or omissions on the part of any Supplier or its employees, (2) any defect in or failure of any vehicle, equipment or instrumentality owned, operated or otherwise used by any Supplier, or (3) any wrongful or negligent acts or omissions on the part of any other party not under the control, direct or otherwise, of AAAI.</p>


<?php include("../../footer.php"); ?>