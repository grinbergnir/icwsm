<?php $section = "Attending"; $subsection = "Venue"; include("../../header.php"); ?>

<br>
<br>

<h2 class="pageTitle">Host City: Montreal, Canada</h2>

<center><p><a href="/2017/images/venue/view-from-mt-royal.jpg"><img style="margin: 0px; border: 0px" src="/2017/images/venue/view-from-mt-royal_thumb.jpg" alt="A view overlooking the city from atop the hill or “small mountain” in the center of the city of Montreal.  Mount Royal, after which the city itself was named, is part of the Monteregian Hills situated between the Laurentians and the Appalachian Mountains.
" /></a></p>
</center>

<br><p>&nbsp;</p>

<p>Montreal is a city of unbridled creativity. It comes out in the magnificent inventions being stirred up in restaurant kitchens, in the barroom and on stage, and in the street with incredible urban art lurking in unexpected places. The French-English bilingualism adds dynamism to the city as does its unique mash-up of European and North American culture. Most captivating of all are the people themselves. Montrealers embody joie de vivre.</p>

<div style="float: right; width: 220px; margin-left: 1em; text-align:justify;"><a href="/2017/images/venue/biodome.jpg"><img src="/2017/images/venue/biodome_thumb.jpg" alt="The unique architecture of the Olympic Park, site of the 1976 olympics, now the site of the the Biodome the Botanical Garden.
" /></a>The unique architecture of the Olympic Park, site of the 1976 olympics, now the site of the the Biodome the Botanical Garden.
</div>
<p>Toronto may be Canada's economic capital, but Montreal remains the country's cultural juggernaut. The city, standard bearer of an entire linguistic-cultural identity -- Francophone Canada -- simply lives for public celebration of the arts. There are some 250 theater and dance companies, more than 90 festivals and a fascinating medley of neighborhoods where artists, writers and musicians have helped cement the city's reputation as a great arts center. The Festival International de Jazz de Montreal is the headline event, followed by parties dedicated to world cinema, comedy and gay pride.</p>

<div style="float: right; width: 220px; margin-left: 1em; text-align:justify;"><a href="/2017/images/venue/notre-dame-basilica-inside.jpg"><img src="/2017/images/venue/notre-dame-basilica-inside_thumb.jpg" alt="Located in the historic district of Old Montreal, The church's Gothic Revival architecture is among the most dramatic in the world; its interior is grand and colourful, its ceiling is coloured deep blue and decorated with golden stars, and the rest of the sanctuary is a polychrome of blues, azures, reds, purples, silver, and gold." /></a>Located in the historic district of Old Montreal, The church's Gothic Revival architecture is among the most dramatic in the world; its interior is grand and colourful, its ceiling is coloured deep blue and decorated with golden stars, and the rest of the sanctuary is a polychrome of blues, azures, reds, purples, silver, and gold.
</div>
<p>Montreal is also blessed with one of the most exciting food scenes in North America. The city brims with temples dedicated to Kamouraska lamb, Arctic char and, of course, poutine (fries smothered in cheese curds and gravy). You'll find irresistible patisseries, English pubs, 80-plus-year-old Jewish delis and magnificent food markets reminiscent of Paris. There are hipster bars with tiny bowling alleys and innumerable cafes.</p>

<p>Montreal is a slice of old Europe in a pie of contemporary design. Don't miss the photogenic 18th-century facades of Old Montreal and the lovely Canal de Lachine. The architectural sweep of the city takes in a wealth of heritage churches such as the breathtaking Basilique Notre-Dame, as well as 20th-century icons like the Stade Olympique and Habitat 67. Montreal's hotels and museums additionally push the edges of contemporary interior design.</p>

<p><a href="/2017/images/venue/old-port.jpg"><img style="margin: 0; border: 0px" src="/2017/images/venue/old-port_thumb.jpg" alt="The Old Port of Montreal is the historic port of Montreal, located in the area of the city called Old Montreal.  It stretches for over two kilometres along the St-Lawrence River.  It was used as early as 1611, when French fur traders used it as a trading post.  The Old Port was redeveloped in the early 1990s, and today is a recreational and historical area drawing six million tourists annually." /></a>
The Old Port of Montreal is the historic port of Montreal, located in the area of the city called Old Montreal.  It stretches for over two kilometres along the St-Lawrence River.  It was used as early as 1611, when French fur traders used it as a trading post.  The Old Port was redeveloped in the early 1990s, and today is a recreational and historical area drawing six million tourists annually.</p>

<br>

<h2 class="pageTitle">Conference Venue</h2>

<h3>Hyatt Regency Montreal</h3>
<p>1255 Jeanne-Mance<br>
Montreal, Quebec, Canada, H5B 1E5<br>
Tel: +1 514 982 1234<br>
Website: <a href='https://montreal.regency.hyatt.com/en/hotel/home.html'>https://montreal.regency.hyatt.com/en/hotel/home.html</a>
</p>

<p>AAAI has reserved a block of rooms at the conference venue hotel, the Hyatt Regency Montreal, at reduced conference rates. Space is limited so please be sure to reserve well in advance of the cut-off date of April 14. See the <a href='/2017/attending/accommodation'>accommodation</a> section for more details about hotel reservations.</p>

<h3>Hotel Information</h3>
<p>Experience the warm hospitality of Montreal with a distinct French flavor. The Hyatt Regency Montreal is located in the downtown area and offers direct underground access to the Palais des Congrès and two Montreal metro lines, with easy access to Sainte-Catherine Street, downtown shopping centers, Chinatown, Old Montreal, Place des Arts, and the Metro transport system. </p>

<h3>Transportation from Montreal Trudeau International Airport</h3>
<p><b>Bus</b>: "747 bus" is the shuttle service from the airport to the Montreal Central Bus Station. The 747 bus line service runs 24 hours a day, 7 days a week, between the Montréal-Trudeau airport and the Gare d’autocars de Montréal terminal (Berri-UQAM métro station). Travel time may vary between 45 to 60 minutes, depending on traffic conditions. Wi-Fi service is available on most 747 buses. The fare is $10.00 (cash only) for unlimited travel throughout STM bus and métro networks during 24 consecutive hours. The hôtel is located at the corner of Jeanne-Mance and René Levesque streets (bus stop number 7). <br>
Bus: $10 CAD (cash only)<br>
Taxi: $40 CAD one way <br></p>

<p><b>Local Subway Station</b>: Place Des Arts. Exit station onto Jeanne-Mance, walk south (left toward St. Catherine Street). The Hotel is located on the corner of St. Catherine Street and Jeanne-Mance Street.</p>

<p><b>Parking</b><br>
VALET HOURLY: No in and out privileges, spaces are limited<br>
 $17 (3 hours or less) <br>
 $22 (between 3 hours - 6 hours)<br>
 $32 (more than 6 hours)<br> 
OVERNIGHT VALET: Includes in and out privileges, spaces are limited<br>
 $32 Daily<br>
</p>

<h3>Disclaimer</h3>
<p>In offering Hyatt Regency Montreal, the Montreal Trudeau International Airport, and all other service providers (hereinafter referred to as "Supplier(s)" for the Eleventh International AAAI Conference on Web and Social Media (ICWSM-17), AAAI acts only in the capacity of agent for the Suppliers that are the providers of the service. Because AAAI has no control over the personnel, equipment or operations or providers of accommodations or other services included as part of the ICWSM-17 program, AAAI assumes no responsibility for and will not be liable for any personal delay, inconveniences or other damage suffered by conference participants which may arise by reason of (1) any wrongful or negligent acts or omissions on the part of any Supplier or its employees, (2) any defect in or failure of any vehicle, equipment or instrumentality owned, operated or otherwise used by any Supplier, or (3) any wrongful or negligent acts or omissions on the part of any other party not under the control, direct or otherwise, of AAAI.</p>

<br>

<!-- <br><br>
<center><IMG class="displayed" style="border : 0;" src="/2016/images/venue/GESISGebaeude.jpg" alt="Maternushaus - Tagungszentrum des Erzbistums K&ouml;ln" align="middle"></center>
 -->
<?php include("../../footer.php"); ?>
























