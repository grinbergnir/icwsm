<?php $section = "Attending"; $subsection = "ACM WEB SCIENCE 2016"; include("../../header.php"); ?>

<br>
<br>



<h2 class="pageTitle">ACM Web Science 2016</h2>
<h4>Hosted in Germany from May 22-25, 2016</h4>

<center><IMG class="displayed" style="border : 0;" src="/2016/images/venue/WebSci'16_No_dates-2-150.png" alt="ACM Web Science 2016" align="middle"></center>


<p><br> The 8th International <a href="http://www.websci16.org/" target=_blank>ACM Web Science Conference 2016</a> will be held from May 22 to May 25, 2016 in Hannover, Germany and is organized by L3S Research Center. More details can be found <a href="http://www.websci16.org/" target=_blank>here</a>.
</p>

<?php include("../../footer.php"); ?>
























