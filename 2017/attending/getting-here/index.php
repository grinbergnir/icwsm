<?php $section = "Attending"; $subsection = "Getting Here"; include("../../header.php"); ?>

<br><br>
<h2 class="pageTitle">Getting Here</h2>

<img style="float: left; margin: 0px 1em 0px 0em; border: 0px;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Symbol_lightbulb.svg/1024px-Symbol_lightbulb.svg.png" alt="In Canada it's customary to tip service providers including taxi drivers, waiters, and food delivery persons. An average tip is 15% and a polite minimum is 10%." width="48px" />
<p>In Canada it's customary to tip service providers including taxi drivers, waiters, and food delivery persons. An average tip is 15% and a polite minimum is 10%.</p>

<p style="border:1px; border-style: solid; padding: 2px 4px 2px 4px"><b>Venue Location</b>: The Hyatt Regency Montreal Hotel, located in the Complexe Desjardins, at <a href="https://www.google.ca/maps/place/Hyatt+Regency+Montreal/@45.5073237,-73.5676147,17z/data=!3m1!4b1!4m5!3m4!1s0x4cc91a4fba9dd1c5:0xf77d3e62fbe10c91!8m2!3d45.50732!4d-73.565426" target="_blank">1255 Jeanne Mance St, Montreal, QC H5B 1E5</a></p>

<p>
	Most attendees from out of town will travel to Montreal by air, and land at the <a href="http://www.admtl.com/en" target="_blank">Pierre Elliot Trudeau Airport</a> (YUL). Attendees coming from major cities within a few hundred kilometers might also consider ground transit: nearby <a href="http://www.viarail.ca/en/explore-our-destinations/trains/ontario-and-quebec" target="_blank">major Canadian cities</a> like Toronto and Ottawa are connected by train (<a href="http://www.viarail.ca/en/explore-our-destinations/stations/quebec/montreal" target="_blank">VIA rail</a>), and both Canadian and US cities are connected by bus (<a href="https://www.greyhound.ca/" target="_blank">greyhound</a>).
</p> 
  
<!-- <ul>
<li>Ann Arbor is 25 minutes by car from the Detroit airport (DTW).</li>
<li>Bus service is $12 each way. <a href='http://myairride.com/AirRide/Schedule'>http://myairride.com/AirRide/Schedule</a></li>
<li>Taxi service is $55-70 each way.</li>
<li>Car rentals are available at the airport, but not recommended if you stay at one of the housing options within 1km of the conference venue.</li>
<li>Parking is available in public lots a few blocks from the conference site, at a cost of $1.20/hour. The closest parking structure with public spaces is located on Maynard Street between Liberty and William (<a href='http://www.a2state.com/images/subpages/DDGwalkmap0407.jpg'>Parking Map</a>).</li>
 </ul>-->


<h3>Travel information</h3>

<h4>By Air (Recommended)</h4>
<p style="margin-bottom: 0px;">You will land at the Pierre Elliot Trudeau Airport (YUL). To get from the airport to the venue, you have two options:
<ul style="margin: 0px;">
<li><b>Taxi (Easiest)</b> -- Taxi from the airport to the venue has a flat rate of CAD$40 (tip your driver!)</li>
<li><b>City Bus (Cheapest)</b> -- You can take the #747 city bus for CAD$10. It will take you all the way to the Berri/UQAM subway station, which is a central node of the public transit network. Then, from Berri/UQAM you can either <a href="https://www.google.ca/maps/dir/berri%2Fuqam+station+google+map/1255+Rue+Jeanne-Mance,+Montr%C3%A9al,+QC+H2X/@45.5107619,-73.5672354,16z/am=t/data=!4m14!4m13!1m5!1m1!1s0x4cc91bb2b0ae66c9:0x4be86e79a672e1a0!2m2!1d-73.5597267!2d45.5148865!1m5!1m1!1s0x4cc91a4fbcc8461f:0xdc22faae302d88e4!2m2!1d-73.5655099!2d45.5070665!3e2" target="_blank">walk</a> (14 minutes), or hop on the <a href="https://www.google.ca/maps/dir/berri%2Fuqam+station+google+map/1255+Rue+Jeanne-Mance,+Montr%C3%A9al,+QC+H2X/@45.5103711,-73.567203,16z/am=t/data=!3m1!4b1!4m14!4m13!1m5!1m1!1s0x4cc91bb2b0ae66c9:0x4be86e79a672e1a0!2m2!1d-73.5597267!2d45.5148865!1m5!1m1!1s0x4cc91a4fbcc8461f:0xdc22faae302d88e4!2m2!1d-73.5655099!2d45.5070665!3e3" target="_blank">the green subway line</a> (free with your bus ticket) in the direction <b>Angrignon</b> for two stops (Berri/UQAM -> St. Laurent -> Place des Arts) which puts you very close to the venue.</li>
</ul>
</p>

<h4>By Train</h4>
<p>You will arrive at the "Gare Centrale" (Central Station), located at 895 de la Gauchetiere West Montr&eacute;al. The central station is only a 13-minute walk to the conference venue (<a href="https://www.google.ca/maps/dir/895+Rue+de+la+Gaucheti%C3%A8re+O,+Montr%C3%A9al,+QC+H3B+4G1/1255+Jeanne+Mance+Street,+Montreal,+QC/@45.5034205,-73.5690502,16z/am=t/data=!4m14!4m13!1m5!1m1!1s0x4cc91a5cb0bcd01f:0x8fdbd13be58df1ed!2m2!1d-73.566459!2d45.499918!1m5!1m1!1s0x4cc91a4fbcc8461f:0xdc22faae302d88e4!2m2!1d-73.5655099!2d45.5070665!3e2" target="_blank">directions</a>), so it is probably not worth trying to use public transit. If you're carrying a lot, there is a taxi stand at the train station, and the trip to the venue will be less than CAD$10 (tip your driver!).
</p>

<h4>By Bus</h4>
<p>You will arrive at the "Gare D'Autocars de Montreal" (Montreal Bus Station) located at 1717 Rue Berri St. (<a href="https://www.greyhound.ca/en/locations/terminal.aspx?city=111252" target="_blank">coordinates</a>). From there, it's an 18-minute walk to the venue (<a href="https://www.google.ca/maps/dir/1717+Rue+Berri,+Montr%C3%A9al,+QC+H2L+4E9/1255+Rue+Jeanne-Mance,+Montr%C3%A9al,+QC+H2X/@45.5119583,-73.568386,16z/data=!3m1!4b1!4m14!4m13!1m5!1m1!1s0x4cc91bb320bbe227:0x3c57fd2483923718!2m2!1d-73.5635052!2d45.5170038!1m5!1m1!1s0x4cc91a4fbcc8461f:0xdc22faae302d88e4!2m2!1d-73.5655099!2d45.5070665!3e2" target="_blank">directions</a>). The easiest alternative if you don't want to / can't walk to the venue is to catch a taxi, which will cost less than CAD$10.
</p>
  
<?php include("../../footer.php"); ?>