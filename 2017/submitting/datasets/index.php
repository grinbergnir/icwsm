<?php $section = "Submitting"; $subsection = "Datasets"; include("../../header.php"); ?>

<P></P>
<P>ICWSM will use <a href="https://zenodo.org/" target="_blank">Zenodo</a> as data sharing service.<br>
Please upload your dataset, get a DOI and add it to the camera ready version of your paper.</P>
  
<?php include("../../footer.php"); ?>