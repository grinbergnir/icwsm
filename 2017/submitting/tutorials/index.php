<?php $section = "Submitting"; $subsection = "Tutorials"; include("../../header.php"); ?>

  <h2 class="pageTitle">Call for Tutorials</h2>
         
  <h4>Tutorials at the Eleventh International AAAI Conference on Web and Social Media</h4>
  <h6>Sponsored by the Association for the Advancement of Artificial Intelligence</h6>
  
  <ul>
    <li>Submission Deadline: <b><a href="http://www.timeanddate.com/countdown/generic?iso=20170125T235959&p0=103&msg=ICWSM+%2716+Tutorial+Proposal+Deadline&font=sanserif&csz=1" target=_blank>January 25, 2017</a></b></li>
    <li>Proposal Submission Email: <b><a href="mailto:icwsm17tutorials@googlegroups.com">icwsm17tutorials@googlegroups.com</a></b></li>
    <li>Notification of acceptance: February 15, 2017</li>
  </ul>
  
  <p>The ICWSM-17 Committee invites proposals for Tutorials Day at the Eleventh International AAAI Conference on Weblogs and Social Media (ICWSM-17). Tutorials will be held on <b>*May 15, 2017*</b> in Montreal, Canada.
</p>
  
 <p>ICWSM is seeking proposals for advanced tutorials on topics related to the analysis and understanding of social phenomena, particularly as seen on social media. We are looking for contributions from experts in both the social and computational sciences. The tutorials will be an opportunity for cross-disciplinary engagement and a deeper understanding of new tools, techniques, and research methodologies. Each tutorial should provide either an in depth look at an emerging technique or software package or a broad summary of an important direction in the field. For a list of 2016 tutorials from last year, visit <a href="http://www.icwsm.org/2016/program/tutorial/">here</a>. </p>



<h4>Acceptance criteria</h4>

<p>The tutorial format will be entirely determined by the tutorial organizers. The proposals will be selected considering the following criteria:</p>

<ul>
<li><b>Cross-pollination potential.</b> Tutorials that attract an interdisciplinary audience will be given preference over others. The proposals should highlight the tutorial’s potential to transfer knowledge from one discipline/area to another.</li>
<li><b>Interactivity.</b> We will favor tutorials that embed hands-on experiences, collaborative approaches, and interactivity above tutorials that provide only frontal lectures. </li>
<li><b>Teaching experience of the tutors.</b></li>
</ul>


<p>Depending on the type of tutorial, not all the criteria above might be met, although tutorials that include more of them will be scored higher. Proposals of tutorials presented in past events are allowed, although novelty is a plus.</p>


<h4>Proposal content and format</h4>

<p>Proposals for tutorials should be no more than four (4) pages in length (10pt, single column, with reasonable margins), written in English, and should include the following:</p>

<ul>
<li><b>Tutorial title and summary.</b> A short description (500 words max) of the scope main objective of the tutorial that will be published on the main ICWSM website.</li>
<li><b>Names, affiliations, emails, and personal websites of the tutorial organizers.</b> A main contact author should be specified. A typical proposal should include no more than four co-organizers.</li>
<li><b>Duration.</b> The tutorial duration can range between two hours to the full day, depending on the type of activities scheduled. The Tutorial Chairs might conditionally accept a proposal and suggest a different duration to best fit the organization of the whole event. </li>
<li><b>Tutorial schedule and activities.</b> A description of the proposed tutorial format, a schedule of the proposed activities (e.g., presentations, interactive sessions) along with a <b>*detailed*</b> description for each of them. The tutor(s) for each activity should be specified as well.</li>
<li><b>Target audience, prerequisites and outcomes.</b> A description of the target audience, the prerequisite skill set for the attendee as well as a list of goals for the tutor to accomplish by the end of the tutorial. 
<li><b>Expected number of attendees.</b> This is required for logistics planning.</li>
<li><b>Material available on the tutorial website.</b> The organizers of accepted tutorials are encouraged to set up on their own a web page containing all the information for the tutorial attendees before the tutorial day (roughly 1 week before, at most). The proposal should contain a detailed list of the material that will be made available on the website.</li>
<li><b>Precedent [when available]:</b> A list of other tutorials held previously at related conferences, if any, together with a brief statement on how the proposed tutorial differs from or how it follows-up on previous events. If the authors of the proposal have organized other tutorials in the past, pointers to the relevant material (e.g., slides, videos, web pages, code) should be provided.</li>
<li><b>Special requirements [when needed]:</b> A list of equipment and that needs to be made available by conference organizers (other than wifi, a projector, and a regular workshop room setup). </li>

</ul>


<p>Submissions must be in PDF to the submission email (<a href="mailto:icwsm17tutorials@googlegroups.com">icwsm17tutorials@googlegroups.com</a>). Pre-submission questions can be sent to the chair at the following address: <a href="mailto:icwsm16@aaai.org">icwsm16@aaai.org</a>.</p>


<h4>Financial support.</h4>

<p>Tutorial organizers will be given up to $1000 ($500/organizer) to cover conference-related expenses.</p>


  <h4>Important Dates<br />(All deadlines are on 23:59:59 Hawaii Standard Time)</h4>


  <ul>
<li>Tutorials proposal submission deadline: <b><a href="http://www.timeanddate.com/countdown/generic?iso=20170125T235959&p0=103&msg=ICWSM+%2716+Tutorial+Proposal+Deadline&font=sanserif&csz=1" target=_blank>January 25, 2017</a></b></li>
<li>Tutorials acceptance notification: February 15, 2017</li>
<li>Tutorial sites and materials online: May 9, 2017</li>
<li>ICWSM-17 Tutorials Day: May 15, 2017</li>
  </ul>


  
  <p>- Tutorial Chairs: Vanessa Frias-Martinez (University of Maryland), Sarita Schoenebeck (University of Michigan). 
</p>
  
<?php include("../../footer.php"); ?>