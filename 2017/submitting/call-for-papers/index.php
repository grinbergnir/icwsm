<?php $section = "Submitting"; $subsection = "Call for Papers"; $share_desc = "ICWSM-17, one of the premier conferences in computational social science, will be held in Montreal, Canada this coming May. Abstracts due January 6 and full papers on January 13."; include("../../header.php"); ?>

  <h2 class="pageTitle">Call for Papers</h2>

  <h4>THE 11TH INTERNATIONAL AAAI CONFERENCE ON WEB AND SOCIAL MEDIA (ICWSM-17)</h4>
  
  <h6>SPONSORED BY THE ASSOCIATION FOR THE ADVANCEMENT OF ARTIFICIAL INTELLIGENCE</h6>
  
<ul>
  <li><a href="https://easychair.org/conferences/?conf=icwsm17">Technical Paper Submission Site</a></li>
  <li>Abstract submission: <b><a href="http://www.timeanddate.com/countdown/generic?iso=20170106T2359&p0=103&msg=ICWSM+%2717+Abstract+Submission+Deadline&font=cursive&csz=1" target=_blank>January 6, 2017 (by 23:59 Hawaii Standard Time)</a></b></li>
  <li>Full paper submission: <b><a href="http://www.timeanddate.com/countdown/generic?iso=20170113T2359&p0=103&msg=ICWSM+%2716+Full+Paper+Submission+Deadline&font=sanserif&csz=1" target=_blank>January 13, 2017 (by 23:59 Hawaii Standard Time)</a></b></li>
  <li>Paper and poster notifications: February 28, 2017</li>
  </ul>
  

  <p>The International AAAI Conference on Web and Social Media (ICWSM) is a forum for researchers from multiple disciplines to come together to share knowledge, discuss ideas, exchange information, and learn about cutting-edge research in diverse fields with the common theme of online social media. This overall theme includes research in new perspectives in social theories, as well as computational algorithms for analyzing social media. ICWSM is a singularly fitting venue for research that blends social science and computational approaches to answer important and challenging questions about human social behavior through social media while advancing computational tools for vast and unstructured data.
</p>

  <p>ICWSM, now in its eleventh year, has become one of the premier venues for computational social science, and <a href="/2017/contact/previous-conferences/">previous years of ICWSM</a> have featured papers, posters, and demos that draw upon network science, machine learning, computational linguistics, sociology, communication, and political science. The uniqueness of the venue and the quality of submissions have contributed to a fast growth of the conference and a competitive acceptance rate around 20% for full-length research papers published in the proceedings by the Association for the <a href="http://www.aaai.org/">Association for the Advancement of Artificial Intelligence (AAAI)</a>. The accepted papers will be open access and have equal time for a full oral presentation. Full papers will also have the opportunity to display their paper as a poster at the venue.</p>

  <p>For ICWSM-17, in addition to the usual program of contributed talks, posters and demos, the main conference will include a selection of keynote talks from prominent scientists and technologists. Building on successes in previous years, ICWSM-17 will also hold a day of workshops and tutorials in addition to the main conference.</p>


  <h3>Disciplines</h3>

  <ul>
    <li>Computational approaches to social media research including
        <ul>
            <li>Natural language processing</li>
            <li>Text / data mining</li>
            <li>Machine learning</li>
            <li>Image / multimedia processing</li>
            <li>Graphics and visualization</li>
            <li>Distributed computing</li>
            <li>Graph theory and Network Science</li>
            <li>Human-computer interaction</li>
        </ul>
    </li>
    <li>Social science approaches to social media research including
        <ul>
            <li>Psychology</li>
            <li>Sociology and social network analysis</li>
            <li>Communication</li>
            <li>Political science</li>
            <li>Economics</li>
            <li>Anthropology</li>
            <li>Media studies and journalism</li>
            <li>Digital Humanities</li>
        </ul>
    </li>
    <li>Interdisciplinary approaches to social media research combining computational algorithms and social science methodologies</li>
  </ul>


  <h3>Topics Include (BUT ARE NOT LIMITED TO)</h3>

    <ul>
     <li>Studies of digital humanities (culture, history, arts) using social media</li>
     <li>Psychological, personality-based and ethnographic studies of social media</li>
     <li>Analysis of the relationship between social media and mainstream media</li>
     <li>Qualitative and quantitative studies of social media</li>
     <li>Centrality/influence of social media publications and authors</li>
     <li>Ranking/relevance of social media content and users</li>
     <li>Credibility of online content</li>
     <li>Social network analysis; communities identification; expertise and authority discovery</li>
     <!--<li>Collaborative filtering</li>-->
     <li>Trust; reputation; recommendation systems</li>
     <li>Human computer interaction; social media tools; navigation and visualization</li>
     <li>Subjectivity in textual data; sentiment analysis; polarity/opinion identification and extraction, linguistic analyses of social media behavior</li>
     <li>Text categorization; topic recognition; demographic/gender/age identification</li>
     <li>Trend identification and tracking; time series forecasting</li>
     <li>Measuring predictability of real world phenomena based on social media, e.g., spanning politics, finance, and health</li>
     <li>New social media applications; interfaces; interaction techniques</li>
     <li>Engagement, motivations, incentives, and gamification.</li>
     <li>Social innovation and effecting change through social media</li>
     <li>Social media usage on mobile devices; location, human mobility, and behavior</li>
     <li>Organizational and group behavior mediated by social media; interpersonal communication mediated by social media</li>


  </ul>

  <h3>TYPES OF SOCIAL MEDIA INCLUDE</h3>
  <ul>
<li>Social networking sites (e.g., Facebook, LinkedIn)</li>
<li>Microblogs (e.g., Twitter, Tumblr)</li>
<li>Wiki-based knowledge sharing sites (e.g., Wikipedia)</li>
<li>Social news sites and websites of news media (e.g., Huffington Post)</li>
<li>Forums, mailing lists, newsgroups</li>
<li>Community media sites (e.g., YouTube, Flickr, Instagram)</li>
<li>Social Q & A sites (e.g., Quora, Yahoo Answers)</li>
<li>User reviews (e.g., Yelp, Amazon.com)</li>
<li>Social curation sites (e.g., Reddit, Pinterest)</li>
<li>Location-based social networks (e.g., Foursquare, Pok&eacute;mon Go)</li>
<li>Online dating sites (e.g., Match, Tinder, Grindr)</li>
<li>Messaging platforms (e.g., Snapchat, Messenger, WhatsApp)</li>
  </ul>




<h3>Summary Submission Guidelines</h3>


<p>All ICWSM-17 authors are required to use the AAAI Author Kit to format their papers. The author kit is available at <b><a href="http://www.aaai.org/Publications/Templates/AuthorKit17.zip">http://www.aaai.org/Publications/Templates/AuthorKit17.zip</a></b>.</p>


<p>Please note that the formatting and submission instructions in the author kit are for final, accepted papers; ICWSM submissions are anonymous, and must conform to all detailed instructions for blind review (<a href="http://icwsm.org/2017/submitting/guidelines/">Detailed guidelines</a>). In addition, the copyright slug may be omitted in the initial submission phase and no copyright form is required until a paper is accepted for publication.</p>

<p>Detailed guidelines for authors submitting to ICWSM-17 can be found <a href="/2017/submitting/guidelines/">here</a>.</p>

<p>This year there are three submission options; ICWSM is introducing a new paper format, Dataset Papers, for researchers whose primary contribution is to provide new data resources and methodological novelty in procuring those resources.</p>

<p><b>1. Full paper format:</b> Full paper submissions to ICWSM are recommended to be 8 pages long, and must be at most 10 pages long, including figures and references. The final camera-ready length (between 8-10 pages) for each full paper in the proceedings will be at the discretion of the program chairs. All papers must follow AAAI formatting guidelines.</p>

<p><b>2. Dataset paper format:</b> Dataset paper submissions must comprise two parts: a dataset or group of datasets, and metadata describing the content, quality, structure, potential uses of the dataset(s), and methods employed for data collection. Descriptive statistics may be included in the metadata (more sophisticated analyses should be part of a regular paper submission). Datasets and metadata must be published as part of ICWSM <a href="http://icwsm.org/2017/submitting/datasets/">data sharing service</a> (if the paper gets accepted) and the papers will be part of the full proceedings. Dataset paper submissions must be between 8-10 pages long. All papers must follow AAAI formatting guidelines.</p>

<p><b>3. Poster and demo paper format:</b> Poster paper submissions to ICWSM must be 4 pages long, including figures and references. Demo paper submissions to ICWSM must be 2 pages long, including figures and references. All papers must follow AAAI formatting guidelines.</p>

<p><b>Anonymity:</b> Paper submissions to ICWSM must be anonymized.</p>

<p><b>Social sciences and sociophysics papers with optional publication:</b> We will be continuing the 'social science and sociophysics' track at ICWSM-17 following its successful debut in 2013. This option is for researchers in social science and sociophysics who wish to submit full papers without publication in the conference proceedings. While papers in this track will not be published, we expect these submissions to describe the same high-quality and complete work as the main track submissions. Papers accepted to this track will be full presentations integrated with the conference, but they will be published only as abstracts in the conference proceedings.</p>

<p><b>Ethics</b>: We encourage the authors to obtain ethical approval for experiments with human subjects from their corresponding institutions' Internal Review Board (IRB) and demonstrate this information as part of the submission. </p>


  <h3>Important Dates</h3>

  <ul>
    <li>Abstract Submission: <b><a href="http://www.timeanddate.com/countdown/generic?iso=20170106T2359&p0=103&msg=ICWSM+%2717+Abstract+Submission+Deadline&font=cursive&csz=1" target=_blank>January 6, 2017 (by 23:59 Hawaii Standard Time)</a></b></li>

    <li>Full Papers Submission: <b><a href="http://www.timeanddate.com/countdown/generic?iso=20170113T2359&p0=103&msg=ICWSM+%2716+Full+Paper+Submission+Deadline&font=sanserif&csz=1" target=_blank>January 13, 2017 (by 23:59 Hawaii Standard Time)</a></b></li>

    <li>Paper and poster notifications: February 28, 2017</li>

    <li>Camera Ready Version Due: March 10, 2017</li>

    <li>ICWSM-17, Montreal, Canada: May 15-18, 2017</li>

    <!--<li style="list-style: none">-->
    </ul>


<br><br>
<h4>SANDRA GONZALEZ-BAILON, ALICE MARWICK & WINTER MASON</h4>
<h5>(ICWSM-17 Program Co-chairs)</h5>
 
<?php include("../../footer.php"); ?>
