<?php $section = "Submitting"; $subsection = "Guidelines"; include("../../header.php"); ?>

  <h2 class="pageTitle">Detailed Submission Guidelines</h2>

  <h4>Content Guidelines</h4>

  <p><strong>Format:</strong> Papers must be in trouble-free, high resolution PDF format, formatted for US Letter (8.5" x 11") paper, using Type 1 or TrueType fonts. Full papers are recommended to be 8 pages and must be at most 10 pages long, including references.  Final camera-ready papers will be allowed between 8-10 pages, at the discretion of the program chairs.  Poster papers must be no longer than 4 pages, and demo descriptions must be no longer than 2 pages, and all must be submitted by the deadlines given above, and formatted in AAAI two-column, <a href="http://www.aaai.org/Publications/Templates/AuthorKit17.zip">camera-ready style</a>. Please note that the formatting and submission instructions at the author site are for final, accepted papers; no additional pages can be purchased at the review stage. In addition, no source files (Word or LaTeX) are required at the time of submission for review; only the PDF file is permitted. Finally, the copyright slug may be omitted in the initial submission phase and no copyright form is required until a paper is accepted for publication.</p>

  <p><strong>Anonymity:</strong> ICWSM-17 review is double-blind. Therefore, please anonymize your submission: do not put the author(s) names or affiliation(s) at the start of the paper, and do not include funding or other acknowledgments in papers submitted for review. References to authors' own prior relevant work should be included, but should not specify that this is the authors' own work. Citations to the author's own work should be anonymized, if possible, or can be added later to the final camera-ready version for publication. It is up to the authors' discretion how much to further modify the body of the paper to preserve anonymity. The requirement for anonymity does not extend outside of the review process, e.g. the authors can decide how widely to distribute their papers over the Internet before the program committee meeting. Even in cases where the author's identity is known to a reviewer, the double-blind process will serve as a symbolic reminder of the importance of evaluating the submitted work on its own merits without regard to authors' reputation.</p>

  <p><strong>Duplicate Submission:</strong> Submissions to other conferences or journals: ICWSM-17 will not accept any paper that, at the time of submission, is under review for or has already been published or accepted for publication in a journal or conference. This restriction does not apply to submissions for workshops and other venues with a limited audience.  Nor does this restriction apply to papers that are submitted to ICWSM's new social sciences track (please see details below), that are not targeted for publication in the proceedings. If in doubt please contact the PC Chairs.</p>

  <p>If duplicate submissions are identified during the review process then:</p>

  <ul>
  <li>All submissions from that author will be disqualified from the current ICWSM conference;</li>
  <li>And authors will not be permitted to submit papers to the ICWSM conference in the following year.</li>
  </ul>

  
  <p><strong>Language:</strong> All submissions must be in English.</p>

  <h4>Optional publication for social sciences and sociophysics papers</h4>
  
  <p>Researchers who wish to submit full papers without publication in the conference proceedings, may submit to the 'social sciences and sociophysics (not for publication)' track at ICWSM-17. While papers in this track will not be published, we expect the submissions will represent high-quality, completed work.  While we will not accept previously published papers to this track, papers submitted to the social sciences and sociophysics (not for publication) track may be under review concurrently at a journal.  Submissions must adhere to the formatting and content guidelines above. Papers accepted to this track will be full presentations, integrated with the conference, but will be published only as abstracts in the conference proceedings.
  
  <p>Researchers who do wish to publish their papers in the ICWSM proceedings should submit to the regular track.  All submitted papers, whether targeted for publication or not, will be judged to the same acceptance criteria.</p>
  
  <h4>Conference Registration</h4>

  <p>All accepted papers and extended abstracts will be published in the conference proceedings. At least one author must register for the conference by the deadline for camera-ready copy submission. In addition, the registered author must attend the conference to present the paper in person.</p>

  <h4>Publication</h4>

  <p>All papers accepted for publication will be allocated between eight (8) and ten (10) pages in the conference proceedings, at the discretion of the program chairs based on the length warranted by the content and contribution. Authors will be required to transfer copyright to AAAI.</p>

  <h4>Datasets</h4>

  <p>ICWSM provides a service for hosting datasets pertaining to research presented at the conference. Authors of accepted papers will be encouraged to share the datasets on which their papers are based, while adhering to the terms and conditions of the data provider. 

Of these datasets, one will be selected for an award which will be based on the quality, scope, and timeliness of each dataset. More information will be available on our website. </p>

<?php include("../../footer.php"); ?>