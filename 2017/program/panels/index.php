<?php $section = "Program"; $subsection = "Panels"; include("../../header.php"); ?>

<br>  <h2 class="pageTitle">Panels</h2>

<h3>The Ethics of Social Data</h3>
<p>In this panel, two senior scholars discuss the ethical complexity of Big Data. While social media has provided computer scientists and social scientists alike with a rich trove of personal information, scholarly communities are grappling with how to use this data ethically. Guidelines and institutional review boards designed in an era of "small data" do not provide appropriate guidance for questions of informed consent, privacy protection, and accountability. This panel delves into the issues around big social data and discusses different approaches and possible solutions.</p>

<b>Participants:<b>
<ul>
<li><b>Mary Gray</b>, Senior Researcher at Microsoft Research, Fellow at the Berkman Center for Internet and Society and Associate Professor of the Media School at Indiana University.</li>
<li><b>Jeff Hancock</b>, Professor, Department of Communication, Stanford University</li>
</ul>

<h3>Politics and News in a Networked Age</h3>

<p>Social media has changed how news and political information circulates. People spread articles and activist alerts across platforms like Facebook and Twitter, but this includes "fake news", misinformation, and even disinformation. While political campaigns can use Big Data to target potential voters and spread their messaging, they must confront the ability of participatory media to amplify rumors and conspiracy theories. In this panel, participants share their expertise in journalism, activism, and politics, and discuss the challenges that new forms of media bring to information in a digital age. </p>

<b>Participants:<b>

<ul>
<li><b>Thomas Pitfield</b>, President, Data Sciences; former Chief Digital Strategist for Trudeau campaign</li>
<li><b>Kate Starbird</b>, Assistant Professor, Department of Human Centered Design & Engineering, University of Washington</li>
<li><b>Craig Silverman</b>, Media Editor, Buzzfeed; author of <i>Regret the Error: How Media Mistakes Pollute the Press and Imperil Free Speech</i></li>
</ul>

<?php include("../../footer.php"); ?>?