<?php $section = "Program"; $subsection = "Speakers"; include("../../header.php"); ?>
<br>
  <h2 class="pageTitle">Keynote Speakers</h2>

<ul>
       <!--<li class="">
          <a name="speaker3"></a>
          <img src="/2016/images/speakers/kate_crawford.jpg" class="project-thumb" alt="">
          <h3 class="">Kate Crawford</h3>
          <p>Principal Researcher  <br>  Microsoft Research New York City</p>
          <p><b>Talk title: From Human Subjects to Data Subjects</b></p>
          <p><b><a href="http://www.katecrawford.net/index.html" target="_blank">Kate Crawford</a></b> is a Principal Researcher at Microsoft Research NYC, a Visiting Professor at MIT's Center for Civic Media, and a Senior Fellow at NYU's Information Law Institute. She is widely published on the social and political implications of large-scale data systems and she co-edited a special issue in IJOC on interdisciplinary responses to data science. She is on the World Economic Forum's Council on Data for Development, and is a member of the UN's new Thematic Network on Data. She's also a co-director of the NSF's Council for Big Data, Ethics & Society. She is currently completing a new book on data and ethics forthcoming with Yale University Press.</p>
        </li>-->



        <li class="">
          <a name="speaker1"></a>
          <img src="/2017/images/speakers/matt-salganik-thumb.png" class="project-thumb" alt="">
          <h3 class="">Matt Salganik</h3>
          <p>Professor, Department of Sociology
            <br>
            Princeton University</p>
   <p><b>Talk title: Beyond Big Data</b></p>
<p>The digital age has transformed how we are able to study social behavior. Unfortunately, researchers have not yet taken full advantage of these opportunities because we are too focused on "big data," such as digital traces of behavior. These big data can be wonderful for some research questions, but they have fundamental limitations for addressing many questions because they were never designed to be used for research. This talk will argue that rather than focusing on "found data", researchers should use the capabilities of the digital age to create new forms of "designed data." This talk is based on my forthcoming book -- Bit by Bit: Social Research in the Digital Age -- which is currently in Open Review at <a href="http://www.bitbybitbook.com" target="_blank">http://www.bitbybitbook.com</a>.</p>
            <p><b><a href="http://www.princeton.edu/~mjs3/" target="_blank">Matthew Salganik</a></b> is Professor of Sociology at Princeton University, and he is affiliated with several of Princeton's interdisciplinary research centers: the Office for Population Research, the Center for Information Technology Policy, the Center for Health and Wellbeing, and the Center for Statistics and Machine Learning. His research interests include social networks and computational social science. He is the author of the forthcoming book <a href="http://www.bitbybitbook.com/" target="_blank">Bit by Bit: Social Research in the Digital Age</a>.</p>

            <p>Salganik's research has been published in journals such as Science, PNAS, Sociological Methodology, and Journal of the American Statistical Association. His papers have won the Outstanding Article Award from the Mathematical Sociology Section of the American Sociological Association and the Outstanding Statistical Application Award from the American Statistical Association. Popular accounts of his work have appeared in the New York Times, Wall Street Journal, Economist, and New Yorker. Salganik's research is funded by the National Science Foundation, National Institutes of Health, Joint United Nations Program for HIV/AIDS (UNAIDS), Facebook, and Google. During sabbaticals from Princeton, he has been a Visiting Professor at Cornell Tech and a Senior Research at Microsoft Research.</p>

        </li>


        <li class="">
          <a name="speaker2"></a>
          <img src="/2017/images/speakers/lorrie-cranor-thumb.png" class="project-thumb" alt="">
          <h3 class="">Lorrie Cranor</h3>
          <p>Professor, <br>Carnegie Mellon University<br></p>
          <p><b>Talk title: The Art of Privacy</b></p>
          <p>In this talk I will explore privacy through art. I will show examples of privacy-related artwork created by myself and by other artists, as well as common privacy visualizations. Then I will discuss the Privacy Illustrated project, in which we invite everyday people to draw pictures of privacy and what it means to them. The material we have collected from children and adults is illustrative of American views on privacy over the past few years – including contributors' thoughts on physical privacy, information privacy, surveillance, privacy on social networks, and more. It offers insights into how people conceptualize privacy, and suggests symbols that convey privacy concepts. </p>
          <p><b><a href="http://lorrie.cranor.org/" target="_blank">Lorrie Cranor</a></b> is a Professor of Computer Science and of Engineering and Public Policy at Carnegie Mellon University where she is director of the CyLab Usable Privacy and Security Laboratory (CUPS) and co-director of the MSIT-Privacy Engineering masters program. In 2016 she served as Chief Technologist at the US Federal Trade Commission. She is also a co-founder of Wombat Security Technologies, Inc, a security awareness training company. She was named an ACM Fellow for her contributions to usable privacy and security research and education, and an IEEE Fellow for her contributions to privacy engineering.</p>
        </li>
  
<li class="">
          <a name="speaker3"></a>
          <img src="/2017/images/speakers/hilary-mason-thumb.png" class="project-thumb" alt="">
          <h3 class="">Hilary Mason</h3>
          <p>Founder & CEO of Fast Forward Labs<br>Data Scientist in Residence at Accel Partners</p>
          <!-- <p><b>Talk title: Algorithmic Fairness: From social good to mathematical framework</b></p>
          <p>Machine learning has taken over our world, in more ways than we realize. You might get book recommendations, or an efficient route to your destination, or even a winning strategy for a game of Go. But you might also be admitted to college, granted a loan, or hired for a job based on algorithmically enhanced decision-making. We believe machines are neutral arbiters: cold, calculating entities that always make the right decision, that can see patterns that our human minds can't or won't. But are they? Or is decision-making-by-algorithm a way to amplify, extend and make inscrutable the biases and discrimination that is prevalent in society? To answer these questions, we need to go back &mdash; all the way to the original ideas of justice and fairness in society. We also need to go forward &mdash; towards a mathematical framework for talking about justice and fairness in machine learning. Dr. Venkatasubramanian will talk about the growing landscape of research in algorithmic fairness: how we can reason systematically about biases in algorithms, and how we can make our algorithms fair(er).</p> -->
          <p><b><a href="https://hilarymason.com" target="_blank">Hilary Mason</a></b>  is the founder and CEO of Fast Forward Labs, a machine intelligence research company, and the Data Scientist in Residence at Accel, advising companies large and small on their data strategy. Previously, she spent four years as the Chief Scientist at bitly, leading a team that studied attention on the internet in realtime, doing a mix of research, exploration, and engineering. She co-founded HackNY, a non-profit that helps talented engineering students find their way into the startup community of creative technologists in New York City. She is a co-host DataGotham, and is a member of NYCResistor. She an advisor for several organizations, including Mortar, knod.es, collective[i], and DataKind. She is also a mentor to Betaspring, the Providence, Rhode Island-based startup accelerator, and the Harvard Business School Digital Initiative. She was a member of Mayor Bloomberg's Technology and Innovation Advisory Council, which was a fascinating way to learn how government and industry can work together. She is the recipient of multiple awards, including the TechFellows Engineering Leadership award, and was on the Forbes 40 under 40 Ones to Watch list and Crain's New York 40 under Forty list.
 </p>
        </li>
      </ul>


<?php include("../../footer.php"); ?>