<?php $section = "Program"; $subsection = "Tutorial"; include("../../header.php"); ?>
<br>  <h2 class="pageTitle">Tutorial Program</h2>
<h3>Monday, May 15</h3>
<br><br>



<h3>Full Day Tutorials (8:30am - 5:30pm)</h3>

<p><b><a href="#t1">T1: Building Your Own Online Lab with Volunteer Science</a></b><br>
Jason Radford, David Lazer, Luke Horgan</p>

<p><b><a href="#t2">T2: A Practical Introduction to Spatial Datasets and Urban Applications </a></b><br>
Bruno Goncalves, Desislava Hristova, Anastasios Noulas</p>

<h3>Half-Day Tutorials</h3>

<h4>8.30 AM - 12.30 PM</h4>
<p><b><a href="#ta3">TA3: Polarization on Social Media</a></b><br>
Kiran Garimella, Gianmarco De Francisci Morales, Michael Mathioudakis, Aristides Gionis</p>

<h4>1:30 PM - 5:30 PM </h4>
<p><b><a href="#tp4">TP4: Social Media for Health Research</a></b><br>
Ingmar Weber, Yelena Mejova, Kenneth W. Goodman</p>

<br>

<!-- <p> Additional information about the tutorials will be available soon.</p> -->

<p> Details about each tutorial are below. </p>
<hr><br>


<a name="t1"><h4><a href="https://volunteerscience.com/workshop/">T1: Building Your Own Online Lab with Volunteer Science</a></h4></a>
<p> This tutorial workshop will provide attendees with an overview of the resources in the field, provide them with access to cutting edge lab technology, and then sustained help in creating their own online studies. In the first half of our workshop, we will host presentations and discussions on performing online studies with human participants in different scientific fields. These talks will focus on the process of conducting online lab studies, including designing experiments and human-in-the-loop studies, the types of studies that can be conducted online, and how to recruit participants and validate subject behavior. </p>
<p>In the second half of the workshop, attendees will have the opportunity to work with developers and researchers to create their own experiments using Volunteer Science. Attendees will be given access to the experiment development system, system documentation, experiment templates, and testing platform. The workshop participants will be able to develop and test their own experiments or link survey-based experiments into Volunteer Science and, when possible, pilot their study. Attendees will have their own experiments/mygames pages where they can post and recruit without going through us. </p>

<b>Organizers</b>
<ul>
<li><p><b>Jason Radford</b> is a doctoral student in Sociology at the University of Chicago and Manager of the Volunteer Science website.  His research focuses on how people create social order in organizations.</p></li>
<li><p><b>David Lazer</b> is Distinguished Professor of Political Science and Computer and Information Science, Northeastern University, and Co-Director, NULab for Texts, Maps, and Networks. His research focuses on the nexus of network science, computational social science, and collaborative intelligence. He is the founder of the citizen science website Volunteer Science. </p></li>
<li><p><b>Luke Horgan</b> is an undergraduate student in Computer Science at Northeastern University. He has developed experiments and built tools for computational social science on Volunteer Science for two years.</p></li>
</ul>
<hr><br>


<a name="t2"><a href="http://www.bgoncalves.com/icwsm17tutorial"><h4>T2: A Practical Introduction to Spatial Datasets and Urban Applications</h4></a>

<p>The proliferation of the mobile web and the availability of large scale digital datasets has enabled a new wave of research studies that are largely driven by these new types of data generated in urban environments. This tutorial aims to offer an overview of the opportunities and challenges posed by geolocated datasets with a particular emphasis on their use for the study of urban data science. We will provide an extensive overview of some of the theory underlying the study of urban systems followed by a practical introduction on how to use several different datasets and APIs in the second part of the day. Inspired by a fusion of computational approaches and complex systems, this tutorial will integrate elements from geography, computer science, urban studies, sociology, physics and complex systems. This will involve the description of methodologies for the collection of geo-referenced and spatial datasets, techniques for the analysis and modeling of geographic data and mobility, network science as a tool to understand cities, machine learning as a medium to solve optimization problems and define prediction tasks in urban environments, and finally, ways to visualize raw datasets and corresponding outputs on maps.</p>

<b>Organizers</b>
<ul>

<li><p><b>Bruno Goncalves</b> is a tenured faculty member at Aix-Marseille University that is currently on leave of absence at the NYU Center for Data Science. He has a strong expertise in using large scale datasets for the analysis of human behavior in time and space.</p></li>
<li><p><b>Desislava Hristova</b> is a data scientist at the Future Cities Catapult, a government-funded urban innovation centre in London. Prior to that, she spent time at Bell Labs in Cambridge, UK and the United Nations in New York, whereas she has obtained her PhD in Computer Science from the University of Cambridge.</p></li>
<li><p><b>Anastasios Noulas</b> is a Lecturer at Lancaster University, where he leads projects on location-based technologies, mobile computing and complex social and technological systems. Anastasios completed his PhD at the Computer Laboratory in the University of Cambridge. Anastasios was also a Data Scientist at Foursquare Labs in New York and Telefonica Research.</p></li>
</ul>

<hr><br>


<a name="ta3"><h4><a href="https://users.ics.aalto.fi/kiran/controversy-tutorial/">TA3: Polarization on Social Media</a></h4></a>

<p>This tutorial presents a systematic review of the study of polarization as manifested online, in particular on social media. In the first part, we clarify the concept of polarization, and the related nomenclature, by drawing from the social and political sciences. Then, we focus on the understanding of polarization as manifested online. In the second part, we review techniques for detection, quantification, and mitigation of polarization. Specifically, we show how content and network structure of social media can be used to these ends. We conclude the tutorial by presenting open challenges and promising research directions for researchers interested in this area.</p>

<b>Organizers</b>
<ul>
<li><p><b>Kiran Garimella</b> is a PhD student at Aalto University. His research focuses on identifying and combating filter bubbles on social media. Previously he worked as a Research Engineer at Yahoo Research, QCRI and as an Research Intern at LinkedIn and Amazon.</p>
<li><p><b>Gianmarco De Francisci Morales</b> is a Scientist at QCRI. Previously he worked as a Visiting Scientist at Aalto University, as a Research Scientist at Yahoo Labs Barcelona, and as a Research Associate at ISTI-CNR in Pisa. His research focuses on scalable data mining, with an emphasis on Web mining and data-intensive scalable computing systems. He is one of the lead developers of Apache SAMOA, an open-source platform for mining big data streams. He presented a tutorial on graph centrality measures at WWW '16, and a tutorial on stream mining at BigData '14, KDD '16, CIKM '16, and AAAI ’17.</p></li>
<li><p><b>Michael Mathioudakis</b> is a Postdoctoral Researcher at Aalto University. His research focuses on the analysis of user generated content on social media, with a recent emphasis on urban computing and online polarization. At Aalto University, he organized and taught new courses on 'Modern Database Systems' and 'Social Web Mining'. He also serves as advisor to Master's students and Aalto's representative at the SoBigData EU project. Outside academia, he works as a data scientist at Helvia and Sometrik, two data analytics companies.</p></li>
<li><p><b>Aristides Gionis</b> is an associate professor at Aalto University. His research focuses on data mining and algorithmic data analysis. He is particularly interested in algorithms for graphs, social-network analysis, and algorithms for web-scale data. Since 2013 he has been leading the Data Mining Group, in the Department of Computer Science of Aalto University. Before coming to Aalto he was a senior research scientist in Yahoo! Research, and previously an Academy of Finland postdoctoral scientist in the University of Helsinki. He obtained his Ph.D. from Stanford University in 2003. He has presented several tutorials on graph mining and web mining in conferences such as WWW (2008), KDD (2013 and 2015), ECML PKDD (2008, 2013, and 2015), IJCAI (2011), as well as several summer schools.</p></li>
</ul>

<hr><br>


<a name="tp4"><h4><a href="https://sites.google.com/site/socialmediaforhealthresearch/">TP4: Social Media for Health Research</a></h4></a>
<p>In this tutorial we showcase the latest advances in the use of social media for health research.</p>
<p>The scale, reach, and real-time nature of internet allows for epidemiological studies, tracking seasonal illnesses like flu, as well as understanding the context of some conditions, such as anorexia and bulimia. Such passive monitoring allows for nowcasting disease, that is, estimating its prevalence in the moment it happens. Other efforts bring the technology back to the individual, enabling better health data gathering, and eventually feedback and intervention for behavior change and improving health.</p>
<p>Here, we address data quality issues, involving not only selection bias but also truthfulness, and show how external data ranging from public health data, to quantified self data, to electronic healthcare records can be used for validation. Further, we address a suite of interesting and important ethical issues raised by the use of social media in research, including the need and scope of informed consent, the challenges raised by database, registry and software design and use, the need for some studies to include attention to social groups to achieve community-based participation, and the many challenges to privacy and confidentiality.</p>
 
<b>Organizers</b>
<ul>
<li><p><b>Ingmar Weber</b> is a principal scientist at the Qatar Computing Research Institute. His interdisciplinary research uses large amounts of online data to study human behavior at scale. Particular topics of interest include lifestyle diseases and population health, international migration, and political polarization. He has published over 100 peer-reviewed articles and is an ACM Distinguished Speaker.</p></li>

<li><p><b>Yelena Mejova</b> is a Scientist at the Qatar Computing Research Institute. Specializing in social media analysis, her work concerns the quantification of health in social media, as well as tracking of social and political phenomena. She co-edited the book "Twitter: A Digital Socioscope". Yelena has also published widely on sentiment analysis and its application to social media and political speech.</p></li>

<li><p><b>Kenneth W. Goodman</b> is founder and director of the University of Miami Miller School of Medicine’s Institute for Bioethics and Health Policy and co-director of the university’s Ethics Programs. He is a co-founder of the North American Center for the Study of Ethics and Health Information Technology. He has widely published on health computing and ethics. He co-founded the AMIA Ethical, Legal and Social Issues Working Group of which he has been elected a Fellow.</p></li>
</p></li>
</ul>
<p></p>

<br><br>

<?php include("../../footer.php"); ?>