<?php $section = "Program"; $subsection = "Program"; include("../../header.php"); ?>

<br>
  <h2 class="pageTitle">ICWSM-17 Technical Program</h2>

<p>The ICWSM-17 technical program will be held in the Grand Salon Opera B/C on Level 4 of the Hyatt, May 16-18. Each author has been allotted 18 minutes for oral presentations, including 3 minutes for Q&amp;A.</p>

<h3>Tuesday, May 16</h3>

<h3>9:00 &ndash; 9:15 AM: Awards and Opening Remarks</h3>
<h4>Derek Ruths, General Chair, ICWSM-17</h4>

<h3>9:15 &ndash; 10:15 AM: Keynote Address: </h3>
<h4>Matt Salganik (Princeton University)</h4>
<h5>Beyond Big Data</h5>
<p>The digital age has transformed how we are able to study social behavior. Unfortunately, researchers have not yet taken full advantage of these opportunities because we are too focused on &ldquo;big data,&rdquo; such as digital traces of behavior. These big data can be wonderful for some research questions, but they have fundamental limitations for addressing many questions because they were never designed to be used for research. This talk will argue that rather than focusing on &ldquo;found data&rdquo;, researchers should use the capabilities of the digital age to create new forms of &ldquo;designed data.&rdquo; This talk is based on my forthcoming book&mdash;Bit by Bit: Social Research in the Digital Age&mdash;which is currently in Open Review at&nbsp;<a href="http://www.bitbybitbook.com">http://www.bitbybitbook.com</a>.</p>

<h3>10:15 &ndash; 10:45 AM: Coffee Break</h3>

<h3>10:45 AM &ndash; 12:35 PM: Paper Session I: Digital Currency</h3>
<p>
<b>Cold Hard E-Cash: Friends and Vendors in the Venmo Digital Payments System</b><br>
Xinyi Zhang, Shiliang Tang, Yun Zhao, Gang Wang, Haitao Zheng, Ben Y. Zhao<br><br>
<b>The Rich Get Richer? Limited Learning in Charitable Giving on donorschoose.org</b><br>
Chankyung Pak, Rick Wash<br><br>
<b>When Does More Money Work? Examining the Role of Perceived Fairness in Pay on the Performance Quality of Crowdworkers</b><br>
Teng Ye, Sangseok You, Lionel P. Robert Jr.<br><br>
<b>Echo Chambers in Investment Discussion Boards</b><br>
Shiliang Tang, Qingyun Liu, Megan McQueen, Scott Counts, Apurv Jain, Haitao Zheng, Ben Zhao<br><br>
<b>Online Popularity under Promotion: Viral Potential, Forecasting, and the Economics of Time</b><br>
Marian-Andrei Rizoiu, Lexing Xie Xie<br><br>
<b>The Misleading Narrative of the Canonical Faculty Productivity Trajectory</b><br>
Samuel Way, Allison Morgan, Aaron Clauset, Daniel Larremore</p>

<h3>12:35 &ndash; 2:00 PM: Hosted Lunch</h3>
<h4>Grand Salon Opera A</h4>

<h3>2:00 &ndash; 3:15 PM: ICWSM-17 Panel: Politics and News in a Networked Age</h3>
<h4>Panelists: Craig Silverman (Media Editor, Buzzfeed), Kate Starbird (Assistant Professor, Department of Human Centered Design & Engineering, University of Washington), and Thomas Pitfield (President, Data Sciences)</h4>

<p>Social media has changed how news and political information circulates. People spread articles and activist alerts across platforms like Facebook and Twitter, but this includes "fake news," misinformation, and even disinformation. While political campaigns can use Big Data to target potential voters and spread their messaging, they must confront the ability of participatory media to amplify rumors and conspiracy theories. In this panel, participants share their expertise in journalism, activism, and politics, and discuss the challenges that new forms of media bring to information in a digital age.</p>

<h3>3:15 &ndash; 3:45 PM: Session II: Dataset Papers</h3>
<p>
<b>Fashion Conversation Data on Instagram</b><br>
Yu-I Ha, Sejeong Kwon, Meeyoung Cha, Jungseock Joo<br><br>
<b>TokTrack: A Complete Token Provenance and Change Tracking Dataset for the English Wikipedia</b><br>
Fabian Fl&ouml;ck, Kenan Erdogan, Maribel Acosta<br><br>
<b>Data Sets: Word Embeddings Learned from Tweets and General Data</b><br>
Quanzhi Li, Sameena Shah, Xiaomo Liu, Armineh Nourbakhsh<br><br>
<b>EmojiNet: An Open Service and API for Emoji Sense Discovery</b><br>
Sanjaya Wijeratne, Lakshika Balasuriya, Amit Sheth, Derek Doran</p>

<h3>3:45 &ndash; 4:00 PM: Coffee Break</h3>

<h3>4:00 &ndash; 6:10 PM: Paper Session III: Online Communities</h3>
<p>
<b>Community Identity and User Engagement in a Multi-Community Landscape</b><br>
Justine Zhang, William L. Hamilton, Cristian Danescu-Niculescu-Mizil, Dan Jurafsky, Jure Leskovec<br><br>
<b>Why Do Men Get More Attention? Exploring Factors behind Success in an Online Design Community</b><br>
Johannes Wachs, Aniko Hannak, Andr&aacute;s V&ouml;r&ouml;s, B&aacute;lint Dar&oacute;czy<br><br>
<b>Characterizing Online Discussion Using Coarse Discourse Sequences</b><br>
Amy X. Zhang, Bryan Culbertson, Praveen Paritosh<br><br>
<b>To Thread or Not to Thread: The Impact of Conversation Threading on Online Discussion</b><br>
Pablo Arag&oacute;n, Vicen&ccedil; G&oacute;mez, Andreas Kaltenbrunner<br><br>
<b>To Help or Hinder: Real-Time Chat in Citizen Science</b><br>
Ramine Tinati, Elena Simperl, Markus Luczak-Roesch<br><br>
<b>Which Size Matters? Effects of Crowd Size on Solution Quality in Big Data Q&amp;A Communities</b><br>
Yla Tausczik, Ping Wang, Joohee Choi<br><br>
<b>Better When It Was Smaller? Community Content and Behavior after Massive Growth</b><br>
Zhiyuan Lin, Niloufar Salehi, Bowen Yao, Yiqi Chen, Michael S. Bernstein<br><br>

<h3>7:00 &ndash; 9:00 PM: ICWSM-17 Opening Reception</h3>
<h4>Place des Arts, Salon Urbain, 175 Sainte-Catherine West, Montreal</h4>

<h3>Wednesday, May 17</h3>

<h3>9:00 &ndash; 10:00 AM: Keynote Address: </h3>
<h4>Lorrie Cranor (Carnegie Mellon University)</h4>
<h5>The Art of Privacy</h5>
<p>In this talk I will explore privacy through art. I will show examples of privacy-related artwork created by myself and by other artists, as well as common privacy visualizations. Then I will discuss the Privacy Illustrated project, in which we invite everyday people to draw pictures of privacy and what it means to them.&nbsp; The material we have collected from children and adults is illustrative of American views on privacy over the past few years &ndash; including contributors&rsquo; thoughts on physical privacy, information privacy, surveillance, privacy on social networks, and more. It offers insights into how people conceptualize privacy, and suggests symbols that convey privacy concepts.</p>

<h3>10:00 &ndash; 10:35 AM: Paper Session IV: Best Paper Candidates</h3>
<p>
<b>Examining the Alternative Media Ecosystem through the Production of Alternative Narratives of Mass Shooting Events on Twitter</b><br>
Kate Starbird<br><br>
<b>Kek, Cucks, and God Emperor Trump: A Measurement Study of 4chan's Politically Incorrect Forum and Its Effects on the Web</b><br>
Gabriel Emile Hine, Jeremiah Onaolapo, Emiliano De Cristofaro, Nicolas Kourtellis, Ilias Leontiadis, Riginos Samaras, Gianluca Stringhini, Jeremy Blackburn</p>

<h3>10:35 &ndash; 11:00 AM: Coffee Break</h3>

<h3>11:00 AM&ndash; 12:30 PM: Paper Session V: News and Networks</h3>
<p>
<b>Armed Conflicts in Online News: A Multilingual Study</b><br>
Robert West, J&uuml;rgen Pfeffer<br><br>
<b>State of the Geotags: Motivations and Recent Changes</b><br>
Dan Tasse, Zichen Liu, Alex Sciuto, Jason Hong<br><br>
<b>Online Human-Bot Interactions: Detection, Estimation, and Characterization</b><br>
Onur Varol, Emilio Ferrara, Clayton A. Davis, Filippo Menczer, Alessandro Flammini<br><br>
<b>Wearing Many (Social) Hats: How Different Are Your Different Social Network Personae?</b><br>
Changtao Zhong, Hau-wen Chang, Dmytro Karamshuk, Dongwon Lee, Nishanth Sastry<br><br>
<b>How Editorial, Temporal and Social Biases Affect Online Food Popularity and Appreciation</b><br>
Markus Rokicki, Eelco Herder, Christoph Trattner</p>

<h3>12:30 &ndash; 2:00 PM: Lunch Break</h3>

<h3>2:00 &ndash; 3:00 PM: ICWSM-17 Panel: The Ethics of Social Data</h3>
<h4>Panelists: Mary Gray (Senior Researcher at Microsoft Research, Fellow at the Berkman Center for Internet and Society and Associate Professor of the Media School at Indiana University) and Jeff Hancock (Professor, Department of Communication, Stanford University)</h4>
<p>In this panel, two senior scholars discuss the ethical complexity of Big Data. While social media has provided computer scientists and social scientists alike with a rich trove of personal information, scholarly communities are grappling with how to use this data ethically. Guidelines and institutional review boards designed in an era of "small data" do not provide appropriate guidance for questions of informed consent, privacy protection, and accountability. This panel delves into the issues around big social data and discusses different approaches and possible solutions.</p>

<h3>3:00 &ndash; 4:15 PM: Paper Session VI: Biases and Abuse</h3>
<p>
<b>Who Makes Trends? Understanding Demographic Biases in Crowdsourced Recommendations</b><br>
Abhijnan Chakraborty, Johnnatan Messias, Fabricio Benevenuto, Saptarshi Ghosh, Niloy Ganguly, Krishna P. Gummadi<br><br>
<b>Identifying Effective Signals to Predict Deleted and Suspended Accounts on Twitter across Languages</b><br>
Svitlana Volkova, Eric Bell<br><br>
<b>Adaptive Spammer Detection with Sparse Group Modeling</b><br>
Liang Wu, Xia Hu, Fred Morstatter, Huan Liu<br><br>
<b>&ldquo;Be Careful; Things Can Be Worse than They Appear&rdquo;: Understanding Biased Algorithms and Users&rsquo; Behavior around Them in Rating Platforms</b><br>
Motahhare Eslami, Kristen Vaccaro, Karrie Karahalios, Kevin Hamilton</p>

<h3>4:15 &ndash; 6:00 PM: ICWSM-17 Poster/Demo Session</h3>
<h4>Grand Salon Opera A</h4>
<p>Light refreshments will be served.</p>

<h3>Posters</h3>
<p>
<b>Recognizing Pathogenic Empathy in Social Media</b><br>
Muhammad M. Abdul-Mageed, Anneke Buffone, Hao Peng, Johannes Eichstaedt, Lyle Ungar<br><br>
<b>On Quitting: Performance and Practice in Online Game Play</b><br>
Tushar Agarwal, Keith Burghardt, Kristina Lerman<br><br>
<b>QT2S: A System for Monitoring Road Traffic via Fine Grounding of Tweets</b><br>
Noora Al Emadi, Sofiane Abbar, Javier Borge-Holthoefer, Francisco Guzman, Fabrizio Sebastiani<br><br>
<b>Are There Gender Differences in Professional Self-Promotion? An Empirical Case Study of LinkedIn Profiles among Recent MBA Graduates</b><br>
Kristen M. Altenburger, Rajlakshmi De, Kaylyn Frazier, Nikolai Avteniev, Jim Hamilton<br><br>
<b>25 Tweets to Know You: A New Model to Predict Personality with Social Media</b><br>
Pierre-Hadrien Arnoux, Anbang Xu, Neil Boyette, Jalal Mahmud, Rama Akkiraju, Vibha Sinha<br><br>
<b>What Gets Media Attention and How Media Attention Evolves over Time &mdash; Large-Scale Empirical Evidence from 196 Countries</b><br>
Jisun An, Haewon Kwak<br><br>
<b>Separating the Wheat from the Chaff: Evaluating Success Determinants for Online Q&amp;A Communities</b><br>
Erik Aumayr, Conor Hayes<br><br>
<b>Identifying Leading Indicators of Product Recalls from Online Reviews using Positive Unlabeled Learning and Domain Adaptation</b><br>
Shreesh Kumara Bhat, Aron Culotta<br><br>
<b>Is Slacktivism Underrated? Measuring the Value of Slacktivists for Online Social Movements</b><br>
Lia Bozarth, Ceren Budak<br><br>
<b>Exploiting Contextual Information for Fine-Grained Tweet Geolocation</b><br>
Wen-Haw Chong, Ee-Peng Lim<br><br>
<b>Nasty, Brutish, and Short: What Makes Election News Popular on Twitter?</b><br>
Sophie Chou, Deb Roy<br><br>
<b>A Motif-Based Approach for Identifying Controversy</b><br>
Mauro Coletto, Kiran Garimella, Aristides Gionis, Claudio Lucchese<br><br>
<b>&lsquo;Just the Facts&rsquo;: Exploring the Relationship between Emotional Language and Member Satisfaction in Enterprise Online Communities</b><br>
Ryan James Compton, Jilin Chen, Eben Haber, Hernan Badenes, Steve Whittaker<br><br>
<b>Suitable for All Ages: Using Reviews to Determine Appropriateness of Products</b><br>
Elizabeth M. Daly, Oznur Alkan, Michael Muller<br><br>
<b>How Fast Will You Get a Response? Predicting Interval Time for Reciprocal Link Creation</b><br>
Vachik S. Dave, Mohammad Al Hasan, Chandan K. Reddy<br><br>
<b>Automated Hate Speech Detection and the Problem of Offensive Language</b><br>
Thomas Davidson, Dana Warmsley, Michael Macy, Ingmar Weber<br><br>
<b>A Gendered Analysis of Leadership in Enterprise Social Networks</b><br>
Giorgia Di Tommaso, Giovanni Stilo, Paola Velardi<br><br>
<b>Dynamics of Content Quality in Collaborative Knowledge Production</b><br>
Emilio Ferrara, Nazanin Alipoufard, Keith Burghardt, Chiranth Gopal, Kristina Lerman<br><br>
<b>The Ebb and Flow of Controversial Debates on Social Media</b><br>
Kiran Garimella, Gianmarco De Francisci Morales, Aristides Gionis, Michael Mathioudakis<br><br>
<b>A Long-Term Analysis of Polarization on Twitter</b><br>
Venkata Rama Kiran Garimella, Ingmar Weber<br><br>
<b>Spam Users Identification in Wikipedia via Editing Behavior</b><br>
Thomas Green, Francesca Spezzano<br><br>
<b>Antagonism Also Flows through Retweets: The Impact of Out-of-Context Broadcasts in Opinion Polarization Analysis</b><br>
Pedro Calais Guerra, Roberto Nalon, Renato Assun&ccedil;&atilde;o, Wagner Meira Jr.<br><br>
<b>Loyalty in Online Communities</b><br>
William L Hamilton, Justine Zhang, Cristian Danescu-Niculescu-Mizil, Dan Jurafsky, Jure Leskovec<br><br>
<b>&ldquo;Voters of the Year&rdquo;: 19 Voters Who Were Unintentional Election Poll Sensors on Twitter</b><br>
William Hobbs, Lisa Friedland, Kenneth Joseph, Oren Tsur, Stefan Wojcik, David Lazer<br><br>
<b>A World of Difference: Divergent Word Interpretations among People</b><br>
Tianran Hu, Ruihua Song, Maya Abtahian, Philip Ding, Xing Xie, Jiebo Luo<br><br>
<b>Mood Congruence or Mood Consistency? Examining Aggregated Twitter Sentiment towards Ads in 2016 Super Bowl</b><br>
Yuheng Hu, Tingting Nian, Cheng Chen<br><br>
<b>A Longitudinal Study of Topic Classification on Twitter</b><br>
Zahra Iman, Scott Sanner, Mohamed Reda Bouadjenek, Lexing Xie<br><br>
<b>Modeling of Political Discourse Framing on Twitter</b><br>
Kristen Johnson, Di Jin, Dan Goldwasser<br><br>
<b>Behavioral Analysis of Review Fraud: Linking Malicious Crowdsourcing to Amazon and Beyond</b><br>
Parisa Kaghazgaran, James Caverlee, Majid Alfifi<br><br>
<b>How to Manipulate Social Media: Analyzing Political Astroturfing Using Ground Truth Data from South Korea</b><br>
Franziska B. Keller, David Schoch, Sebastian Stier, JungHwan Yang<br><br>
<b>US Presidential Election: What Engaged People on Facebook</b><br>
Milad Kharratzadeh, Deniz Ustebay<br><br>
<b>Face-to-BMI: Using Computer Vision to Infer Body Mass Index on Social Media</b><br>
Enes Kocabey, Mustafa Camurcu, Ferda Ofli, Yusuf Aytar, Javier Marin, Antonio Torralba, Ingmar Weber<br><br>
<b>From Camera to Deathbed: Understanding Dangerous Selfies on Social Media</b><br>
Hemank Lamba, Varun Bharadhwaj, Mayank Vachher, Divyansh Agarwal, Megha Arora, Niharika Sachdeva, Ponnurangam Kumaraguru<br><br>
<b>Controlling for Unobserved Confounds in Classification Using Correlational Constraints</b><br>
Virgile Landeiro, Aron Culotta<br><br>
<b>Scalable News Slant Measurement Using Twitter</b><br>
Huyen Thi Thanh Le, Zubair Shafiq, Padmini Srinivasan<br><br>
<b>Understanding How People Attend to and Engage with Foreign Language Posts in Multilingual Newsfeeds</b><br>
Hajin Lim, Susan R. Fussell<br><br>
<b>Fostering User Engagement: Rhetorical Devices for Applause Generation Learnt from TED Talks</b><br>
Zhe Liu, Anbang Xu, Mengdi Zhang, Jalal Mahmud, Vibha Sinha<br><br>
<b>Distinguishing the Wood from the Trees: Contrasting Collection Methods to Understand Bias in a Longitudinal Brexit Twitter Dataset</b><br>
Clare Llewellyn, Laura Cram<br><br>
<b>Sparse Overlap Cross-Platform Recommendation via Adaptive Similarity Structure Regularization</b><br>
Hanqing Lu, Chaochao Chen, Qinyue Jiang<br><br>
<b>A Computational Approach to Perceived Trustworthiness of Airbnb Host Profiles</b><br>
Xiao Ma, Trishala Neeraj, Mor Naaman<br><br>
<b>Detecting the Hate Code on Social Media</b><br>
Rijul Magu, Kshitij Joshi, Jiebo Luo<br><br>
<b>Language Use Matters: Analysis of the Linguistic Structure of Question Texts Can Characterize Answerability in Quor</b><br>
Suman Kalyan Maity, Aman Kharb, Animesh Mukherjee<br><br>
<b>The Role of Optimal Distinctiveness and Homophily in Online Dating</b><br>
Danaja Maldeniya, Arun Varghese, Toby Stuart, Daniel Romero<br><br>
<b>Two-Phase Influence Maximization in Social Networks with Seed Nodes and Referral Incentives</b><br>
Sneha Mondal, Swapnil Dhamal, Y. Narahari<br><br>
<b>Predicting Movie Genre Preferences from Personality and Values of Social Media Users</b><br>
Md. Saddam Hossain Mukta, Euna Mehnaz Khan, Mohammed Eunus Ali, Jalal Mahmud<br><br>
<b>Automatically Identifying Good Conversations Online (Yes, They Do Exist!)</b><br>
Courtney Napoles, Aasish Pappu, Joel Tetreault<br><br>
<b>Robust Classification of Crisis-Related Data on Social Networks using Convolutional Neural Networks</b><br>
Dat Tien Nguyen, Kamela Ali Al Mannai, Shafiq Joty, Hassan Sajjad, Muhammad Imran, Prasenjit Mitra<br><br>
<b>Fertility and its Meaning: Evidence from Search Behavior</b><br>
Jussi Ojala, Emilio Zagheni, Francesco Billari, Ingmar Weber<br><br>
<b>Inverse Dynamical Inheritance in Stack Exchange Taxonomies</b><br>
C&eacute;sar A. Ojeda, Kostadin Cvejoski, Rafet Sifa, Christian Bauckhage<br><br>
<b>Changes in Social Media Behavior during Life Periods of Uncertainty</b><br>
Xinru Page, Marco Marabelli<br><br>
<b>DeepCity: A Feature Learning Framework for Mining Location Check-ins</b><br>
Jun Pang, Yang Zhang<br><br>
<b>Headlines Matter: Using Headlines to Predict the Popularity of News Articles on Twitter and Facebook</b><br>
Alicja Piotrkowicz, Vania Dimitrova, Jahna Otterbacher, Katja Markert<br><br>
<b>Ranking with Social Cues: Integrating Online Review Scores and Popularity Information</strong> Pantelis Pipergias Analytis, Alexia Delfino, Juliane K&auml;mmer, Mehdi Moussaid, Thorsten Joachims<br><br>
<b>Querying Documents Annotated by Interconnected Entities</b><br>
Shouq Sadah, Moloud Shahbazi, Vagelis Hristidis<br><br>
<b>What Comments Did I Get? How Post and Comment Characteristics Predict Interaction Satisfaction on Facebook</b><br>
Shruti Sannon, Yoon Hyung Choi, Jessie G. Taft, Natalya N. Bazarova<br><br>
<b>Towards Measuring Fine-Grained Diversity Using Social Media Photographs</b><br>
Vivek Kumar Singh, Saket Hegde, Akanksha Atrey<br><br>
<b>Measuring, Predicting and Visualizing Short-Term Change in Word Representation and Usage in VKontakte Social Network</b><br>
Ian Stewart, Dustin Arendt, Eric Bell, Svitlana Volkova<br><br>
<b>Characteristics of On-Time and Late Reward Delivery Projects</b><br>
Thanh D. Tran, Kyumin Lee<br><br>
<b>On the Interpretability of Thresholded Social Networks</b><br>
Oren Tsur, David Lazer<br><br>
<b>Mapping Twitter Conversation Landscapes</b><br>
Soroush Vosoughi, Prashanth Vijayaraghavan, Ann Yuan, Deb Roy<br><br>
<b>Designing a Social Support System for College Adjustment and Social Support</b><br>
Donghee Yvette Wohn, Mousa Ahmadi, Leiping Gong, Indraneel Kulkarni, Atisha Poojari<br><br>
<b>Early Identification of Personalized Trending Topics in Microblogging</b><br>
Liang Wu, Xia Hu, Huan Liu<br><br>
<b>Detecting Camouflaged Content Polluters</b><br>
Liang Wu, Xia Hu, Fred Morstatter, Huan Liu<br><br>
<b>A First Look at User Switching Behaviors Over Multiple Video Content Providers</b><br>
Huan Yan, Tzu-Heng Lin, Gang Wang, Yong Li, Haitao Zheng, Depeng Jin, Ben Zhao<br><br>
<b>Self-Disclosure and Channel Difference in Online Health Support Groups</b><br>
Diyi Yang, Zheng Yao, Robert Kraut<br><br>
<b>Should We Be Confident in Peer Effects Estimated from Social Network Crawls?</b><br>
Jiasen Yang, Bruno Ribeiro, Jennifer Neville<br><br>
<b>Twitter911: A Cautionary Tale</b><br>
Anis Zaman, Nabil Hossain, Henry Kautz<br><br>
<b>Event Organization 101: Understanding Latent Factors of Event Popularity</b><br>
Shuo Zhang, Qin Lv<br><br>
<b>Detecting Socio-Economic Impact of Cultural Investment through Geo-Social Network Analysis</b><br>
Xiao Zhou, Desislava Hristova, Anastasios Noulas, Cecilia Mascolo</p>

<h3>Demos</h3>
<p>
<b>Visualizing Health Awareness in the Middle East</b><br>
Matheus Lima Diniz Araujo, Yelena Mejova, Michael Aupetit, Ingmar Weber<br><br>
<b>Programming Languages in GitHub: A Visualization in Hyperbolic Plane</b><br>
Dorota Celi&#x144;ska, Eryk Kopczy&#x144;ski<br><br>
<b>CitizenHelper: A Streaming Analytics System to Mine Citizen and Web Data for Humanitarian</strong> <strong>Organizations</b><br>
Prakruthi Karuna, Mohammad Rana, Hemant Purohit<br><br>
<b>Junet: A Julia Package for Network Research</b><br>
Igor Zakhlebin&nbsp;</p>

<h3>Thursday, May 18</h3>

<h3>9:00 &ndash; 10:00 AM: Keynote Address: Hilary Mason (Fast Forward Labs)</h3>
<p>Hilary Mason is the founder and CEO of Fast Forward Labs, a machine intelligence research company, and the Data Scientist in Residence at Accel, advising companies large and small on their data strategy. Previously, she spent four years as the Chief Scientist at bitly, leading a team that studied attention on the internet in realtime, doing a mix of research, exploration, and engineering. She co-founded HackNY, a non-profit that helps talented engineering students find their way into the startup community of creative technologists in New York City. She is a co-host DataGotham, and is a member of NYCResistor. She an advisor for several organizations, including Mortar, knod.es, collective[i], and DataKind. She is also a mentor to Betaspring, the Providence, Rhode Island-based startup accelerator, and the Harvard Business School Digital Initiative. She was a member of Mayor Bloomberg's Technology and Innovation Advisory Council, which was a fascinating way to learn how government and industry can work together. She is the recipient of multiple awards, including the TechFellows Engineering Leadership award, and was on the Forbes 40 under 40 Ones to Watch list and Crain's New York 40 under Forty list.</p>

<h3>10:00 &ndash; 10:35 AM: Paper Session VII: Best Paper Candidates</h3>
<p>
<b>The Substantial Interdependence of Wikipedia and Google: A Case Study on the Relationship between Peer Production Communities and Intelligent Technologies</b><br>
Connor McMahon, Isaac Johnson, Brent Hecht<br><br>
<b>The Language of Social Support in Social Media and its Effect on Suicidal Ideation Risk</b><br>
Munmun De Choudhury, Emre Kiciman<br><br>

<h3>10:35 &ndash; 11:00 AM: Coffee Break</h3>

<h3>11:00 AM &ndash; 12:15 PM: Paper Session VIII: Wikipedia</h3>
<p>
<b>The Evolution and Consequences of Peer Producing Wikipedia's Rules</b><br>
Brian Keegan, Casey Fiesler<br><br>
<b>Analysing Timelines of National Histories across Wikipedia Editions: A Comparative Computational Approach</b><br>
Anna Samoilenko, Florian Lemmerich, Katrin Weller, Maria Zens, Markus Strohmaier<br><br>
<b>Spatio-temporal Analysis of Reverted Wikipedia Edits</b><br>
Johannes Kiesel, Martin Potthast, Matthias Hagen, Benno Stein<br><br>
<b>Shocking the Crowd: The Effect of Censorship Shocks on Chinese Wikipedia</b><br>
Ark Fangzhou Zhang, Danielle Livneh, Ceren Budak, Lionel Robert, Daniel Romero</p>

<h3>12:15 &ndash; 1:45 PM: Lunch Break</h3>
<h3>12:45 &ndash; 1:45 PM: Town Hall Meeting</h3>

<h3>1:45 &ndash; 3:10 PM: Paper Session IX: Gender and Health</h3>
<p>
<b>Changes in Social Media Affect, Disclosure, and Sociality for a Sample of Transgender Americans in 2016's Political Climate</b><br>
Oliver L. Haimson, Gillian R. Hayes<br><br>
<b>Gendered Conversation in a Social Game-Streaming Platform</b><br>
Supun Nakandala, Giovanni Luca Ciampaglia, Norman Makoto Su, Yong-Yeol Ahn<br><br>
<b>#NotOkay: Understanding Gender-Based Violence in Social Media</b><br>
Mai ElSherief, Elizabeth Belding, Dana Nguyen<br><br>
<b>The Power of the Patient Voice: Learning Indicators of Hormonal Therapy Adherence from an Online Breast Cancer Forum</b><br>
Zhijun Yin, Bradley Malin, Jeremy Warner, Pei-Yun Hsueh, Ching-Hua Chen<br><br>
<b>How User Condition Affects Community Dynamics in a Forum on Autism</b><br>
Mattia Samory, Cinzia Pizzi, Enoch Peserico</p>

<h3>3:10 &ndash; 3:40 PM: Coffee Break</h3>

<h3>3:40 &ndash; 6:05 PM: Paper Session X: Visual Culture</h3>
<p>
<b>Cultural Diffusion and Trends in Facebook Photographs</b><br>
Quanzeng You, Dar&iacute;o Garc&iacute;a-Garc&iacute;a, Manohar Paluri, Jiebo Luo, Jungseock Joo<br><br>
<b>Tracing the Use of Practices through Networks of Collaboration</b><br>
Rahmtin Rotabi, Cristian Danescu-Niculescu-Mizil, Jon Kleinberg<br><br>
<b>Selfie-Presentation in Everyday Life: A Large-Scale Characterization of Selfie Contexts on Instagram</b><br>
Julia Deeb-Swihart, Christopher Polack, Eric Gilbert, Irfan Essa<br><br>
<b>Predicting Popular and Viral Image Cascades in Pinterest</b><br>
Jinyoung Han, Daejin Choi, Jungseock Joo, Chen-Nee Chuah<br><br>
<b>Cultural Values and Cross-Cultural Video Consumption on YouTube</b><br>
Minsu Park, Jaram Park, Young Min Baek, Michael Macy<br><br>
<b>Spice Up Your Chat: The Intentions and Sentiment Effects of Using Emojis</b><br>
Tianran Hu, Han Guo, Hao Sun, Thuy-vy Thi Nguyen, Jiebo Luo<br><br>
<b>Untangling Emoji Popularity through Semantic Embeddings</b><br>
Wei Ai, Xuan Lu, Xuanzhe Liu, Ning Wang, Gang Huang, Qiaozhu Mei<br><br>
<b>Understanding Emoji Ambiguity in Context: The Role of Text in Emoji-Related Miscommunication</b><br>
Hannah Miller, Daniel Kluver, Jacob Thebault-Spieker, Loren Terveen, Brent Hecht</p>

<h3>6:05 &ndash; 6:20 PM: Closing Remarks: Derek Ruths, General Chair, ICWSM-17</h3>

<?php include("../../footer.php"); ?>?