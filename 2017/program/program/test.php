<?php $section = "Program"; $subsection = "Program"; include("../../header.php"); ?>

  <h2 class="pageTitle">Conference Program</h2>

<!--
<style type="text/css">
    
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
  /*.tg .tg-lkh3{background-color:#9aff99}*/
.tg .tg-lkh3{background-size: 100% 100%; background-image: url('../../images/colours/orange/sub-menu-background.png')}
  /*.tg .tg-fqpo{background-color:#cbcefb}*/
.tg .tg-fqpo{background-size: 100% 100%; background-image: url('../../images/sub-menu-background.png')}
  /*.tg .tg-huad{background-color:#ffccc9}*/
.tg .tg-huad{background-size: 100% 100%; background-image: url('../../images/colours/yellow/sub-menu-background.png')}
  /*.tg .tg-uhkr{background-color:#ffce93}*/
.tg .tg-uhkr{background-size: 100% 100%; background-image: url('../../images/colours/yellow/sub-menu-background.png')}
  /*.tg .tg-wsnc{background-color:#fffc9e}*/
.tg .tg-wsnc{background-size: 100% 100%; background-image: url('../../images/colours/blue/sub-menu-background.png')}
  /*.tg .tg-702o{background-color:#cdffff}*/
.tg .tg-702o{background-size: 100% 100%; background-image: url('../../images/colours/blue/sub-menu-background.png')}
.tg .tg-e3zv{font-weight:bold}

}

</style>


<table class="tg">
  <tr>
    <th class="tg-031e"></th>
    <th class="tg-e3zv">June 2nd<br>Monday</th>
    <th class="tg-e3zv">June 3rd<br>Tuesday</th>
    <th class="tg-e3zv">June 4th<br>Wednesday</th>
  </tr>
  <tr>
    <td class="tg-031e">9:00-9:15</td>
    <td class="tg-lkh3">9:00-9:15<br>Opening Remarks</td>
    <td class="tg-fqpo" rowspan="3">9:00-10:15<br><a href="#session4">Plenary IV: Brokerage and Altruism</a></td>
    <td class="tg-fqpo" rowspan="3">9:00-10:15<br><a href="#session8">Plenary VIII: Big data, big issues</a></td>
  </tr>
  <tr>
    <td class="tg-031e">9:15-10:00</td>
    <td class="tg-lkh3" rowspan="3">9:15-10:30<br><a href='../speakers/#speaker1'>Opening Keynote</a></td>
  </tr>
  <tr>
    <td class="tg-031e">10:00-10:15</td>
  </tr>
  <tr>
    <td class="tg-031e">10:15-10:30</td>
    <td class="tg-uhkr">10:15-10:30<br><a href="#light3">Lightning III</a></td>
    <td class="tg-uhkr">10:15-10:30<br><a href="#light6">Lightning VI</a></td>
  </tr>
  <tr>
    <td class="tg-031e">10:30-11:00</td>
    <td class="tg-031e">10:30-11:00<br>Coffee Break</td>
    <td class="tg-031e">10:30-11:00<br>Coffee Break</td>
    <td class="tg-031e">10:30-11:00<br>Coffee Break</td>
  </tr>
  <tr>
    <td class="tg-031e">11:00-11:40</td>
    <td class="tg-fqpo" rowspan="2">11:00-11:50<br><a href="#session1">Plenary I: Social behavior and attitudes</a></td>
    <td class="tg-fqpo" rowspan="2">11:00-11:50<br><a href="#session5">Plenary V: Revisiting networks and their fundamentals</a></td>
    <td class="tg-wsnc">11:00-11:40<br>Townhall</td>
  </tr>
  <tr>
    <td class="tg-031e">11:40-11:50</td>
    <td class="tg-fqpo" rowspan="3">11:40-12:30<br><a href="#session9">Plenary IX: Friends, Trust, and Polarization</a></td>
  </tr>
  <tr>
    <td class="tg-031e">11:50-12:00</td>
    <td class="tg-uhkr" rowspan="2">11:50-12:30<br><a href="#light1">Lightning I</a></td>
    <td class="tg-uhkr" rowspan="2">11:50-12:30<br><a href="#light4">Lightning IV</a></td>
  </tr>
  <tr>
    <td class="tg-031e">12:00-12:30</td>
  </tr>
  <tr>
    <td class="tg-031e">12:30-2:00</td>
    <td class="tg-031e">12:30-2:00<br>Lunch</td>
    <td class="tg-031e">12:30-2:00<br>Lunch</td>
    <td class="tg-031e">12:30-2:00<br>Lunch</td>
  </tr>
  <tr>
    <td class="tg-031e">2:00-3:00</td>
    <td class="tg-fqpo" rowspan="2">2:00-3:15<br><a href="#session2">Plenary II: When social meets urban</a></td>
    <td class="tg-fqpo" rowspan="2">2:00-3:15<br><a href="#session6">Plenary VI: Health and wellness</a></td>
    <td class="tg-uhkr">2:00-3:00<br><a href="#light7">Lightning VII</a></td>
  </tr>
  <tr>
    <td class="tg-031e">3:00-3:15</td>
    <td class="tg-huad" rowspan="3">3:00-4:00<br><a href="#spot3">Spotlight III</a></td>
  </tr>
  <tr>
    <td class="tg-031e">3:15-3:30</td>
    <td class="tg-uhkr">3:15-3:30<br><a href="#light2">Lightning II</td>
    <td class="tg-uhkr">3:15-3:30<br><a href="#light5">Lightning V</td>
  </tr>
  <tr>
    <td class="tg-031e">3:30-4:00</td>
    <td class="tg-huad" rowspan="2">3:30-4:15<br><a href="#spot1">Spotlight I</a></td>
    <td class="tg-huad" rowspan="2">3:30-4:15<br><a href="#spot2">Spotlight II</a></td>
  </tr>
  <tr>
    <td class="tg-031e">4:00-4:15</td>
    <td class="tg-031e" rowspan="2">4:00-4:30<br>Coffee Break</td>
  </tr>
  <tr>
    <td class="tg-031e">4:15-4:30</td>
    <td class="tg-031e" rowspan="2">4:15-4:45&nbsp;<br>Coffee Break</td>
    <td class="tg-031e" rowspan="2">4:15-4:45<br>Coffee Break</td>
  </tr>
  <tr>
    <td class="tg-031e">4:30-4:45</td>
    <td class="tg-lkh3" rowspan="2">4:30-5:45<br><a href='../speakers/#speaker2'>Closing Keynote</a></td>
  </tr>
  <tr>
    <td class="tg-031e">4:45-5:45</td>
    <td class="tg-fqpo" rowspan="2">4:45-6:00<br><a href="#session3">Plenary III: You are what you speak</a></td>
    <td class="tg-fqpo" rowspan="2">4:45-6:00<br><a href="#session7">Plenary VII: The new social spectacle</a></td>
  </tr>
  <tr>
    <td class="tg-031e">5:45-6:00</td>
    <td class="tg-lkh3">5:45-6:00<br>Closing Remarks</td>
  </tr>
  <tr>
    <td class="tg-031e">6-8pm<br></td>
    <td class="tg-702o">6:00-8:00<br>Opening Reception</td>
    <td class="tg-702o">6:00-8:00<br>Poster/Demo Session</td>
    <td class="tg-031e"></td>
  </tr>
</table>


<br><br>
<hr>
<br><br>

-->
      <h3><b>Wednesday, May 27</b></h3>

<h3>9:00 - 9:15: Awards and Opening Remarks <br> Daniele Quercia, 
	General Chair
</h3>

<h3>09:15 - 10:30:
<a href='../speakers/#speaker1'>Keynote Address: Andrew Campbell</a></h3>

<h4>10:30 - 11: Coffee Break</h4>

<a name="plenary1"></a>
<h3>11:00 - 12:30: Session I <br> News and Politics</h3>
<p>
<b>26: Fair and Balanced? Quantifying Media Bias through Crowdsourced Content Analysis</b><br>
Ceren Budak, Sharad Goel, Justin Rao
<br>
<br>
<b>27: Dissecting the Spirit of Gezi: Emergence vs. Selection in the Occupy Gezi Movement</b><br>
Ceren Budak, Duncan Watts
<br>
<br>
<b>125: Breaking the News: First Impressions Matter on Online News</b><br>
Julio Reis, Fabr�cio Benevenuto, Pedro Olmo, Raquel Prates, Haewoon Kwak, Jisun An
<br>
<br>
<b>128: Voting Behaviour and Power in Online Democracy: A Study of LiquidFeedback in Germany's Pirate Party</b><br>
Christoph Carl Kling, J�r�me Kunegis, Heinrich Hartmann, Markus Strohmaier, Steffen Staab
<br>
<br>
<b>315: Comparing Events Coverage in Online News and Social Media: The Case of Climate Change</b><br>
Alexandra Olteanu, Carlos Castillo, Nicholas Diakopoulos, Karl Aberer
<br>
<br>
<b>366: Editorial Algorithms: Using Social Media to Discover and Report Local News</b><br>
Raz Schwartz, Mor Naaman, Rannie Teodoro
<br>
<br>
<b>419: Popularity Dynamics and Intrinsic Quality in Reddit and Hacker News</b><br>
Greg Stoddard
</p>

<h4>12:30 - 2:00: Lunch Break</h4>

<a name="plenary2"></a>
<h3>2:00 � 3:30: Session II <br> User Engagement</h3>
<p>
<b>2: Understanding Engagement and Willingness to Speak Up in Social-Television: A Full-Season, Cross-Genre Analysis of TV Audience Participation on Twitter</b><br>
Fabio Giglietto, Giovanni Boccia Artieri, Laura Gemini, Mario Orefice
<br>
<br>
<b>12: Modeling Response Time in Digital Human Communication</b><br>
Nicholas Navaroli, Padhraic Smyth
<br>
<br>
<b>31: Predicting User Engagement on Twitter with Real-World Events</b><br>
Yuheng Hu, Shelly Farnham, Kartik Talamadupula
<br>
<br>
155: Characterizing Silent Users in Social Media Communities</b><br>
Wei Gong, Ee-Peng Lim, Feida Zhu
<br>
<br>
<b>195: Emotions and Social Movement Engagement on Twitter</b><br>
Amanda Jean Stevenson
<br>
<br>
<b>297: Why We Filter Our Photos and How It Impacts Engagement</b><br>
Saeideh Bakhshi, David A. Shamma, Lyndon Kennedy, Eric Gilbert
<br>
<br>
<b>298: Coordination and Efficiency in Decentralized Collaboration</b><br>
Daniel M. Romero, Dan Huttenlocher, Jon Kleinberg
</p>


<h4>3:30 - 4:00: Coffee Break</h4>


<a name="plenary3"></a>
<h3>4:00 � 5:15: Session III <br> Networks</h3>
<p>


<b>117: User Effort and Network Structure Mediate Access to Information in Networks</b><br>
Jeon-Hyung Kang, Kristina Lerman
<br>
<br>
<b>204: Patterns in Interactive Tagging Networks</b><br>
Yuto Yamaguchi, Mitsuo Yoshida, Christos Faloutsos, Hiroyuki Kitagawa
<br>
<br>
<b>280: DUKE: A Solution for Discovering Neighborhood Patterns in Ego Networks</b><br>
Syed Agha Muhammad, Kristof Van Laerhoven
<br>
<br>
<b>333: Multilayer Brokerage in Geo-Social Networks</b><br>
Desislava Hristova, Pietro Panzarasa, Cecilia Mascolo
<br>
<br>
<b>425: Impact of Entity Disambiguation Errors on Social Network Properties</b><br>
Jana Diesner, Craig S. Evans, Jinseok Kim
<br>
<br>
<b>541: WhichStreams: A Dynamic Approach for Focused Data Capture from Large Social Media</b><br>
Thibault Gisselbrecht, Ludovic Denoyer, Patrick Gallinari, Sylvain Lamprier


</p>

<h3>6:00 � 8:00 PM: Opening Reception, Britain Williams Room, Somerville College</h3>


<br><br>
<hr>
<br><br>
<h3><b>Thursday, May 28</b></h3>

<a name="plenary4"></a>
<h3>9:00 � 10:30: Session IV <br> Space and Place </h3>
<p>

<b>35: Towards Lifestyle Understanding: Predicting Home and Vacation Locations from User�s Online Photo Collections</b><br>
Danning Zheng, Tianran Hu, Quanzeng You, Henry Kautz, Jiebo Luo
<br>
<br>
<b>275: Taxonomy-Based Discovery and Annotation of Functional Areas in the City</b><br>
Carmen Vaca, Daniele Quercia, Francesco Bonchi, Piero Fraternali
<br>
<br>
<b>276: Like Partying? Your Face Says It All. Predicting the Ambiance of Places with Profile Pictures</b><br>
Miriam Redi, Daniele Quercia, Lindsay T. Graham, Samuel D. Gosling
<br>
<br>
<b>281: Where is the Soho of Rome? Measures and Algorithms for Finding Similar Neighborhoods in Cities</b><br>
G�raud Le Falher, Aristides Gionis, Michael Mathioudakis
<br>
<br>
<b>320: Smelly Maps: The Digital Life of Urban Smellscapes</b><br>
Daniele Quercia, Rossano Schifanella, Luca Maria Aiello, Kate McLean
<br>
<br>
<b>388: Geolocation Prediction in Twitter Using Social Networks: A Critical Analysis and Review of Current Practice</b><br>
David Jurgens, Tyler Finnethy, James McCorriston, Yi Tian Xu, Derek Ruths
<br>
<br>
<b>404: Where in the %#&! Are You? Twitter, Place, and the Nuances of Knowing with Big Data</b><br>
Darren Stevenson

</p>

<h4>10:30 - 11:00: Coffee Break</h4>

<a name="plenary5"></a>
<h3>11:00 � 12:30: Session V <br> Communities, Personality, and Taste </h3>
<p>

<b>22: Making Use of Derived Personality: The Case of Social Media Ad Targeting</b><br>
Jilin Chen, Eben Haber, Ruogu Kang, Gary Hsieh, Jalal Mahmud
<br>
<br>
<b>34: Understanding Musical Diversity via Online Social Media</b><br>
Minsu Park, Ingmar Weber, Mor Naaman, Sarah Vieweg
<br>
<br>
<b>62: Inferring Sentiment from Web Images with Joint Inference on Visual and Social Cues: A Regulated Matrix Factorization Approach</b><br>
Yilin Wang, Yuheng Hu, Subbarao Kambhampati, Baoxin Li
<br>
<br>
<b>74: It's a Man's Wikipedia? Assessing Gender Inequality in an Online Encyclopedia</b><br>
Claudia Wagner, David Garcia, Mohsen Jadidi, Markus Strohmaier
<br>
<br>
<b>76: Conducting Massively Open Online Social Experiments with Volunteer Science</b><br>
Jason Radford, Brian Keegan, Jefferson Hoye, Ceyhun Karbeyaz, Katherine Ognyanova, Brooke Foucault Welles, Waleed Meleis, David Lazer
<br>
<br>
<b>202: Self-Characterized Illness Phase and Information Needs of Participants in an Online Cancer Forum</b><br>
Jordan Eschler, Zakariya Dehlawi, Wanda Pratt
<br>
<br>
<b>370: Antisocial Behavior in Online Discussion Communities</b><br>
Justin Cheng, Cristian Danescu-Niculescu-Mizil, Jure Leskovec

</p>


<h4>12:30 - 2:00: Lunch Break</h4>

<a name="plenary6"></a>
<h3>2:00 � 3:30: Session VI <br> Topics and Language </h3>
<p>

<b>68: Hierarchical Estimation Framework of Multi-Label Classifying: A Case of Tweets Classifying into Real Life Aspects</b><br>
Shuhei Yamamoto, Tetsuji Satoh
<br>
<br>
<b>78: Predicting Speech Acts in MOOC Forum Posts</b><br>
Jaime Arguello, Kyle Shaffer
<br>
<br>
<b>85: Linguistic Bias in Collaboratively Produced Biographies: Crowdsourcing Social Stereotypes?</b><br>
Jahna Otterbacher
<br>
<br>
<b>336: On Analyzing Hashtags in Twitter</b><br>
Paolo Ferragina, Francesco Piccinno, Roberto Santoro
<br>
<br>
<b>436: Don't Let Me Be #Misunderstood: Linguistically Motivated Algorithm for Predicting the Popularity of Textual Memes</b><br>
Oren Tsur, Ari Rappoport
<br>
<br>
<b>442: Values in Words: Using Language to Evaluate and Understand Personal Values</b><br>
Ryan L. Boyd, Steven R. Wilson, James W. Pennebaker, Michal Kosinski, David J. Stillwell, Rada Mihalcea
<br>
<br>
<b>487: Analysis and Prediction of Question Topic Popularity in Community Q&A Sites: A Case Study of Quora</b><br>
Suman Kalyan Maity, Jot Sarup Singh Sahni, Animesh Mukherjee


</p>

<h4>3:30 - 4:00: Coffee Break</h4>

<a name="plenary7"></a>
<h3>4:00 � 4:45: Session VII <br> Event Detection </h3>
<p>

<b>137: A Bayesian Graphical Model to Discover Latent Events from Twitter</b><br>
Wei Wei, Kenneth Joseph, Wei Lo, Kathleen M. Carley
<br>
<br>
<b>237: SEEFT: Planned Social Event Discovery and Attribute Extraction by Fusing Twitter and Web Content</b><br>
Yu Wang, David Fink, Eugene Agichtein
<br>
<br>
<b>257: Degeneracy-Based Real-Time Sub-Event Detection in Twitter Stream</b><br>
Polykarpos Meladianos, Giannis Nikolentzos, Fran�ois Rousseau, Yannis Stavrakas, Michalis Vazirgiannis


</p>


<h3>4:45 - 5:30: Town Hall Meeting</h3>

<h3>6:00 - 8:00PM: ICWSM-15 Poster/Demo Session</h3>

<br><br>
<hr>
<br><br>


<h3><b>Friday, May 29</b></h3>

<h3>9:00 - 10:30: 
<a href='../speakers/#speaker2'>Keynote Address: Usman Haque</a></h3>
</h3>

<h4>10:30 - 11:00: Coffee Break</h4>

<a name="plenary8"></a>
<h3>11:00 � 12:30: Session VIII <br> Popularity and Sociality </h3>
<p>
<b>30: How Viral are Viral Videos?</b><br>
Christian Bauckhage, Fabian Hadiji, Kristian Kersting
<br>
<br>
<b>166: Are People Really Social on Porn 2.0?</b><br>
Gareth Tyson, Yehia Elkhatib, Nishanth Sastry, Steve Uhlig
<br>
<br>
<b>176: An Image is Worth More than a Thousand Favorites: Surfacing the Hidden Beauty of Flickr Pictures</b><br>
Rossano Schifanella, Miriam Redi, Luca Maria Aiello
<br>
<br>
<b>179: Characterizing Information Diets of Social Media Users</b><br>
Juhi Kulshrestha, Muhammad Bilal Zafar, Lisette Esp�n Noboa, Krishna P. Gummadi, Saptarshi Ghosh
<br>
<br>
<b>180: The Lifecyle of a Youtube Video: Phases, Content and Popularity</b><br>
Honglin Yu, Lexing Xie, Scott Sanner
<br>
<br>
<b>385: The International Affiliation Network of YouTube Trends</b><br>
Edward L. Platt, Rahul Bhargava, Ethan Zuckerman
<br>
<br>
<b>446: Audience Analysis for Competing Memes in Social Media</b><br>
Samuel Carton, Souneil Park, Nicole Zeffer, Eytan Adar, Qiaozhu Mei, Paul Resnick
</p>


<h4>12:30 - 2:00: Lunch Break</h4>

<a name="plenary9"></a>
<h3>2:00 � 3:30: Session IX <br> Quality, Crowds, and Money </h3>
<p>
<b>91: Leveraging Product Adopter Information from Online Reviews for Product Recommendation</b><br>
Jinpeng Wang, Wayne Xin Zhao, Yulan He, Xiaoming Li
<br>
<br>
<b>116: Understanding Lending Behaviors on Online Microlending Platforms: The Case for Kiva</b><br>
Gaurav Paruthi, Enrique Frias-Martinez, Vanessa Frias-Martinez
<br>
<br>
<b>132: Analyzing and Modeling Special Offer Campaigns in Location Based Social Networks</b><br>
Ke Zhang, Konstantinos Pelechrinis, Theodoros Lappas
<br>
<br>
<b>185: Project Recommendation Using Heterogeneous Traits in Crowdfunding</b><br>
Vineeth Rakesh, Jaegul Choo, Chandan K. Reddy
<br>
<br>
<b>304: Crowds, Gigs, and Super Sellers: A Measurement Study of a Supply-Driven Crowdsourcing Marketplace</b><br>
Hancheng Ge, James Caverlee, Kyumin Lee
<br>
<br>
<b>398: CREDBANK: A Large-Scale Social Media Corpus with Associated Credibility Annotations</b><br>
Tanushree Mitra, Eric Gilbert
<br>
<br>
<b>438: Misalignment between Supply and Demand of Quality Content in Peer Production Communities</b><br>
Morten Warncke-Wang, Vivek Ranjan, Loren Terveen, Brent Hecht

</p>

<h4>3:30 - 4:00: Coffee Break</h4>


<a name="plenary10"></a>
<h3>4:00 � 5:15: Session X <br> Privacy and Censorship </h3>
<p>

<b>144: Predicting Privacy Behavior on Online Social Networks</b><br>
Cailing Dong, Hongxia Jin, Bart P. Knijnenburg
<br>
<br>
<b>289: Privacy and the City: User Identification and Location Semantics in Location-Based Social Networks</b><br>
Luca Rossi, Matthew J. Williams, Christoph Stich, Mirco Musolesi
<br>
<br>
<b>290: On the k-Anonymization of Time-Varying and Multi-Layer Social Graphs</b><br>
Luca Rossi, Mirco Musolesi, Andrea Torsello
<br>
<br>
<b>314: The Many Shades of Anonymity: Characterizing Anonymous Social Media Content</b><br>
Denzil Correa, Leandro Ara�jo Silva, Mainack Mondal, Fabr�cio Benevenuto, Krishna P. Gummadi
<br>
<br>
<b>396: Algorithmically Bypassing Censorship on Sina Weibo with Nondeterministic Homophone Substitutions</b><br>
Chaya Hiruncharoenvate, Zhiyuan Lin, Eric Gilbert
<br>
<br>
<b>537: �I Didn�t Sign Up for This!�: Informed Consent in Social Network Research</b><br>
Luke Hutton, Tristan Henderson

</p>


<h3>5:15 - 5:30: Closing Remarks <br> Daniele Quercia, 
	General Chair
</h3>

<?php include("../../footer.php"); ?>?