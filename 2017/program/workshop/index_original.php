<?php $section = "Program"; $subsection = "Workshop"; include("../../header.php"); ?>
  <h2 class="pageTitle">Workshop Program</h2>
     <h4>ALL WORKSHOPS WILL BE HELD ON MAY 26th</h4>
  
<p>There are seven workshops at ICWSM-15:<br>
<br>
<b>Full Day Workshops</b><br>
9:00 AM - 5:00 PM<br>
W2: Digital Placemaking: Augmenting Physical Places with Contextual Social Data<br>
Germaine Halegoua, Raz Schwartz, and Ed Manley<br>
Room C4, Maths Institute<br>
Maps and Directions: <a href = "https://www.maths.ox.ac.uk/about-us/contact-us">https://www.maths.ox.ac.uk/about-us/contact-us</a><br>
<br>
9:00 AM - 5:30 PM<br>
W4: Religion on Social Media<br>
Yelena Mejova, Ingmar Weber, Lu Chen, and Adam Okulicz-Kozaryn<br>
Room C6, Maths Institute<br>
Maps and Directions: <a href = "https://www.maths.ox.ac.uk/about-us/contact-us">https://www.maths.ox.ac.uk/about-us/contact-us</a><br>
<br>
9:00 AM - 5:00 PM<br>
W6: Standards and Practices in Large-Scale Social Media Research<br>
Derek Ruths and Juergen Pfeffer<br>
New Council Room, Somerville College<br>
Maps and Directions: <a href = "http://www.some.ox.ac.uk/268/all/1/Maps_and_directions.aspx">http://www.some.ox.ac.uk/268/all/1/Maps_and_directions.aspx</a><br>
<br>
8:45 AM - 5:00 PM<br>
W7: Wikipedia, a Social Pedia: Research Challenges and Opportunities<br>
Robert West, Leila Zia, and Jure Leskovec<br>
Syndicate Room, St. Antony's College<br>
Maps and Directions: <a href = "http://www.sant.ox.ac.uk/about-st-antonys/directions-college">http://www.sant.ox.ac.uk/about-st-antonys/directions-college</a><br>
<br>
<b>Half Day Workshops</b><br>
8:50 AM - 1:00 PM<br>
W3: Modeling and Mining Temporal Interactions<br>
Bruno Gon�alves, M�rton Karsai and Nicola Perra<br>
Room L6, Maths Institute<br>
Maps and Directions: <a href = "https://www.maths.ox.ac.uk/about-us/contact-us">https://www.maths.ox.ac.uk/about-us/contact-us</a><br>
<br>
1:00 PM - 5:00 PM<br>
W1: Auditing Algorithms From the Outside: Methods and Implications<br>
Mike Ananny, Karrie Karahalios, Christian Sandvig and Christo Wilson<br>
Deakin Room, St. Antony's College<br>
Maps and Directions: <a href = "http://www.sant.ox.ac.uk/about-st-antonys/directions-college">http://www.sant.ox.ac.uk/about-st-antonys/directions-college</a><br>
<br>
<b>Evening Workshop</b><br>
6:30 PM - 10:00 PM<br>
W5: The ICWSM Science Slam<br>
David Garcia, Ingmar Weber and Aniko Hannak<br>
Oxford Old Fire Station Loft, 40 George Street<br>
Maps and Directions: <a href = "http://www.oldfirestation.org.uk/about-us/visit-us/">http://www.oldfirestation.org.uk/about-us/visit-us/</a>
</p>

<p> The details of each workshop could be found below: </p>

<h4><a href="https://auditingalgorithms.wordpress.com">Auditing Algorithms From the Outside: Methods and Implications</a></h4>

<p> An emerging area of research we call �algorithm auditing� allows researchers, designers, and users new ways to understand the algorithms that increasingly shape our online life. This research investigates algorithms �from the outside,� testing them for problems and harms without the cooperation of online platform providers. So far, researchers have investigated the systems that handle recommendations, prices, news, commenting, and search, examining them for individually and societally undesirable consequences such as racism or fraud. In this workshop we treat this new area as a development of the traditional social science technique�the audit study�that creates new ways of investigating the function and relevance of algorithms. Through a cumulative set of activities designed by a group of multidisciplinary organizers, participants will create, document, trick, interrogate, and propose revisions to social media and other algorithms, leaving the attendees with an appreciation of both the ethical and methodological challenges of studying networked information algorithms, as well as a chance for the development of new research designs and agendas to guide future work in this area.</p>

<b>Organizers</b>

<ul>
<li>Mike Ananny</li>
<li>Karrie Karahalios</li>
<li>Christian Sandvig</li>
<li>Christo Wilson</li>
</ul>

<p></p>


<h4><a href="http://digitalplacemaking.apps-1and1.com/">Digital Placemaking: Augmenting Physical Places with Contextual Social Data</a></h4>

<p>People have embraced social media as a means to express their experiences within and knowledge about particular places, and researchers have continued to analyze these digital traces in order to better understand social activities within particular places. Geo-tagged social media data such as photos, tweets, check-ins, audio, video, and status updates have proliferated and reveal individual and collective senses of place and local insights into interactions between people and place (Schwartz, 2014). However, these digital traces alone cannot reveal a holistic sense of place and placemaking. One of the key goals of the workshop is to create a hands on experience and encourage participants from a variety of backgrounds to discover, share, and interact with experimental techniques of data gathering and urban sensing.</p>


<b>Organizers</b>

<ul>
<li>Germaine Halegoua</li>
<li>Raz Schwartz</li>
<li>Ed Manley</li>
</ul>

<p></p>


<h4><a href="http://m2ti.weebly.com/">Modeling and Mining Temporal Interactions</a></h4>

<p>The scope of the M2TI workshop is to foster the interaction between different scientific communities actively working in mining, analyzing, and modeling datasets that capture the dynamical nature of human interactions. The workshop aims at creating a fertile ground where to discuss a wide variety of approaches used to study the temporal evolution of online and offline social systems and user behavior. In particular, the workshop will stimulate the crosspollination between novel data mining techniques, used to effectively collect, represent, and explore large-scale time-resolved datasets, and new modeling approaches, aimed at understanding and reproducing their relevant features. The end result will be not only a view of the state of the art in this important and growing field but also a common framework to help shape its future development.</p>


<b>Organizers</b>

<ul>
<li>Bruno Gon�alves</li>
<li>M�rton Karsai</li>
<li>Nicola Perra</li>
</ul>

<p></p>

<h4><a href="https://sites.google.com/site/religiononsocialmedia/">Religion on Social Media</a></h4>

<p>Religion is one of the major forces in both personal and social life of a great number of people across the world. In a 2012 Gallup study, 59% of the world said that they think of themselves as religious. As any cultural force of this scale, it has found expression in various forms on social media. In a time of growing communication between cultural and religious locales, social media has played an important role in religious expression, interfaith discussion, and contextualization of current events. Online religious communities reflect established cultural norms and organizations, while simultaneously presenting a new venue for religious expression and networking. The aim of this workshop is to outline the research questions, methodologies, and collaborations to foster the use of computational methods -- especially applied to social media -- to support the study of religious and cultural phenomena.</p>

<b>Organizers</b>

<ul>
<li>Yelena Mejova</li>
<li>Ingmar Weber</li>
<li>Lu Chen</li>
<li>Adam Okulicz-Kozaryn</li>
</ul>

<p></p>

<h4><a href="https://sites.google.com/site/icwsmscienceslam/">The ICWSM Science Slam</a></h4>

<p>A Science Slam is an epic scientific event where scientists compete with short talks on their research. It's just like a poetry slam, but with science instead of poems. Slammers are completely free to do whatever they want on stage, everything is allowed including slides, games, the more creative, the better! The only two rules are: The topic of the slam has to be related to social media and the presentation should not take more than 10 minutes</p>

<b>Organizers</b>

<ul>
<li>David Garcia</li>
<li>Ingmar Weber</li>
<li>Aniko Hannak</li>
</ul>

<p></p>

<h4><a href="http://snap.stanford.edu/wiki-icwsm15/">Wikipedia, a Social Pedia: Research Challenges and Opportunities</a></h4>

<p>Wikipedia is one of the most popular sites on the Web, a main source of knowledge for a large fraction of Internet users, and one of very few projects that make not only their content but also many activity logs available to the public. For these reasons, Wikipedia has become an important object of study for researchers across many subfields of the computational and social sciences, such as social�� network analysis, social psychology, education, anthropology, political science, human-computer interaction, cognitive science, artificial intelligence, linguistics, and natural�� language processing. The goal of this workshop is to create a forum for researchers exploring all social aspects of Wikipedia, including the creation and consumption of content, participation in discussions and their dynamics, task management and the evolution of hierarchies, finding consensus on editorial issues, etc. With a member of the Wikimedia Foundation's research team in the organizing committee and a keynote speaker from the Foundation, we aim to establish a direct exchange of ideas between the organization that operates Wikipedia and the researchers that are interested in studying it.</p>

<b>Organizers</b>

<ul>
<li>Robert West</li>
<li>Leila Zia</li>
<li>Jure Leskovec</li>
</ul>

<p></p>

<h4><a href="http://networkdynamics.org/events/icwsm2015_smsp/">Workshop on Standards and Practices in Large-Scale Social Media Research</a></h4>

<p>A number of recent papers have raised concerns about methodological practices in large-scale studies of social behavior, particularly those using social media data. Unaddressed, such concerns could call into question much work being done by the research community. The goal of this workshop is to encourage constructive and informed debate on these topics with the goal of producing a body of peer-reviewed work and a working paper of methodological recommendations for the community. Specifically, this workshop will consider methodological issues that may threaten the validity, reproducibility, and generalizability of large-scale social media research studies. </p>

<b>Organizers</b>

<ul>
<li>Derek Ruths</li>
<li>Juergen Pfeffer</li>
</ul>



<?php include("../../footer.php"); ?>