<?php $section = "Program"; $subsection = "Workshop"; include("../../header.php"); ?>

<br>  <h2 class="pageTitle">Workshop program</h2>

<h3>Full Day Workshops</h3>
<p style="margin-bottom:0px; padding-bottom: 0px"><b><a href="#w1">W1: Social Media and Demographic Research</a></b><br>Emilio Zagheni; Ingmar Weber; Tom Le Grand</p>
<p style="margin-bottom:0px; padding-bottom: 0px"><b><a href="#w2">W2: Observational Studies through Social Media</a></b><br>Emre Kiciman; Munmun De Choudhury; Elad Yom-Tov</p>
<p style="margin-bottom:0px; padding-bottom: 0px"><b><s><a href="#w3">W3: Queering Social Media Research</a></s> (Cancelled)</b></p>
<p style="padding-bottom: 0px"><b><a href="#w4">W4: Digital Misinformation</a></b><br>Filippo Menczer; Alessandro Flammini; Giovanni Luca Ciampaglia; Gregory Maus; Alexio Mantzarlis</p>

<h3>Half Day Workshops</h3>
<p style="margin-bottom:0px; padding-bottom: 0px"><b><a href="#w5">W5: Events Analytics Using Social Media Data</a></b><br>Yuheng Hu; Yu-Ru Lin</p>
<p style="margin-bottom:0px; padding-bottom: 0px"><b><a href="#w6">W6: Studying User Perceptions and Experiences with Algorithms</a></b><br>Nick Proferes; Alissa Centivany; Caitlin Lustig; Jed Brubaker</p>
<p style="margin-bottom:0px; padding-bottom: 0px"><b><a href="#w7">W7: News and Public Opinion</a></b><br>Jisun An; Haweoon Kwak; Fabr&iacute;cio Benevenuto</p>
<p style="margin-bottom:0px; padding-bottom: 0px"><b><a href="#w8">W8: Perceptual Biases and Social Media</a></b><br>Nir Grinberg; Kenneth Joseph; Brooke Foucault Welles</p>
<p style="padding-bottom:0px"><b><a href="#w9">W9: The ICWSM Science Slam</a></b><br>David Garcia; Ingmar Weber; Aniko Hannak; Robert West</p>


<!-- <h2>ICWSM-17 Workshop Schedule</h2>
<h3>Tuesday, May 17</h3>

<h4>8:30 AM - 5:30 PM</h4>
<p><b> <a href="#w1">W1: CityLab</b></a><br>
<b> <a href="#w2">W2: Ethical Social Media Research</b></a><br>
<b> <a href="#w3">W3: Social Media & Demographic Research</b></a><br>
<b> <a href="#w4">W4: Wiki Workshop</b></a><br></p>



<h4>8:30 AM - 12:30 PM</h4>
<p><b> <a href="#w7">W7: Social Media in the Newsroom</b></a><br>
<b> <a href="#w8">W8: Social Web for Environmental and Ecological Monitoring</b></a><br>
</p>



<h4>1:30 - 5:30 PM</h4>
<p><b> <a href="#w5">W5: #FAIL - Things That Didn't Work Out</b></a><br>
<b> <a href="#w6">W6: News and Public Opinion</b></a><br>
</p>


<h4>8:30 - 10:30 PM</h4>
<p><b> <a href="#w9">W9: The ICWSM Science Slam</b></a><br>
</p>

<br><br>-->

<!-- <h3>Full Day Workshops</h3>

<p><b><a href="https://sites.google.com/site/smdrworkshop/">Social Media and Demographic Research</a></b><br>Emilio Zagheni; Ingmar Weber; Tom Le Grand</p>
<p><b><a href="#">Observational Studies through Social Media</a></b><br>Emre Kiciman; Munmun De Choudhury; Elad Yom-Tov</p>
<p><b><a href="http://queeringsocialmediaresearch.blogspot.ca/2016/12/workshop-queering-social-mediaresearch.html">Queering Social Media Research</a></b><br>M&eacute;lanie Millette; David Myles; Anna Lauren Hoffman</p>
<p><b><a href="http://cnets.indiana.edu/blog/2016/12/29/icwsm-2017-misinformation-workshop/">Digital Misinformation</a></b><br>Filippo Menczer; Alessandro Flammini; Giovanni Luca Ciampaglia; Gregory Maus; Alexio Mantzarlis</p>

<h3>Half Day Workshops</h3>
<p><b><a href="#">Events Analytics Using Social Media Data</a></b><br>Yuheng Hu; Yu-Ru Lin</p>
<p><b><a href="#">Studying User Perceptions and Experiences with Algorithms</a></b><br>Nick Proferes; Alissa Centivany; Caitlin Lustig; Jed Brubaker</p>
<p><b><a href="#">News and Public Opinion</a></b><br>Jisun An; Haweoon Kwak; Fabr&iacute;cio Benevenuto</p>
<p><b><a href="#">Perceptual Biases and Social Media</a></b><br>Nir Grinberg; Kenneth Joseph; Brooke Foucault Welles</p>
 -->
<p>Details about each workshop are below.</p>

<hr><br>


<a name="w1"><h4><a href="https://sites.google.com/site/smdrworkshop/">W1: Social Media and Demographic Research</a></h4></a>

<p>Demography has been a data-driven discipline since its birth. Data collection and the development of formal methods have sustained most of the major advances in our understanding of population processes. The global spread of Social Media has generated new opportunities for demographic research, as individuals leave an increasing quantity of traces online that can be aggregated and mined for population research. At the same time, the use of Social Media and Internet are affecting people's daily activities as well as life planning, with implications for demographic behavior.</p>

<p>The goal of this workshop is to favor communication and exchange between the communities of demographers and data scientists. The workshop would revolve around the main theme of applications and implications of social media and online data for demographic research. We invite contributions from colleagues interested in Computational Demography. We encourage submissions from researchers who wish to present their work, as well as the attendance of scholars interested in broadening their exposure to the topic. </p>


<b>Organizers</b>

<ul>
<li>Emilio Zagheni, University of Washington, Seattle</li>
<li>Ingmar Weber, Qatar Computing Research Institute, HBKU</li>
<li>Thomas LeGrand, Montreal University, Canada</li>
</ul>

<hr><br>

<a name="w2"><h4><a href="https://www.microsoft.com/en-us/research/event/ossm17/">W2: Observational Studies through Social Media</a></h4></a>

<p>People’s usage of mobile devices, Internet services, and applications creates a rich repository of data across many domains. As explorations and applications by the ICWSM community become more focused on understanding mechanisms and addressing societal and individual-level problems through policy- and individual-level interventions, the importance of careful studies and causal reasoning methods is becoming more critical. The ICWSM workshop on Observational Studies in Social Media and other human-generated content (OSSM, pronounced “awesome”), brings together social scientists, computer scientists, and others who are investigating observational studies of these interactions and data across many areas, including public health, medicine, sociology, education and others. The goal of the workshop is to foster discussion and brainstorming in this area.</p>


<b>Organizers</b>

<ul>
<li>Elad Yom-Tov, Microsoft Research</li>
<li>Munmun De Choudhury, Georgia Tech</li>
<li>Emre Kiciman, Microsoft Research</li>
<li>Tim Althoff, Stanford University</li>
</ul>

<!-- <hr><br>


<a name="w3"><h4><a href="http://queeringsocialmediaresearch.blogspot.de/2016/12/workshop-queering-social-mediaresearch.html">W3: Queering Social Media Research</a></h4></a>

<p>This workshop will address two main topics. Its first topic will focus on the study of queer subjects. Here, participants are invited to collectively establish a current (although inevitably non-exhaustive) "state of knowledge" on the intersection between social media research and: 1) gender diversity, such as trans*, genderfuck, genderqueer, and other non-normative identities; 2) sexual diversity, in terms of sexual orientation and practices; and 3) relational diversity, such as polyamory, casual dating, non-monogamous relationships, etc. In an emancipatory logic, this workshop will be used as an opportunity to identify how research has so far documented the role of social media platforms in the development or reconfiguration of queer visibilities (Duguay, 2016), publics (Berlant and Warner, 1995), spaces (Bell and Binnie, 2004; Dunn, 2011), and methods (Browne and Nash, 2010; King and Cronin, 2010). Conversely, it will also address how social media platforms participate in reproducing and promoting dominant cis/heteronormative identities (Bivens, 2015; Bivens and Haimson, 2016). Thus, how has social media research been addressing gender, sexual, and relational diversity so far and what future avenues need to be explored?</p>

<b>Organizers</b>

<ul>
<li>M&eacute;lanie Millette, Universit&eacute; du Qu&eacute;bec &agrave; Montr&eacute;al, Canada</li>
<li>David Myles, Universit&eacute; de Montr&eacute;al, Canada</li>
<li>Anna Lauren Hoffmann, University of California Berkeley, USA</li>
</ul>

 --><hr><br>

<a name="w4"><h4><a href="http://cnets.indiana.edu/blog/2016/12/29/icwsm-2017-misinformation-workshop/">W4: Digital Misinformation</a></h4></a>

<p>The deluge of online and offline misinformation is overloading the exchange of ideas upon which democracies depend. Many have argued that echo chambers are increasingly constricting the ability of alternative perspectives to provide a check on one’s viewpoints. Suffering fragmentation and declining public trust, the Fourth Estate struggles to carry out its traditional editorial role distinguishing facts from fiction. Without those safeguards, fake news, conspiracy theories, and deceptive social bots proliferate, facilitating the manipulation of public opinion. Countering misinformation while protecting freedom of speech will require collaboration between stakeholders across industry, journalism, and academia. To foster such collaboration, the Workshop on Digital Misinformation is intended to bring together key stakeholders to discuss practical countermeasures, including: (1) Identify the cognitive, social, political, financial, and technological factors contributing to the generation and propagation of misinformation; (2) Explore institutional standards such as a classification of different types of misinformation; (3) Devise technical tools to facilitate fact checking; (4) Delineate the distinction between countering misinformation and infringing upon the freedoms of speech, thought, and association; and (5) Develop a shared statement of principles.</p>


<b>Organizers</b>

<ul>
<li>Giovanni Luca Ciampaglia, Indiana University</li>
<li>Alessandro Flammini, Indiana University</li>
<li>Alexios Mantzarlis, Poynter International Fact-Checking Network</li>
<li>Gregory Maus, Indiana University</li>
<li>Filippo Menczer, Indiana University</li>
</ul>

<hr><br>



<a name="w5"><h4><a href="http://easmworkshop.github.io">W5: Events Analytics Using Social Media Data</a></h4></a>

<p>The purpose of the Event Analytics using Social Media Data (EASM) workshop is to bring together researchers that are working in a variety of areas that are all related to the larger problem of analyzing and understanding events using social media responses, to
discuss: 1) what are the recently developed machine learning and data mining techniques that can be leveraged to address challenges in analyzing events using social media data, and 2) from challenges in analyzing events, what are the practical research directions in the machine learning and data mining community.</p>

<b>Organizers</b>

<ul>
<li>Yuheng Hu, University of Illinios at Chicago</li>
<li>Yu-Ru Lin, University of Pittsburgh</li>
</ul>

<hr><br>



<a name="w6"><h4><a href="http://www.studyingusers.org/">W6: Studying User Perceptions and Experiences with Algorithms</a></h4></a>

<p>From Facebook's News Feed algorithm that shapes the posts and updates we see, to Spotify's recommendation service that introduces us to new music that we might love, to dating site algorithms that attempt to match us with potential romantic partners, algorithms play an increasingly important role in shaping many aspects of our daily lives. We seek to bring together a community of researchers interested in taking a human-centered perspective on studying the experience of algorithms.</p>

<p>To this end, we will be holding a half-day workshop on Monday, May 15th, 2017 in Montreal, Canada. The objective of this workshop is to articulate the grand challenges of studying the user-algorithm relationship and to bring together participants interested in developing projects to address these grand challenges. This workshop is action oriented, and we welcome participants from a variety of disciplinary perspectives who are interested in starting new projects on studying user perceptions and experiences with algorithms or who are in the early stages of an ongoing project.</p>

<b>Organizers</b>

<ul>
<li>Nicholas Proferes, College of Information Studies, University of Maryland, USA</li>
<li>Alissa Centivany, Information & Media Studies, Western University, Canada</li>
<li>Caitlin Lustig, Department of Informatics, University of California Irvine, USA</li>
<li>Jed Brubaker, Department of Information Science, University of Colorado Boulder, USA</li>
</ul>

<hr><br>


<a name="w7"><h4><a href="http://neco.io/">W7: News and Public Opinion</a></h4></a>

<p>Computational study of journalism has been an emerging discipline in recent years. A lot of inspiring data-driven studies on news production and consumption, news audience, news bias, and tools have been published and discussed. On the other hands, social media has received a lot of attention from researchers who study public opinion. The data collected from social media is a valuable asset to see what people have in their mind about at the very moment. Election prediction based on Twitter is a good example of such efforts.</p>

<p>In this workshop, we are trying to make this two disciplines meet. How news media formulate public opinion and how public opinion influences on news media are our questions to ask. For this, of course, the parallel efforts on the understanding of news media and public opinion are required first and then based on the findings, we can look into the interplay between them. As a result, the workshop topics can be categorized into three groups: news, public opinion, and their interplay.</p>

<b>Organizers</b>

<ul>
<li>Jisun An, Qatar Computing Research Institute, HBKU</li>
<li>Haewoon Kwak, Qatar Computing Research Institute, HBKU</li>
<li>Fabricio Benevenuto, Federal University of Minas Gerais (UFMG)</li>
</ul>

<hr><br>




<a name="w8"><h4><a href="https://pbsm2017.github.io/">W8: Perceptual Biases and Social Media</a></h4></a>

<p>Recent demonstrations of racial and gender bias in the United States have drawn public attention to stereotyping and discrimination in American society and its institutions. However, stereotyping and prejudice are only two examples of how our perceptual biases impact behavior. Confirmation bias, bias in media coverage of events and preferential attachment are all examples of perceptual biases that shape social processes like the creation of echo chambers and discriminatory behaviors. The emergence of social media as a prominent medium for human communication has the potential to provide a new lens for studying the relationship between perceptual biases and social processes, and the role social media plays in affecting this relationship.
</p>

<p>The goal of this workshop is to bring together those interested in studying how social media systems and perceptual biases co-evolve, and to write a position paper that outlines what existing research has shown and where new research could improve our understanding. We purposely leave the definition of perceptual bias broad, as the way a person (or institution) constructs its reality. We do so in order to address a variety of possible biases, from those that impact hiring practices of social media companies to those that play a role in social interaction.</p>

<b>Organizers</b>

<ul>
<li>Nir Grinberg, Network Science Institute, Northeastern University</li>
<li>Kenneth Joseph, Network Science Institute, Northeastern University</li>
<li>Brooke Foucault Welles, Network Science Institute, Northeastern University</li>
</ul>



<hr><br>




<a name="w9"><h4><a href="https://sites.google.com/site/icwsmscienceslam/">W9: The ICWSM Science Slam</a></h4></a>

<p>A Science Slam is an epic scientific event where scientists compete with short talks on their research. It's just like a poetry slam, but with science instead of poems. Slammers are completely free to do whatever they want on stage, everything is allowed including slides, games, the more creative, the better! The only two rules are: The topic of the slam has to be related to social media and the presentation should not take more than 8 minutes.
</p>

<b>Organizers</b>

<ul>
<li>David Garcia</li>
<li>Ingmar Weber</li>
<li>Aniko Hannak</li>
<li>Robert West</li>
</ul>

<p></p>


<?php include("../../footer.php"); ?>?