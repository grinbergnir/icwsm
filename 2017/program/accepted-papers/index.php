<?php $section = "Program"; $subsection = "Accepted Papers"; include("../../header.php"); ?>

<h2 class="pageTitle">ICWSM-17 Accepted Papers</h2>

<h3>Full Papers</h3>

<p><b>  Viral Potential and the Economics of Attention for Modeling Online Popularity under Promotion </b><br>
Marian-Andrei Rizoiu and Lexing Xie </p>
<p><b>  Cultural Diffusion and Trends in Facebook Photographs </b><br>
Quanzeng You, Dar&iacute;o Garc&iacute;a-Garc&iacute;a, Manohar Paluri, Jiebo Luo and Jungseock Joo </p>
<p><b>  The Evolution and Consequences of Peer Producing Wikipedia's Rules </b><br>
Brian Keegan </p>
<p><b>  The misleading narrative of the canonical faculty productivity trajectory </b><br>
Samuel Way, Allison Morgan, Aaron Clauset and Daniel Larremore </p>
<p><b>  Why Do Men Get More Attention? Exploring Factors Behind Success in an Online Design Community </b><br>
Johannes Wachs, Aniko Hannak, Andras Voros and Balint Daroczy </p>
<p><b>  Changes in Social Media Affect, Disclosure, and Sociality for a Sample of Transgender Americans in 2016's Political Climate </b><br>
Oliver L. Haimson and Gillian R. Hayes </p>
<p><b>  To Help or Hinder: Real-time Chat in Citizen Science </b><br>
Ramine Tinati, Markus Luczak-Roesch and Elena Simperl </p>
<p><b>  Spatio-temporal Analysis of Reverted Wikipedia Edits </b><br>
Johannes Kiesel, Martin Potthast, Matthias Hagen and Benno Stein </p>
<p><b>  Kek, Cucks, and God Emperor Trump: A Measurement Study of 4chan's Politically Incorrect Forum and Its Effects on the Web </b><br>
Gabriel Emile Hine, Jeremiah Onaolapo, Emiliano De Cristofaro, Nicolas Kourtellis, Ilias Leontiadis, Riginos Samaras, Gianluca Stringhini and Jeremy Blackburn </p>
<p><b>  Which Size Matters? Effects of Crowd Size on Solution Quality in Big Data Q&amp;A Communities </b><br>
Yla Tausczik, Ping Wang and Joohee Choi </p>
<p><b>  Characterizing Online Discussion Using Coarse Discourse Sequences </b><br>
Amy Zhang, Bryan Culbertson and Praveen Paritosh </p>
<p><b>  The Rich Get Richer? Limited Learning in Charitable Giving on DonorsChoose.org </b><br>
Chankyung Pak and Rick Wash </p>
<p><b>  Shocking the Crowd: The Effect of Censorship Shocks on Chinese Wikipedia </b><br>
Ark Fangzhou Zhang, Danielle Livneh, Ceren Budak, Lionel Robert and Daniel Romero </p>
<p><b>  Spice up Your Chat: The Intentions and Sentiment Effects of Using Emojis </b><br>
Tianran Hu, Han Guo, Hao Sun, Thuy-Vy Nguyen and Jiebo Luo </p>
<p><b>  Gendered Conversation in a Social Game-Streaming Platform </b><br>
Supun Nakandala, Giovanni Ciampaglia, Norman Su and Yong-Yeol Ahn </p>
<p><b>  Cultural Values and Cross-cultural Video Consumption on YouTube </b><br>
Minsu Park, Jaram Park, Young Min Baek and Michael Macy </p>
<p><b>  Cold Hard E-Cash: Friends and Vendors in the Venmo Digital Payments System </b><br>
Xinyi Zhang, Shiliang Tang, Yun Zhao, Gang Wang, Haitao Zheng and Ben Y. Zhao </p>
<p><b>  Tracing the Use of Practices through Networks of Collaboration </b><br>
Rahmtin Rotabi, Cristian Danescu-Niculescu-Mizil and Jon Kleinberg </p>
<p><b>  Echo Chambers in Investment Discussion Boards </b><br>
Shiliang Tang, Qingyun Liu, Megan McQueen, Scott Counts, Apurv Jain, Haitao Zheng and Ben Zhao </p>
<p><b>  Examining the Alternative News Ecosystem through Tweets about Alternative Narratives of Mass Shooting Events </b><br>
Kate Starbird </p>
<p><b>  State of the Geotags: Motivations and Recent Changes </b><br>
Dan Tasse, Zichen Liu, Alex Sciuto and Jason Hong </p>
<p><b>  The Language of Social Support in Social Media and its Effect on Suicidal Ideation Risk </b><br>
Munmun De Choudhury and Emre Kiciman </p>
<p><b>  Untangling Emoji Popularity through Semantic Embeddings </b><br>
Wei Ai, Xuan Lu, Xuanzhe Liu, Ning Wang, Gang Huang and Qiaozhu Mei </p>
<p><b>  Who Makes Trends? Understanding Demographic Biases in Crowdsourced Recommendations </b><br>
Abhijnan Chakraborty, Johnnatan Messias, Fabricio Benevenuto, Saptarshi Ghosh, Niloy Ganguly and Krishna P. Gummadi </p>
<p><b>  Selfie-Presentation in Everyday Life: A Large-scale Characterization of Selfie Contexts on Instagram </b><br>
Julia Deeb-Swihart, Christopher Polack, Eric Gilbert and Irfan Essa </p>
<p><b>  Better When It Was Smaller? Community Content and Behavior After Massive Growth </b><br>
Zhiyuan Lin, Niloufar Salehi, Bowen Yao, Yiqi Chen and Michael Bernstein </p>
<p><b>  Analysing Timelines of National Histories across Wikipedia Editions: A Comparative Computational Approach </b><br>
Anna Samoilenko, Florian Lemmerich, Katrin Weller, Maria Zens and Markus Strohmaier </p>
<p><b>  Online Human-Bot Interactions:  Detection, Estimation, and Characterization </b><br>
Onur Varol, Emilio Ferrara, Clayton Davis, Filippo Menczer and Alessandro Flammini </p>
<p><b>  Wearing Many (Social) Hats:How Different are Your Different Social Network Personae? </b><br>
Changtao Zhong, Hau-Wen Chang, Dmytro Karamshuk, Dongwon Lee and Nishanth Sastry </p>
<p><b>  When Does More Money Work? Examining the Role of Perceived Fair-ness in Pay on the Performance Quality of Crowdworkers </b><br>
Teng Ye, Sangseok You and Lionel P. Robert Jr. </p>
<p><b>  To Thread or Not to Thread: The Impact of Conversation Threading on Online Discussion </b><br>
Pablo Arag&oacute;n, Vicen&ccedil; G&oacute;mez and Andreas Kaltenbrunner </p>
<p><b>  How Editorial, Temporal and Social Biases Affect Online Food Popularity and Appreciation </b><br>
Markus Rokicki, Eelco Herder and Christoph Trattner </p>
<p><b>  Armed Conflicts in Online News: A Multilingual Study </b><br>
Robert West and J&uuml;rgen Pfeffer </p>
<p><b>  Community Identity and User Engagement in a Multi-Community Landscape </b><br>
Justine Zhang, William Hamilton, Cristian Danescu-Niculescu-Mizil, Dan Jurafsky and Jure Leskovec </p>
<p><b>  #NotOkay: Understanding Gender-based Violence in Social Media </b><br>
Mai Elsherief, Elizabeth Belding and Dana Nguyen </p>
<p><b>  Identifying Effective Signals to Predict Deleted and Suspended Accounts on Twitter across Languages </b><br>
Svitlana Volkova and Eric Bell </p>
<p><b>  Predicting Popular and Viral Image Cascades in Pinterest </b><br>
Jinyoung Han, Daejin Choi, Jungseock Joo and Chen-Nee Chuah </p>
<p><b>  The Substantial Interdependence of Wikipedia and Google &ndash; A Case Study on the Relationship Between Peer Production Communities and Intelligent Technologies </b><br>
Connor McMahon, Isaac Johnson and Brent Hecht </p>
<p><b>  Understanding Emoji Ambiguity in Context: The Role of Text in Emoji-Related Miscommunication </b><br>
Hannah Miller, Daniel Kluver, Jacob Thebault-Spieker, Loren Terveen and Brent Hecht </p>
<p><b>  The Power of the Patient Voice: Learning Indicators of Hormonal Therapy Adherence From an Online Breast Cancer Forum </b><br>
Zhijun Yin, Bradley Malin, Jeremy Warner, Pei-Yun Hsueh and Ching-Hua Chen </p>
<p><b>  Adaptive Spammer Detection with Sparse Group Modeling </b><br>
Liang Wu, Xia Hu, Fred Morstatter and Huan Liu </p>
<p><b>  &ldquo;Be careful; things can be worse than they appear&rdquo;: Understanding Biased Algorithms and Users&rsquo; Behavior around Them in Rating Platforms </b><br>
Motahhare Eslami, Kristen Vaccaro, Karrie Karahalios and Kevin Hamilton </p>
<p><b>  How User Condition Affects Community Dynamics in a Forum on Autism </b><br>
Mattia Samory and Enoch Peserico </p>

<h3>Dataset Papers</h3>

<p><b>  Fashion conversation Data on Instagram </b><br>
Yu-I Ha, Sejeong Kwon, Jungseock Joo and Meeyoung Cha </p>
<p><b>  TokTrack: A Complete Token Provenance and Change Tracking Dataset for the English Wikipedia </b><br>
Fabian Fl&ouml;ck, Kenan Erdogan and Maribel Acosta </p>
<p><b>  Data Sets: Word Embeddings Learned from Tweets and General Data </b><br>
Quanzhi Li and Sameena Shah </p>
<p><b>  EmojiNet: An Open Service and API for Emoji Sense Discovery </b><br>
Sanjaya Wijeratne, Lakshika Balasuriya, Amit Sheth and Derek Doran </p>

<h3>Posters</h3>

<p><b>  A Gendered Analysis of Leadership in Enterprise Social Networks </b><br>
Giorgia Di Tommaso, Giovanni Stilo and Paola Velardi </p>
<p><b>  DeepCity: A Feature Learning Framework for Mining Location Check-ins </b><br>
Jun Pang and Yang Zhang </p>
<p><b>  Decoding the Hate Code on Social Media </b><br>
Rijul Magu, Kshitij Joshi and Jiebo Luo </p>
<!-- <p><b>  Dialectic: Enhancing Text Input Fields with Automatic Feedback to Improve Writing Quality </b><br>
Hamed Nilforoshan, James Sands, Kevin Lin, Rahul Khanna and Eugene Wu </p> -->
<p><b>  Event Organization 101: Understanding Latent Factors of Event Popularity </b><br>
Shuo Zhang and Qin Lv </p>
<p><b>  Sparse Overlap Cross-Platform Recommendation via Adaptive Similarity Structure Regularization </b><br>
Hanqing Lu, Chaochao Chen and Qinyue Jiang </p>
<p><b>  Are there Gender Differences in Professional Self-Promotion? An Empirical Case Study of LinkedIn Profiles among Recent MBA Graduates </b><br>
Kristen M. Altenburger, Rajlakshmi De, Kaylyn Frazier, Nikolai Avteniev and Jim Hamilton </p>
<!-- <p><b>  Unbiased Sampling of Social Media Networks for Well-connected Subgraphs </b><br>
Dong Wang, Zhenyu Li, Zhenhua Li and Gaogang Xie </p> -->
<!-- <p><b>  An Investigation of Large-scale Ubiquitous Data for Fine-Grained Crime Prediction </b><br> 
Cristina Kadar and Irena Pletikosa </p>-->
<!-- <p><b>  &quot;The two never even met&quot;: Differences in Hosting Standards, Service Orientation, and Power Relationships between Couchsurfing and Airbnb </b><br>
Maximilian Klein, Jiajun Ni, Isaac Johnson, Leon Ding, Benjamin Hill and Haiyi Zhu </p> -->
<!-- <p><b>  Pricing Your Knowledge: On Monetary Incentives for Social Question Answering </b><br>
Steve Jan, Chun Wang, Qing Zhang and Gang Wang </p> -->
<!-- <p><b>  Defending against Social Sybils via Signed Directed Network Embedding </b><br>
Cui Li, Zhi Yang, Yusi Zhang and Yafei Dai </p> -->
<p><b>  Should We Be Confident in Peer Effects Estimated From Partial Crawls of Social Networks? </b><br>
Jiasen Yang, Bruno Ribeiro and Jennifer Neville </p>
<p><b>  Antagonism also Flows through Retweets: The Impact of Out-of-Context Broadcasts in Opinion Polarization Analysis </b><br>
Pedro Calais Guerra, Roberto Nalon, Renato Assun&ccedil;&atilde;o and Wagner Meira Jr. </p>
<p><b>  Nasty, Brutish, and Short: What Makes Election News Popular on Twitter? </b><br>
Sophie Chou and Deb Roy </p>
<p><b>  On Migratory Behavior in Video Consumption </b><br>
Huan Yan, Tzu-Heng Lin, Gang Wang, Yong Li, Haitao Zheng, Depeng Jin and Ben Zhao </p>
<p><b>  Dynamics of Content Quality in Collaborative Knowledge Production </b><br>
Emilio Ferrara, Nazanin Alipoufard, Chiranth Gopal, Kristina Lerman and Keith Burghardt </p>
<p><b>  On Quitting: Performance and Practice in Online Game Play </b><br>
Tushar Agarwal, Keith Burghardt and Kristina Lerman </p>
<p><b>  Exploiting Contextual Information for Fine-grained Tweet Geolocation </b><br>
Wen Haw Chong and Ee-Peng Lim </p>
<p><b>  A World of Difference: Divergent Word Interpretations among People </b><br>
Tianran Hu, Ruihua Song, Xing Xie, Maya Abtahian, Philip Ding and Jiebo Luo </p>
<p><b>  Face-to-BMI: Using Computer Vision to Study Body Weight in Social Media Profile Pictures </b><br>
Enes Kocabey, Ingmar Weber, Ferda Ofli, Yusuf Aytar, Javier Marin and Antonio Torralba </p>
<p><b>  What Comments Did I Get? How Post and Comment Characteristics Predict Interaction Satisfaction on Facebook </b><br>
Shruti Sannon, Yoon Hyung Choi, Jessie G. Taft and Natalya N. Bazarova </p>
<p><b>  A Computational Approach to Perceived Trustworthiness of Self-Disclosure in Online Lodging Marketplaces </b><br>
Xiao Ma, Trishala Neeraj and Mor Naaman </p>
<!-- <p><b>  Understanding the Impact of Data Withdrawal for Social Media Studies on Twitter </b><br>
Mainack Mondal, Krishna P. Gummadi and Balachander Krishnamurthy </p> -->
<p><b>  Twitter911: A Cautionary Tale </b><br>
Anis Zaman, Nabil Hossain and Henry Kautz </p>
<!-- <p><b>  Friends Don&rsquo;t Need Receipts: The Curious Case of Social Awareness Streams in the Mobile Payment App Venmo </b><br>
Monica Caraway, Daniel Epstein and Sean Munson </p> -->
<p><b>  Scalable News Slant Measurement Using Twitter </b><br>
Huyen Le, Zubair Shafiq and Padmini Srinivasan </p>
<p><b>  Ranking with social cues: Integrating review scores with popularity information on the web </b><br>
Pantelis Pipergias Analytis, Alexia Delfino, Mehdi Moussaid, Juliane Kammer and Thorsten Joachims </p>
<p><b>  Is Slacktivism Underrated? Measuring the value of slacktivists for online social movements </b><br>
Lia Bozarth and Ceren Budak </p>
<p><b>  Predicting Movie Genre Preferences from User Personality and Values </b><br>
Euna Mehnaz Khan, Md Saddam Hossain Mukta, Mohammed Eunus Ali and Jalal Mahmud </p>
<p><b>  Detecting Camouflaged Content Polluters </b><br>
Liang Wu, Xia Hu, Fred Morstatter and Huan Liu </p>
<p><b>  Early Identification of Personalized Trending Topics in Microblogging </b><br>
Liang Wu, Xia Hu and Huan Liu </p>
<p><b>  Mood Congruence or Mood Consistency? Examining Aggregated Twitter Sentiment during the Super Bowl 2016 </b><br>
Yuheng Hu </p>
<p><b>  A Motif-based Approach for Identifying Controversy </b><br>
Mauro Coletto, Venkata Rama Kiran Garimella, Aristides Gionis and Claudio Lucchese </p>
<p><b>  The Ebb and Flow of Controversial Debates on Social Media </b><br>
Venkata Rama Kiran Garimella, Gianmarco De Francisci Morales, Aristides Gionis and Michael Mathioudakis </p>
<p><b>  A long-term Analysis of Polarisation on Twitter </b><br>
Venkata Rama Kiran Garimella and Ingmar Weber </p>
<p><b>  Two-Phase Influence Maximization in Social Networks with Seed Nodes and Referral Incentives </b><br>
Sneha Mondal, Swapnil Dhamal and Y. Narahari </p>
<p><b>  Spatiotemporal Dynamics of Media Attention in 196 Countries </b><br>
Haewoon Kwak and Jisun An </p>
<p><b>  Distinguishing the Wood from the Trees: Contrasting Collection Methods to Understand Bias in a Longitudinal Brexit Twitter Data Set </b><br>
Clare Llewellyn and Laura Cram </p>
<!-- <p><b>  Diversity of Private Activities and Economic Development of Neighborhoods </b><br>
Daniele Quercia, Luca Maria Aiello and Rossano Schifanella </p> -->
<p><b>  Studying Fertility Trends via Search Behavior </b><br>
Jussi Ojala, Ingmar Weber, Emilio Zagheni and Francesco Billari </p>
<p><b>  Suitable for All Ages: Using Reviews to Determine Appropriateness of Products </b><br>
Elizabeth Daly, Oznur Alkan and Michael Muller </p>
<!-- <p><b>  Modeling Non-Local Contributions to Volunteered Geographic Information: The Cost of Distance in Geographic Content Production </b><br>
Jacob Thebault-Spieker, Aaron Halfaker, Loren Terveen and Brent Hecht </p> -->
<p><b>  Automated Hate Speech Detection and the Problem of Offensive Language </b><br>
Thomas Davidson, Dana Warmsley, Michael Macy and Ingmar Weber </p>
<p><b>  Detecting and Predicting Socio-Economic Impact of Cultural Investment Through Geo-Social Network Analysis </b><br>
Xiao Zhou, Desislava Hristova, Anastasios Noulas and Cecilia Mascolo </p>
<p><b>  How to Create Fake Public Opinion: Analyzing Political Astroturfing on Twitter Using Ground Truth Data from South Korea </b><br>
Franziska Barbara Keller, David Schoch, Sebastian Stier and Junghwan Yang </p>
<p><b>  Robust Classification of Crisis-Related Data on Social Networks using Convolutional Neural Networks </b><br>
Dat Tien Nguyen, Kamla Al-Mannai, Shafiq Joty, Hassan Sajjad, Muhammad Imran and Prasenjit Mitra </p>
<p><b>  Identifying leading indicators of product recalls from online reviews using positive unlabeled learning </b><br>
Aron Culotta and Shreesh Kumara Bhat </p>
<!-- <p><b>  Motivations for &amp; Barriers to Interpersonal Commenting  on Facebook in Seven Countries </b><br>
Alex Leavitt, Lauren Scissors and Eden Litt </p> -->
<p><b>  Separating the Wheat from the Chaff: Evaluating Success Determinants for Online Q&amp;A Communities </b><br>
Erik Aumayr and Conor Hayes </p>
<p><b>  Characteristics of On-time and Late Reward Delivery Projects </b><br>
Thanh D. Tran and Kyumin Lee </p>
<p><b>  Behavioral Analysis of Review Fraud: Linking Malicious Crowdsourcing to Amazon and Beyond </b><br>
Parisa Kaghazgaran, James Caverlee and Majid Alfifi </p>
<p><b>  Measuring, Predicting and Visualizing Short-Term Change in Word Representation and Usage in VKontakte Social Network </b><br>
Ian Stewart, Dustin Arendt, Eric Bell and Svitlana Volkova </p>
<p><b>  Real-Time Road Traffic Monitoring Via Tweet Grounding </b><br>
Noora Al Emadi, Sofiane Abbar, Javier Borge-Holthoefer, Francisco Guzman and Fabrizio Sebastiani </p>
<p><b>  Identifying Good Conversations Online (Yes, they do exist!) </b><br>
Courtney Napoles, Aasish Pappu, Joel Tetreault, Enrica Rosato and Brian Provenzale </p>
<p><b>  25 Tweets to Know You: A New Model to Predict Personality with Social Media </b><br>
Pierre-Hadrien Arnoux and Anbang Xu </p>
<p><b>  Recognizing Pathogenic Empathy in Social Media </b><br>
Muhammad Abdul-Mageed, Anneke Buffone, Hao Peng, Salvatore Giorgi, Johannes Eichstaedt and Lyle Ungar </p>
<!-- <p><b>  Twitter's Tampered Data Samples </b><br>
Juergen Pfeffer, Fred Morstatter and Katja Mayer </p> -->
<p><b>  Modeling of Political Discourse Framing on Twitter </b><br>
Kristen Johnson, Di Jin and Dan Goldwasser </p>
<p><b>  On the Interpretability of Thresholded Social Network </b><br>
Oren Tsur and David Lazer </p>
<p><b>  Inverse Dynamical Inheritance in Stack Exchange Taxonomies </b><br>
Cesar Ojeda, Kostadin Cvejoski, Rafet Sifa and Christian Bauckhage </p>
<p><b>  &ldquo;Voters of the Year&rdquo;: 19 Voters Who Were Unintentional Election Poll Sensors on Twitter </b><br>
William Hobbs, Lisa Friedland, Kenneth Joseph, Oren Tsur, Stefan Wojcik and David Lazer </p>
<p><b>  Spammer Users Identification in Wikipedia via Editing Behavior </b><br>
Thomas Green and Francesca Spezzano </p>
<p><b>  Changes in Social Media Behavior During Life Periods of Uncertainty </b><br>
Xinru Page and Marco Marabelli </p>
<!-- <p><b>  &ldquo;No Prejudice Here&rdquo;:  Examining Social Identity Work in Starter Pack Memes </b><br>
Jordan Eschler and Amanda Menking </p> -->
<p><b>  Querying Documents Annotated by Interconnected Entities </b><br>
Shouq Sadah, Moloud Shahbazi and Vagelis Hristidis </p>
<p><b>  An Exploratory Methodology for Measuring Fine-Grained Diversity Using Social Media Images </b><br>
Vivek Singh, Saket Hegde and Akanksha Atrey </p>
<p><b>  Loyalty in Online Communities </b><br>
William Hamilton, Justine Zhang, Cristian Danescu-Niculescu-Mizil, Dan Jurafsky and Jure Leskovec </p>
<p><b>  Language Use Matters: Analysis of the Linguistic Structure of Question Texts can Characterize Answerability in Quora </b><br>
Suman Kalyan Maity, Aman Kharb and Animesh Mukherjee </p>
<p><b>  &rsquo;Just the Facts&rsquo;: Exploring the Relationship between Emotional Language and Member Satisfaction in Enterprise Online Communities </b><br>
Ryan Compton, Hernan Badenes, Jilin Chen, Eben Haber and Steve Whittaker </p>
<p><b>  When to Shift Channel: Self-disclosure and Reciprocity in Online Support Groups </b><br>
Diyi Yang, Zheng Yao and Robert Kraut </p>
<p><b>  US Presidential Election: What Engaged People on Facebook </b><br>
Milad Kharratzadeh and Deniz Ustebay </p>
<p><b>  Headlines Matter: Using Headlines to Predict the Popularity of News Articles on Twitter and Facebook </b><br>
Alicja Piotrkowicz, Vania Dimitrova, Jahna Otterbacher, and Katja Markert </p>
<p><b>  How Fast Will You Get a Response? Predicting Interval Time for Reciprocal Link Creation </b><br>
Vachik Dave, Mohammad Hasan and Chandan K. Reddy </p>
<!-- <p><b>  Demographic Differences in Image Posting on Twitter </b><br>
Daniel Preotiuc-Pietro, Leqi Liu and Lyle Ungar </p> -->
<p><b>  Fostering User Engagement: Rhetorical Devices for Applause Generation Learnt from TED Talks </b><br>
Zhe Liu, Anbang Xu, Mengdi Zhang, Jalal Mahmud and Vibha Sinha </p>
<p><b>  Mapping Twitter Conversation Landscapes </b><br>
Soroush Vosoughi, Prashanth Vijayaraghavan, Ann Yuan and Deb Roy </p>
<p><b>  Controlling for Unobserved Confounds in Social Media Classification Using Correlational Constraints </b><br>
Virgile Landeiro and Aron Culotta </p>
<p><b>  Designing a Social Support System for College Adjustment and Social Support </b><br>
Donghee Yvette Wohn, Mousa Ahmadi, Leiping Gong, Indraneel Kulkarni and Atisha Poojari </p>
<p><b>  The Role of Optimal Distinctiveness and Homophily in Online Dating </b><br>
Danaja Maldeniya, Arun Varghese, Toby Stuart and Daniel Romero </p>
<p><b>  Understanding how people attend to and engage with foreign language posts in multilingual newsfeeds </b><br>
Hajin Lim and Susan Fussell </p>
<p><b>  A Longitudinal Study of Topic Classification on Twitter </b><br>
Zahra Iman, Scott Sanner, Mohamed Reda Bouadjenek and Lexing Xie </p>
<p><b>  From Camera to Deathbed: Understanding Dangerous Selfies on Social Media </b><br>
Hemank Lamba, Varun Bharadhwaj, Mayank Vachher, Divyansh Agarwal, Megha Arora, Niharika Sachdeva and Ponnurangam Kumaraguru </p>



<h3>Demos</h3>

<p><b>  Programming Languages in GitHub: a Visualization in Hyperbolic Plane </b><br>
Dorota Celi&#x144;ska and Eryk Kopczy&#x144;ski </p>
<p><b>  CitizenHelper: A Streaming Analytics System to Mine Citizen and Web Data for Nonprofit Organizations </b><br>
Prakruthi Karuna, Mohammad Rana, and Hemant Purohit </p>
<p><b>  Graphene: a Julia Package for Network Research </b><br>
Igor Zakhlebin </p>
<p><b>  Visualizing Health Awareness in the Middle East </b><br>
Matheus Ara&uacute;jo, Yelena Mejova, Michael Aupetit and Ingmar Weber </p>

<?php include("../../footer.php"); ?>