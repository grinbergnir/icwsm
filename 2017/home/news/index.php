<?php $section = "Home"; $subsection = "News"; include("../../header.php"); ?>

  <h2 class="pageTitle">News</h2>

<ul id="newsList">
 
    <li class="">
      <h3 class="newsTitle"> Keynote speakers announced!</h3>
      <p class="newsDate">December 9th, 2015</p>
      <p class="">  <a href="https://getoor.soe.ucsc.edu/home" target=_blank>Lise Getoor</a> and <a href="http://web.stanford.edu/~amirgo/" target=_blank>Amir Goldberg</a> to be keynote speakers at ICWSM '16.</a></p>
    </li>

    <li class="">
      <h3 class="newsTitle"> ICWSM'16 to be held in Cologne, Germany from May 17th-20th 2016.</h3>
      <p class="newsDate">July 7th, 2015</p>
      <p class=""> <a href="http://www.icwsm.org/2016/"> Click for more details.</a> </p>
    </li>

  </ul>
  
<?php include("../../footer.php"); ?>?