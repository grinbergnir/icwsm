        </div></div>  

        
        <!-- SIDEBAR NAV -->
        <div id="sidebar" class="sidebar">

  
 <div id="importantDates">

  <h4 class="importantDates">WHEN, WHERE, WHO</h4>

<!--To be announced.-->


  <ul>
<li class=""><span class="datePart">January 6, 2017</span><span class="description"><a href="http://icwsm.org/2017/submitting/call-for-papers/">Abstract Submission</a></span></li>
<li class=""><span class="datePart">January 13, 2017</span><span class="description"><a href="http://icwsm.org/2017/submitting/call-for-papers/">Full Paper Submission</a></span></li>
<li class=""><span class="datePart">January 25, 2017</span><span class="description"><a href="http://www.icwsm.org/2017/submitting/tutorials/">Tutorial Submission</a></span></li>
<li class=""><span class="datePart">February 28, 2017</span><span class="description"><a href="http://icwsm.org/2017/submitting/call-for-papers/">Paper & Poster Notification</a></span></li>
<li class=""><span class="datePart">March 10, 2017</span><span class="description"><a href="http://icwsm.org/2017/submitting/call-for-papers/">Camera Ready Version Due</a></span></li>

<li class=""><span class="datePart">May 15, 2017</span><span class="description"><a href="http://icwsm.org/2017">Tutorials / Workshops</a></span></li>

<li class=""><span class="datePart">May 16-18, 2017</span><span class="description"><a href="http://icwsm.org/2017">Main Conference</a></span></li>
    
  </ul>

  <br class="clear">
          </div>

 <div id="importantDates">


<!--

URL: www.sshrc-crsh.gc.ca/home-accueil-eng.aspx
Canadian Government Logo?

Silver Sponsor
--------------
Microsoft Research
URL: www.microsoft.com/research
Please use logo from last year.

Bronze Sponsor
--------------
Circadia Labs
URL: www.circadialabs.com
Logo attached

General Sponsor
---------------
McGill University
URL: www.mcgill.ca/datascience/
Derek can provide logo?

-->

   <h4 class="importantDates">SPONSORS</h4>
  <h5>Platinum Sponsor</h5>
    <a href="http://www.sshrc-crsh.gc.ca/home-accueil-eng.aspx"><img style="border: 0; width: 125px" src="/2017/images/sponsors/sshrc.jpg" alt="Social Sciences and Humanities Research Council of Canada"></a><br><br><br> 

   <h5>Gold Sponsor</h5>
    <a href="http://research.microsoft.com/"><img style="border: 0;" src="/2017/images/sponsors/microsoft.png" alt="Microsoft Research"></a><br><br><br>
   <h5>Silver Sponsor</h5>
    <a href="http://research.facebook.com/"><img style="border: 0;" src="/2017/images/sponsors/fblogo.png" alt="Facebook"></a><br><br><br> 
   <h5>Bronze Sponsors</h5>
    <a href="https://nexalogy.com/"><img style="border: 0; margin-left: 6%;" src="/2017/images/sponsors/nexalogylogo.png" alt="Nexalogy"></a><br><br><br> 
    <a href="http://www.circadialabs.com"><img style="border: 0;" src="/2017/images/sponsors/circadialogo.png" alt="Circadia Labs"></a><br><br><br> 
   <h5>General Sponsor</h5>
    <a href="http://www.mcgill.ca/datascience/"><img style="border: 0; margin-left: 6%;" src="/2017/images/sponsors/mcgill.svg" alt="Data Science at McGill University"></a>
<br>

</div>


  
<div id="twitterMessages">
  <h4 class="twitterMessages">Updates From Twitter</h4>
  <!--<p class="twitterSmall"><a href="http://www.twitter.com/icwsm/">Follow ICWSM on Twitter</a></p> -->

<a class="twitter-timeline"  href="https://twitter.com/search?q=%23icwsm%20OR%20%23icwsm2017%20OR%20%40icwsm%20-from%3ACMasden7" data-widget-id="915242820205719552">Tweets about #icwsm OR #icwsm2017 OR @icwsm -from:CMasden7</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>




<!--<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/search?q=%23icwsm" data-widget-id="458431519749455873">Tweets about "#icwsm"</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>-->

<!--
  <script src="http://widgets.twimg.com/j/2/widget.js"></script>
  <script>
  new TWTR.Widget({
    version: 2,
    type: 'profile',
    rpp: 4,
    interval: 6000,
    width: 250,
    height: 300,
    theme: {
      shell: {
        background: 'transparent',
        color: 'black'
      },
      tweets: {
        background: 'transparent',
        color: 'black',
        links: 'gray'
      }
    },
    features: {
      scrollbar: false,
      loop: false,
      live: false,
      hashtags: true,
      timestamp: true,
      avatars: false,
      behavior: 'all'
    }
  }).render().setUser('icwsm').start();
  </script>  
  
  -->
  
  <br class="clear">
</div>

<!--
<div id="flickrPhotoStream">
  <h4 class="flickrPhotoStream">Flickr Photo Stream</h4>
  <iframe align="center" src="http://www.flickr.com/slideShow/index.gne?tags=icwsm" frameBorder="0" width="240" height="180" scrolling="no"></iframe>
    <br class="clear">
</div>
-->

</div>

          <div class="clear"></div>
      </div>


</div>

      </div>
      
      <div class="container_12 mainContainer">
        <div id="sponsors" >

</div>

      </div>
    </div>
   <div id='footer' class="container_12" >
<div id="footerContent">
  
    <div class="grid_6"><h4 id="footerLogo"><a href="http://www.icwsm.org/2017/" title="ICWSM-17"><span>ICWSM-17<span></a></h4></div>

  <div id="footerPagesMethods" class="grid_3">
    <h5>ICWSM-17</h5>
    <ul>
              
                <li class="home">
        
          <a href="/2017/">Welcome</a>
              
                <li class="submitting">
        
          <a href="/2016/submitting/call-for-papers/">Submitting</a>
        </li>
              
                <li class="program">
        
          <a href="/2016/program/program/">Program</a>
        </li>
              
                <li class="organization">
        
          <a href="/2016/organization/organization/">Organization</a>
        </li>
              
                <li class="contact">
        
          <a href="/2016/contact/contact/">Contact</a>
        </li>
      

    </ul>
  </div>
  <div id="footerContactMethods" class="grid_3">
    <h5>Get In Touch</h5>
    <ul>
      <li><span class="label">Email:</span> <a href="mailto:icwsm17&#64;aaai.org">icwsm17&#64;aaai.org</a></li>
<!-- <li><span class="label">RSS:</span> <a href="/2012/feed">News Feed</a></li> -->
      <li><span class="label">Twitter:</span> <a href="http://twitter.com/icwsm">&#64;icwsm</a> <a href="http://twitter.com/#!/search/realtime/icwsm">#icwsm</a></li>
      <li><span class="label">Facebook:</span> <a href="http://facebook.com/icwsm">facebook.com/icwsm</a></li>
      <li><span class="label">Google+:</span> <a href="http://gplus.to/icwsm">gplus.to/icwsm</a></li>
      <li><span class="label">Flickr:</span> <a href="http://www.flickr.com/photos/tags/icwsm">ICWSM Photostream</a></li>
    </ul>
    
    <p><a href="#top">Back To Top</a><p>
  </div>

  <div class="clear">
</div>
    
    <script type="text/javascript" charset="utf-8">
      document.getElementsByTagName('body')[0].className += ' js-enabled';
    </script>
    <script src="./docs/js/jquery-1.3.2.js" type="text/javascript" charset="utf-8"></script>
    <script src="./docs/js/jquery.timers-1.2.js" type="text/javascript" charset="utf-8"></script>
    <script src="./docs/js/gallery.js" type="text/javascript" charset="utf-8"></script>
    <script src="./docs/js/init-gallery.js" type="text/javascript" charset="utf-8"></script>
    
    
  </body>
  
  
</html>