$(function() {

	// don't init gallery if it consists of a single photo
	if($("div.slideshowImage").length > 1) {
		// wrap images
		$("div.slideshowImage").wrapAll("<div id='slideShowImageWrapper'><div id='slideShowImageHolder'></div></div>");
		$("div.slideshowImage").show();
		
		// init gallery: Gallery.init(imageHolder, imageWrapperWidth, imageCountHolder, nextButton, prevButton)
		Gallery.init($("div#slideShowImageHolder"), 590, $("p#galleryCount").children("span")[0], $("a#nextImage"), $("a#previousImage"));

		Gallery.autoplay();

	} else {
	  // hide gallery count and navigation
	  $("p#galleryCount, div#galleryNavigation").hide();
	  // show project navigation
	  $("p#projectCount").show();
	}
	
	
});