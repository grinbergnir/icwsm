<?php $section = "Home"; $subsection="Welcome"; include("header.php"); ?>


<h2 class="pageTitle">THE 12TH INTERNATIONAL AAAI CONFERENCE ON WEB AND SOCIAL MEDIA (ICWSM-18)</h2>


  <p>The International AAAI Conference on Web and Social Media (ICWSM) is a forum for researchers from multiple disciplines to come together to share knowledge, discuss ideas, exchange information, and learn about cutting-edge research in diverse fields with the common theme of online social media. This overall theme includes research in new perspectives in social theories, as well as computational algorithms for analyzing social media. ICWSM is a singularly fitting venue for research that blends social science and computational approaches to answer important and challenging questions about human social behavior through social media while advancing computational tools for vast and unstructured data.
</p>

  <p>ICWSM, now in its eleventh year, has become one of the premier venues for computational social science, and <a href="http://icwsm.org/2018/contact/previous-conferences/">previous years of ICWSM</a> have featured papers, posters, and demos that draw upon network science, machine learning, computational linguistics, sociology, communication, and political science. The uniqueness of the venue and the quality of submissions have contributed to a fast growth of the conference and a competitive acceptance rate around 20% for full-length research papers published in the proceedings by the Association for the <a href="http://www.aaai.org/">Association for the Advancement of Artificial Intelligence (AAAI)</a>. </p>

<!-- The accepted papers will be open access and have equal time for a full oral presentation. Full papers will also have the opportunity to display their paper as a poster at the venue.</p> -->

 <p>For ICWSM-18, in addition to the usual program of contributed talks, posters and demos, the main conference will include a selection of keynote talks from prominent scientists and technologists. Building on successes in previous years, ICWSM-18 will also hold a day of workshops and tutorials in addition to the main conference.</p>


  <h3>Disciplines</h3>

  <ul>
    <li>Computational approaches to social media research including
        <ul>
            <li>Natural language processing</li>
            <li>Text / data mining</li>
            <li>Machine learning</li>
            <li>Image / multimedia processing</li>
            <li>Graphics and visualization</li>
            <li>Distributed computing</li>
            <li>Graph theory and Network Science</li>
            <li>Human-computer interaction</li>
        </ul>
    </li>
    <li>Social science approaches to social media research including
        <ul>
            <li>Psychology</li>
            <li>Sociology and social network analysis</li>
            <li>Communication</li>
            <li>Political science</li>
            <li>Economics</li>
            <li>Anthropology</li>
            <li>Media studies and journalism</li>
            <li>Digital Humanities</li>
        </ul>
    </li>
    <li>Interdisciplinary approaches to social media research combining computational algorithms and social science methodologies</li>
  </ul>


  <h3>Topics Include (BUT ARE NOT LIMITED TO)</h3>

    <ul>
     <li>Studies of digital humanities (culture, history, arts) using social media</li>
     <li>Psychological, personality-based and ethnographic studies of social media</li>
     <li>Analysis of the relationship between social media and mainstream media</li>
     <li>Qualitative and quantitative studies of social media</li>
     <li>Centrality/influence of social media publications and authors</li>
     <li>Ranking/relevance of social media content and users</li>
     <li>Credibility of online content</li>
     <li>Social network analysis; communities identification; expertise and authority discovery</li>
     <!--<li>Collaborative filtering</li>-->
     <li>Trust; reputation; recommendation systems</li>
     <li>Human computer interaction; social media tools; navigation and visualization</li>
     <li>Subjectivity in textual data; sentiment analysis; polarity/opinion identification and extraction, linguistic analyses of social media behavior</li>
     <li>Text categorization; topic recognition; demographic/gender/age identification</li>
     <li>Trend identification and tracking; time series forecasting</li>
     <li>Measuring predictability of real world phenomena based on social media, e.g., spanning politics, finance, and health</li>
     <li>New social media applications; interfaces; interaction techniques</li>
     <li>Engagement, motivations, incentives, and gamification.</li>
     <li>Social innovation and effecting change through social media</li>
     <li>Social media usage on mobile devices; location, human mobility, and behavior</li>
     <li>Organizational and group behavior mediated by social media; interpersonal communication mediated by social media</li>


  </ul>

  <h3>TYPES OF SOCIAL MEDIA INCLUDE</h3>
  <ul>
<li>Social networking sites (e.g., Facebook, LinkedIn)</li>
<li>Microblogs (e.g., Twitter, Tumblr)</li>
<li>Wiki-based knowledge sharing sites (e.g., Wikipedia)</li>
<li>Social news sites and websites of news media (e.g., Huffington Post)</li>
<li>Forums, mailing lists, newsgroups</li>
<li>Community media sites (e.g., YouTube, Flickr, Instagram)</li>
<li>Social Q & A sites (e.g., Quora, Yahoo Answers)</li>
<li>User reviews (e.g., Yelp, Amazon.com)</li>
<li>Social curation sites (e.g., Reddit, Pinterest)</li>
<li>Location-based social networks (e.g., Foursquare)</li>
  </ul>



<br><br>
<h5>Social sciences and sociophysics papers with optional publication: </h5>
<p>We will be continuing the 'social science and sociophysics' track at ICWSM-18 following its successful debut in 2013. This option is for researchers in social science and sociophysics who wish to submit full papers without publication in the conference proceedings. While papers in this track will not be published, we expect these submissions to describe the same high-quality and complete work as the main track submissions. Papers accepted to this track will be full presentations integrated with the conference, but they will be published only as abstracts in the conference proceedings.</p>



<a href="http://www.aaai.org/home.html" target="_blank"><img style="border: 0;" src="/2018/images/sponsors/aaai.png" alt="AAAI"></a>&nbsp;&nbsp;

 

<?php include("footer.php"); ?>?