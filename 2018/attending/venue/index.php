<?php $section = "Attending"; $subsection = "Venue"; include("../../header.php"); ?>

<br>
<br>

<h2 class="pageTitle">Host City : Cologne, Germany</h2>
<p>Founded more than 2000 years ago, Cologne is one of Central Europe's oldest cities. With more than one million inhabitants it is Germany's fourth largest city. Cologne hosts thirteen public and private universities. Its 72,000 students make it the most populous and also popular university town in Germany. The University of Cologne, partner of GESIS, and the University of Applied Sciences are the two largest of these institutions.</p>

<p>36 museums, more than 120 art galleries, 200 music ensembles, 60 theatres and numerous international festivals make Cologne one of Europe's leading cities for art and culture. The Roman heritage, a captivating array of Romanesque churches, the Cologne Cathedral (UNESCO World Heritage) and many other historic sites hold something for every taste - and of course the "K&ouml;lsch" both as an idiom and local brew complete what the city is most proud of: Cologne is a feeling!</p>

<br>

<h2 class="pageTitle">Conference Venue</h2>

<h3>Maternushaus - Tagungszentrum des Erzbistums K&ouml;ln</h3>
Kardinal-Frings-Str. 1-3<br>
50668 Cologne, Germany<br>
E-Mail : info(at)maternushaus.de<br>
Phone: +49 (221) 16 31 - 0<br>
Fax : +49 (221) 16 31 - 215<br>
<a href='www.maternushaus.de'>www.maternushaus.de</a>

<br><br>
<!--<center><IMG class="displayed" style="border : 0;" src="/2016/images/venue/maternus.jpg" alt="Maternushaus - Tagungszentrum des Erzbistums K&ouml;ln" align="middle"></center>-->

<br>
<p>Maternushaus is located in the center of Cologne within walking distance of the Central Station and GESIS's building. Two lecture halls and several bright and friendly seminar rooms, comprehensive technical infrastructure, plenty of exhibition space for posters, access for disabled attendees and inviting inner courtyards offer a singular atmosphere for ICWSM-16. </p>

<br>


<h3>GESIS - Leibniz Institute for the Social Sciences</h3>
Unter Sachsenhausen 6-8<br>
50667 Cologne<br>
Tel.: +49 (0)221-47694 0<br>
<a href='www.gesis.org'>www.gesis.org</a>

<br><br>
<center><IMG class="displayed" style="border : 0;" src="/2016/images/venue/GESISGebaeude.jpg" alt="Maternushaus - Tagungszentrum des Erzbistums K&ouml;ln" align="middle"></center>

<br>
<p>The building is located in the center of Cologne near the Cologne Cathedral. GESIS is the largest research-based infrastructure organization for the Social Sciences in Germany. The institute renders substantial, nationally and internationally relevant research-based infrastructure services. GESIS is organized in five scientific departments covering the whole range of empirical social research: Survey Design and Methodology, Monitoring Society and Social Change, Data Archive for the Social Sciences, Computational Social Science and Knowledge technologies for the Social Sciences. The institution has broad experience in organizing scientific events and strong ties with the social science community nationally and internationally.</p>


<!--<h2 class="pageTitle">ACM Web Science 2016</h2>
<h4>Hosted in Germany from May 22-25, 2016</h4>

<center><IMG class="displayed" style="border : 0;" src="/2016/images/venue/WebSci'16_No_dates-2-150.png" alt="Maternushaus - Tagungszentrum des Erzbistums K&ouml;ln" align="middle"></center>


<p><br> The 8th International <a href="http://www.websci16.org/" target=_blank>ACM Web Science Conference 2016</a> will be held from May 22 to May 25, 2016 in Hannover, Germany and is organized by L3S Research Center. More details can be found <a href="http://www.websci16.org/" target=_blank>here</a>.
</p>-->

<?php include("../../footer.php"); ?>
























