<?php $section = "Attending"; $subsection = "Getting Here"; include("../../header.php"); ?>

<br><br>
  <h2 class="pageTitle">Getting Here</h2>
  
<!-- <ul>
<li>Ann Arbor is 25 minutes by car from the Detroit airport (DTW).</li>
<li>Bus service is $12 each way. <a href='http://myairride.com/AirRide/Schedule'>http://myairride.com/AirRide/Schedule</a></li>
<li>Taxi service is $55-70 each way.</li>
<li>Car rentals are available at the airport, but not recommended if you stay at one of the housing options within 1km of the conference venue.</li>
<li>Parking is available in public lots a few blocks from the conference site, at a cost of $1.20/hour. The closest parking structure with public spaces is located on Maynard Street between Liberty and William (<a href='http://www.a2state.com/images/subpages/DDGwalkmap0407.jpg'>Parking Map</a>).</li>
 </ul>-->


<h3>Travel information</h3>


<p>Cologne is easily accessible via international air travel and by train.</p>

<h4>Via airports</h4>
<p>Cologne is very well connected via 3 international airports in the region: Cologne/Bonn Airport is a hub for numerous low-cost carriers and can be reached within 15mins from Cologne Central Station. Cologne is also within less than an hour's train ride from Frankfurt/Main (55mins from Frankfurt Airport station to Cologne Central Station directly) and Düsseldorf (40mins from Düsseldorf Airport station to Cologne Central Station directly) airports.</p>

<h4>Via high speed train</h4>
<p>The conference venue is within a 10 minute walk from Cologne Central Station. High-speed trains offer fast connections between Cologne and many German and European cities. The high-speed Thalys train connects Cologne with Brussels (1:47) and Paris (3:14); the ICE train directly connects Amsterdam and Utrecht with Cologne in 2:38 and 2:13 respectively.</p>


<h3> Map: How to get to GESIS and Maternushaus </h3>

<p>GESIS and Maternushaus are located in the heart of Cologne near Cologne Central Station and the famous Cologne cathedral. The buildings are approximately 500m and 850m respectively away from the main station and the cathedral (less than 1/2 a mile).</p>


<center><IMG class="displayed" style="border : 0;" src="/2016/images/venue/gesis_map.jpg" alt="Map" align="middle"></center>


  
<?php include("../../footer.php"); ?>