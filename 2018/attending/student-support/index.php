<?php $section = "Attending"; $subsection = "Student Support"; include("../../header.php"); ?>

<h2>ICWSM-16 Student Grant Program</h2>

<p>ICWSM and AAAI are pleased to announce the availability of a small fund to help support student travel to ICWSM-16 in Cologne, Germany. ICWSM-16 student grants provide modest travel support for students who are full-time undergraduate or graduate students at colleges and universities; are members of AAAI; have an accepted paper to the conference program or are participating in another way (workshops, demos); and submit scholarship applications to AAAI by March 25, 2016. In the event that scholarship applications exceed available funds, preference will be given to students who have an accepted technical paper, and then to students who are actively participating in the conference in some way.</p>
 
<p>As part of the requirements for accepting a grant, students may be asked to assist ICWSM organizers onsite in Cologne for a few hours, May 17-20. A very small number of complimentary technical program registrations will be available for students who are asked to volunteer during the conference.</p>
 
<p>To apply for the ICWSM-16 Student Grant Program, please complete the application below, and return it to AAAI no later than April 1, 2016. Notifications will be sent by April 15, and grants will be issued upon receipt of expense report and receipts after the conference. Please note that the awards will be small, and are intended as only a subsidy of a student's airfare. Students should plan on securing the bulk of their funding from another source.</p>
 
<p>Submit form by email attachment to <a href="mailto:icwsm16@aaai.org">icwsm16@aaai.org</a>, by fax to +1 650-321-4457, or mail to: ICWSM-16 Student Grants, c/o AAAI, 2275 East Bayshore Road, Suite 160, Palo Alto, CA 94303.</p>

<b><a href="../../docs/misc/ICWSM_16_Student_Grant_App.pdf">Download Application.</a></b>


<?php include("../../footer.php"); ?>