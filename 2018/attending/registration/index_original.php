<?php $section = "Attending"; $subsection = "Registration"; include("../../header.php"); ?>

<h2 class="pageTitle">Registration</h2>

<p><b> Online registration is available at <a href='https://www.regonline.com/icwsm16'>https://www.regonline.com/icwsm16</a>. </b> </p>

<p>The ICWSM-16 technical conference registration fee includes admission to all technical sessions, invited talks, lunch each day of the main conference, the opening reception, the poster/demo reception, and access to the electronic version of the ICWSM-16 Conference Proceedings.</p>

<h3>Registration Fees (all in USD)</h3>

<table border=1 cellpadding=0 cellspacing=0 width=600 style='border-collapse:
 collapse;table-layout:fixed'>
 <tr height=45>
  <td height=28 width=110></td>
  <td width=110 align="center"><b>Early <br> (by April 1)</b></td>
  <td width=110 align="center"><b>Late <br> (by April 22)</b></td>
  <td width=110 align="center"><b>Onsite <br> (after April 22)</b></td>
 </tr>
 <tr height=28>
  <td height=28 width=110 align="center"><b>AAAI Member</b></td>
  <td width=110 align="center">550$</td>
  <td width=110 align="center">590$</td>
  <td width=110 align="center">650$</td>
 </tr>
 <tr height=28>
  <td height=28 width=110 align="center"><b>AAAI Student Member</b></td>
  <td width=110 align="center">310$</td>
  <td width=110 align="center">345$</td>
  <td width=110 align="center">390$</td>
 </tr>
 <tr height=28>
  <td height=28 width=110 align="center"><b> Nonmember </b></td>
  <td width=110 align="center">655$</td>
  <td width=110 align="center">695$</td>
  <td width=110 align="center">750$</td>
 </tr>
 <tr height=28>
  <td height=28 width=110 align="center"><b>Student Nonmember</b></td>
  <td width=110 align="center">360$</td>
  <td width=110 align="center">395$</td>
  <td width=110 align="center">440$</td>
 </tr>
<!-- <tr height=28>
  <td height=28 width=110 align="center"><b>Regular Silver*</b></td>
  <td width=110 align="center">599$</td>
  <td width=110 align="center">629$</td>
  <td width=110 align="center">679$</td>
 </tr>
 <tr height=28>
  <td height=28 width=110 align="center"><b>Student Silver*</b></td>
  <td width=110 align="center">355$</td>
  <td width=110 align="center">390$</td>
  <td width=110 align="center">435$</td>
 </tr>
 <tr height=28>
  <td height=28 width=110 align="center"><b>Platinum**</b></td>
  <td width=110 align="center">645$</td>
  <td width=110 align="center">675$</td>
  <td width=110 align="center">725$</td>
 </tr>-->
</table>
<br>
<!--<p>*Includes discounted conference registration, plus a one-year online new membership in AAAI.<br>
**Includes discounted conference registration, plus a one-year new or renewal membership in AAAI.</p>-->


<br>
<h4>Silver Membership</h4>

<p>Includes discounted conference registration, plus a one-year online new membership in AAAI. </p>

<table border=1 cellpadding=0 cellspacing=0 width=600 style='border-collapse:
 collapse;table-layout:fixed'>
 <tr height=45>
  <td height=28 width=110></td>
  <td width=110 align="center"><b>Early <br> (by April 1)</b></td>
  <td width=110 align="center"><b>Late <br> (by April 22)</b></td>
  <td width=110 align="center"><b>Onsite <br> (after April 22)</b></td>
 </tr>
<tr height=28>
  <td height=28 width=110 align="center"><b>Regular Silver</b></td>
  <td width=110 align="center">649$</td>
  <td width=110 align="center">689$</td>
  <td width=110 align="center">749$</td>
</tr>
<tr height=28>
  <td height=28 width=110 align="center"><b>Student Silver</b></td>
  <td width=110 align="center">359$</td>
  <td width=110 align="center">394$</td>
  <td width=110 align="center">439$</td>
 </tr>
</table>
<br>
<br>

<h4>Workshop/Tutorial Day Fee</h4>

<p>ICWSM-16 workshops and tutorials will be held May 17, just prior to the technical conference. The Workshop/Tutorial Day fee includes participation in any combination of workshops and/or tutorials on May 17. Participants should not sign up for concurrent events, so please consult the schedule carefully before making your selections. There is no additional fee for Workshop 9, the Science Slam.
<br>

Tutorial Information: <a href="http://www.icwsm.org/2016/program/tutorial/">http://www.icwsm.org/2016/program/tutorial/</a> <br>
Workshop Information: <a href="http://www.icwsm.org/2016/program/workshop/">http://www.icwsm.org/2016/program/workshop/</a> <br>
<br>
<br>
<b>Workshop / Tutorial fees:</b>
<br>
Regular    100$
<br>
Student    75$
<br>

<!--For complete workshop information, please see the <a href="http://www.icwsm.org/2016/program/workshop/">Workshop page</a>.

<br>
<br>
<b>Full Day:</b>
<br>
W2: Digital Placemaking: Augmenting Physical Places with Contextual Social Data
<br>
Germaine Halegoua, Raz Schwartz and Ed Manley
<br>
<br>
W4: Religion on Social Media
<br>
Yelena Mejova, Ingmar Weber, Lu Chen and Adam Okulicz-Kozaryn
<br>
<br>
W6: Standards and Practices in Large-Scale Social Media Research
<br>
Derek Ruths and Juergen Pfeffer
<br>
<br>
W7: Wikipedia, a Social Pedia: Research Challenges and Opportunities
<br>
Robert West, Leila Zia and Jure Leskovec
<br>
<br>
<b>Half Day:</b>
<br>
<br>
W1: Auditing Algorithms From the Outside: Methods and Implications
<br>
Mike Ananny, Karrie Karahalios, Christian Sandvig and Christo Wilson
<br>
<br>
W3: Modeling and Mining Temporal Interactions
<br>
Bruno Gon?alves, M?rton Karsai and Nicola Perra
<br>
<br>
<b>Evening:</b>
<br>
<br>
W5: The ICWSM Science Slam
<br>
David Garcia, Ingmar Weber and Aniko Hannak
</p>-->

<h4>Opening Reception Guest</h4>
<p>The ICWSM-16 Opening Reception will be held on Wednesday, May 18. Hearty Hors d'oeuvres, wine, beer, and soft drinks will be available. Your admittance to the Opening Reception is included in your conference registration. Guests are welcome to attend for an additional fee. 
<br>
<br>
Opening Reception Guest 55.00$</p>

<h4>Poster/Demo Session Guest</h4>
<p> The ICWSM-16 evening poster/demo session will be held Thursday, May 19. Light refreshments, and a choice of beer or soft drinks will be available. Your admittance to the Poster/Demo Session is included in your conference registration. Guests are welcome to attend for an additional fee.
<br>
<br>
Poster/Demo Session Guest 50.00$</p>



<h4>ICWSM-16 Hard Copy Proceedings</h4>
<p>Preregistrants may reserve a hard copy of the ICWSM-16 proceedings, which will be mailed after the conference. The preconference discounted price will be determined in early summer. Shipping and handling will be added. Individuals who select this option will be contacted regarding the final cost after the conference.</p>

<h4>Registration / Proof of Student Status</h4>
<p>To register online, please complete the form at <a href='https://www.regonline.com/icwsm16'>https://www.regonline.com/icwsm16</a>. If you qualify for student rates, please submit your proof of student status to AAAI by email attachment to <a href="mailto:icwsm16@aaai.org">icwsm16@aaai.org</a> or by fax to +1-650-321-4457.</p>

<h4>Refund Requests</h4>
<p>The deadline for refund requests is April 29, 2016. All refund requests must be made in writing to AAAI at <a href="mailto:icwsm16@aaai.org">icwsm16@aaai.org</a>. A $100.00 processing fee will be assessed for all refunds.</p>

<h4>Visa Information</h4>
<p>Letters of invitation can be requested by accepted ICWSM-16 authors or registrants with a completed registration with payment. If you are attending ICWSM-16 and require a letter of invitation, please send the following information to <a href="mailto:icwsm16@aaai.org">icwsm16@aaai.org</a>:
<ul>
<li>First/Given Name:
</li>
<li>Family/Last Name:
</li>
<li>Position:
</li>
<li>Organization:
</li>
<li>Department:
</li>
<li>City:
</li>
<li>Zip/Postal Code:
</li>
<li>Country:
</li>
<li>Email:
</li>
<li>Are you an author of a paper?
</li>
<li>Paper title:
</li>
<li>If not accepted author, Registration Confirmation ID#:
</li>
<li>Email address where the letter should be sent (if different from above):
</li>
</ul>
</p>

<h4>Onsite Registration</h4>
<p>Preregistration is strongly encouraged via the online form due to limited facilities onsite. Onsite registration will be held in the foyer of Maternushaus - Tagungszentrum des Erzbistums Köln, Kardinal-Frings-Str. 1-3, 50668 Cologne, Germany, (<a href="www.maternushaus.de">www.maternushaus.de</a>). All attendees must pick up their registration packets for admittance to programs.
<br>
<br>
Registration hours are scheduled for Tuesday, May 17, 8:00am - 2:00pm, and Wednesday and Thursday, 8:30am - 5:00pm, and Friday, May 20, 8:30am - 11:00am. These hours are subject to change depending on the final program.</p>

<?php include("../../footer.php"); ?>?