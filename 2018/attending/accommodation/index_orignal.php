<?php $section = "Attending"; $subsection = "Accommodation"; include("../../header.php"); ?>

  <h2 class="pageTitle">Accommodation</h2>

  <p>ICWSM has reserved a block of rooms at the conference venue and nearby hotels at reduced conference rates. Space is limited and attendees are encouraged to make their reservations early. (Information about student accommodations is available by writing to <a href="mailto:aaai16@aaai.org">aaai16@aaai.org</a>. Please attach proof of full-time status.)
</p>


<h3>ICWSM-16 Conference Venue/Hotel</h3>

<h4>Maternushaus - Tagungszentrum des Erzbistums K&ouml;ln</h4>
<ul>
<li>Address : Kardinal-Frings-Str. 1-3 <br>
50668 Cologne, Germany<br>
</li>
<li>E-Mail Reservation: info(at)maternushaus.de
<li>Phone: +49 (221) 16 31 - 0<br></li>
<li>Fax : +49 (221) 16 31 - 215</li>
<li>web : <a href="www.maternushaus.de">www.maternushaus.de</a></li>
</ul>
<br>

<p>Conference Rates available: May 16 - May 20, 2016<br>
Reservation Reference: ICWSM<br>
Superior Single/Double Rooms: 72.00 EUR/night per room incl. breakfast plus applicable state and local taxes<br>
Premium Single/Double Rooms: 87.00 EUR/night per room incl. breakfast plus applicable state and local taxes<br>
Check-in: 3:00 pm<br>
Check-out: 10:00 am<br>
Cut-off date for conference rate reservations: Wednesday, April 6, 2016</p>

<p>Reservation requests received after the cut-off date will be based on availability at the Hotel's prevailing rates. Maternushaus accepts Visa and American Express. Please contact Maternushaus for cancellation policy.</p>





<h3>Other Accommodations</h3>

<p>There is a large array of hotels close to the venues. As Cologne hosts numerous international trade fairs, lodging options are available but hotels in the city center can be fully booked at early times. We highly recommend that you book your accommodation as soon as you know the dates for your stay in Cologne!</p>

<p>ICWSM has reserved a block of rooms at reduced conference rates at the following hotels. Attendees must contact the hotels directly and the use the appropriate reservation method in order to secure a room.</p>


<h4>HOPPER HOTEL ST. ANTONIUS</h4>
<ul>
<li>Address: Dagobertstrasse 32, 50668 Cologne</li>
<li>Phone: +49-221-1660-166</li>
<li>Email Reservation: st.antonius(at)hopper.de</li>
</ul>
<br>

<p>Conference Rates available: 05/16/16 - 05/20/16 <br>
Reservation Reference: GESIS <br>
Single Occupancy: &euro; 85/night incl. breakfast, plus applicable state and local taxes <br>
Check-in: 3:00pm <br>
Check-out: noon <br>
Cancellation Policy: 48 hours in advance <br>
Cut-Off Date: Saturday, April 16, 2016 <br>
Distance to GESIS and Maternushaus: 12 min walk, 0,6 mile</p>

<h4>LINDNER HOTEL DOM RESIDENCE</h4>
<ul>
<li>Address: Stolkgasse / An den Dominikanern 4a, 50668 Cologne</li>
<li>Phone: +49-221-44755-401</li>
<li>Email Reservation: info.domresidence(at)Lindner.de</li>
</ul>
<br>

<p>Conference Rates available: 05/16/16 - 05/20/16<br>
Reservation Reference: GESIS<br>
Single Occupancy: &euro; 119/night incl. breakfast, plus applicable state and local taxes<br>
Check-in: 3:00pm <br>
Check-out: 10:00am <br>
Cancellation Policy: 72 hours in advance <br>
Cut-Off Date: Saturday, April 16, 2016 <br>
Distance to GESIS and Maternushaus: 6 min walk, 0,3 mile</p>

<p>The City of Cologne generally charges an extra tax of 5% on the room accommodation rate. However, travelers who have to stay overnight for essential professional or business-related reasons are exempted from this tax. You can save money by handing in the filled in exemption request form (357 KB) to a hotel employee at the check-in upon your arrival.</p>



<?php include("../../footer.php"); ?>