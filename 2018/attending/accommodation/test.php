<?php $section = "Attending"; $subsection = "Accommodation"; include("../../header.php"); ?>

  <h2 class="pageTitle">Accommodation</h2>

  <p>AAAI has reserved a block of rooms at reduced conference rates at the five hotels listed below, as well as on-campus accommodations at MIT (see below). Attendees must contact the hotels directly and the use the appropriate reservation method in order to secure a room.
<br>
<br>
<b>Please note that the Boston/Cambridge area is very busy in July, so we encourage you to make your reservations early. Space is limited.</b>
</p>
  

<h4>Boston Marriott Cambridge</h4>
<ul class="ul-plain">
<li>Address: Two Cambridge Center, 50 Broadway, Cambridge, MA 02142</li>
<li>Phone: 1-800-228-9290 or 1-617-494-6600</li>
<li>Online Reservation: Book Standard Room at Boston Marriott Cambridge (<a href='http://www.marriott.com/hotels/travel/boscb-boston-marriott-cambridge/?toDate=7/11/13&groupCode=m1lm1la&stop_mobi=yes&fromDate=7/7/13&app=resvlink'>Website</a>)</li>
<li>Conference Rates available: 07/07/13 - 07/10/13</li>
<li>Reservation Reference: ICWSM-13</li>
<li>Single/Double Occupancy: $245/night, plus applicable state and local taxes</li>
<li>Cut-Off Date: Friday, June 7, 2013</li>
<li>Distance to Kresge Auditorium: 0.8 miles/~16-minute walk</li>
</ul>
<br>
<br>
<br>
<h4>Boston Park Plaza</h4>
<ul class="ul-plain">
<li>Address: 50 Park Plaza at Arlington Street, Boston, MA 02116</li>
<li>Phone: 1-800-225-2008 or 1-617-426-2000</li>
<li>Online Reservation: <a href='http://www.bostonparkplaza.com/'>http://www.bostonparkplaza.com/</a></li>
<li>Conference Rates available: 07/07/13 - 07/10/13</li>
<li>Reservation Reference: ICWSM-13 (AAAI Conference on Weblogs and Social Media)</li>
<li>Single/Double Occupancy: $189/night, plus applicable state and local taxes</li>
<li>Triple Occupancy:  $209/night, plus applicable state and local taxes</li>
<li>Cut-Off Date: Friday, June 7, 2013 (5:00 PM Boston time)</li>
<li>Distance to Kresge Auditorium: 1.9 miles/~23 minutes by 'T'/walking</li>
</ul>
<br>
<br>
<br>
<h4>Cambridge Residence Inn</h4>
<ul class="ul-plain">
<li>Address: 6 Cambridge Center, Cambridge, Massachusetts 02142</li>
<li>Phone: 1-800-331-3131</li>
<li>Online Reservations: <a href='http://www.marriott.com/hotels/travel/boscm?groupCode=icwicwA&app=resvlink&fromDate=7/8/13&toDate=7/12/13'>www.marriott.com </a></li>
<li>Conference Rates available: 07/07/13 - 07/12/13</li>
<li>Reservation Reference: ICWSM-13 (AAAI)</li>
<li>One Bedroom Suite: $259/night, plus applicable state and local taxes</li>
<li>Cut-Off Date: Friday, June 7, 2013</li>
<li>Distance to Kresge Auditorium: 0.7 miles/~15-minute walk</li>
</ul>
<br>
<br>
<br>
<h4>Hotel Marlowe</h4>
<ul class="ul-plain">
<li>Address: 25 Edwin H. Land Blvd, Cambridge, MA 02141</li>
<li>Phone: 617-868-8000</li>
<li>Phone Reservations: (617) 772-5838</li>
<li>Online Reservations: <a href='https://gc.synxis.com/rez.aspx?Hotel=26746&Chain=10179&arrive=7/7/2013&depart=7/12/2013&adult=1&child=0&group=11290303632'>gc.synxis.com</a></li>
<li>Website: http://www.hotelmarlowe.com/</li>
<li>Conference Rates available: 07/07/13 - 07/10/13</li>
<li>Reservation Reference: ICWSM-13</li>
<li>Single/Double Occupancy: $209/night, plus applicable state and local taxes</li>
<li>Cut-Off Date: Friday, June 7, 2013</li>
<li>Distance to Kresge Auditorium: 1.4 miles/~30-minute walk</li>
</ul>
<br>
<br>
<br>
<h4>Kendall Hotel</h4>
<ul class="ul-plain">
<li>Address: 350 Main Street, Cambridge, MA 02142</li>
<li>Phone: 866 566-1300 or 617-577-1300</li>
<li>Online Reservations: <a href='http://kendallhotel.com'>http://kendallhotel.com</a></li>
<li>Promo Code: ICWSM</li>
<li>Conference Rates available: 07/07/13 - 07/10/13</li>
<li>Single/Double Occupancy: $245/night, plus applicable state and local taxes*</li>
<li>Cut-Off Date: Friday, June 6, 2013</li>
<li>Distance to Kresge Auditorium: 0.6 miles/~13-minute walk</li>
<p>*Rates include breakfast, internet access, hosted wine hour, and pass to CAC Fitness Center</p>
</ul>






<h4>MIT On-Campus Housing</h4>
<ul class="ul-plain">
<li>Information/Reservations: <a href='http://mit.edu/conferences/www/ICWSM/ICWSM_dorm_info.html'>mit.edu/conferences/www/ICWSM/ICWSM_dorm_info.html</a></li>
<li>Deadline for Reservations: June 30, 2013 (space is very limited)</li>
<li>Conference Rates available: 07/07/13 - 07/10/13</li>
<li>Single rooms: $86.00/night</li>
<li>Single accommodations are available in air-conditioned residence halls. Rooms with private baths are not available. Shared bathroom facilities are available on every floor. All room types have a twin-size (single) bed, a desk and chair, and a closet. Please refer to the link above for complete information.</li>
<li>Check-in time: 3:00 PM</li>
<li>Check-out time: 11:00 AM</li>
<li><b>Cancellation:</b> All cancellations of on-campus housing must be sent in writing (via fax or e-mail) to the MIT Conference Services Office. Written cancellations received on or before June 30, 2013 will receive a full refund (excluding any parking fees). After that date, refunds will be made less a penalty equivalent to the cost of one night's stay; provided the cancellation is received no later than 24 hours prior to the scheduled date of arrival. No refunds (full or partial) will be granted for cancellations received within 24 hours of the scheduled arrival date or thereafter, nor for any nights the dormitory room is not occupied during a reservation period.</li>









<?php include("../../footer.php"); ?>