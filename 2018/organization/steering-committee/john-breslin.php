<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "John Breslin"; include("../../header.php"); ?>

  <h2 class="pageTitle">John Breslin</h2>

  <div class="image"><img src="/2012/images/organisation/john-breslin.png" alt="John Breslin"></div>

  <p>John Breslin is a lecturer in Engineering and Informatics at <a href="http://www.nuigalway.ie/">NUI Galway</a>, and is a lead researcher with <a href="http://www.deri.ie/">DERI</a>, the world's largest Semantic Web research institute. He is co-author of the book "The Social Semantic Web", published by Springer in 2009. He is creator of the <a href="en.wikipedia.org/wiki/SIOC">SIOC</a> framework which aims to interlink online communities with semantics, used in Yahoo! SearchMonkey, Drupal 7 and the Newsweek website.</p>

  <p>In 1998, John set up an Internet forum to discuss video games which evolved into <a href="http://boards.ie">boards.ie</a>, one of Ireland's largest indigenous websites with over 35M monthly page views and 2.25M monthly visitors in 2011. He serves as a non-executive director with boards.ie Ltd., and is co-founder of its spin-off company Adverts Marketplace Ltd. He has received a number of awards for his web work, including a Golden Spider award and two IIA Net Visionary awards.</p>

  <p>In 2010, John founded <a href="http://newtechpost.com/">New Tech Post</a>, an online publisher of daily articles on emerging technologies. In 2011, John co-founded <a href="http://streamglider.com/">StreamGlider</a>, a visual real-time dashboard for tracking interests across various types of devices, along with Nova Spivack and Bill McDaniel. He is also an advisor to some social media companies including CrowdGather LLC and Social Bits. He has been interviewed or cited by various media sources including New Scientist, Computer, PC World, The Sunday Times and RT�.</p>

<?php include("../../footer.php"); ?>
