<?php $section = "Organisation"; $subsection = "Steering Commmittee"; $subsubsection = "Matthew Hurst"; include("../../header.php"); ?>

  <h2 class="pageTitle">Matthew Hurst</h2>

  <div class="image"><img src="/2012/images/steering-committee/matthew-hurst.png" alt="Matthew Hurst"></div>

  <p>Matthew Hurst is has been involved in the world of social media analysis since the dawn of the century
when he worked at Intelliseek on text and data mining methods for businesses interested in seeing
what the web was saying about them.</p>

  <p>He co-created BlogPulse.com, one of the first blog search engines, 
with Natalie Glance and later went on to co-found the International Conference on Weblogs and Social Media
after a number of successful WWW workshops. Currently, he works for Microsoft where he leads a team
mining the web for local search applications.</p>

<?php include("../../footer.php"); ?>
