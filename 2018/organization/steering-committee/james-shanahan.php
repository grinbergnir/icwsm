<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "James G. Shanahan"; include("../../header.php"); ?>

  <h2 class="pageTitle">James G. Shanahan</h2>

  <div class="image"><img src="/2012/images/organisation/james-shanahan.png" alt="James G. Shanahan"></div>

  <p>Jimi has spent the last 22 years developing and researching cutting-edge information management systems that harness machine learning, information retrieval, and linguistics. During the summer of 2007, he started a boutique consultancy (Church and Duncan Group Inc., in San Francisco) whose major goal is to help companies leverage their vast repositories of data using statistics, machine learning, optimisation theory and data mining for big data applications (billions of examples) in areas such as web search, local and mobile search, and online advertising. Church and Duncan Group's clients include Adobe, AT&T Interactive, Digg.com, eBay, SearchMe.com, Ancestry.com, MyOfferPal.com, and SkyGrid.com. In addition, Jimi has been affiliated with the University of California at Santa Cruz since 2009. He advises several high-tech startups in the Silicon Valley Area and is executive VP of science and technology at the Irish Innovation Center (IIC). He has served as a fact and expert witness.</p>

  <p>Prior to starting Church and Duncan Group Inc., Jimi was Chief Scientist and executive team member at Turn Inc. (an online ad network that has recently morphed to a demand-side platform).  Prior to joining Turn, Jimi was Principal Research Scientist at Clairvoyance Corporation where he led the "Knowledge Discovery from Text" Group. Before that he was a Research Scientist at Xerox Research Center Europe (XRCE) where he co-founded Document Souls. In the early 90s, he worked on the AI Team within the Mitsubishi Group in Tokyo.</p>

  <p>He has published six books, over 50 research publications, and 15 patents in the areas of machine learning and information processing. Jimi chaired CIKM 2008 (Napa Valley) and co-chaired the International Conference on Weblogs and Social Media (ICWSM) 2011 in Barcelona. He is a regularly invited to give talks at international conferences and universities around the world. Jimi received his PhD in engineering mathematics from the University of Bristol, UK and holds a bachelor of science degree from the University of Limerick, Ireland. He is a Marie Curie fellow and member of IEEE and ACM. In 2011 he was selected as a member of the Silicon Valley 50 (Top 50 Irish Americans in Technology).</p>

<?php include("../../footer.php"); ?>
