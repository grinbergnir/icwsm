<?php $section = "Organisation"; $subsection = "Steering Commmittee"; $subsubsection = "Eytan Adar"; include("../../header.php"); ?>

  <h2 class="pageTitle">Eytan Adar</h2>

  <div class="image"><img src="/2012/images/steering-committee/eytan-adar.png" alt="Eytan Adar"></div>

  <p>Eytan Adar is an Assistant Professor of Information and Computer Science at the University of Michigan. He works on temporal-informatics which is the study of the change of information - and our consumption of it - over time. This is primarily at Internet scales, and he therefore also works on text and log mining, visualizations, social network analysis, etc. He was previously a researcher at HP Labs' Information Dynamics Group and at Xerox PARC.</p>

  <p>Among other things, Eytan is associated with the MISC and MIDAS groups (HCI and data mining, respectively). With Jaime Teevan, he is also running WSDM 2012 which will take place in Seattle.</p>

<?php include("../../footer.php"); ?>
