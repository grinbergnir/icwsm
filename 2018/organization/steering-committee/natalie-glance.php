<?php $section = "Organisation"; $subsection = "Steering Commmittee"; $subsubsection = "Natalie Glance"; include("../../header.php"); ?>

  <h2 class="pageTitle">Natalie Glance</h2>

  <div class="image"><img src="/2012/images/steering-committee/natalie-glance.png" alt="Natalie Glance"></div>

  <p>Natalie S. Glance is a software engineer and manager at Google working on making online shopping a social activity. A physicist by training, her research interests have landed her somewhere in the intersection of social networks, information extraction and discovering global behavior from local actions. In recent years, her research focused on mining consumer sentiment and buzz from online social media. Previous to Google, Natalie has held research scientist appointments at BuzzMetrics (formerly, Intelliseek) from 2002-2007, WhizBang! Labs (2000-2002) and Xerox Research Centre Europe (1994-2000).</p>

  <p>Natalie has published over 30 peer-reviewed papers in the areas of text mining, recommender systems, and mulit-agent systems. She holds 12 U.S. patents. She has been co-chair/organizer for ICWSM 2007-2009 and has organized several workshops including WWW Workshop on the Weblogging Ecosystem (2004-2006), HICSS'2000 Workshop on Knowledge Ecologies, and Spring Days 2000 Workshop on People Based Networking. She is on the program committee and/or a reviewer for a number of conferences, workshops and journals, including: KDD, CEAS, ACL, WWW, WSDM, CIKM, and AIRWeb.</p>

<?php include("../../footer.php"); ?>
