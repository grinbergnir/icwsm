<?php $section = "Organisation"; $subsection = "Program Committee"; include("../../header.php"); ?>

<h2 class="pageTitle">ICWSM-18 Program Committee</h2>


<h3>Program Committee Members</h3>

<ul>
	<li> Abigail Jacobs (University of California, Berkeley) </li>
	<li> Afra Mashhadi (Bell Labs) </li>
	<li> Aixin Sun (Nanyang Technological University) </li>
	<li> Alejandro Jaimes (Aicure) </li>
	<li> Alessandro Epasto (Google) </li>
	<li> Alex Dow (Facebook) </li>
	<li> Alex Hanna (University of Toronto) </li>
	<li> Alexandra Olteanu (IBM) </li>
	<li> Allison Chaney (Princeton University) </li>
	<li> Amanda Hughes (Utah State University) </li>
	<li> Amy Zhang (Massachusetts Institute of Technology) </li>
	<li> Ángel Cuevas (Universidad Carlos III de Madrid) </li>
	<li> Anne Oeldorf-Hirsch (University of Connecticut) </li>
	<li> Arkadiusz Stopczynski (Technical University of Denmark) </li>
	<li> Arkaitz Zubiaga (The University of Warwick) </li>
	<li> Aziz Mohaisen (University of Central Florida) </li>
	<li> Baichuan Zhang (Indiana University Purdue University Indianapolis) </li>
	<li> Baoxu Shi (Notre Dame)</li>
	<li> Bamshad Mobasher (DePaul University) </li>
	<li> Bimal Viswanath (MPI-SWS) </li>
	<li> Blaine Robbins (New York University) </li>
	<li> Bongwon Suh (Seoul National University) </li>
	<li> Camille Roth (Sciences Po) </li>
	<li> Casey Fiesler (University of Colorado Boulder) </li>
	<li> Changtao Zhong (King's College London) </li>
	<li> Chaya Hiruncharoenvate (Mahasarakham University) </li>
	<li> Cheng Li (Google) </li>
	<li> Cheng-Te Li (National Cheng Kung University) </li>
	<li> Chengkai Li (University of Texas at Arlington) </li>
	<li> Chenhao Tan (University of Colorado Boulder) </li>
	<li> Christian Bauckhage (Fraunhofer) </li>
	<li> Christopher Homan (Rochester Institute of Technology) </li>
	<li> Cindy Chung (Intel) </li>
	<li> Ciro Cattuto (ISI Foundation) </li>
	<li> Claudia Hauff (Delft University of Technology) </li>
	<li> Claudia Lopez (Universidad Tecnica Federico Santa Maria) </li>
	<li> Claudio Lucchese (ISTI-CNR) </li>
	<li> Cody Buntain (University of Maryland) </li>
	<li> Conor Hayes (Insight Centre for Data Analytics, NUI Galway) </li>
	<li> Cristian Lumezanu (NEC Laboratories) </li>
	<li> Daniel Gatica-Perez (Idiap and EPFL) </li>
	<li> Defu Lian (University of Electronic Science and Technology of China) </li>
	<li> Diego Saez-Trumper (Wikimedia) </li>
	<li> Dietmar Jannach (AAU Klagenfurt) </li>
	<li> Dmytro Karamshuk (King's College London) </li>
	<li> Dongwon Lee (The Pennsylvania State University) </li>
	<li> Edward Platt (University of Michigan School of Information) </li>
	<li> Ee-Peng Lim (Singapore Management University) </li>
	<li> Elizabeth M. Daly (IBM) </li>
	<li> Emiliano De Cristofaro (University College London) </li>
	<li> Emilio Zagheni (University of Washington) </li>
	<li> Emma Spiro (University of Washington) </li>
	<li> Eni Mustafaraj (Wellesley College) </li>
	<li> Evaggelia Pitoura (Univ. of Ioannina) </li>
	<li> Fabian Fl&ouml;ck (GESIS Cologne) </li>
	<li> Feida Zhu (Singapore Management University) </li>
	<li> Francesco Gullo (UniCredit) </li>
	<li> Frank Schweitzer (ETH Zurich) </li>
	<li> Fred Morstatter (University of Southern California) </li>
	<li> Gang Wang (Virginia Tech) </li>
	<li> Gareth Tyson (Queen Mary University of London) </li>
	<li> Geert-Jan Houben (Delft University of Technology) </li>
	<li> Giancarlo Ruffo (Universita' di Torino) </li>
	<li> Gianluca Demartini (The University of Queensland) </li>
	<li> Gianmarco De Francisci Morales (Qatar Computing Research Institute) </li>
	<li> Hady Lauw (Singapore Management University) </li>
	<li> Haiko Lietz (GESIS - Leibniz Institute for the Social Sciences) </li>
	<li> Hanghang Tong (City College, CUNY) </li>
	<li> Hao Ma (Microsoft) </li>
	<li> Hemant Purohit (George Mason University) </li>
	<li> Hirozumi Yamaguchi (Osaka University) </li>
	<li> Hsun-Ping Hsieh (National Cheng Kung University) </li>
	<li> Huan Liu (Arizona State University) </li>
	<li> Hui Fang (University of Delaware) </li>
	<li> Huiji Gao (Arizona State University) </li>
	<li> Ilias Leontiadis (Telefonica Research) </li>
	<li> Isabella Peters (ZBW) </li>
	<li> Jacob Eisenstein (Georgia Institute of Technology) </li>
	<li> Jack Hessel (Cornell University) </li>
	<li> Jae-Gil Lee (Korea Advanced Institute of Science and Technology) </li>
	<li> Jaime Settle (College of William & Mary) </li>
	<li> James Caverlee (Texas A&M University) </li>
	<li> Jana Diesner (University of Illinois at Urbana-Champaign) </li>
	<li> Jed Brubaker (University of Colorado Boulder) </li>
	<li> Jeremy Blackburn (University of Alabama at Birmingham) </li>
	<li> Jesse Chandler (Mathematica Policy Research) </li>
	<li> Jiebo Luo (University of Rochester) </li>
	<li> Jiliang Tang (Michigan State University) </li>
	<li> Jisun An (Qatar Computing Research Institute, Hamad Bin Khalifa University) </li>
	<li> Joel Nishimura (Arizona State University) </li>
	<li> Jong Gun Lee (United Nations Global Pulse) </li>
	<li> Juhi Kulshrestha (MPI-SWS) </li>
	<li> Jussara Almeida (UFMG) </li>
	<li> Juyong Park (Graduate School of Culture Technology, Korea Advanced Institute of Science and Technology) </li>
	<li> Kate Niederhoffer (dachis group) </li>
	<li> Katharina Kinder-Kurlanda (GESIS Leibniz Institute for the Social Sciences) </li>
	<li> Katrin Weller (GESIS - Leibniz Institute for the Social Sciences) </li>
	<li> Kavé Salamatian (Universite de Savoie) </li>
	<li> Kevin Munger (New York University) </li>
	<li> Kiran Lakkaraju (Sandia National Labs) </li>
	<li> Komal Kapoor (University of Minnesota) </li>
	<li> Kristen Altenburger (Stanford University) </li>
	<li> Kristina Lerman (University of Southern California) </li>
	<li> Kunwoo Park (Divison of Web Science, KAIST) </li>
	<li> Kyumin Lee (Worcester Polytechnic Institute) </li>
	<li> Lei Zhang (ADOBE SYSTEMS INC) </li>
	<li> Leto Peel (Universite catholique de Louvain) </li>
	<li> Lexing Xie (Australian National University) </li>
	<li> Liz Liddy (Center for Natural Language Processing, Syracuse University) </li>
	<li> Lu Wang (Northeastern University) </li>
	<li> Lu Chen (LinkedIn) </li>
	<li> Ludovico Boratto (Eurecat) </li>
	<li> Lyle Ungar (University of Pennsylvania) </li>
	<li> Manuel Gomez Rodriguez (MPI for Software Systems) </li>
	<li> Marco Pellegrini (Institute for Informatics and Telematics of C.N.R.) </li>
	<li> Marco Brambilla (Politecnico di Milano) </li>
	<li> Maria Glenski (Notre Dame) </li>
	<li> Marina Litvak (BGU) </li>
	<li> Marina Kogan (University of Colorado Boulder) </li>
	<li> Marios Belk (Cognitive UX GmbH) </li>
	<li> Mark Keane (UCD Dublin) </li>
	<li> Marko Skoric (City University of Hong Kong) </li>
	<li> Markus Luczak-Roesch (Victoria University of Wellington) </li>
	<li> Matteo Manca (Eurecat (Technological Center of Catalunya)) </li>
	<li> Matteo Magnani (Uppsala University) </li>
	<li> Mauro Sozio (Telecom ParisTech) </li>
	<li> Michael Mathioudakis (INSA Lyon & LIRIS CNRS) </li>
	<li> Michael Gamon (Microsoft) </li>
	<li> Michael O'Mahony (University College Dublin) </li>
	<li> Michael Paul (University of Colorado Boulder) </li>
	<li> Michalis Vazirgiannis (Department of Informatics, AUEB) </li>
	<li> Michele Coscia (Harvard University) </li>
	<li> Minsu Park (Cornell University) </li>
	<li> Momin Malik (Carnegie Mellon University) </li>
	<li> Motahhare Eslami (University of Illinois at Urbana-Champaign) </li>
	<li> Muhammad Imran (Qatar Computing Research Institute) </li>
	<li> Myle Ott (Facebook) </li>
	<li> Nathan Hodas (PNNL) </li>
	<li> Nicholas Lane (University College London and Bell Labs) </li>
	<li> Nicholas Jing Yuan (Microsoft) </li>
	<li> Nicolas Kourtellis (Telefonica Research) </li>
	<li> Nir Grinberg (Northeastern and Harvard University) </li>
	<li> Olessia Koltsova (National Research University Higher School of Economics) </li>
	<li> Omar Alonso (Microsoft) </li>
	<li> Onur Varol (Northeastern University) </li>
	<li> Pablo Arag&oacute;n (Universitat Pompeu Fabra) </li>
	<li> Pan Hui (HKUST & University of Helsinki) </li>
	<li> Panayiotis Tsaparas (University of Ioannina) </li>
	<li> Patty Kostkova (University College London) </li>
	<li> Pedro Olmo Vaz de Melo (UFMG) </li>
	<li> Peng Gao (Princeton University) </li>
	<li> Pinar Ozturk (Duquesne University) </li>
	<li> Piotr Sapiezynski (Technical University of Denmark) </li>
	<li> Ponnurangam Kumaraguru (IIITD) </li>
	<li> Pritam Gundecha (IBM Research, Almaden) </li>
	<li> Przemyslaw Grabowicz (Max Planck Institute for Software Systems (MPI-SWS)) </li>
	<li> Qingbo Hu (LinkedIn) </li>
	<li> Ramnath Balasubramanyan (Carnegie Mellon University) </li>
	<li> Reza Farahbakhsh (Institut Mines-Telecom, Telecom SudParis) </li>
	<li> Rok Sosic (Stanford University) </li>
	<li> Rub&eacute;n Cuevas (Telematics Engineering Departement, Universidad Carlos III de Madrid) </li>
	<li> Rui Zhang (IBM) </li>
	<li> Rumi Ghosh (Robert Bosch LLC) </li>
	<li> Sabrina Gaito (Politecnico di Milano) </li>
	<li> Salvatore Scellato (Google) </li>
	<li> Sanjay Kairam (Stanford University) </li>
	<li> Seungwon Eugene Jeong (Facebook) </li>
	<li> Shenghua Bao (IBM Watson Group) </li>
	<li> Shuang Yang (Operator Inc) </li>
	<li> Sofiane Abbar (Qatar Computing Research Institute, HBKU) </li>
	<li> Song Feng (IBM) </li>
	<li> Srinath Srinivasa (International Institute of Information Technology, Bangalore) </li>
	<li> Stephan Raaijmakers (TNO, The Netherlands) </li>
	<li> Styliani Kleanthous (University of Cyprus) </li>
	<li> Suzy Moat (The University of Warwick) </li>
	<li> Taejoong Chung (Northeastern University) </li>
	<li> Talayeh Aledavood (Aalto University) </li>
	<li> Tawfiq Ammari (University of Michigan) </li>
	<li> Tetyana Lokot (Dublin City University) </li>
	<li> Thomas Feliciani (University of Groningen) </li>
	<li> Tim Althoff (Stanford University) </li>
	<li> Tuan-Anh Hoang (L3S Research Center) </li>
	<li> Umashanthi Pavalanathan (Georgia Institute of Technology) </li>
	<li> Vanesa Frias-Martinez (University of Maryland, College Park) </li>
	<li> Venkata Rama Kiran Garimella (Aalto University) </li>
	<li> Wagner Meira Jr. (UFMG) </li>
	<li> Wang-Chien Lee (The Pennsylvania State University) </li>
	<li> Wei Ai (University of Michigan) </li>
	<li> Weike Pan (Shenzhen University) </li>
	<li> William Hamilton (Stanford University) </li>
	<li> Wolfgang Nejdl (L3S and University of Hannover) </li>
	<li> Xiaoran Yan (Indiana University Bloomington) </li>
	<li> Xin Huang (The University of British Columbia) </li>
	<li> Xingquan Zhu (Florida Atlantic University) </li>
	<li> Yelena Mejova (Qatar Computing Research Institute) </li>
	<li> Yi-Shin Chen (National Tsing Hua University) </li>
	<li> Ying Ding (Indiana University Bloomington) </li>
	<li> Yong Zheng (Illinois Institute of Technology) </li>
	<li> Yong Li (Tsinghua University) </li>
	<li> Younghoon Kim (Hanyang University) </li>
	<li> Yuheng Hu (University of Illinois at Chicago) </li>
	<li> Zhe Zhao (Google) </li>
	<li> Zhiyuan Cheng (Google) </li>
</ul>



<!--<h3>Program Committee Members</h3>
<ul>

</ul>
-->
<?php include("../../footer.php"); ?>
