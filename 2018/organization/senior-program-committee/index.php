<?php $section = "Organisation"; $subsection = "Senior Program Committee"; include("../../header.php"); ?>

<h2 class="pageTitle">ICWSM-18 Senior Program Committee</h2>


<h3>Senior Program Committee Members</h3>
<ul>
	<li> Alan Mislove (Northeastern University) </li>
	<li> Andrea Forte (Drexel University) </li>
	<li> Andreas Kaltenbrunner (NTENT) </li>
	<li> Ben Zhao (University of Chicago) </li>
	<li> Bogdan State (Stanford University) </li>
	<li> Brent Hecht (Northwestern University) </li>
	<li> Brooke Foucault Welles (Northeastern University) </li>
	<li> Carlos Castillo (Universitat Pompeu Fabra) </li>
	<li> Ceren Budak (University of Michigan School of Information) </li>
	<li> Christian Sandvig (University of Michigan) </li>
	<li> Christo Wilson (Northeastern University) </li>
	<li> Daniel Romero (University of Michigan) </li>
	<li> Daniel Archambault (Swansea University) </li>
	<li> David Garcia (Medical University of Vienna and Complexity Science Hub) </li>
	<li> David Jurgens (Stanford University) </li>
	<li> David Liben-Nowell (Carleton College) </li>
	<li> Derek Ruths (McGill University) </li>
	<li> Emilio Ferrara (University of Southern California) </li>
	<li> Emre Kiciman (Microsoft) </li>
	<li> Eugene Agichtein (Emory University) </li>
	<li> Fabricio Benevenuto (Federal University of Minas Gerais (UFMG)) </li>
	<li> Francesco Bonchi (The ISI Foundation, Turin, Italy) </li>
	<li> Haewoon Kwak (Qatar Computing Research Institute) </li>
	<li> Jahna Otterbacher (Open University of Cyprus) </li>
	<li> Jake Hofman (Microsoft) </li>
	<li> Jalal Mahmud (IBM) </li>
	<li> Javier Borge-Holthoefer (Internet Interdisciplinary Institute (IN3-UOC)) </li>
	<li> Jimmy Lin (University of Waterloo) </li>
	<li> Katherine Ognyanova (Rutgers University) </li>
	<li> Lada Adamic (Facebook Inc) </li>
	<li> Luca Maria Aiello (Bell Labs) </li>
	<li> Markus Strohmaier (RWTH Aachen University & GESIS) </li>
	<li> Meeyoung Cha (KAIST & Facebook) </li>
	<li> Mirco Musolesi (University College London) </li>
	<li> Mounia Lalmas (Yahoo) </li>
	<li> Munmun De Choudhury (Georgia Institute of Technology) </li>
	<li> Nicholas Diakopoulos (University of Maryland) </li>
	<li> Nishanth Sastry (King's College London) </li>
	<li> Renaud Lambiotte (University of Oxford) </li>
	<li> Robert West (Ecole Polytechnique Fédérale de Lausanne) </li>
	<li> Rossano Schifanella (University of Turin) </li>
	<li> Rosta Farzan (University of Pittsburgh) </li>
	<li> Ruth Garcia Gavilanes (University of Oxford) </li>
	<li> Saeideh Bakhshi (Georgia Institute of Technology) </li>
	<li> Sandra Gonzalez-Bailon (University of Pennsylvania) </li>
	<li> Sarita Yardi Schoenebeck (University of Michigan) </li>
	<li> Scott Counts (Microsoft) </li>
	<li> Shaomei Wu (Facebook Inc.) </li>
	<li> Steffen Staab (Institut WeST, University Koblenz-Landau and WAIS, University of Southampton) </li>
	<li> Sune Lehmann (Technical University of Denmark) </li>
	<li> Virgilio Almeida (UFMG) </li>
	<li> Winter Mason (Facebook) </li>
	<li> Xing Xie (Microsoft) </li>
	<li> Yu-Ru Lin (University of Pittsburgh) </li>
</ul>


<!--<h3>Program Committee Members</h3>
<ul>

</ul>
-->
<?php include("../../footer.php"); ?>
