<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Lee Humphreys"; include("../../header.php"); ?>

  <h2 class="pageTitle">Workshops Co-Chair - Lee Humphreys</h2>

  <div class="image"><img src="/2013/images/organisation/lee-humphreys.png" alt="Lee Humphreys"></div>

  <p>Lee Humphreys is an Assistant Professor in Communication at Cornell University. She studies the social uses and perceived effects of communication technology. She received her Ph.D. from the Annenberg School for Communication at the University of Pennsylvania in 2007. Currently, she is researching historical media practices, everyday conceptions and practices regarding privacy on social media, and cultural differences in mobile social network use.
</p>
  
 <p>Her previous research has explored mobile phone use in public spaces and emerging norms on mobile social networks both in the US and in Southeast Asia. Using qualitative field methods, she focuses on how people integrate communication technology in their everyday lives in order to facilitate identity management and social interaction. Her research has appeared in Journal of Communication, New Media & Society, and the Journal of Computer-Mediated Communication. With Paul Messaris, she co-edited the book, Digital Media: Transformations in Human Communication (Peter Lang, 2006).
</p>
  
<?php include("../../footer.php"); ?>
