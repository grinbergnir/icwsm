<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Vyacheslav W. Polonski"; include("../../header.php"); ?>

  <h2 class="pageTitle">Social Media Chair :<br> Vyacheslav W. Polonski</h2>

  <div class="image"><img src="/2016/images/organisation/vyacheslav-polonski.png" alt="Vyacheslav W. Polonski"></div>

  <p><a href='http://www.oii.ox.ac.uk/people/?id=312'>Vyacheslav Polonski</a> is the Social Media Chair of the 10th International AAAI Conference on Web and Social Media and is responsible for all social media activities around #ICWSM16. </p>

<p>He is currently working towards a PhD/DPhil in Computational Social Science at the Oxford Internet Institute with a special emphasis on digital identity, social network sites and technology adoption. His academic supervisors are Dr Bernie Hogan and Dr Felix Reed-Tsochas.</p>

<p>In 2014, Vyacheslav was a Visiting Fellow at Harvard University and the Harvard Graduate School of Arts and Sciences. In 2013, he was awarded the Master of Science (MSc) degree with Distinction in the Social Sciences of the Internet from Oxford University. He also obtained the Bachelor of Science (BSc) degree with First Class Honours in Management from the London School of Economics in 2012. Vyacheslav has been a Stelios Scholar and received awards from the British ESRC and the German Studienstiftung. In 2011, the British Council named Vyacheslav one of the UK's most distinguished International Students of the Year, and in 2012, he won the Future Business Leader of the Year award in the national TARGETjobs Student of the Year competition.</p>

<p>Vyacheslav is actively involved in the World Economic Forum and its Global Shapers community, where he is the Vice-Curator of the Oxford Hub. He writes about the intersection of sociology, network science and technology on Medium and Twitter.</p>
  
<?php include("../../footer.php"); ?>
