<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Christian Sandvig"; include("../../header.php"); ?>

  <h2 class="pageTitle">Program Co-chair - Christian Sandvig</h2>

  <div class="image"><img src="/2015/images/organisation/christian_sandvig.png" alt="Christian Sandvig"></div>



  <p>Christian Sandvig is an Associate Professor at the University of Michigan and a Faculty Associate of the Berkman Center for Internet & Society at Harvard University. At Michigan he is appointed at the Department of Communication Studies (College of Literature, Science, & the Arts) and the School of Information. Christian is also a member of the Center for Political Studies at the Institute for Social Research.</p>

  <p>Christian's research investigates communication and information technology infrastructure and public policy -- especially the way that legal, social, and technical elements work together (or don't work together) to shape systems of communication like the Internet.</p>

  


  <p></p>
  <p></p>
  <p></p>
  <p></p>
<?php include("../../footer.php"); ?>
