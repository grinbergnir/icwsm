<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Paul Resnick"; include("../../header.php"); ?>

  <h2 class="pageTitle">Paul Resnick</h2>

  <div class="image"><img src="/2013/images/organisation/paul-resnick.png" alt="Paul Resnick"></div>

  <p>Paul Resnick is a Professor at the University of Michigan School of Information. He previously worked as a researcher at AT&T Labs and AT&T Bell Labs, and as an Assistant Professor at the MIT Sloan School of Management. He received the master's and Ph.D. degrees in Electrical Engineering and Computer Science from MIT, and a bachelor's degree in mathematics from the University of Michigan.</p>

  <p>Professor Resnick's research focuses on SocioTechnical Capital, productive social relations that are enabled by the ongoing use of information and communication technology. His current projects include making recommender systems resistant to manipulation through rater reputations, nudging people toward politically balanced news consumption and health behavior change, and crowdsourcing fact-correction on the Internet.</p>
  
  <p>Resnick was a pioneer in the field of recommender systems (sometimes called collaborative filtering or social filtering). Recommender systems guide people to interesting materials based on recommendations from other people.  The GroupLens system he helped develop was awarded the 2010 ACM Software Systems Award. His articles have appeared in Scientific American, Wired, Communications of the ACM, The American Economic Review, Management Science, and many other venues.  He has a forthcoming MIT Press book (co-authored with Robert Kraut), titled “Building Successful Online Communities: Evidence-based Social Design�.</p>
  
<?php include("../../footer.php"); ?>
