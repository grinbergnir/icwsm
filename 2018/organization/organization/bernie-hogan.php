<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Bernie Hogan"; include("../../header.php"); ?>

  <h2 class="pageTitle">Local Chair - Bernie Hogan</h2>

  <div class="image"><img src="/2012/images/organisation/bernie-hogan.png" alt="Bernie Hogan"></div>

  <p>Bernie Hogan's research interests lie at the intersection of social networks and media convergence. That is, what medium do people use with their ties, and when? With new media, individuals simultaneously have more convenience but also more complexity, expenses and social pressure to adopt. Some individuals thrive in this new media ecology, while others feel it has isolated them. Within this framework, Bernie examines the eroding home-work boundary, the digital self, and the shift from public spaces to cyberpublics.

  <p>He is also working on a number of methodological issues, including reliable capture of online networks, efficient strategies for capturing networks in interviews, social science software development and the application of audit studies to online housing and job markets.

  <p>Bernie Hogan completed his BA (Honours) at the Memorial University of Newfoundland in Canada, where he received the University Medal in Sociology. Since then he has been working on Internet use and social networks at the University of Toronto under social network analysis pioneer Barry Wellman.</p>

  <p>Bernie received his Masters of Arts at Toronto in 2003, and defended his PhD Dissertation in the Fall of 2008. His dissertation examines how the use of ICTs alters the way people maintain their relationships in everyday life. In 2005 he was an intern at Microsoft's Community Technologies Lab, working with Danyel Fisher on new models for email management.</p>
  
<?php include("../../footer.php"); ?>
