<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Andres Monroy-Hernandez"; include("../../header.php"); ?>

  <h2 class="pageTitle">Demos Co-Chair - Andres Monroy-Hernandez</h2>

  <div class="image"><img src="/2013/images/organisation/andres-monroy-hernandez.png" alt="Andres Monroy-Hernandez"></div>

  <p>Andres Monroy-Hernandez is a researcher at Microsoft Research and a Affiliate at the Berkman Center for Internet & Society at Harvard University. Previously, he was a doctoral student at the MIT Media Lab.</p>

  <p>His research examines the social, technical, and cultural dimensions of social computing systems. In particular, he focuses on the design and study of online communities. Andres uses quantitative and qualitative methodologies to understand the social dynamics and cultural artifacts that emerge from peer-production systems. He then applies those insights to the design of the underlying sociotechnical infrastructure and the policies that it embodies. His work on human-computer interaction is positioned at the frontier of computer science, social science, and digital humanities.</p>
  
  <p></p>
  
<?php include("../../footer.php"); ?>