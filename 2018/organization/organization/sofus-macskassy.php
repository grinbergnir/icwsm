<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Sofus Macskassy"; include("../../header.php"); ?>

  <h2 class="pageTitle">Workshops Chair - Sofus Macskassy</h2>

  <div class="image"><img src="/2012/images/organisation/sofus-macskassy.png" alt="Sofus Macskassy"></div>

  <p>Dr. Sofus A. Macskassy is the Director of Fetch Labs at Fetch Technologies and an Adjunct Professor of Computer Science at the University of Southern California.  His work spans information analytics, data mining, machine learning and social network analysis. He has recently focused much of his work in the social media space, where he has looked at issues such as extracting topic-specific networks, exploring content-specific retweeting and information diffusion behaviors, extracting useful psychographic and demographic information of users based on their user generated content, and exploring differences in communities based on content and linking.</p>

  <p>Dr. Macskassy serves on the editorial board of the Machine Learning Journal, the premier machine learning journal in the world.  He is also the primary developer and maintainer of the open-source Network Learning Toolkit for Statistical Relational Learning.</p>
  
<?php include("../../footer.php"); ?>
