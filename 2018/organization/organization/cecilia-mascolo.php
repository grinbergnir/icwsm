<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Cecilia Mascolo"; include("../../header.php"); ?>

  <h2 class="pageTitle">Program Co-chair - Cecilia Mascolo</h2>

  <div class="image"><img src="/2015/images/organisation/cecilia.png" alt="Cecilia Mascolo"></div>

  <p>Cecilia Mascolo is a Professor of Mobile Systems in the Computer Laboratory, University of
Cambridge. Her research interests are in human mobility modelling. She has published in the
areas of mobile computing, mobility and social modelling and sensor networking. She is and has
been PI and co-PI of numerous projects related to sensor networks, mobile computing and
complex network analysis. Cecilia received direct industrial funding from
Google, Intel, Microsoft and Samsung. She has served as an organizing and programme
committee member of many mobile, sensor systems, social network and communication conferences and workshops.</p>

<p>She sits on the editorial boards of IEEE Internet Computing, IEEE Pervasive Computing, IEEE
Transactions on Mobile Computing and ACM Transactions on Sensor Networks. 
Cecilia has taught courses on mobile and sensor systems and social and technological network analysis. More details are available <a href="http://www.cl.cam.ac.uk/users/cm542" target="_blank"> here</a>.</p>
  
  <p></p>

  <p></p>
  <p></p>

  <p></p>
<?php include("../../footer.php"); ?>
