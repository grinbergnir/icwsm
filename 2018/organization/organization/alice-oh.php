<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Alice Oh"; include("../../header.php"); ?>

  <h2 class="pageTitle">Program Co-chair - Alice Oh</h2>
  <p>Alice Oh is currently a visiting researcher at Harvard Center for
Research on Computation and Society. Her home institution is Korea
Advanced Institute of Science and Technology where she is an Assistant
Professor of Computer Science. She conducts research in computational
social science, broadly defined as studying individual and group
social behaviors using computational tools applied to large-scale
social media data. Recently, she and her students have written
research papers about emotions in conversations, self-disclosure
behavior, and sentiment analysis. She is also interested in machine
learning research and has published papers on novel topic modeling
techniques for text, image, and Web data. Alice completed her M.S. in
Language and Information Technologies at CMU and her Ph.D. in Computer
Science at MIT.</p>
  
<?php include("../../footer.php"); ?>