<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Xin Rong"; include("../../header.php"); ?>

  <h2 class="pageTitle">Xin Rong</h2>

  <div class="image"><img src="/2014/images/organisation/xin-rong-thumb.jpg" alt="Xin Rong"></div>

  <p>Xin Rong (<a href="http://www-personal.umich.edu/~ronxin/">personal website</a>) is a third-year doctoral student at University of Michigan, School of Information. His research interests are text mining, natural language processing, and human computer interaction. He is advised by Qiaozhu Mei.</p>

<?php include("../../footer.php"); ?>