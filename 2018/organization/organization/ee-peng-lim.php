<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Ee-Peng Lim"; include("../../header.php"); ?>

  <h2 class="pageTitle">Tutorial Co-Chair :<br>Ee-Peng Lim</h2>

  <div class="image"><img src="/2016/images/organisation/ee-peng-lim.png" alt="Ee-Peng Lim"></div>

  <p><a href='https://sites.google.com/site/aseplim/'>Ee-Peng Lim</a> is a professor at the School of Information Systems of Singapore Management University (SMU).  He received Ph.D. from the University of Minnesota, Minneapolis in 1994 and B.Sc. in Computer Science from National University of Singapore.  His research interests include social network and web mining, information integration, and digital libraries.  He is the co-Director of the Living Analytics Research Center (LARC) jointly established by SMU and Carnegie Mellon University.  He is currently an Associate Editor of the ACM Transactions on Information Systems (TOIS), ACM Transactions on the Web (TWeb), IEEE Transactions on Knowledge and Data Engineering (TKDE), Information Processing and Management (IPM), Social Network Analysis and Mining, Journal of Web Engineering (JWE), IEEE Intelligent Systems, International Journal of Digital Libraries (IJDL) and International Journal of Data Warehousing and Mining (IJDWM).  He was a member of the ACM Publications Board until December 2012.  He serves on the Steering Committee of the International Conference on Asian Digital Libraries (ICADL), Pacific Asia Conference on Knowledge Discovery and Data Mining (PAKDD), and International Conference on Social Informatics (Socinfo).</p>


 
<?php include("../../footer.php"); ?>