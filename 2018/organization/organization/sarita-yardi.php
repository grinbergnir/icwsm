<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Sarita Yardi Schoenebeck"; include("../../header.php"); ?>

  <h2 class="pageTitle">Social Media / Publicity Chair - Sarita Yardi Schoenebeck</h2>

  <div class="image"><img src="/2013/images/organisation/sarita-yardi.jpg" alt="Sarita Yardi Schoenebeck"></div>

  <p>Sarita Yardi Schoenebeck an Assistant Professor in the School of Information at the University of Michigan and a member of the Michigan Interactive & Social Computing group. She received her PhD in Human-Centered Computing in the School of Interactive Computing at Georgia Tech. During her graduate work, she interned with HP Lab's Social Computing group and Microsoft Research's Social Media Group. Her research has been supported by the NSF and she is a recipient of a Google Anita Borg Fellowship.</p>

  <p>Sarita's research is in the areas of Social Computing, Social Media, and HCI. She is interested in boundary setting in social media, or how people navigate social media use in their daily lives. She is particularly interested in families�parents and youth�and how they use technology in home and school contexts. She uses a variety of methods including in-depth fieldwork, log file analysis, and real-world deployments.</p>
  
  <p></p>
  <p></p>
  <p></p>
  
<?php include("../../footer.php"); ?>