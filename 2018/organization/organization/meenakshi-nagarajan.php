<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Meenakshi Nagarajan"; include("../../header.php"); ?>

  <h2 class="pageTitle">Sponsorship Chair - Meenakshi Nagarajan</h2>

  <div class="image"><img src="/2012/images/organisation/meenakshi-nagarajan.png" alt="Meenakshi Nagarajan"></div>

  <p>Dr. Meena Nagarajan is a Research Staff Member at IBM Research Almaden in San Jose, California. Her work is at the intersection of social content analysis and computational social science, in coding and analyzing behavioral data on social media and answering questions about the underlying social processes. Some of the areas her work explores are identifying cultural named entities and topics that people are making references to, studying how cultures are interpreting any situation in local contexts and supporting them in their variable observations, exploring the intentions that produce the diverse content on social media, etc.</p>
  
  <p>Results of her work have been absorbed into deployed social intelligence Web applications such as the BBC SoundIndex, that measures the pulse of a music populace by tapping into content from online music communities and Twitris, that aggregates user perceptions behind real-time events using data from Twitter.</p>
  
  <p>Dr. Nagarajan also co-organized the first workshop on Language on Social Media at ACL/HLT 2011 and serves as the co-editor of the Journal of Web Semantics Special Issue on the Semantic and Social Web, 2012.</p>

<?php include("../../footer.php"); ?>
