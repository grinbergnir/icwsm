<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Munmun De Choudhury"; include("../../header.php"); ?>

  <h2 class="pageTitle">Workshop Co-chair :<br>Munmun De Choudhury</h2>

  <div class="image"><img src="/2013/images/organisation/munmun-dechoudhury.png" alt="Munmun De Choudhury"></div>

<!--  <p> Munmun De Choudhury is an Assistant Professor in the School of Interactive Computing at Georgia Tech. Her broad area of interest comprises computational social science: an emerging field that lies at the intersection of machine learning, social science, and human computer interaction. Specifically, her research develops methods to understand and analyze human behavior as manifested via our online footprints. She is motivated by how the availability of large-scale online social data, coupled with computational methods and resources can help us answer fundamental questions relating to our social lives: our actions, interactions, emotions and linguistic expression, both from an individual perspective, at the same time, as part of a larger collective.</p>

  <p>In her current research, Munmun is investigating how longitudinal trails of social activity online can be harnessed for better healthcare, particularly in being able to detect and predict mental and behavioral health concerns in people, so as to provide timely, valuable, and smart interventions.</p>
  
  <p> Munmun received her Ph.D in 2011 from the Department of Computer Science at Arizona State University, Tempe, where she was also a part of the transdisciplinary program and venture on digital media: Arts, Media & Engineering. Following graduate school, she also spent some time at the School of Communication and Information, Rutgers University.</p> -->






<p><a href='http://www.munmund.net/'>Munmun De Choudhury</a> is an Assistant Professor in the School of Interactive Computing at Georgia Tech. She is also affiliated with the GVU Center at Georgia Tech. Her research interests are in the area of computational social science, wherein she is interested in questions around making sense of human behavior as manifested via our online social footprints. She is motivated by how the availability of large-scale online social data, coupled with computational methods and resources can help answer fundamental questions relating to our social lives, particularly our health and well-being.</p>

<p>At Georgia Tech, she leads the Social Dynamics and Wellbeing Lab (SocWeb). They study, mine, and analyze social media to derive insights into improving our health and well-being. Their research is generously supported by the National Institutes of Health, the United Nations Foundation, Yahoo!, and Samsung.</p>

<p>Before moving to Georgia Tech in Spring 2014, she was a postdoctoral researcher in the neXus group at Microsoft Research, Redmond, between 2011 and 2013. She received her Ph.D in 2011 from the Department of Computer Science at Arizona State University, Tempe, where she was a part of the transdisciplinary program and venture on digital media: Arts, Media & Engineering. Following grad school, she also spent some time at the School of Communication and Information, Rutgers. In the academic year 2014-15, she was also a faculty associate with the Berkman Center for Internet and Society at Harvard.</p>



  
<?php include("../../footer.php"); ?>