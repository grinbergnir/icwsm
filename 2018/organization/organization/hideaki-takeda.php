<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Hideaki Takeda"; include("../../header.php"); ?>

  <h2 class="pageTitle">Regional Chair: Asia Pacific - Hideaki Takeda</h2>

  <div class="image"><img src="/2012/images/organisation/hideaki-takeda.png" alt="Hideaki Takeda"></div>

  <p>Dr. Hideaki Takeda is a Professor in the Principles of Informatics Research Division, National Institute of Informatics (NII), Japan and is also Director of the R&D Center for Scientific Information Resources in NII. His research background is in artificial intelligence, but his focus is now on the social and semantic web. In his semantic web research, he proposed various semantic blogging systems and also a LISP-based OWL processor. He has carried out research on online communities such as analyses of social networks in popular video sites.</p>
  
  <p>He now leads a project called <a href="http://lod.ac">LODAC</a> that collects, integrates and publishes various types of scientific information such as museum information and species information as Linked Open Data (LOD). He has worked on the program committees for many WWW and Semantic Web-related conferences.</p>
  
<?php include("../../footer.php"); ?>
