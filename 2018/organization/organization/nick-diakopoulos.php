<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "NICK Diakopoulos"; include("../../header.php"); ?>

  <h2 class="pageTitle">Workshop Co-chair :<br>NICK Diakopoulos</h2>

  <div class="image"><img src="/2016/images/organisation/nick-diakopolos.jpg" alt="NICK DIAKOPOULOUS"></div>

<p><a href='http://www.nickdiakopoulos.com/'>Nicholas Diakopoulos</a> is an Assistant Professor at the University of Maryland, College Park College of Journalism with courtesy appointments in the College of Information Studies and Department of Computer Science. His research is in computational and data journalism with an emphasis on algorithmic accountability, narrative data visualization, and social computing in the news. He received his Ph.D. in Computer Science from the School of Interactive Computing at Georgia Tech where he co-founded the program in Computational Journalism. Before UMD he worked as a researcher at Columbia University, Rutgers University, and CUNY studying the intersections of information science, innovation, and journalism.
</p>
  
<?php include("../../footer.php"); ?>
