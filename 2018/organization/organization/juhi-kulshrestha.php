<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Juhi Kulshrestha"; include("../../header.php"); ?>

  <h2 class="pageTitle">Web Chair :<br>Juhi Kulshrestha</h2>

  <div class="image"><img src="/2016/images/organisation/juhi-kulshrestha.png" alt="Juhi Kulshrestha"></div>

<p><a href='http://www.mpi-sws.org/~juhi/'>Juhi Kulshrestha</a> is a PhD student at Max Planck Institute for Software Systems (MPI-SWS), Germany. She has a M.Sc. in Computer Science from the University of Saarland. Prior to that she obtained a M.Sc. in Informatics  and B.Sc. (Hons.) in Physics from the University of Delhi.
</p>
  
<p>Her research focuses on studying how the users are consuming information on Online Social Networks. In particular, she is interested in exploring what sort of information diets are the users producing or consuming on social media and in evaluating what role the recommendation systems are playing in shaping their diets.
</p>

<p>She is a recipient of Google European Doctoral Fellowship in Social Networking (2011) and the Google Anita Borg Fellowship (2013).
</p>
<?php include("../../footer.php"); ?>
