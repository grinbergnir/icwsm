<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Derek Greene"; include("../../header.php"); ?>

  <h2 class="pageTitle">Local Co-Chair - Derek Greene</h2>

  <div class="image"><img src="/2012/images/organisation/derek-greene.png" alt="Derek Greene"></div>

  <p>Dr. Derek Greene is a Research Fellow at the School of Computer Science and Informatics, University College Dublin, Ireland. As part of the SFI-funded Clique Research Cluster, his current work relates to challenges in network analysis, cluster analysis, and content mining, with a particular focus on the analysis of activities and trends in highly-dynamic social media networks.</p>
  
  <p>Derek successfully finished his PhD in 2006 at Trinity College Dublin, where his research pertained to the development and application of spectral and matrix factorisation methods for the unsupervised exploration of document
collections.</p>
  
<?php include("../../footer.php"); ?>
