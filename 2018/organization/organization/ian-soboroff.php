<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Ian Soboroff"; include("../../header.php"); ?>

  <h2 class="pageTitle">Program Co-Chair - Ian Soboroff</h2>

  <div class="image"><img src="/2012/images/organisation/ian-soboroff.png" alt="Ian Soboroff"></div>

  <p>Dr. Ian Soboroff is a computer scientist and manager of the Retrieval Group at the National Institute of Standards and Technology (NIST). The Retrieval Group organizes the Text REtrieval Conference (TREC), the Text Analysis Conference (TAC), and the TREC Video Retrieval Evaluation (TRECVID).  These are all large, community-based research workshops that drive the state-of-the-art in information retrieval, video search, web search, text summarization and other areas of information access.</p>
  
  <p>He has co-authored many publications in information retrieval evaluation, test collection building, text filtering, collaborative filtering, and intelligent software agents. His current research interests include building test collections for social media environments and nontraditional retrieval tasks.</p>
  
<?php include("../../footer.php"); ?>
