<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Claudia Wagner"; include("../../header.php"); ?>

  <h2 class="pageTitle">Program Co-Chair :<br>Claudia Wagner</h2>

  <div class="image"><img src="/2016/images/organisation/claudia-wagner-new.jpg" alt="Claudia Wagner"></div>

  <p><a href='http://claudiawagner.info/'>Claudia Wagner</a> is a post doctoral researcher at the Computational Social Science department at GESIS / Leibniz Institute for the Social Sciences and an adjunct lecturer at the University of Koblenz-Landau.
She received her PhD in Computer Science at Graz University of Technology. Her PhD research has been supported by a DOC-fFORTE fellowship of the Austrian Academy of Science. During her graduate work, she interned with HP Lab's Social Computing group and Xerox PARC's Augmented Social Cognition team.</p>
 
<p>Claudia is interested in exploring to what extent digital traces can be used to learn about the interests, knowledge, habits and biases of individuals and societies and how new technologies may impact them. She is particularly interested in culinary habits and preferences and digital interactions with food, cultural analytics and disparities and political power and communication strategies.</p>

<p></p>
<p></p>
<p></p>
 
<?php include("../../footer.php"); ?>
