<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Farshad Kooti"; include("../../header.php"); ?>

  <h2 class="pageTitle">Web Chair - Farshad Kooti</h2>

  <div class="image"><img src="/2013/images/organisation/farshad-kooti.png" alt="Farshad Kooti"></div>

  <p><a href="http://www-scf.usc.edu/~kooti/">Farshad Kooti</a> is a PhD student at University of Southern California and a member of Intelligent Systems Division at Information Sciences Institute.</p>

  <p>Farshad's main research interest is the study of large and complex networks, especially online social networks, which includes the measurement and analysis of social activity, and the design of systems that can leverage the findings of this analysis.</p>

<p></p>  
<p></p>  
<p></p>  
<?php include("../../footer.php"); ?>