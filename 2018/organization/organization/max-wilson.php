<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Max L. Wilson"; include("../../header.php"); ?>

  <h2 class="pageTitle">Social Media / Publicity Chair - Max L. Wilson</h2>

  <div class="image"><img src="/2012/images/organisation/max-wilson.png" alt="Max L. Wilson"></div>

  <p>Max Wilson is a Lecturer in HCI and Information Seeking, in the Future Interaction Technology Lab at Swansea University, UK. His research focuses on Search User Interface design, taking a multidisciplinary perspective from both Human-Computer Interaction (the presentation and interaction) and Information Science (the information and seeking behaviours). His doctoral work, which won best JASIST article in 2009, focused on evaluating Search User Interfaces using models of Information Seeking. Max received his PhD from Southampton University, under the supervision of m.c. schraefel and Dame Wendy Hall, where much of his work was grounded in supporting Exploratory Search, and within the developing context of Web Science.</p>
  
  <p>Max mainly publishes in HCI and Information Science communities, including a monograph with co-authors schraefel, Kules, and Shneiderman on future Search User Interfaces for the Web, and a book chapter on Casual-Leisure Information Behaviour. Given the social evolution of the Web, and his interest in Web Science, some of Max's more recent work has focused on how people use social media, especially microblogging, as an information resource. Research from Max's team, published at ICWSM-11, focused on Information Retrieval and examining the concept of relevance in Twitter search systems.</p>
  
<?php include("../../footer.php"); ?>
