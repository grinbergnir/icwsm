<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Jimmy Lin"; include("../../header.php"); ?>

  <h2 class="pageTitle">Data Chair - Jimmy Lin</h2>

  <div class="image"><img src="/2015/images/organisation/jimmy-lin.png" alt="Jimmy Lin"></div>

  <p>Jimmy Lin is an associate professor in the iSchool at the University of Maryland, with appointments in the Institute for Advanced Computer Studies (UMIACS) and the Department of Computer Science. He joined the faculty in August 2004, shortly after completing his Ph.D. in Electrical Engineering and Computer Science at MIT, and was promoted to associate professor in March 2009.

 <p>He works on "big data", with a particular focus on large-scale distributed algorithms for text processing. His research lies at the intersection of natural language processing (NLP) and information retrieval (IR). He is a member of both the Computational Linguistics and Information Processing Lab (CLIP) and the Human-Computer Interaction Lab (HCIL).</p>
  
  <p></p>

  <p></p> 
 <p></p>
  <p></p>

  <p></p>
<?php include("../../footer.php"); ?>
