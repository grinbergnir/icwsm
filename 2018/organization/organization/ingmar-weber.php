<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Ingmar Weber"; include("../../header.php"); ?>

  <h2 class="pageTitle">Workshop Co-chair :<br>Ingmar Weber</h2>

  <div class="image"><img src="/2016/images/organisation/ingmar-weber.png" alt="Ingmar Weber"></div>

<p><a href='http://ingmarweber.de/'>Ingmar Weber</a> is a senior scientist in the Social Computing Group at Qatar Computing Research Institute. His interdisciplinary research looks at what online user-generated data can tell us about the offline world and society at large. He works with sociologists, political scientists, demographers and medical professionals and several of his projects have been covered by international media.
</p>

<p>His recent work focuses on how online user-generated data can be used to answer questions about the offline world and society at large. Recurring themes in his research include political polarization, international mobility, and cultural influences on communication patterns.
</p>
  
<?php include("../../footer.php"); ?>