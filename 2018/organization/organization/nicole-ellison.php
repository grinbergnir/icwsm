<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Nicole Ellison"; include("../../header.php"); ?>

  <h2 class="pageTitle">Programme Co-Chair - Nicole Ellison</h2>

  <div class="image"><img src="/2012/images/organisation/nicole-ellison.png" alt="Nicole Ellison"></div>

  <p>Nicole B. Ellison is an associate professor in the School of Information at University of Michigan. Her research explores issues of self-presentation, relationship development, and identity in online environments such as online dating and social network sites. Nicole received her Ph.D in Communication Theory and Research from the Annenberg School for Communication at the University of Southern California in 1999. Currently she is exploring ad-hoc collaboration in social network sites, perceptions regarding the acceptability of profile discrepancies in online dating profiles, and the role of social media in relation to college-going activities among low-income youth.</p>
  
  <p>Her previous research has examined the formation of virtual communities and the ways in which telecommuters use information and communication technologies to calibrate the permeability of their work/home boundaries, as explored in her 2004 book, Telework and Social Change. Her work has been published in the Journal of Computer-Mediated Communication, New Media & Society, Communication Research, and the Personality and Social Psychology Bulletion and her research has been funded by the National Science Foundation and the Gates Foundation.  Representative publications can be found on <a href="https://www.msu.edu/~nellison/pubs.html">her publications page</a>.

</p>
  
<?php include("../../footer.php"); ?>
