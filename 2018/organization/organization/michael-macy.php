<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Michael Macy"; include("../../header.php"); ?>

  <h2 class="pageTitle">Program Co-chair :<br>Michael Macy</h2>

  <div class="image"><img src="/2014/images/organisation/michael-macy.jpg" alt="Michael Macy"></div>

  <!--<p>Michael Macy is a Goldwin Smith Professor of Arts and Sciences at Cornell University Department of Sociology and Department of Information Science.
     He is the the director of the Social Dynamics Laboratory at Cornell University.
     His research explores how norms, opinions, emotions, and collective action emerge and spread through local interaction.   
  </p>-->

<p><a href='http://infosci.cornell.edu/faculty/michael-macy'>Michael Macy</a> is Director of the Social Dynamics Laboratory at Cornell and Goldwin Smith Professor of Arts and Sciences, with dual appointments in the Departments of Sociology and Information Science. With support from the US National Science Foundation, the Korean National Science Foundation, the Department of Defense, and Google, his research team has used computational models, online laboratory experiments, and digital traces of device-mediated interaction to explore familiar but enigmatic social patterns, such as circadian rhythms, the emergence and collapse of fads, the spread of self-destructive behaviors, cooperation in social dilemmas, the critical mass in collective action, the spread of high-threshold contagions on small-world networks, the polarization of opinion, segregation of neighborhoods, and assimilation of minority cultures. Recent research uses 509 million Twitter messages to track diurnal and seasonal mood changes in 54 countries, and telephone logs for 12B calls in the UK to measure the economic correlates of network structure. His research has been published in leading journals, including Science, PNAS, PloS One, American Journal of Sociology, American Sociological Review, and Annual Review of Sociology.
</p>
  
<?php include("../../footer.php"); ?>

?>