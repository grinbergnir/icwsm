<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Conor Hayes"; include("../../header.php"); ?>

  <h2 class="pageTitle">Local Co-Chair - Conor Hayes</h2>

  <div class="image"><img src="/2012/images/organisation/conor-hayes.png" alt="Conor Hayes"></div>

  <p>Dr. Conor Hayes is a senior research fellow, adjunct lecturer and leader the Information Mining and Retrieval Unit at the Digital Enterprise Research Institute, NUI Galway. Dr Hayes is a PI on the SFI-funded Clique Research Cluster on large scale graph and network analysis and on the EU-Funded Project ROBUST on risk and opportunity detection in online social networks. His research interests focus on challenges in role analysis in social media, in detecting, tracking  and interpreting significant changes in dynamic networks and in adaptive personalisation.</p>
  
  <p>Previous to this, Dr. Hayes worked for 3 years as a post-doctoral research fellow at the Fondazione Bruno Kessler (Formerly ITC-IRST) in Trento, Italy. Dr Hayes's completed his PhD on personalisation in on-line communities in 2003 at the AI lab in Trinity College Dublin, Ireland.</p>
  
<?php include("../../footer.php"); ?>
