<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Luca Maria Aiello"; include("../../header.php"); ?>

  <h2 class="pageTitle">Tutorial Co-Chair :<br>Luca Maria Aiello</h2>

  <div class="image"><img src="/2016/images/organisation/luca-aiello.jpg" alt="Luca Maria Aiello"></div>

  <p><a href='http://www.lajello.com/'>Luca Aiello</a> received his Ph.D. in Computer Science from the University of Torino, Italy in 2012. He is currently a Research Scientist at Yahoo Labs in London. He conducts interdisciplinary work connecting computer science, physics of complex systems, and computational social science through quantitative big data analysis. Recently his research has been devoted to study social phenomena such as homophily, influence, conversational norms, status and social attention, with applications to personalization, ranking, recommendation and link prediction. He is General Chair for the SocInfo'14 conference and co-organizer of the SNOW'14 Workshop. He has been member of the PC of major computer science conferences, including WWW, WSDM, ICWSM, CIKM, ACM MM. He is leading the work of Yahoo in the SocialSensor EU project consortium.</p>


 
<?php include("../../footer.php"); ?>
