<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Winter Mason"; include("../../header.php"); ?>

  <h2 class="pageTitle">Sponsorship Chair :<br> Winter Mason</h2>

  <div class="image"><img src="/2016/images/organisation/winter-mason.jpeg" alt="Winter Mason"></div>

  <p><a href='https://research.facebook.com/researchers/1471244349809491/winter-mason/'>Winter Mason</a> is a Computational Social Scientist at Facebook. His research touches on social influence, social networks, crowdsourcing, and group dynamics, and typically integrates methods from psychology and computer science. Prior to joining Facebook, Winter received his Ph.D. from Indiana University in social psychology and cognitive science, was a post-doc at Yahoo! Research, and was an assistant professor at Stevens Institue of Technology.  Working on a project with partners in Germany and Korea, he has helped define what determines how social conventions occur.</p>

  <p>He is a member of the American Psychological Society, the Cognitive Science Society, and the Society of Personality and Social Psychology.</p>
  
  <p></p>

  <p></p>
  <p></p>

  <p></p>
<?php include("../../footer.php"); ?>
