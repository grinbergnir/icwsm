<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Sihem Amer-Yahia"; include("../../header.php"); ?>

  <h2 class="pageTitle">Regional Chair: Middle East and Africa - Sihem Amer-Yahia</h2>

  <div class="image"><img src="/2012/images/organisation/sihem-amer-yahia.png" alt="Sihem Amer-Yahia"></div>

  <p>Dr. Sihem Amer-Yahia joined QCRI in May 2011 as Principal Research Scientist. Sihem focuses on data management and novel analytics architectures for the Social Web. Before joining QCRI, she was Senior Research Scientist at Yahoo! Labs for five years where she worked on revisiting relevance models and top-k processing algorithms on Delicious, Yahoo! Personals and Flickr datasets. Prior to that, Sihem spent seven years at AT&T Labs focusing on XML query processing and XML full-text search. She received her PhD in Computer Science from U. Paris-Orsay and INRIA in Paris in 1999 and her Diplome d'Ingenieur from INI, Algeria in 1994.</p>

  <p>Sihem leads research in Social Computing, the science of gathering and processing social breadcrumbs left by millions of users, in order to enhance their online experience.</p>
  
<?php include("../../footer.php"); ?>
