<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Zeynep Tufekci"; include("../../header.php"); ?>

  <h2 class="pageTitle">Programme Co-Chair - Zeynep Tufekci</h2>

  <div class="image"><img src="/2012/images/organisation/zeynep-tufekci.png" alt="Zeynep Tufekci"></div>

  <p>Zeynep Tufekci is an assistant professor at the University of North Carolina, Chapel Hill at the School of Information and Library Science with an affiliate appointment in the Department of Sociology. She is also a Fellow at the Berkman Center for Internet and Society at Harvard University.</p>
  
  <p>Her research revolves around the interaction between technology and social, cultural and political dynamics. She is particularly interested in collective action and social movements, complex systems, surveillance, privacy, and sociality. Her blog can be found at <a href="http://www.technosociology.org/">technosociology.org</a>.</p>
  
<?php include("../../footer.php"); ?>
