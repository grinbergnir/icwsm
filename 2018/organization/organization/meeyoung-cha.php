<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Meeyoung Cha"; include("../../header.php"); ?>

  <h2 class="pageTitle">Program Co-chair - Meeyoung Cha</h2>
  <div class="image"><img src="/2014/images/organisation/mcha_bk.jpg" alt="Meeyoung Cha"></div>

  <p>
Meeyoung Cha is an associate professor at Graduate School of Culture
Technology in KAIST. Meeyoung received a Ph.D. degree in Computer
Science from KAIST in 2008. Previously, she was a post-doctral
researcher at Max Planck Institute for Software Systems in Germany.
Her research interests are in the analysis of large-scale online
social networks with emphasis the spread of information, moods, and
user influence. Her research has been published in leading journals
and conferences including PLoS One, Information Sciences, WWW, and
ICWSM, and has been featured at the popular media outlets including
the New York Times websites, Harvard Business Review's research blog,
the Washington Post, the New Scientist.
</p>
 
<p></p>
<p></p>
<p></p>
<p></p>
<p></p>
<p></p>
<?php include("../../footer.php"); ?>