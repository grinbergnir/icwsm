<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Kristina Lerman"; include("../../header.php"); ?>

  <h2 class="pageTitle">Tutorial Co-Chair :<br>Kristina Lerman</h2>

  <div class="image"><img src="/2016/images/organisation/kristina-lerman.png" alt="Kristina Lerman"></div>

  <p><a href='http://www.isi.edu/integration/people/lerman/index.html'>Kristina Lerman</a> is a Project Leader at the Information Sciences Institute and holds a joint appointment as a Research Associate Professor in the USC Viterbi School of Engineering's Computer Science Department. Her research focuses on applying network- and machine learning-based methods to problems in social computing.</p>	 
 
<!--<p>She also was principal organizer of the 2008 American Association for the Advancement of Artificial Intelligence (AAAI) Social Information Processing Symposium.</p>-->


 
<?php include("../../footer.php"); ?>