<?php $section = "Organisation"; $subsection = "Organisation"; include("../../header.php"); ?>

  <h2 class="pageTitle">Organization</h2>

  <ul id="organisationList">

    <li class="">
      <h3 class="">Jeff Hancock</h3>
      <h5>Stanford University</h5>
      <p class="">General Chair</p>
    </li>

    <li class="">
      <h3 class="">Kate Starbird</h3>
      <h5>University of Washington</h5>
      <p class="">PC Co-chair</p>
    </li>


  <li class="">
      <!-- <a href="ingmar-weber.php"><img src="/2016/images/organisation/ingmar-weber-thumb.png" class="project-thumb" alt=""></a> -->
      <!-- <h3 class=""><a href="ingmar-weber.php">Ingmar Weber</a></h3> -->
      <h3 class="">Ingmar Weber</h3>
      <h5>Qatar Computing Research Institute</h5>
      <p class="">PC Co-chair</p>
    </li>

    <li class="">
      <h3 class="">Carol Hamilton</h3>
      <h5>AAAI</h5>
      <p class="">AAAI Exec. Director</p>
    </li>

    <li class="">
      <h3 class="">Catalina Toma</h3>
      <h5>University of Wisconsin, Madison</h5>
      <p class="">Workshop Co-chair</p>
    </li>

    <li class="">
      <h3 class="">Feida Zhu</h3>
      <h5>Singapore Management University</h5>
      <p class="">Workshop Co-chair</p>
    </li>

    <li class="">
      <h3 class="">Alexandra Olteanu</h3>
      <h5>IBM Research</h5>
      <p class="">Tutorial Co-Chair</p>
    </li>

    <li class="">
      <h3 class="">Kiran Garimella</h3>
      <h5>Aalto University</h5>
      <p class="">Tutorial Co-Chair</p>
    </li>

    <li class="">
      <h3 class="">Fabricio Benevenuto</h3>
      <h5>Nokia Bell Labs</h5>
      <p class="">Data Co-chair</p>
    </li>

    <li class="">
      <h3 class="">Luca Aiello</h3>
      <h5>UFMG</h5>
      <p class="">Data Co-chair</p>
    </li>

    <li class="">
      <h3 class="">Dana&euml; Metaxa-Kakavouli</h3>
      <h5>Stanford University</h5>
      <p class="">Web Chair</p>
    </li>

    <li class="">
      <h3 class="">Nir Grinberg</h3>
      <h5>Northeastern University</h5>
      <p class="">Social Media & Publicity Chair</p>
    </li>

    <li class="">
      <h3 class="">Kylie Sewall</h3>
      <h5>Stanford University</h5>
      <p class="">Local Chair</p>
    </li>

  </ul>

<?php include("../../footer.php"); ?>