<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Fabricio Benevenuto"; include("../../header.php"); ?>

  <h2 class="pageTitle">Data Co-chair :<br>Fabricio Benevenuto</h2>

  <div class="image"><img src="/2016/images/organisation/fabricio-benevenuto.png" alt="Fabricio Benevenuto"></div>

  <p><a href='http://homepages.dcc.ufmg.br/~fabricio/'>Fabricio Benevenuto</a> is an associate professor in the Computer Science Department of Federal University at Minas Gerais (UFMG). He received a Ph.D. in Computer Science from UFMG, in 2010, and his thesis received the CAPES award as the best Brazilian thesis in computer science for that year. During his Ph.D., he held research intern positions at HP Labs and MPI-SWS (Max Planck Institute for Software Systems) and, more recently, he worked for two years as assistant professor on Federal University of Ouro Preto (UFOP). He is actively working on social computing and sentiment analysis related projects. His research plans are in the measurement, analysis, design, and evaluation of complex social systems and his main projects are related to complex networks, data mining, machine learning and natural language processing. His work on these topics has led to a number of important publications and widely cited papers. Recently, he was elected member of the Brazilian Academy of Science.</p>
  
<?php include("../../footer.php"); ?>
