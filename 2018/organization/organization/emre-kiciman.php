<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Emre Kiciman"; include("../../header.php"); ?>

  <h2 class="pageTitle">General Chair - Emre Kiciman</h2>

  <div class="image"><img src="/2013/images/organisation/emre-kiciman.png" alt="Farshad Kooti"></div>

  <p> <a href="http://research.microsoft.com/en-us/people/emrek/">Emre Kiciman</a> is a researcher in the Internet Services Research Center at Microsoft Research.</p>

  <p> His interests are in using social data to help people find what they want and need.  To this end, he uses data from social networks to better understand people and their intents, to support exploration of relevant information, and to directly answer questions.  His work involves information extraction from social data, including entity recognition, higher-level analyses, and infrastructure building, as well as more fundamental work to understand and characterize the biases inherent in social media.</p>
  
  <p> Emre's previous research interests include JavaScript application monitoring and optimization, as well as improving the reliability of Internet services architectures and operations. He earned a Ph.D. and M.S. in computer science from Stanford University, and a B.S. in Electrical Engineering and Computer Science from U.C. Berkeley.</p>
  
<?php include("../../footer.php"); ?>