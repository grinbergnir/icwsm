<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Eric Gilbert"; include("../../header.php"); ?>

  <h2 class="pageTitle">Program Co-chair :<br> Eric Gilbert</h2>

  <div class="image"><img src="/2016/images/organisation/eric-gilbert.jpg" alt="Eric Gilbert"></div>

<p><a href='http://eegilbert.org/'>Eric Gilbert</a> is an assistant professor in Georgia Tech's School of Interactive Computing. He joined Tech in 2011, after a PhD at Illinois with Karrie Karahalios.</p>

<p>He is broadly interested in social computing (the social uses of the internet). He has won a number of best paper awards and honorable mentions from ACM'S SIGCHI. His lab also enjoys building experimental web systems, such as Link Different and piggyback prototypes.</p>

<p>Recently, he became a DERP fellow and won Georgia Tech's Young Faculty Award. He is also a Teach For America alum (Chicago, 2002-2004).</p>
  
<?php include("../../footer.php"); ?>