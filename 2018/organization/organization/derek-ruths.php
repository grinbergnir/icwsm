<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Derek Ruths"; include("../../header.php"); ?>

  <h2 class="pageTitle">Data Co-chair :<br>Derek Ruths</h2>

  <div class="image"><img src="/2016/images/organisation/derek-ruths.jpg" alt="Derek Ruths"></div>

  <p><a href='http://www.derekruths.com/'>Derek Ruths</a> is an associate professor of Computer Science at McGill University.  He joined the faculty in 2009 after completing his PhD in Computer Science at Rice University.  A major research direction in his group considers the problem of characterizing and predicting the large-scale dynamics of human behavior in online social platforms.  His ongoing work in this area includes measuring and predicting group demographics from unstructured user-generated content, quantitatively extracting and modeling discourse and narrative from online text, and topic-modeling.  His work has been published in top-tier journals and conferences including Science, EMNLP, ICWSM, and PLoS Computational Biology.  His research is currently funded by a wide array of organizations including NSERC, SSHRC, tech companies, and the US National Science Foundation - underscoring the broad, interdisciplinary nature of his work.</p>
  
<?php include("../../footer.php"); ?>
