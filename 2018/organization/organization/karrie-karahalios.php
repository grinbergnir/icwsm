<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Karrie Karahalios"; include("../../header.php"); ?>

  <h2 class="pageTitle">Demos Co-Chair - Karrie Karahalios</h2>

  <div class="image"><img src="/2013/images/organisation/karrie-karahalios.png" alt="Karrie Karahalios"></div>

  <p>Karrie Karahalios is an associate professor in the department of computer science at University of Illinois at Urbana-Champaign.</p>

  <p> Her research interest include designing and implementing communication channels for interaction between people in networked environments. These channels involve sensing the social cues people perceive in networked electronic spaces and incorporating them into the physical and virtual interface such that mediated interaction becomes intuitive and breaks away from the traditional computer screen.</p>
  
  
  
<?php include("../../footer.php"); ?>