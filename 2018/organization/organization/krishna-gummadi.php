<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Krishna P. Gummadi"; include("../../header.php"); ?>

  <h2 class="pageTitle">General Co-chair :<br> Krishna P. Gummadi</h2>

  <div class="image"><img src="/2016/images/organisation/krishna-gummadi.png" alt="Krishna Gummadi"></div>

  <p><a href='http://www.mpi-sws.org/~gummadi/'>Krishna Gummadi</a> is an active member of the ICWSM community and a co-author of seven ICWSM papers over the years, including the Best Paper in 2012. He is a tenured faculty member and head of the Networked Systems research group at the Max Planck Institute for Software Systems (MPI-SWS) in Germany. Krishna's research interests are in the measurement, analysis, design, and evaluation of complex Internet-scale systems. His current projects focus on understanding and building social Web systems. Krishna's work on online social networks, Internet access networks, and peer-to-peer systems has led to a number of widely cited papers and award (best) papers at ACM/Usenix's SOUPS, AAAI's ICWSM, Usenix's OSDI, ACM's SIGCOMM IMW, and SPIE's MMCN conferences. He has served as the PC co-chair for the ACM Internet Measurement Conference 2013, the ACM Conference on Online Social Networks 2014, and the International World Wide Web (WWW) Conference 2015.

</p>


<!--<p><a href='http://www.mpi-sws.org/~gummadi/'>Krishna Gummadi</a> is a tenured faculy member and head of the Networked Systems research group at the Max Planck Institute for Software Systems (MPI-SWS) in Germany. He received his Ph.D. (2005) and M.S. (2002) degrees in Computer Science and Engineering from the University of Washington. He also holds a B.Tech (2000) degree in Computer Science and Engineering from the Indian Institute of Technology, Madras.</p>

<p>Krishna's research interests are in the measurement, analysis, design, and evaluation of complex Internet-scale systems. His current projects focus on understanding and building social Web systems. Specifically, they tackle the challenges associated with protecting the privacy of users sharing personal data, understanding and leveraging word-of-mouth exchanges to spread information virally, and finding relevant and trustworthy sources of information in crowds.</p>

<p>Krishna's work on online social networks, Internet access networks, and peer-to-peer systems has led to a number of widely cited papers and award (best) papers at ACM/Usenix's SOUPS, AAAI's ICWSM, Usenix's OSDI, ACM's SIGCOMM IMW, and SPIE's MMCN conferences.</p>-->
  
<?php include("../../footer.php"); ?>
