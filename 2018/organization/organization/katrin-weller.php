<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Katrin Weller"; include("../../header.php"); ?>

  <h2 class="pageTitle">Local Co-Chair :<br>Katrin Weller</h2>

  <div class="image"><img src="/2016/images/organisation/katrin-weller.jpg" alt="Katrin Weller"></div>

  <p><a href='http://katrinweller.net/'>Katrin Weller</a> is an information scientist and postdoctoral researcher at GESIS Leibniz Institute for the Social Sciences in Cologne. Her research focuses on new approaches to using social media in social science research, e.g. for studying elections or scholarly communication. At GESIS she is responsible for developing new services for social scientists that are based on computational social science research, including approaches for documenting and archiving social media datasets.</p>
<p>In 2015, she was awarded one of two inaugural Digital Studies fellowships at the Library of Congress' John W. Kluge Center and spent several months at the Library of Congress to start a new project on the use of social media data as novel resources for future historians.</p>

  
<?php include("../../footer.php"); ?>