<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Daniele Quercia"; include("../../header.php"); ?>

  <h2 class="pageTitle">General Chair - Daniele Quercia</h2>

  <div class="image"><img src="/2013/images/organisation/daniele-quercia.png" alt="Daniele Quercia"></div>

  <p>Daniele is passionate about mining data to answer multidisciplinary research questions. From urban informatics to personality studies, he is exploring the complex relationship between our offline and online worlds at Yahoo! Labs in Barcelona. Previously, as Horizon researcher at the University of Cambridge, he was working on a grand challenge that aimed to transform our digital footprints into real-world services. His research lies at the intersection of data mining, social computing, urban informatics, and computational social science.  His research has been published in leading Computer Science venues including  ICSE, Ubicomp, ICDM, CSCW, RecSys, WSDM, and WWW, received a best paper award from ACM Ubicomp and an honorable mention from AAAI ICWSM, and has been featured on La Repubblica,  The Independent, New Scientist, Le Monde, and BBC. He spoke at TEDx Barcelona and Falling Walls Berlin, wrote for BBC Future, and has been named one of Fortune magazine's 2014 Big Data All-Stars. Previously,he was a Horizon senior researcher at The Computer Laboratory of the University of Cambridge. Before starting to work in Cambridge (UK), he lived in Cambridge (USA) where he was Postdoctoral Associate at MIT and worked on social networks in a city context. Before joining MIT, Daniele received his PhD from UC London, and his thesis was nominated for BCS Best British PhD dissertation in Computer Science. During his PhD, he was a Microsoft Research PhD Scholar and MBA Technology Fellow of London Business School. To understand the meaning of being a computer scientist in today's global society, he interned at the National Research Council in Barcelona and at NII in Tokyo and, before that, he studied at PoliTO (Italy), KIT (Germany), and UIC (USA). He is now a senior member of Wolfson College in Cambridge, and Honary Research Fellow at UC London.</p>

  <p></p>
  
  <p></p>
  
<?php include("../../footer.php"); ?>
