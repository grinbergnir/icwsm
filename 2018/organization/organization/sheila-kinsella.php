<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Sheila Kinsella"; include("../../header.php"); ?>

  <h2 class="pageTitle">Industry Chair - Sheila Kinsella</h2>

  <div class="image"><img src="/2012/images/organisation/sheila-kinsella.png" alt="Sheila Kinsella"></div>

  <p>Sheila Kinsella is a software engineer at <a href="http://datahug.com">Datahug</a>, a company that unlocks business relationships using existing communications data. She completed a PhD in 2011 at the <a href="http://www.deri.ie/">Digital Enterprise Research Institute (DERI)</a>, <a href="http://www.nuigalway.ie/">National University of Ireland, Galway</a>. She graduated from the <a href="http://www.nuigalway.ie/">National University of Ireland, Galway</a> in 2006 and received a BE degree with 1st class honours in <a href="http://www.eee.nuigalway.ie/">Electronic and Computer Engineering</a>.</p>

  <p>Her research at DERI focused on social media, Semantic Web and information retrieval, and her thesis was titled <i>"Augmenting Social Media Items with Metadata using Related Web Content"</i>. During her graduate studies, Sheila performed research internships at <a href="http://labs.yahoo.com/Yahoo_Labs_Barcelona">Yahoo! Labs Barcelona</a> and at the <a href="http://www.epfl.ch/">�cole Polytechnique F�d�rale de Lausanne (EPFL)</a> and was	an associate researcher with <a href="http://www.ibm.com/ibm/cas/sites/dublin">IBM's Centre for Advanced Studies in Dublin</a>.</p>

<?php include("../../footer.php"); ?>
