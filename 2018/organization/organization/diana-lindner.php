<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Diana Lindner"; include("../../header.php"); ?>

  <h2 class="pageTitle">Local Co-Chair :<br>Diana Lindner</h2>

  <div class="image"><img src="/2016/images/organisation/diana-lindner.png" alt="Diana Lindner"></div>

  <p><a href='http://www.gesis.org/das-institut/mitarbeiterverzeichnis/?alpha=L&name=Diana%2CLindner'>Diana Lindner</a> is an administrative assistant at the Computational Social Science department at GESIS - Leibniz Institute for the Social Sciences. Founded in 2013, the department organizes regular networking and training events for social scientists such as the CSS Seminar or the Computational Social Science Winter Symposium. Due to these and other activities, Diana has broad experience in organizing scientific events nationally and internationally.</p>

  
<?php include("../../footer.php"); ?>