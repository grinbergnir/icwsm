<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Jie Tang"; include("../../header.php"); ?>

  <h2 class="pageTitle">Data Chair - Jie Tang</h2>

  <div class="image"><img src="/2014/images/organisation/jie-tang.jpg" alt="Jie Tang"></div>

  <p>Jie Tang is an associate professor in Department of Computer Science and Technology of Tsinghua University. He obtained Ph.D. in DCST of Tsinghua University in 2006. He became ACM Professional member in 2006 and IEEE member in 2007. His research interests include social network theories, data mining methodologies, machine learning algorithms, and semantic web technologies.</p>
    
<?php include("../../footer.php"); ?>
