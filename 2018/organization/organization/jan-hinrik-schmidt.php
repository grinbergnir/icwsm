<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Jan-Hinrik Schmidt"; include("../../header.php"); ?>

  <h2 class="pageTitle">Regional Chair: Europe - Jan-Hinrik Schmidt</h2>

  <div class="image"><img src="/2012/images/organisation/jan-hinrik-schmidt.png" alt="Jan-Hinrik Schmidt"></div>

  <p>Dr. Jan-Hinrik Schmidt is Senior Researcher for digital interactive media and political communication at the Hans-Bredow-Institute, an independent media research institute based in Hamburg (Germany). After studying sociology at Bamberg University (Germany) and West Virginia University (US), he gained his PhD in 2004.</p>
  
  <p>His research interests focus on the characteristics, practices and social consequences of online-based communication and the social web. Additionally, he is researching uses of digital games. He is author of several monographies, journal papers, book chapters and research reports, reviewer for various conferences and journals, and member of the editorial board of the open access online-journal "kommunikation@gesellschaft" as well as the print journal "Medien & Kommunikationswissenschaft". Further information is available on his <a href="http://www.schmidtmitdete.de">weblog</a>.</p>
  
<?php include("../../footer.php"); ?>
