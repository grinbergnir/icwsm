<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Henry Lieberman"; include("../../header.php"); ?>

  <h2 class="pageTitle">MIT Sponsor - Henry Lieberman</h2>

  <div class="image"><img src="/2013/images/organisation/henry-lieberman.jpg" alt="Henry Lieberman"></div>

  <p> Henry Lieberman is Principal Research Scientist at the MIT Media Laboratory. He works with the Agents Group. He is especially interested in using Artificial Intelligence techniques to improve all kinds of user interfaces. Much of Henry's work is centered on the use of Common Sense knowledge, simple facts about people and everyday life. He also works on bringing the full procedural power of computers to non-expert users through Programming by Example, natural language and visual programming. Henry is also interested in a wide variety of other topics in AI and interactive computing.</p>

  <p></p>
  
  <p></p>
  
<?php include("../../footer.php"); ?>