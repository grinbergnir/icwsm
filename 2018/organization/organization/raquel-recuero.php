<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Raquel Recuero"; include("../../header.php"); ?>

  <h2 class="pageTitle">Tutorials Co-Chair: Raquel Recuero</h2>

  <div class="image"><img src="/2013/images/organisation/raquel-recuero.png" alt="Raquel Recuero"></div>

  <p>Dr. Raquel Recuero is an Associate Professor at the Departments of Applied Linguistics and Social Communication in Universidade Cat�lica de Pelotas (UCPel) in Brazil. Her research focuses on Internet social networks, virtual communities and computer mediated-communication in general, trying to understand the impact of the Internet in sociability and language in South America and Brazil.</p>
  
  <p>She received her PhD in Communication and Information from Universidade Federal do Rio Grande do Sul (UFRGS) for her dissertation on "Social Networks in <a href="http://www.fotolog.com/">Fotolog.com</a>" in 2006, and has recently published her first book in Portuguese, Internet Social Networks (Redes Sociais na Internet: Sulina, 2009). She also has worked as a research consultant for several companies, including Google and <a href="http://www.uol.com.br/">UOL</a>.</p>
  
<?php include("../../footer.php"); ?>
