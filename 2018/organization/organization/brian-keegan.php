<?php $section = "Organisation"; $subsection = "Organisation"; $subsubsection = "Brian Keegan"; include("../../header.php"); ?>

  <h2 class="pageTitle">Publicity Co-chair - Brian Keegan</h2>

  <div class="image"><img src="/2014/images/organisation/brian-keegan-thumb.jpg" alt="Eytan Adar"></div>

  <p>Brian Keegan is a post-doctoral research fellow at Northeastern University. He received his PhD in Media, Technology, and Society from the School of Communication at Northwestern University under the supervision of Darren Gergle and Noshir Contractor. His dissertation examined the structure and dynamics of high-tempo knowledge collaboration in Wikipedia's coverage of breaking news events.<br><br>

Brian's research is in the areas of computational social science and social computing. He is interested in digital journalism, crowdsourced creativity, online political behavior, and historical organizational change and uses methods such as social network analysis, data visualization, and statistical learning to examine large-scale datasets.</p>
  
<?php include("../../footer.php"); ?>