<?php $section = "Program"; $subsection = "Speakers"; include("../../header.php"); ?>
<br>
  <h2 class="pageTitle">Keynote Speakers</h2>

<ul>
       <!--<li class="">
          <a name="speaker3"></a>
          <img src="/2016/images/speakers/kate_crawford.jpg" class="project-thumb" alt="">
          <h3 class="">Kate Crawford</h3>
          <p>Principal Researcher  <br>  Microsoft Research New York City</p>
          <p><b>Talk title: From Human Subjects to Data Subjects</b></p>
          <p><b><a href="http://www.katecrawford.net/index.html" target="_blank">Kate Crawford</a></b> is a Principal Researcher at Microsoft Research NYC, a Visiting Professor at MIT's Center for Civic Media, and a Senior Fellow at NYU's Information Law Institute. She is widely published on the social and political implications of large-scale data systems and she co-edited a special issue in IJOC on interdisciplinary responses to data science. She is on the World Economic Forum's Council on Data for Development, and is a member of the UN's new Thematic Network on Data. She's also a co-director of the NSF's Council for Big Data, Ethics & Society. She is currently completing a new book on data and ethics forthcoming with Yale University Press.</p>
        </li>-->



        <li class="">
          <a name="speaker1"></a>
          <img src="/2016/images/speakers/Lise-Getoor.jpg" class="project-thumb" alt="">
          <h3 class="">Lise Getoor</h3>
          <p>Professor, Computer Science, 
            <br>
            University of California Santa Cruz</p>
   <p><b>Talk title: Scalable Collective Reasoning for Richly Structured Socio-Behavioral Data</b></p>
<p>Online data from weblogs and social media provide richly structured socio-behavioral data. However, using noisy and incomplete data from social media often requires inferring unobserved attributes of individuals and their relationships. And doing this correctly requires complex collective reasoning about dependencies among those individuals. In this talk, I will describe some common inference patterns needed for socio-behavioral networks including: collective classification (predicting missing labels for nodes), link prediction (predicting edges), and entity resolution (determining when two nodes refer to the same underlying entity). I will then describe in detail a highly scalable open-source probabilistic programming language being developed within my group to solve these challenges.</p>
   <p><b><a href="https://getoor.soe.ucsc.edu/" target="_blank">Lise Getoor</a></b> is a Professor in the Computer Science Department at UC Santa Cruz. Her research areas include machine learning, data integration and reasoning under uncertainty, with an emphasis on graph and network data. She is a AAAI Fellow, serves on the Computing Research Association and International Machine Learning Society Boards, was co-chair of ICML 2011, is a recipient of an NSF Career Award and ten best paper and best student paper awards. She received 
her PhD from Stanford University, her MS from UC Berkeley, and her BS from UC Santa Barbara, and was a Professor at the University of Maryland, College Park from 2001 - 2013.<p>
<!--<br><br><br><br>-->
        </li>


        <li class="">
          <a name="speaker2"></a>
          <img src="/2016/images/speakers/Amir-Goldberg.jpg" class="project-thumb" alt="">
          <h3 class="">Amir Goldberg</h3>
          <p>Assistant Professor <br>  Stanford Graduate School of Business</p>
          <p><b>Talk title: Decoding Culture from Digital Traces</b></p>
          <p>Culture is one of the least understood concepts in the social sciences. We all know it exists, but disagree on how to define and measure it. In this talk, I will discuss how the web and online communication open a window into understanding and measuring culture in ways that were unimaginable until recently. In particular, I will focus on language-based algorithms drawn from organizational email communication that measure individual cultural fit, and can predict promotion, dismissal and voluntary turnover. This work improves significantly on existing methods for measuring cultural alignment between individuals and groups, and provides new insights into how culture operates and affects individual and group processes. </p>
          <p><b><a href="http://web.stanford.edu/~amirgo/" target="_blank">Amir Goldberg</a></b> is an Assistant Professor of Organizational Behavior and (by courtesy) Sociology at Stanford University. His research lies at the intersection of organization studies, cultural sociology and network science. He is interested in understanding how social meanings emerge and solidify through social interaction, and what role network structures play in this process. He uses and develops computationally intensive network-based methods to study how new cultural and organizational categories take form as people and organizational actors interact.</p>
        </li>
  
<li class="">
          <a name="speaker3"></a>
          <img src="/2016/images/speakers/suresh.jpg" class="project-thumb" alt="">
          <h3 class="">Suresh Venkatasubramanian</h3>
          <p>Associate Professor <br> School of Computing at the University of Utah </p>
          <p><b>Talk title: Algorithmic Fairness: From social good to mathematical framework</b></p>
          <p>Machine learning has taken over our world, in more ways than we realize. You might get book recommendations, or an efficient route to your destination, or even a winning strategy for a game of Go. But you might also be admitted to college, granted a loan, or hired for a job based on algorithmically enhanced decision-making. We believe machines are neutral arbiters: cold, calculating entities that always make the right decision, that can see patterns that our human minds can't or won't. But are they? Or is decision-making-by-algorithm a way to amplify, extend and make inscrutable the biases and discrimination that is prevalent in society? To answer these questions, we need to go back &mdash; all the way to the original ideas of justice and fairness in society. We also need to go forward &mdash; towards a mathematical framework for talking about justice and fairness in machine learning. Dr. Venkatasubramanian will talk about the growing landscape of research in algorithmic fairness: how we can reason systematically about biases in algorithms, and how we can make our algorithms fair(er).</p>
          <p><b><a href="http://www.cs.utah.edu/~suresh/web/" target="_blank">Suresh Venkatasubramanian</a></b>  is an associate professor in the School of Computing at the University of Utah. He did his Ph.D at Stanford University, and did a stint at AT&T Research before joining the U. His research interests include computational geometry, data mining and machine learning, with special interests in high dimensional geometry, large data algorithms, clustering and kernel methods. He received an NSF CAREER award in 2010. He spends much of his time now thinking about the problem of "algorithmic fairness": how we can ensure that algorithmic decision-making is fair, accountable and transparent. His work has been covered on Science Friday, NBC News, and Gizmodo, as well as in various print outlets. </p>
        </li>
      </ul>


<?php include("../../footer.php"); ?>