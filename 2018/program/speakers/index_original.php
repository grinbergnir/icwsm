<?php $section = "Program"; $subsection = "Speakers"; include("../../header.php"); ?>

  <h2 class="pageTitle">Keynote Speakers</h2>

<ul>
        <li class="">
          <a name="speaker1"></a>
          <img src="/2016/images/speakers/Lise-Getoor.jpg" class="project-thumb" alt="">
          <h3 class="">Lise Getoor</h3>
          <p>Professor, Computer Science, 
            <br>
            University of California Santa Cruz</p>
    <!--<p><b>The StudentLife Project: Using Smartphones to Assess Sociability, Mental Health, and Academic Performance of College Students</b></p>
    <p>Much of the stress and strain of student life remains hidden. The StudentLife continuous sensing app assesses the day-to-day and week-by-week impact of workload on stress, sleep, activity, mood, sociability, mental health and academic performance of a single class of 48 students across a 10 week term at Dartmouth College using Android phones.  Results from the StudentLife study show a number of significant correlations between the automatic objective sensor data from smartphones and mental health and educational outcomes of the student body.  We identify a Dartmouth term lifecycle in the data that shows students start the term with high positive affect and conversation levels, low stress, and healthy sleep and daily activity patterns.  As the term progresses and the workload increases, stress appreciably rises while positive affect, sleep, conversation and activity drops off. We show how passive sensing data from phones can infer studying and partying behavior across the term. Finally, we show how a smartphone can automatically predict student GPA.</p>
    <p><b><a href="http://www.cs.dartmouth.edu/~campbell/" target="_blank">Andrew T. Campbell</a></b> is a professor of computer science at Dartmouth College, where he leads the smartphone sensing group. His group developed a number of early sensing applications for smartphones and is currently focused on turning the everyday smartphone into a cognitive phone.  Andrew received his Ph.D. in computer science (1996) from Lancaster University, England and the NSF Career Award (1999) for his research in programmable wireless networks. Before joining Dartmouth, he was a tenured associate professor of electrical engineering at Columbia University (1996-2005).  Prior to that, he spent ten years in the software industry in the US and Europe leading the development of operating systems and wireless networks.  Andrew has been a technical program chair of a number of conferences in his area including ACM MobiCom, ACM MobiHoc and ACM SenSys; also, he co-chaired the NSF sponsored workshop on pervasive computing at scale. Andrew spent his sabbatical year (2003-2004) at the computer laboratory, Cambridge University, as an UK EPSRC visiting fellow, and fall 2009 as a visiting professor at the University of Salamanca, Spain.<p>-->
<br><br><br><br>
        </li>
  
        <li class="">
          <a name="speaker2"></a>
          <img src="/2016/images/speakers/Amir-Goldberg.jpg" class="project-thumb" alt="">
          <h3 class="">Amir Goldberg</h3>
          <p>Assistant Professor <br>  Stanford Graduate School of Business</p>
          <p><b>Talk title : Modeling culture with big data</b></p>
          <!--<p>The "smart city" approach suggests we simply need appropriate and accurate monitoring equipment to reveal all the intricacies and complexities of a finite and knowable universe -- technology helps us do these things "better", so, the argument goes, we need more technology. Yet, cities are what Russell Ackoff might call a "mess". Every issue interrelates to and interacts with every other issue; there is no clear "solution"; there are no universal objective parameters; and sometimes those working on problems are actually the ones who are causing them. Urban data isn't simply discovered, it is invented, manipulated and crafted; and cities aren't 'solved', they are created through the actions, motivations and decisions of their citizen. This talk will discuss how we can, collaboratively, design cities and structure participation both online and offline, with specific reference to Usman's interactive environments, urban spectacles, collaboration platforms and other concrete examples.</p>-->
          <p><b><a href="http://web.stanford.edu/~amirgo/" target="_blank">Amir Goldberg</a></b> is an Assistant Professor of Organizational Behavior and (by courtesy) Sociology at Stanford University. His research lies at the intersection of organization studies, cultural sociology and network science. He is interested in understanding how social meanings emerge and solidify through social interaction, and what role network structures play in this process. He uses and develops computationally intensive network-based methods to study how new cultural and organizational categories take form as people and organizational actors interact.</p>
        </li>
      </ul>


<?php include("../../footer.php"); ?>