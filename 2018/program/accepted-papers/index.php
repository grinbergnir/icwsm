<?php $section = "Program"; $subsection = "Accepted Papers"; include("../../header.php"); ?>

<h2 class="pageTitle">ICWSM-16 Accepted Papers</h2>

<h3>Full Papers</h3>

<p><b>  Message Impartiality in Social Media Discussions </b><br>
Muhammad Bilal Zafar, Krishna P. Gummadi, Cristian Danescu-Niculescu-Mizil </p>


<p><b>  Distinguishing between Topical and Non-Topical Information Diffusion Mechanisms in Social Media </b><br>
Przemyslaw A. Grabowicz, Niloy Ganguly, Krishna P. Gummadi </p>


<p><b>  Are You Charlie or Ahmed? Cultural Pluralism in Charlie Hebdo Response on Twitter </b><br>
Jisun An, Haewoon Kwak, Yelena Mejova, Sonia Alonso Saenz De Oger, Braulio Gomez Fortes </p>


<p><b>  Fetishizing Food in Digital Age: #foodporn around the World </b><br>
Yelena Mejova, Sofiane Abbar, Hamed Haddadi </p>


<p><b>  "Will Check-in for Badges": Understanding Bias and Misbehavior on Location-Based Social Networks </b><br>
Gang Wang, Sarita Y. Schoenebeck, Haitao Zheng, Ben Y. Zhao </p>


<p><b>  Measuring the Efficiency of Charitable Giving with Content Analysis and Crowdsourcing </b><br>
Ceren Budak, Justin M. Rao </p>


<p><b>  Sentiment-Based Topic Suggestion for Micro-Reviews </b><br>
Ziyu Lu, Nikos Mamoulis, Evaggelia Pitoura, Panayiotis Tsaparas </p>


<p><b>  Your Age Is No Secret: Inferring Microbloggers' Ages via Content and Interaction Analysis </b><br>
Jinxue Zhang, Xia Hu, Yanchao Zhang, Huan Liu </p>


<p><b>  TiDeH: Time-Dependent Hawkes Process for Predicting Retweet Dynamics </b><br>
Ryota Kobayashi, Renaud Lambiotte </p>


<p><b>  Dynamic Data Capture from Social Media Streams: A Contextual Bandit Approach </b><br>
Thibault Gisselbrecht, Sylvain Lamprier, Patrick Gallinari </p>


<p><b>  What the Language You Tweet Says about Your Occupation </b><br>
Tianran Hu, Haoyuan Xiao, Jiebo Luo, Thuy-vy Thi Nguyen </p>


<p><b>  Social Media Participation in an Activist Movement for Racial Equality </b><br>
Munmun De Choudhury, Shagun Jhaver, Benjamin Sugar, Ingmar Weber </p>


<p><b>  Emotions, Demographics and Sociability in Twitter Interactions </b><br>
Kristina Lerman, Megha Arora, Luciano Gallegos, Ponnurangam Kumaraguru, David Garcia </p>


<p><b>  Tracking Secret-Keeping in Emails </b><br>
Yla R. Tausczik, Cindy K. Chung, James W. Pennebaker </p>


<p><b>  On Unravelling Opinions of Issue Specific-Silent Users in Social Media </b><br>
Wei Gong, Ee-Peng Lim, Feida Zhu, Pei Hua Cher </p>


<p><b>  Understanding Anti-Vaccination Attitudes in Social Media </b><br>
Tanushree Mitra, Scott Counts, James W. Pennebaker </p>


<p><b>  Pub Crawling at Scale: Tapping Untappd to Explore Social Drinking </b><br>
Martin J. Chorley, Luca Rossi, Gareth Tyson, Matthew J. Williams </p>


<p><b>  The Status Gradient of Trends in Social Media </b><br>
Rahmtin Rotabi, Jon Kleinberg </p>


<p><b>  When a Movement Becomes a Party: Computational Assessment of New Forms of Political Organization in Social Media </b><br>
Pablo Arag&oacute;n, Yana Volkovich, David Laniado, Andreas Kaltenbrunner </p>


<p><b>  Changing Names in Online News Comments at the New York Times </b><br>
Simranjit Singh Sachar, Nicholas Diakopoulos </p>


<p><b>  Space Collapse: Reinforcing, Reconfiguring and Enhancing Chinese Social Practices through WeChat </b><br>
Yang Wang, Yao Li, Bryan Semaan, Jian Tang </p>


<p><b>  Analyzing Personality through Social Media Profile Picture Choice </b><br>
Leqi Liu, Daniel Preotiuc-Pietro, Zahra Riahi Samani, Mohsen E. Moghaddam, Lyle Ungar </p>


<p><b>  Identifying Platform Effects in Social Media Data </b><br>
Momin M. Malik, J&uuml;rgen Pfeffer </p>


<p><b>  Networks of Gratitude: Structures of Thanks and User Expectations in Workplace Appreciation Systems </b><br>
Emma S. Spiro, J. Nathan Matias, Andr&eacute;s Monroy-Hern&aacute;ndez </p>


<p><b>  The Dynamics of Emotions in Online Interaction </b><br>
David Garcia, Arvid Kappas, Dennis K&uuml;ster, Frank Schweitzer </p>


<p><b>  Cross Social Media Recommendation </b><br>
Xiaozhong Liu, Tian Xia, Yingying Yu, Chun Guo, Yizhou Sun </p>


<p><b>  Predictability of Popularity: Gaps between Prediction and Understanding </b><br>
Benjamin Shulman, Amit Sharma, Dan Cosley </p>


<p><b>  On the Behaviour of Deviant Communities in Online Social Networks </b><br>
Mauro Coletto, Luca Maria Aiello, Claudio Lucchese, Fabrizio Silvestri </p>


<p><b>  Who Did What: Editor Role Identification in Wikipedia </b><br>
Diyi Yang, Aaron Halfaker, Robert Kraut, Eduard Hovy </p>


<p><b>  The Emotional and Chromatic Layers of Urban Smells </b><br>
Daniele Quercia, Luca Maria Aiello, Rossano Schifanella </p>


<p><b>  Mining Pro-ISIS Radicalisation Signals from Social Media Users </b><br>
Matthew Rowe, Hassan Saif </p>


<p><b>  Aligning Popularity and Quality in Online Cultural Markets </b><br>
Pascal Van Hentenryck, Andr&eacute;s Abeliuk, Franco Berbeglia, Felipe Maldonado, Gerardo Berbeglia </p>


<p><b>  Investigating the Observability of Complex Contagion in Empirical Social Networks </b><br>
Clay Fink, Aurora Schmidt </p>


<p><b>  Shirtless and Dangerous: Quantifying Linguistic Signals of Gender Bias in an Online Fiction Writing Community </b><br>
Ethan Fast, Tina Vachovsky, Michael S. Bernstein </p>


<p><b>  EigenTransitions with Hypothesis Testing: The Anatomy of Urban Mobility </b><br>
Ke Zhang, Yu-Ru Lin, Konstantinos Pelechrinis </p>


<p><b>  Lost in Propagation? Unfolding News Cycles from the Source </b><br>
Chenhao Tan, Adrien Friggeri, Lada Adamic </p>


<p><b>  Power Imbalance and Rating Systems </b><br>
Bogdan State, Bruno Abrahao, Karen S. Cook </p>


<p><b>  User Migration in Online Social Networks: A Case Study on Reddit during a Period of Community Unrest </b><br>
Edward Newell, David Jurgens, Haji Mohammad Saleem, Hardik Vala, Jad Sassine, Caitrin Armstrong, Derek Ruths </p>


<p><b>  Twitter's Glass Ceiling: The Effect of Perceived Gender on Online Visibility </b><br>
Shirin Nilizadeh, Anne Groggel, Peter Lista, Srijita Das, Yong-Yeol Ahn, Apu Kapadia, Fabio Rojas </p>


<p><b>  TweetGrep: Weakly Supervised Joint Retrieval and Sentiment Analysis of Topical Tweets </b><br>
Satarupa Guha, Tanmoy Chakraborty, Samik Datta, Mohit Kumar, Vasudeva Varma </p>


<p><b>  Understanding Communities via Hashtag Engagement: A Clustering Based Approach </b><br>
Orianna De Masi, Douglas Mason, Jeffrey Ma </p>


<p><b>  Science, AskScience, and BadScience: On the Coexistence of Highly Related Communities </b><br>
Jack Hessel, Chenhao Tan, Lillian Lee </p>


<p><b>  CrowdLens: Experimenting with Crowd-Powered Recommendation and Explanation </b><br>
Shuo Chang, Maxwell F. Harper, Lingfei He, Loren G. Terveen </p>


<p><b>  Power of Earned Advertising on Social Network Services: A Case Study of Friend Tagging on Facebook </b><br>
Jaimie Y. Park, Yunkyu Sohn, Sue Moon </p>


<p><b>  Bad Apples Spoil the Fun: Quantifying Cheating Influence in Online Gaming </b><br>
Xiang Zuo, Clayton Gandy, John Skovertz, Adriana Iamnitchi </p>


<p><b>  "Blissfully happy" or "ready to fight": Varying Interpretations of Emoji </b><br>
Hannah Miller, Jacob Thebault-Spieker, Shuo Chang, Isaac Johnson, Loren Terveen, Brent Hecht </p>


<p><b>  #PrayForDad: Learning the Semantics behind Why Social Media Users Disclose Health Information </b><br>
Zhijun Yin, You Chen, Daniel Fabbri, Jimeng Sun, Bradley Malin </p>


<p><b>  Discovering Response-Eliciting Factors in Social Question-Answering: A Reddit Inspired Study </b><br>
Danish, Yogesh Dahiya, Partha Talukdar </p>


<p><b>  Sequential Voting Promotes Collective Discovery in Social Recommendation Systems </b><br>
L. Elisa Celis, Peter M. Krafft, Nathan Kobe </p>


<p><b>  Theme-Relevant Truth Discovery on Twitter: An Estimation Theoretic Approach </b><br>
Dong Wang, Jermaine Marshall, Chao Huang </p>


<p><b>  Journalists and Twitter: A Multidimensional Quantitative Description of Usage Patterns </b><br>
Mossaab Bagdouri </p>


<p><b>  Predicting Perceived Brand Personality with Social Media </b><br>
Anbang Xu, Haibin Liu, Liang Gou, Rama Akkiraju, Jalal Mahmud, Vibha Sinha, Yuheng Hu, Mu Qiao </p>


<h3>Posters</h3>

<p><b>  Evidence of Online Performance Deterioration in User Sessions on Reddit </b><br>
Philipp Singer, Emilio Ferrara, Farshad Kooti, Markus Strohmaier, Kristina Lerman </p>


<p><b>  Analyzing the Targets of Hate in Online Social Media </b><br>
Leandro Silva, Mainack Mondal, Denzil Correa, Fabricio Benevenuto, Ingmar Weber </p>


<p><b>  "Come join and let's BOND":  Authenticity and Legitimacy Building on YouTube's Beauty Community </b><br>
Florencia Garcia Rapp </p>


<p><b>  Scalable Urban Data Collection From TheWeb </b><br>
Rijurekha Sen, Daniele Quercia, Carmen Vaca Ruiz, Krishna P. Gummadi </p>


<p><b>  Dissemination Biases of Social Media Channels: On The Topical Coverage of Socially Shared News </b><br>
Abhijnan Chakraborty, Saptarshi Ghosh, Niloy Ganguly, Krishna P. Gummadi </p>


<p><b>  The Road to Popularity: The Dilution of Growing Audience on Twitter </b><br>
Przemyslaw A. Grabowicz, Mahmoudreza Babaei, Juhi Kulshrestha, Ingmar Weber </p>


<p><b>  Tweet Acts: A Speech Act Classifier for Twitter </b><br>
Soroush Vosoughi, Deb Roy </p>


<p><b>  Deciphering the 2016 U.S. Presidential Campaign in the Twitter Sphere: A Comparison of the Trumpists and Clintonists </b><br>
Yu Wang, Yuncheng Li, Jiebo Luo </p>


<p><b>  On the Correlation between Topic and User Behaviour in Online Communities </b><br>
Erik Aumayr, Conor Hayes </p>


<p><b>  A Semi-Automatic Method for Efficient Detection of Stories on Social Media </b><br>
Soroush Vosoughi, Deb Roy </p>


<p><b>  Evaluating Public Response to the Boston Marathon Bombing and Other Acts of Terrorism through Twitter </b><br>
Cody Buntain, Jennifer Golbeck, Brooke Liu, Gary LaFree </p>


<p><b>  Our House, in The Middle of Our Tweets </b><br>
Dan Tasse, Alex Sciuto, Jason I. Hong </p>


<p><b>  Catching Fire via 'Likes': Inferring Topic Preferences of Trump Followers on Twitter </b><br>
Yu Wang, Jiebo Luo, Richard Niemi, Yuncheng Li, Tianran Hu </p>


<p><b>  Finding Sensitive Accounts on Twitter: An Automated Approach Based on Follower Anonymity </b><br>
Sai Teja Peddinti, Keith W. Ross, Justin Cappos </p>


<p><b>  The Quality of Online Answers to Parents Who Suspect that Their Child Has an Autism Spectrum Disorder </b><br>
Ayelet Ben-Sasson, Dan Pelleg, Elad Yom-Tov </p>


<p><b>  You Are What Apps You Use: Demographic Prediction Based on User's Apps </b><br>
Eric Malmi, Ingmar Weber </p>


<p><b>  Tracking the Trackers: A Large-Scale Analysis of Embedded Web Trackers </b><br>
Sebastian Schelter, J&eacute;r&ocirc;me Kunegis </p>


<p><b>  Temporal Opinion Spam Detection by Multivariate Indicative Signals </b><br>
Junting Ye, Santhosh Kumar, Leman Akoglu </p>


<p><b>  Identifying Rhetorical Questions in Social Media </b><br>
Suhas Ranganath, Xia Hu, Jiliang Tang, Suhang Wang, Huan Liu </p>


<p><b>  Analyzing Social Event Participants for a Single Organizer </b><br>
Jyun-Yu Jiang, Cheng-Te Li </p>


<p><b>  Freshman or Fresher? Quantifying the Geographic Variation of Language in Online Social Media </b><br>
Vivek Kulkarni, Bryan Perozzi, Steven Skiena </p>


<p><b>  Precise Localization of Homes and Activities: Detecting Drinking-While-Tweeting Patterns in Communities </b><br>
Nabil Hossain, Tianran Hu, Roghayeh Feizi, Ann Marie White, Jiebo Luo, Henry Kautz </p>


<p><b>  Topic Modeling in Twitter: Aggregating Tweets by Conversations </b><br>
David Alvarez-Melis, Martin Saveski </p>


<p><b>  Hyphe, a Curation-Oriented Approach to Web Crawling for the Social Sciences </b><br>
Mathieu Jacomy, Paul Girard, Benjamin Ooghe-Tabanou, Tommaso Venturini </p>


<p><b>  Characterizing Deleted Tweets and their Authors </b><br>
Parantapa Bhattacharya, Niloy Ganguly </p>


<p><b>  Fusing Audio, Textual and Visual Features for Sentiment Analysis of News Videos </b><br>
Mois&eacute;s H. R. Pereira, Fl&aacute;vio L. C. P&aacute;dua, Adriano C. M. Pereira, Fabricio Benevenuto, Daniel H. Dalip </p>


<p><b>  Detection of Promoted Social Media Campaigns </b><br>
Emilio Ferrara, Onur Varol, Filippo Menczer, Alessandro Flammini </p>


<p><b>  Predicting Group Success in Meetup </b><br>
Soumajit Pramanik, Midhun Gundapuneni, Sayan Pathak, Bivas Mitra </p>


<p><b>  Raising Awareness of Conveyed Personality in Social Media Traces </b><br>
Bin Xu, Liang Gou, Anbang Xu, Dan Cosley, Jalal U. Mahmud </p>


<p><b>  Targeted Interest-Driven Advertising in Cities Using Twitter </b><br>
Aris Anagnostopoulos, Fabio Petroni, Mara Sorella </p>


<p><b>  Sensing Real-World Events Using Arabic Twitter Posts </b><br>
Nasser Alsaedi, Pete Burnap, Omer Rana </p>


<p><b>  Who Will Respond to Your Requests for Instant Trouble-Shooting? </b><br>
Kai-Hsiang Hsu, Cheng-Te Li, Chien-Lin Tseng </p>


<p><b>  Who Are Like-Minded: Mining User Interest Similarity in Online Social Networks </b><br>
Chunfeng Yang, Yipeng Zhou, Dah Ming Chiu </p>


<p><b>  Two Tales of the World: Comparison of Widely Used World News Datasets: GDELT and EventRegistry </b><br>
Haewoon Kwak, Jisun An </p>


<p><b>  #greysanatomy vs. #yankees: Demographics and Hashtag Use on Twitter </b><br>
Jisun An, Ingmar Weber </p>


<p><b>  Comparing Overall and Targeted Sentiments in Social Media during Crises </b><br>
Sa&uacute;l Vargas, Richard McCreadie, Craig Macdonald, Iadh Ounis </p>


<p><b>  A Data-Driven Study of View Duration on YouTube </b><br>
Minsu Park, Mor Naaman, Jonah Berger </p>


<p><b>  Tracking the Yak: An Empirical Study of Yik Yak </b><br>
Martin Saveski, Sophie Chou, Deb Roy </p>


<p><b>  Two Roads Diverged: A Semantic Network Analysis of Guanxi on Twitter </b><br>
Pu Yan, Taha Yasseri </p>


<p><b>  Privacy Preference Inference via Collaborative Filtering </b><br>
Taraneh Khazaei, Lu Xiao, Robert Mercer, Atif Khan </p>


<p><b>  Automatic Detection and Categorization of Election-Related Tweets </b><br>
Prashanth Vijayaraghavan, Soroush Vosoughi, Deb Roy </p>


<p><b>  Understanding Cognitive Styles from User-Generated Social Media Content </b><br>
Yi Wang, Jalal Mahmud, Taikun Liu </p>


<p><b>  Motivational Determinants of Participation Trajectories in Wikipedia </b><br>
Martina Balestra, Ofer Arazy, Coye Cheshire, Oded Nov </p>


<p><b>  Pinning Alone?: A Study of the Role of Social Ties on Pinterest </b><br>
Changtao Zhong, Nicolas Kourtellis, Nishanth Sastry </p>


<p><b>  Towards an Open-Domain Framework for Distilling the Outcomes of Personal Experiences from Social Media Timelines </b><br>
Alexandra Olteanu, Onur Varol, Emre Kiciman </p>


<p><b>  Signed Link Analysis in Social Media Networks </b><br>
Ghazaleh Beigi, Jiliang Tang, Huan Liu </p>


<p><b>  How Content, Community, and Engagement Affect the Life Cycles of Online Photo Sharing Groups </b><br>
Lyndon Kennedy, David A. Shamma </p>


<p><b>  Community Cores: Removing Size Bias from Community Detection </b><br>
Isaac Jones, Ran Wang, Jiawei Han, Huan Liu </p>


<p><b>  #Unconfirmed: Classifying Rumor Stance in Crisis-Related Social Media Messages </b><br>
Li Zeng, Kate Starbird, Emma S. Spiro </p>


<p><b>  Using Organizational Social Networks to Predict Employee Engagement </b><br>
Shion Guha, Michael Muller, N. Sadat Shami, Mikhil Masli, Werner Geyer </p>


<p><b>  Analyzing the Political Sentiment of Tweets in Farsi </b><br>
Elham Vaziripour, Christophe Giraud-Carrier, Daniel Zappala </p>


<p><b>  Tweeting the Mind and Instagramming the Heart: Exploring Differentiated Content Sharing on Social Media </b><br>
Lydia Manikonda, Venkata Vamsikrishna Meduri, Subbarao Kambhampati </p>


<p><b>  On the Impact of Social Cost in Opinion Dynamics </b><br>
Panagiotis Liakos, Katia Papakonstantinopoulou </p>


<p><b>  Exploring Personal Attributes from Unprotected Interactions </b><br>
Pritam Gundecha, Jiliang Tang, Xia Hu, Huan Liu </p>


<p><b>  The News Cycle's Influence on Social Media Activity </b><br>
Andrew Yates, Jonah Joselow, Nazli Goharian </p>


<p><b>  Learning the Relationships between Drug, Symptom, and Medical Condition Mentions in Social Media </b><br>
Andrew Yates, Nazli Goharian, Ophir Frieder </p>


<p><b>  Automatic Summarization of Real World Events Using Twitter </b><br>
Nasser Alsaedi, Pete Burnap, Omer Rana </p>


<p><b>  User Behaviors in Newsworthy Rumors: A Case Study of Twitter </b><br>
Quanzhi Li, Xiaomo Liu, Rui Fang, Armineh Nourbakhsh, Sameena Shah </p>


<p><b>  To Buy or to Read: How a Platform Shapes Reviewing Behavior </b><br>
Edward Newell, Stefan Dimitrov, Andrew Piper, Derek Ruths </p>


<p><b>  What's in a Like? Motivations for Pressing the Like Button </b><br>
Ana Levordashka, Sonja Utz, Renee Ambros </p>


<p><b>  Ephemeral Photowork: Understanding the Mobile Social Photography Ecosystem </b><br>
Barry Brown, Frank Bentley, Saeideh Bakhshi, David A. Shamma, </p>


<p><b>  Are Social Bots on Twitter Political Actors? Empirical Evidence from a Ukrainian Social Botnet </b><br>
Simon Hegelich, Dietmar Janetzko </p>


<p><b>  Measuring Social Jetlag in Twitter Data </b><br>
Tatjana Scheffler, Christopher C. M. Kyba </p>


<p><b>  Expertise in Social Networks: How Do Experts Differ from Other Users? </b><br>
Benjamin D. Horne, Dorit Nevo, Jesse Freitas, Heng Ji, Sibel Adali </p>


<p><b>  Tweets and Votes: A Four-Country Comparison of Volumetric and Sentiment Analysis Approaches </b><br>
Saifuddin Ahmed, Kokil Jaidka, Marko M. Skoric </p>



<h3>Demos</h3>


<p><b>SentiWorld: Understanding Emotions between Countries Based on Tweets</b><br>
Sang-Jun Yea, Sejin Kim, John-Micha&euml;l To, Jae-Gil Lee</p>

<p><b>iFeel 2.0: A Multilingual Benchmarking System for Sentence-Level Sentiment Analysis</b><br>
Matheus Ara&uacute;jo, Jo&atilde;o P. Diniz, Lucas Bastos, Elias Soares, Manoel J&uacute;nior, Miller Ferreira, Filipe Ribeiro, Fabricio Benevenuto</p>

<p><b>Brexit? Analyzing opinion on the UK-EU referendum within Twitter</b><br>
Clare Llewellyn, Laura Cram</p>

<p><b>  Visualization Tool for Collective Awareness in a Platform of Citizen Proposals </b><br>
Pablo Arag&oacute;n, Vicen&ccedil; G&oacute;mez, Andreas Kaltenbrunner </p>

<?php include("../../footer.php"); ?>