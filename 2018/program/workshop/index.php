<?php $section = "Program"; $subsection = "Workshop"; include("../../header.php"); ?>

<br>  <h2 class="pageTitle">Workshop program</h2>

<br><br>

<h2>ICWSM-16 Workshop Schedule</h2>
<h3>Tuesday, May 17</h3>

<h4>8:30 AM - 5:30 PM</h4>
<p><b> <a href="#w1">W1: CityLab</b></a><br>
<b> <a href="#w2">W2: Ethical Social Media Research</b></a><br>
<b> <a href="#w3">W3: Social Media & Demographic Research</b></a><br>
<b> <a href="#w4">W4: Wiki Workshop</b></a><br></p>



<h4>8:30 AM - 12:30 PM</h4>
<p><b> <a href="#w7">W7: Social Media in the Newsroom</b></a><br>
<b> <a href="#w8">W8: Social Web for Environmental and Ecological Monitoring</b></a><br>
</p>



<h4>1:30 - 5:30 PM</h4>
<p><b> <a href="#w5">W5: #FAIL - Things That Didn't Work Out</b></a><br>
<b> <a href="#w6">W6: News and Public Opinion</b></a><br>
</p>


<h4>8:30 - 10:30 PM</h4>
<p><b> <a href="#w9">W9: The ICWSM Science Slam</b></a><br>
</p>

<br><br>

<!--<p>There are eight workshops at ICWSM-16:<br><br>

<b>Full Day Workshops</b><br> <br>

<b>CityLab</b><br>

<b> Ethical Social Media Research </b> <br>

<b> Social Media & Demographic Research </b> <br>

<b>Wiki Workshop</b> <br>



<br><b>Half Day Workshops</b><br><br>

<b> #FAIL - Things That Didn't Work Out </b> <br>

<b> News and Public Opinion </b> <br>

<b> Social Media in the Newsroom </b> <br>

<b> Social Web for Environmental and Ecological Monitoring </b> <br>


<br><br>
<b>Evening Workshop</b><br>
8:30 PM - 10:30 PM<br>
<b> The ICWSM Science Slam</b><br>
David Garcia, Ingmar Weber and Aniko Hannak<br>


<p> The details of each workshop could be found below: </p>-->


<a name="w1"><h4><a href="http://www.sis.pitt.edu/citylab16/">CityLab</a></h4></a>

<p> This workshop aims into bringing together researchers and practitioners to discuss and explore the research challenges and opportunities in applying the web, the mobile, the pervasive and the social computing paradigm to understand cities and, crucially, engage their citizens in an effort to reclaim urban space for improving their quality of life. Our goal is to create a better understanding of cities and a living lab for understanding both technological and social phenomena. The interdisciplinary focus aims into attracting and welcomes diverse researchers from social sciences and information systems.</p>


<b>Organizers</b>

<ul>
<li>Daniele Quercia, Bell Labs.</li>
<li>Konstantinos Pelechrinis, University of Pittsburgh.</li>
</ul>

<p></p>



<a name="w2"><h4><a href="https://icwsmethics2016.wordpress.com/"> Ethical Social Media Research</a></h4></a>

<p>Social media and user-generated content platforms have opened up new possibilities for communication and creativity while also providing huge amounts of information about people and their online behavior. The evolution of technology and research methods presents ongoing ethical challenges to studying people and their digital traces. These challenges are a continuing source of discussion within the research community and also a topic of public discourse as social media scholarship gains greater visibility. This workshop is aimed at exploring difficult and unanswered ethical questions, touching on issues such as consent, privacy, and responsibility, as well as developing a set of best practices for social media and big data research.</p>


<b>Organizers</b>

<ul>
<li>Casey Fiesler, Information Science, University of Colorado Boulder</li>
<li>Stevie Chancellor, Human Centered Computing, Georgia Tech</li>
<li>Anna Lauren Hoffmann, University of California, Berkeley</li>
<li>Jessica Pater, Human Centered Computing, Georgia Tech </li>
<li>Nicholas Proferes, College of Information Studies, University of Maryland </li>
</ul>

<p></p>


<a name="w3"><h4><a href="https://sites.google.com/site/smdrworkshop/">Workshop on Social Media and Demographic Research</a></h4></a>

<p>Demography has been a data-driven discipline since its birth. Data collection and the development of formal methods have sustained most of the major advances in our understanding of population processes. The global spread of Social Media has generated new opportunities for demographic research, as individuals leave an increasing quantity of traces online that can be aggregated and mined for population research. At the same time, the use of Social Media and Internet are affecting people's daily activities as well as life planning, with implications for demographic behavior.</p>

<p>The goal of this workshop is to favor communication and exchange between the communities of demographers and data scientists. The workshop would revolve around the main theme of applications and implications of social media and online data for demographic research. We invite contributions from colleagues interested in Computational Demography. We encourage submissions from researchers who wish to present their work, as well as the attendance of scholars interested in broadening their exposure to the topic. </p>

<p>Announcement on the IUSSP (International Union for the Scientific Study of Population) webpage can be found <a href="http://iussp.org/en/social-media-and-demographic-research-applications-and-implications">here</a></p>

<b>Organizers</b>

<ul>
<li> Bogdan State, Facebook Research & Stanford University</li>
<li> Emilio Zagheni, Sociology and eScience, University of Washington</li>
<li> Francesco Billari, Demography and Sociology, Nuffield College, Oxford University</li>
</ul>

<p></p>

<a name="w4"><h4><a href="http://snap.stanford.edu/wikiworkshop2016/">Wiki Workshop</a></h4></a>

<p> Wikipedia is one of the most popular sites on the Web, a main source of knowledge for a large fraction of Internet users, and one of the very few projects that make not only their content but also many activity logs available to the public. Furthermore, other Wikimedia projects, such as Wikidata and Wikimedia Commons, have been created to share other types of knowledge with the world for free. For a variety of reasons (quality and quantity of content, reach in many languages, process of content production, availability of data, etc.) such projects have become important objects of study for researchers across many subfields of the computational and social sciences, such as social network analysis, artificial intelligence, linguistics, natural language processing, social psychology, education, anthropology, political science, human - computer interaction, and cognitive science.</p>

<p>The goal of this workshop is to bring together researchers exploring all aspects of Wikimedia websites such as Wikipedia, Wikidata, and Commons. With members of the Wikimedia Foundation's Research team on the organizing committee and with the experience of a successful workshop in 2015, we aim to continue facilitating a direct pathway for exchanging ideas between the organization that operates Wikimedia websites and the researchers interested in studying them.</p>


<b>Organizers</b>

<ul>
<li>Robert West, Stanford University</li>
<li>Leila Zia, Wikimedia Foundation</li>
<li>Dario Taraborelli, Wikimedia Foundation</li>
<li>Jure Leskovec, Stanford University</li>
</ul>

<p></p>



<a name="w5"><h4><a href="https://failworkshops.wordpress.com/">#FAIL! Things that didn't work out in social media research  - and what we can learn from them</a></h4></a>

<p>This is the third event in a series of workshops. Our aim for the series is to collect cases in which approaches for social media studies did not work out as expected. This will deepen and refine our understanding of how to use new methods, help saving time and effort by preventing researchers from making again the same “mistakes”.  The workshop travels to different conferences in order to connect social media researchers across disciplines. We are encouraging everyone to share experiences and lessons learnt from all stages of the research process, including but not limited to: challenges in data collection and analysis, technical challenges when working with specific tools, discussions on validity, representativeness and reproducibility of social media research, open questions on research ethics.</p>

<p>The workshop will feature invited talks which are commented on by individual responses from other experts in the field. If you are interested in becoming a respondent, please get in touch with the organizers. We will also provide lots of room for discussions during the workshop. All participants are asked to actively contribute to the discussions and to provide a 5 minute statement summarizing their own experiences with challenges in social media research. Please check the workshop Website for details on the workshop format and information on logistics.</p>

<b>Organizers</b>

<ul>
<li>Katrin Weller, GESIS Leibniz-Institute for the Social Sciences</li>
<li>Luca Rossi, IT University of Copenhagen</li>
<li>Fabio Giglietto, University of Urbino</li>
</ul>

<p></p>



<a name="w6"><h4><a href="http://neco.io/">NEws and publiC Opinion (NECO)</a></h4></a>

<p>Computational journalism has been an emerging discipline in recent years. A lot of inspiring data-driven studies on news production and consumption, news audience, news bias, and tools have been published and discussed. On the other hands, social media has received a lot of attention from researchers who study public opinion. The data collected from social media is a valuable asset to see what people have in their mind about at the very moment. Election prediction based on Twitter is a good example of such efforts.</p>

<p>In this workshop, we are trying to make this two disciplines meet. How news media formulate public opinion and how public opinion influences on news media are our questions to ask. For this, of course, the parallel efforts on the understanding of news media and public opinion are required first, and then based on the findings, we can look into the interplay between them. As a result, the workshop topics can be categorized into three groups: news, public opinion, and their interplay.</p>

<b>Organizers</b>

<ul>
<li>Jisun An, Qatar Computing Research Institute, HBKU</li>
<li>Haewoon Kwak, Qatar Computing Research Institute, HBKU</li>
<li>Fabricio Benevenuto, Federal University of Minas Gerais (UFMG)</li>
</ul>

<p></p>


<a name="w7"><h4><a href="http://www.smnews.newslab.ie/">Social Media in the Newsroom</a></h4></a>

<p> The exponential growth of social media as a central communication practice, and its agility in capturing and announcing breaking news events more rapidly than traditional media, has changed the journalistic landscape: social media has been adopted as a significant source by professional journalists, and conversely, citizens are able to use social media as a form of direct reportage. This brings along new opportunities for newsrooms and journalists by providing new means for newsgathering though access to a wealth of citizen reportage and updates about current affairs, as well as an additional showcase for news dissemination.</p>

<p>This workshop aims to focus on the intersection of social media and journalism, as a subset of Computational and Data Journalism. It will particularly focus on the development of novel algorithms, methods, and tools, and to further understand and to make the most of social media content for the purposes of news and media industries and journalism practitioners. </p>

<b>Organizers</b>

<ul>
<li>Bahareh R. Heravi, NUI Galway, Ireland</li>
<li>Arkaitz Zubiaga, University of Warwick, UK</li>
</ul>

<p></p>




<a name="w8"><h4><a href="http://www.sweem.polimi.it/index.php/2016/">Social Web for Environmental and Ecological Monitoring</a></h4></a>

<p> The exponential growth of the popularity of social media has provided not only novel techniques for public communication and engagement, but has also generated unprecedented volumes of publicly-available, user-generated social media content. These trends open new opportunities for ecological and environmental applications, both in terms of alternative data sources and novel approaches for interacting with the public.
</p>

<p>The goal of the workshop is to bring together a combination of academic and industrial participants to discuss ideas, challenges, and solutions at the intersection of Social Media and Environmental / Ecological Science.</p>

<b>Organizers</b>

<ul>
<li>David J. Crandall, Indiana University Bloomington</li>
<li> Roman Fedorov, Politecnico di Milano</li>
</ul>

<p></p>

<a name="w9"><h4><a href="https://sites.google.com/site/icwsmscienceslam/">The ICWSM Science Slam</a></h4></a>

<p>A Science Slam is an epic scientific event where scientists compete with short talks on their research. It's just like a poetry slam, but with science instead of poems. Slammers are completely free to do whatever they want on stage, everything is allowed including slides, games, the more creative, the better! The only two rules are: The topic of the slam has to be related to social media and the presentation should not take more than 8 minutes</p>

<b>Organizers</b>

<ul>
<li>David Garcia</li>
<li>Ingmar Weber</li>
<li>Aniko Hannak</li>
</ul>

<p></p>


<?php include("../../footer.php"); ?>?