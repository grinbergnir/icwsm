<?php $section = "Program"; $subsection = "Program"; include("../../header.php"); ?>

<br>
  <h2 class="pageTitle">ICWSM-16 Technical Program</h2>

<p>The ICWSM-16 technical program will be held in the Maternusaal Room of Maternushaus, May 18-20.</p>



<h3><b>Wednesday, May 18</b></h3>

<h3>09:00 - 09:15: Awards and Opening Remarks</h3> 
<h4>Markus Strohmaier / Krishna Gummadi, General Chairs, ICWSM-16
</h4>
<br>

<h3>09:15 - 10:30 : Keynote Address</h3>
<h4>Suresh Venkatasubramanian (University of Utah)</h4>

<br>
<h4>10:30 - 11:00 : Coffee Break</h4>

<br>

<h3>11:00 - 12:30: Session I : Biases and Inequalities</h3>
<p>

<b>Power Imbalance and Rating Systems</b><br>
Bogdan State, Bruno Abrahao, Karen S. Cook
<br>
<br>

<b>"Will Check-in for Badges": Understanding Bias and Misbehavior on Location-Based Social Networks</b><br>
Gang Wang, Sarita Y. Schoenebeck, Haitao Zheng, Ben Y. Zhao
<br>
<br>

<b>Shirtless and Dangerous: Quantifying Linguistic Signals of Gender Bias in an Online Fiction Writing Community</b><br>
Ethan Fast, Tina Vachovsky, Michael S. Bernstein
<br>
<br>

<b>Twitter's Glass Ceiling: The Effect of Perceived Gender on Online Visibility</b><br>
Shirin Nilizadeh, Anne Groggel, Peter Lista, Srijita Das, Yong-Yeol Ahn, Apu Kapadia, Fabio Rojas
<br>
<br>

<b>Identifying Platform Effects in Social Media Data</b><br>
Momin M. Malik, J&uuml;rgen Pfeffer

</p>

<h4>12:30 - 14:00 : Lunch Break</h4>

<br>

<h3>14:00 - 15:30 : Session II: Politics and News</h3>
<p>
<b>When a Movement Becomes a Party: Computational Assessment of New Forms of Political Organization in Social Media</b><br>
Pablo Arag&oacute;n, Yana Volkovich, David Laniado, Andreas Kaltenbrunner
<br>
<br>

<b>Changing Names in Online News Comments at the New York Times</b><br>
Simranjit Singh Sachar, Nicholas Diakopoulos
<br>
<br>

<b>Lost in Propagation? Unfolding News Cycles from the Source</b><br>
Chenhao Tan, Adrien Friggeri, Lada A. Adamic
<br>
<br>

<b>Journalists and Twitter: A Multidimensional Quantitative Description of Usage Patterns</b><br>
Mossaab Bagdouri
<br>
<br>

<b>User Migration in Online Social Networks: A Case Study on Reddit during a Period of Community Unrest</b><br>
Edward Newell, David Jurgens, Haji Mohammad Saleem, Hardik Vala, Jad Sassine, Caitrin Armstrong, Derek Ruths




</p>


<h4>15:30 - 16:00: Coffee Break</h4>

<br>

<h3>16:00 - 18:00 : Session III: Recommendations and Popularity</h3>
<p>

<b>Cross Social Media Recommendation</b><br>
Xiaozhong Liu, Tian Xia, Yingying Yu, Chun Guo, Yizhou Sun
<br>
<br>

<b>Aligning Popularity and Quality in Online Cultural Markets</b><br>
Pascal Van Hentenryck, Andr&eacute;s Abeliuk, Franco Berbeglia, Felipe Maldonado, Gerardo Berbeglia
<br>
<br>

<b>Sequential Voting Promotes Collective Discovery in Social Recommendation Systems</b><br>
L. Elisa Celis, Peter M. Krafft, Nathan Kobe
<br>
<br>

<b>Power of Earned Advertising on Social Network Services: A Case Study of Friend Tagging on Facebook</b><br>
Jaimie Y. Park, Yunkyu Sohn, Sue Moon
<br>
<br>

<b>"Blissfully Happy" or "Ready to Fight": Varying Interpretations of Emoji</b><br>
Hannah Miller, Jacob Thebault-Spieker, Shuo Chang, Isaac Johnson, Loren Terveen, Brent Hecht
<br><br>

<b>Measuring the Efficiency of Charitable Giving with Content Analysis and Crowdsourcing</b><br>
Ceren Budak, Justin M. Rao
<br>
<br>

<b>Predictability of Popularity: Gaps between Prediction and Understanding</b><br>
Benjamin Shulman, Amit Sharma, Dan Cosley
</p>

<h3>19:00 - 21:00 : ICWSM-16 Opening Reception</h3>
<h4>Chocolate Museum, Am Schokoladenmuseum, 50678, Cologne</h4>
<p>(28 minute walk or 19 minute bus ride, 1,4 mile from GESIS and Maternushaus)</p>


<br><br>
<hr>
<br><br>



<h3><b>Thursday, May 19</b></h3>

<h3>09:00 - 10:00 : Keynote Address</h4>
<h4>Amir Goldberg (Stanford Graduate School of Business)<br><br>
Decoding Culture from Digital Traces</h4>
<p>Culture is one of the least understood concepts in the social sciences. We all know it exists, but disagree on how to define and measure it. In this talk, I will discuss how the web and online communication open a window into understanding and measuring culture in ways that were unimaginable until recently. In particular, I will focus on language-based algorithms drawn from organizational email communication that measure individual cultural fit, and can predict promotion, dismissal and voluntary turnover. This work improves significantly on existing methods for measuring cultural alignment between individuals and groups, and provides new insights into how culture operates and affects individual and group processes.</p>

<h3>10:00 - 10:35 : Session IV: Best Paper Candidates </h3>
<p>

<b>Who Did What: Editor Role Identification in Wikipedia<br></b>
Diyi Yang, Aaron Halfaker, Robert Kraut, Eduard Hovy
<br><br>

<b>Understanding Communities via Hashtag Engagement: A Clustering Based Approach</b><br>
Orianna DeMasi, Douglas Mason, Jeff Ma

</p>

<h4>10:35 - 11:00 : Coffee Break</h4>
<br>

<h3>11:00 - 12:30 : Session V: Users, Opinions and Attitudes 1 </h3>
<p>

<b>Understanding Anti-Vaccination Attitudes in Social Media</b><br>
Tanushree Mitra, Scott Counts, James W. Pennebaker
<br><br>

<b>Emotions, Demographics and Sociability in Twitter Interactions</b><br>
Kristina Lerman, Megha Arora, Luciano Gallegos, Ponnurangam Kumaraguru, David Garcia
<br><br>

<b>On Unravelling Opinions of Issue Specific-Silent Users in Social Media</b><br>
Wei Gong, Ee-Peng Lim, Feida Zhu, Pei Hua Cher
<br><br>

<b>Are You Charlie or Ahmed? Cultural Pluralism in Charlie Hebdo Response on Twitter</b><br>
Jisun An, Haewoon Kwak, Yelena Mejova, Sonia Alonso Saenz De Oger, Braulio Gomez Fortes
<br><br>

<b>Discovering Response-Eliciting Factors in Social Question-Answering: A Reddit Inspired Study</b><br>
Danish, Yogesh Dahiya, Partha Talukdar

</p>


<h4>12:30 - 14:00: Lunch Break</h4>
<br>


<h3>14:00 - 15:30: Session VI: Users, Opinions and Attitudes 2 </h3>
<p>
<b>The Status Gradient of Trends in Social Media</b><br>
Rahmtin Rotabi, Jon Kleinberg
<br><br>

<b>Your Age Is No Secret: Inferring Microbloggers' Ages via Content and Interaction Analysis</b><br>
Jinxue Zhang, Xia Hu, Yanchao Zhang, Huan Liu
<br><br>

<b>Analyzing Personality through Social Media Profile Picture Choice</b><br>
Leqi Liu, Daniel Preotiuc-Pietro, Zahra Riahi Samani, Mohsen E. Moghaddam, Lyle Ungar
<br><br>

<b>Predicting Perceived Brand Personality with Social Media</b><br>
Anbang Xu, Haibin Liu, Liang Gou, Rama Akkiraju, Jalal Mahmud, Vibha Sinha, Yuheng Hu, Mu Qiao
<br><br>

<b>What the Language You Tweet Says about Your Occupation</b><br>
Tianran Hu, Haoyuan Xiao, Jiebo Luo, Thuy-vy Thi Nguyen

</p>

<h4>15:30 - 16:00: Coffee Break</h4>
<br>


<h3>16:00 - 18:00 : Session VII: Privacy, Networks and Diffusion </h3>
<p>
<b>Tracking Secret-Keeping in Emails</b><br>
Yla R. Tausczik, Cindy K. Chung, James W. Pennebaker
<br><br>

<b>Distinguishing between Topical and Non-Topical Information Diffusion Mechanisms in Social Media</b><br>
Przemyslaw A. Grabowicz, Niloy Ganguly, Krishna P. Gummadi
<br><br>

<b>Investigating the Observability of Complex Contagion in Empirical Social Networks</b><br>
Clay Fink, Aurora Schmidt, Vladimir Barash, John Kelly, Christopher Cameron, Michael Macy
<br><br>

<b>Networks of Gratitude: Structures of Thanks and User Expectations in Workplace Appreciation Systems</b><br>
Emma S. Spiro, J. Nathan Matias, Andr&eacute;s Monroy-Hern&aacute;ndez
<br><br>

<b>Bad Apples Spoil the Fun: Quantifying Cheating Influence in Online Gaming</b><br>
Xiang Zuo, Clayton Gandy, John Skovertz, Adriana Iamnitchi
<br><br>

<b>TiDeH: Time-Dependent Hawkes Process for Predicting Retweet Dynamics</b><br>
Ryota Kobayashi, Renaud Lambiotte
<br><br>

<b>On the Behaviour of Deviant Communities in Online Social Networks</b><br>
Mauro Coletto, Luca Maria Aiello, Claudio Lucchese, Fabrizio Silvestri

</p>




<h3>18:15 - 20:30 : <a href="http://www.icwsm.org/2016/program/accepted-papers/">ICWSM-16 Poster/Demo Session</a></h3>
<h4>Foyer, Maternushaus</h4>

<br><br>
<hr>
<br><br>


<h3><b>Friday, May 20</b></h3>

<h3>09:00 - 10:00 : Keynote Address</h4>
<h4>Lise Getoor (University of California, Santa Cruz)<br><br>
</h4>
<p>Lise Getoor is a Professor in the Computer Science Department at UC Santa Cruz. Her research areas include machine learning, data integration and reasoning under uncertainty, with an emphasis on graph and network data. She is a AAAI Fellow, serves on the Computing Research Association and International Machine Learning Society Boards, was co-chair of ICML 2011, is a recipient of an NSF Career Award and ten best paper and best student paper awards. She received her PhD from Stanford University, her MS from UC Berkeley, and her BS from UC Santa Barbara, and was a Professor at the University of Maryland, College Park from 2001 - 2013.</p>



<h3>10:00 - 10:35 : Session VIII: Best Paper Candidates </h3>
<p>

<b>Social Media Participation in an Activist Movement for Racial Equality</b><br>
Munmun De Choudhury, Shagun Jhaver, Benjamin Sugar, Ingmar Weber
<br><br>

<b>Mining Pro-ISIS Radicalisation Signals from Social Media Users</b><br>
Matthew Rowe, Hassan Saif

</p>

<h4>10:35 - 11:00 : Coffee Break</h4>
<br>




<h3>11:00 - 12:30 : Session IX: Big Data and Well Being </h3>
<p>
<b>Dynamic Data Capture from Social Media Streams: A Contextual Bandit Approach</b><br>
Thibault Gisselbrecht, Sylvain Lamprier, Patrick Gallinari
<br><br>

<b>Pub Crawling at Scale: Tapping Untappd to Explore Social Drinking</b><br>
Martin J. Chorley, Luca Rossi, Gareth Tyson, Matthew J. Williams
<br><br>

<b>Fetishizing Food in Digital Age: #foodporn around the World</b><br>
Yelena Mejova, Sofiane Abbar, Hamed Haddadi
<br><br>

<b>#PrayForDad: Learning the Semantics behind Why Social Media Users Disclose Health Information</b><br>
Zhijun Yin, You Chen, Daniel Fabbri, Jimeng Sun, Bradley Malin
<br><br>

<b>The Dynamics of Emotions in Online Interaction</b><br>
David Garcia, Arvid Kappas, Dennis K&uuml;ster and Frank Schweitzer

</p>


<h4>12:30 - 13:00: Lunch Break</h4>
<br>


<h4>13:00 - 14:00: Townhall Meeting</h4>
<br>


<h3><font color="blue">14:00 - 15:30 : Session X: Sentiment and Text Analysis * </font> </h3>
<p>
<b>CrowdLens: Experimenting with Crowd-Powered Recommendation and Explanation</b><br>
Shuo Chang, F. Maxwell Harper, Lingfei He, Loren G. Terveen
<br>
<br>


<b>Sentiment-Based Topic Suggestion for Micro-Reviews</b><br>
Ziyu Lu, Nikos Mamoulis, Evaggelia Pitoura, Panayiotis Tsaparas
<br><br>

<b>Science, AskScience, and BadScience: On the Coexistence of Highly Related Communities</b><br>
Jack Hessel, Chenhao Tan, Lillian Lee
<br><br>

<!--<b>TweetGrep: Weakly Supervised Joint Retrieval and Sentiment Analysis of Topical Tweets</b><br>
Satarupa Guha, Tanmoy Chakraborty, Samik Datta, Mohit Kumar, Vasudeva Varma
<br><br>-->

<b>Theme-Relevant Truth Discovery on Twitter: An Estimation Theoretic Approach</b><br>
Dong Wang, Jermaine Marshall, Chao Huang
<br><br>

<b>Message Impartiality in Social Media Discussions</b><br>
Muhammad Bilal Zafar, Krishna P. Gummadi, Cristian Danescu-Niculescu-Mizil


</p>

<h4><font color="blue">15:30 - 16:00: Coffee Break *</font></h4>
<br>


<h3>16:00 - 17:05 : Session XI: Places and Spaces </h3>

<p>

<b>The Emotional and Chromatic Layers of Urban Smells</b><br>
Daniele Quercia, Luca Maria Aiello, Rossano Schifanella
<br><br>

<b>EigenTransitions with Hypothesis Testing: The Anatomy of Urban Mobility</b><br>
Ke Zhang, Yu-Ru Lin, Konstantinos Pelechrinis
<br><br>

<b>Space Collapse: Reinforcing, Reconfiguring and Enhancing Chinese Social Practices through WeChat</b><br>
Yang Wang, Yao Li, Bryan Semaan, Jian Tang


</p>


<h3>17:05 - 17:20 : Closing Remarks </h3>
<h4>Markus Strohmaier / Krishna Gummadi, General Chairs, ICWSM-16
</h4>


<font color="blue"> * Recent changes to the schedule </font>


<?php include("../../footer.php"); ?>?