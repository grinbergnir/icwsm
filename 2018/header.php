<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html>
<head>
  <link rel="stylesheet" href="/2018/docs/css/960.css" type="text/css">
  <link rel="stylesheet" href="/2018/docs/css/icwsm14.css" type="text/css">
  <link rel="alternate" type="application/atom+xml" href="/2018/feed/">
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
  <meta http-equiv="Content-Language" content="en-ie">
  <meta name="ROBOTS" content="ALL">
  <meta name="Copyright" content="Copyright (c) 2010 CDVP, CLARITY, Daragh Byrne">
  <meta http-equiv="imagetoolbar" content="no">
  <meta name="MSSmartTagsPreventParsing" content="true">
  <meta name="author" content="Daragh Byrne">
  <meta name="keywords" content="">
  <meta name="description" content=".">
  <meta name="Rating" content="General">
  <meta name="revisit-after" content="5 Days">
  <meta name="doc-class" content="Living Document">

  <title>ICWSM-18 - <?php echo $section; ?> - <?php echo $subsection; if ($subsubsection <> "") echo " - ".$subsubsection; ?></title>
</head>

<body id="top" class="<?php if($section=="About") { echo "contact"; } else { echo strtolower($section); } ?>">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=430496670301788";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
  <div id="wrapper">
    <div class="container_12">
      <!-- HEADER -->

      <div class="grid_12">
        <h1 id="logoHolder"><a href="/2018/" title="ICWSM-18 | Back to welcome page"><span>ICWSM-18</span></a></h1>
      </div>

      <div class="clear"></div>
    </div><!-- container 12 -->
    
    <!-- FIRST NAV -->

    <div id="mainMenu" class="container_12 headerContainer">
      <ul class="mainMenu grid_12">
        <li class="<?php if($section=="Welcome") echo "selected "; ?>home"><a href="/2018/">Home</a></li>

        <li class="<?php if($section=="Submitting") echo "selected "; ?>submitting"><a href="/2018/submitting/call-for-papers/">Submitting</a></li>

        <!-- <li class="<?php if($section=="Attending") echo "selected "; ?>attending"><a href="/2018/attending/venue/">Attending</a></li> -->


        <!-- <li class="<?php if($section=="Program") echo "selected "; ?>speakers"><a href="/2018/program/accepted-papers/">Program</a></li> -->

        <!-- <li class="<?php if($section=="Datasets") echo "selected "; ?>datasets"><a href="/2018/datasets/datasets/">Datasets</a></li> -->


        <li class="<?php if($section=="Organisation") echo "selected "; ?>organisation"><a href="/2018/organization/organization/">Organization</a></li>

        <li class="<?php if($section=="Contact" || $section=="About") echo "selected "; ?>contact"><a href="/2018/contact/contact/">Contact / About</a></li>
      
      </ul>
    </div>

    <div class="clear"></div><!-- SECOND NAV -->

    <div id="subMenu">
      <div id="subMenuWrapper" class="container_12">
        <ul class="subMenu">
        <?php if($section=="Home") {
          echo '<li class="';
            if($subsection=="Welcome") echo "selected ";
            echo 'welcome"><a href="/2018/">Welcome</a></li>';
      echo '<li class="';
      
      echo '<li class="';
//      if($subsection=="News") echo "selected ";
//      echo 'news"><a href="/2018/home/news/">News</a></li>';
//          echo '<li class="';
//          if($subsection=="Media") echo "selected ";
//          echo 'media"><a href="/2018/home/media/">Media</a></li>';
    }
          if($section=="Attending") {
            echo '<li class="';
            if($subsection=="Venue") echo "selected ";
            echo 'welcome"><a href="/2018/attending/venue/">Venue</a></li>';
            echo '<li class="';
            if($subsection=="Getting Here") echo "selected ";
            echo 'media"><a href="/2018/attending/getting-here/">Getting Here</a></li>';
            echo '<li class="';
            if($subsection=="Registration") echo "selected ";
            echo 'welcome"><a href="/2018/attending/registration/">Registration</a></li>';
            //echo '<li class="';
            //if($subsection=="Boston") echo "selected ";
            //echo 'news"><a href="/2018/attending/boston/">Boston</a></li>';
            //echo '<li class="';
            //if($subsection=="Social Event") echo "selected ";
            //echo 'news"><a href="/2018/attending/social-event/">Social Event</a></li>';
            echo '<li class="';
            if($subsection=="Accommodation") echo "selected ";
            echo 'media"><a href="/2018/attending/accommodation/">Accommodation</a></li>';
            echo '<li class="';
            if($subsection=="Student Support") echo "selected ";
            echo 'media"><a href="/2018/attending/student-support/">Student Support</a></li>';
            echo '<li class="';
            if($subsection=="ACM WEB SCIENCE 2018") echo "selected ";
            echo 'media"><a href="/2018/attending/websci/">ACM WEB SCIENCE 2018</a></li>';
    }
        if($section=="Submitting") {
          echo '<li class="';
            if($subsection=="Call for Papers") echo "selected ";
            echo 'welcome"><a href="/2018/submitting/call-for-papers/">Call for Papers</a></li>';

          echo '<li class="';
      if($subsection=="Guidelines") echo "selected ";
      echo 'welcome"><a href="/2018/submitting/guidelines/">Guidelines</a></li>';


          echo '<li class="';
          if($subsection=="Tutorials") echo "selected ";
         echo 'media"><a href="/2018/submitting/tutorials/">Tutorials</a></li>';

//          echo '<li class="';
//      if($subsection=="Demos") echo "selected ";
//      echo 'news"><a href="/2018/submitting/demos/">Demos</a></li>';
          echo '<li class="';
      if($subsection=="Datasets") echo "selected ";
      echo 'news"><a href="/2018/submitting/datasets/">Datasets</a></li>';

     echo '<li class="';
      if($subsection=="Workshops") echo "selected ";
      echo 'news"><a href="/2018/submitting/workshops/">Workshops</a></li>';
    }
        if($section=="Program") {
          echo '<li class="';
            if($subsection=="Program") echo "selected ";
            echo 'welcome"><a href="/2018/program/program/">Program</a></li>';
            echo '<li class="';
            if($subsection=="Speakers") echo "selected ";
            echo 'news"><a href="/2018/program/speakers/">Keynotes</a></li>';
            echo '<li class="';
            if($subsection=="Tutorial") echo "selected ";
            echo 'media"><a href="/2018/program/tutorial/">Tutorial</a></li>';
            echo '<li class="';
            if($subsection=="Workshop") echo "selected ";
            echo 'welcome"><a href="/2018/program/workshop/">Workshop</a></li>';
            echo '<li class="';
          if($subsection=="Accepted Papers") echo "selected ";
          echo 'welcome"><a href="/2018/program/accepted-papers/">Accepted Papers</a></li>';
          // echo '<li class="';
          //   if($subsection=="Industry Event") echo "selected ";
          //  echo 'welcome"><a href="/2018/program/industry-event/">Industry Event</a></li>';
    }
        if($section=="Organisation") {
          echo '<li class="';
            if($subsection=="Organisation") echo "selected ";
            echo 'welcome"><a href="/2018/organization/organization">Organization</a></li>';
      echo '<li class="';
      if($subsection=="Senior Program Committee") echo "selected ";
      echo 'news"><a href="/2018/organization/senior-program-committee/">Senior Program Committee</a></li>';
      echo '<li class="';
      if($subsection=="Program Committee") echo "selected ";
      echo 'news"><a href="/2018/organization/program-committee/">Program Committee</a></li>';
      echo '<li class="';
      if($subsection=="Steering Committee") echo "selected ";
      echo 'news"><a href="/2018/organization/steering-committee/">Steering Committee</a></li>';
        }
if($section=="Datasets") {
    echo '<li class="';
    if($subsection=="Datasets") echo "selected ";
  echo 'datasets"><a href="/2018/datasets/datasets/">Datasets</a></li>';
}
  if($section=="Contact" || $section=="About") {
          echo '<li class="';
            if($subsection=="Contact") echo "selected ";
            echo 'welcome"><a href="/2018/contact/contact">Contact</a></li>';
          echo '<li class="';
            if($subsection=="Sponsorship") echo "selected ";
            echo 'welcome"><a href="/2018/contact/sponsorship">Sponsorship</a></li>';
      echo '<li class="';
      if($subsection=="Future Conferences") echo "selected ";
      echo 'news"><a href="/2018/contact/future-conferences/">Future Conferences</a></li>';
      echo '<li class="';
      if($subsection=="Previous Conferences") echo "selected ";
      echo 'news"><a href="/2018/contact/previous-conferences/">Previous Conferences</a></li>';
//      echo '<li class="';
//      if($subsection=="Related Events") echo "selected ";
//      echo 'news"><a href="/2018/contact/related-events/">Related Events</a></li>';
    }
        ?>
        </ul>
      </div>
    </div>

    <div class="clear"></div><!-- content container -->

    <div class="container_12 bodyContainer">
      <!-- CONTENT NAV -->

      <div class="grid_8 textContent">
        <div class="textContentWrapper">
        
        <table border=0 cellpadding=0 cellspacing=0 width=600 style='border-collapse:
 collapse;table-layout:fixed'><tr>
        <td width=150><g:plusone></g:plusone><script type="text/javascript">(function() { var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true; po.src = 'https://apis.google.com/js/plusone.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s); })();</script></td>
        <td width=150><a href="https://twitter.com/share" class="twitter-share-button" data-via="icwsm" data-related="icwsm" data-hashtags="icwsm">Tweet</a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></td>
        <td width=150><script src="//platform.linkedin.com/in.js" type="text/javascript"></script><script type="IN/Share" data-counter="right"></script></td>
        <td width=150><div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-font="arial"></div></td>
        </tr></table>