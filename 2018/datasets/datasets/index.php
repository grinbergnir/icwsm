<?php $section = "Datasets"; $subsection = "Datasets"; include("../../header.php"); ?>

  <h2 class="pageTitle">Datasets</h2>

<style type="text/css">

table, tr, td {
    font-family: "Helvetic Neue", "HelveticNeue", "Helvetic-Neue", helvetica, arial,  _sans;
}
.CSSTableGenerator {

	margin:0px;padding:0px;

	width:100%;
	box-shadow: 10px 10px 5px #888888;

	border:1px solid #000000;

	

	-moz-border-radius-bottomleft:0px;

	-webkit-border-bottom-left-radius:0px;

	border-bottom-left-radius:0px;

	

	-moz-border-radius-bottomright:0px;

	-webkit-border-bottom-right-radius:0px;

	border-bottom-right-radius:0px;

	

	-moz-border-radius-topright:0px;

	-webkit-border-top-right-radius:0px;

	border-top-right-radius:0px;

	

	-moz-border-radius-topleft:0px;

	-webkit-border-top-left-radius:0px;

	border-top-left-radius:0px;

}.CSSTableGenerator table{

	width:100%;

	height:100%;

	margin:0px;padding:0px;

}.CSSTableGenerator tr:last-child td:last-child {

	-moz-border-radius-bottomright:0px;

	-webkit-border-bottom-right-radius:0px;

	border-bottom-right-radius:0px;

}

.CSSTableGenerator table tr:first-child td:first-child {

	-moz-border-radius-topleft:0px;

	-webkit-border-top-left-radius:0px;

	border-top-left-radius:0px;

}

.CSSTableGenerator table tr:first-child td:last-child {

	-moz-border-radius-topright:0px;

	-webkit-border-top-right-radius:0px;

	border-top-right-radius:0px;

}.CSSTableGenerator tr:last-child td:first-child{

	-moz-border-radius-bottomleft:0px;

	-webkit-border-bottom-left-radius:0px;

	border-bottom-left-radius:0px;

}.CSSTableGenerator tr:hover td{

	

}
.CSSTableGenerator tr:nth-child(odd){ background-color:#B6B7BA; }

.CSSTableGenerator tr:nth-child(even)    { background-color:#ffffff; }
.CSSTableGenerator td{

	vertical-align:middle;

	

	

	border:1px solid #000000;

	border-width:0px 1px 1px 0px;

	text-align:left;

	padding:7px;

	font-size:10px;

	font-family:Arial;

	font-weight:normal;

	color:#000000;

}.CSSTableGenerator tr:last-child td{

	border-width:0px 1px 0px 0px;

}.CSSTableGenerator tr td:last-child{

	border-width:0px 0px 1px 0px;

}.CSSTableGenerator tr:last-child td:last-child{

	border-width:0px 0px 0px 0px;

}

.CSSTableGenerator tr:first-child td{

		background:#B6B7BA;

	border:0px solid #000000;

	text-align:center;

	border-width:0px 0px 1px 1px;

	font-size:14px;

	font-family:Arial;

	font-weight:bold;

	color:#ffffff;

}

.CSSTableGenerator tr:first-child:hover td{

	background:#B6B7BA;
}

.CSSTableGenerator tr:first-child td:first-child{

	border-width:0px 0px 1px 0px;

}

.CSSTableGenerator tr:first-child td:last-child{

	border-width:0px 0px 1px 1px;

}

.textContent li {
	line-height:1em;
}

</style>

<h1>ICWSM Dataset Sharing Service</h1>
<BR>
<BR>
<BR>

<p>As part of the ICWSM Data Sharing Initiative, ICWSM provides a hosting service for new datasets used by papers published in the proceedings of the annual <a href="http://www.icwsm.org">ICWSM conference</a>.  All datasets are released as openly available community resources.  Please see the instructions on the <a href="#obtaining">registration process</a> in order to gain access to the datasets.</p>

<h1>Available Datasets</h1>
<br>
<p>ICWSM-16 is the fifth year of this initiative. 
Description of datasets will be uploaded soon.
<br>

<p>You can find the list available datasets from <a href="http://www.icwsm.org/2015">ICWSM 2015</a> below.</p>
<ul>
<li>Is the Sample Good Enough? Comparing Data from Twitter's Streaming API with Twitter's Firehose
<br>
Fred Morstatter, Jrgen Pfeffer, Huan Liu, Kathleen M. Carley</li>
<br>
<br>
<li>#Bigbirds Never Die: Understanding Social Dynamics of Emergent Hashtags
<br>
Yu-Ru Lin, Drew Margolin, Brian Keegan, Andrea Baronchelli, David Lazer</li>
<br>
<br>
<li>Detecting Comments on News Articles in Microblogs
<br>
Alok Kothari, Walid Magdy, Kareem Darwish, Ahmed Mourad, Ahmed Taei</li>
<br>
<br>
<li>A Data-Driven Analysis to Question Epidemic Models for Citation Cascades on the Blogosphere
<br>
Abdelhamid Salah Brahim, Lionel Tabourier, Bndicte Le Grand </li>
<br>
<br>
<li>Properties, Prediction, and Prevalence of Useful User-Generated Comments for Descriptive Annotation of Social Media Objects
<br>
Elaheh Momeni, Claire Cardie, Myle Ott</li>
<br>
<br>
<li>A Multi-Indicator Approach for Geolocalization of Tweets
<br>
Axel Schulz, Aristotelis Hadjakos, Heiko Paulheim, Johannes Nachtwey, Max Mhlhuser</li>
<br>
<br>
<li>Competition and Success in the Meme Pool: A Case Study on Quickmeme.com
<br>
Michele Coscia</li>
<br>
<br>
<li>Artist Popularity: Do Web and Social Music Services Agree?
<br>
Alejandro Bellogn, Arjen P. de Vries, Jiyin He</li>
<br>
<br>
<li>Extracting Diurnal Patterns of Real-World Activities from Social Media
<br>
Nir Grinberg, Mor Naaman, Blake Shaw, Gilad Lotan</li>
<br>
<br>
<li>Political Orientation Inference on Twitter: It's Not Easy!
<br>
Raviv Cohen and Derek Ruths</li>
<br>
<br>
<li>Quantifying Political Leaning from Tweets and Retweets
<br>
Felix Ming Fai Wong, Chee Wei Tan, Soumya Sen, Mung Chiang</li>
</ul></p>


<p> You can find the list of available datasets from <a href="http://www.icwsm.org/2012">ICWSM 2012</a> in the following table.</p>

<table style="margin-bottom:15px;" class="CSSTableGenerator">
	<tr><td><B>Paper</B></td><td><B>Description</B></td><td><B># of files</B></td><td><B># of observations (tweets/facebook accounts/entries)</B></td><td><B># of Twitter users</B></td><td><B># of nodes</B></td><td><B># of edges</B></td></tr>
	<tr><td>Z Luo, Miles O and T Wang. <i>Opinion Retrieval in Twitter.</i></td><td>Tweets tagged as relevant or irrelevant for 50 specific queries</td><td>1</td><td>5000</td><td></td><td></td><td></td></tr>
	<tr><td>L Chen, W Wang, M Nagarajan, S Wang and AP Sheth. <i>Extracting Diverse Sentiment Expressions with Target-dependent Polarity from Twitter.</i></td><td>Tweets describing movies and persons</td><td>4</td><td>426660</td><td></td><td></td><td></td></tr>
	<tr><td>J Mahmud, J Nichols, and C Drews. <i>Where is This Tweet From? Inferring Home Locations of Twitter Users.</i></td><td>Geo tagged tweets from 100 top cities</td><td>2</td><td>1005259</td><td>19390</td><td></td><td></td></tr>
	<tr><td>L Rossi and M Magnani. <i>Conversation practices and network structure in Twitter.</i></td><td>Tweets about the fifth edition of the popular TV show XFactor Italia</td><td>2</td><td>22287</td><td></td><td></td><td></td></tr>
	<tr><td>LM Aiello, M Deplano, R Schifanella, and G Ruffo. <i>People are Strange when you're a Stranger: Impact and Influence of Bots on Social Networks.</i></td><td>Social info from anobii.com</td><td>7</td><td>671585</td><td></td><td>179654</td><td>1566369</td></tr>
	<tr><td>J Park, M Cha, H Kim, and J Jeong. <i>Managing Bad News in Social Media: A Case Study on Domino’s Pizza Crisis.</i></td><td>Tweet collection for Domino's pizza crisis</td><td>10</td><td>4645</td><td></td><td>6158</td><td>29987</td></tr>
	<tr><td>Y He, C Lin, W Gao, and KF Wong. <i>Tracking Sentiment and Topic Dynamics from Social Media.</i></td><td>Mozilla add on review data</td><td>1</td><td>9300</td><td></td><td></td><td></td></tr>
	<tr><td>D O'Callaghan, M Harrigan, J Carthy, and P Cunningham. <i>Network Analysis of Recurring YouTube Spam Campaigns.</i></td><td>Youtube comments data</td><td>1</td><td>6466882</td><td></td><td></td><td></td></tr>
	<tr><td>P Agarwal, R Vaithiyanathan, S Sharma, and G Shroff. <i>Catching the Long-Tail: Extracting Local News Events from Twitter.</i></td><td>Tweets that possibly describe a fire in a factory.</td><td>354</td><td>20751</td><td></td><td></td><td></td></tr>
	<tr><td>FA Zamal, W Liu, and D Ruths. <i>Homophily and Latent Attribute Inference: Inferring Latent Attributes of Twitter Users from Neighbors.</i></td><td>Tweet data for gender/age/political orientation labeled users</td><td>9</td><td>233653995</td><td>340248</td><td></td><td></td></tr>
	<tr><td>F Giglietto. <i>If Likes Were Votes: An Empirical Study on the 2011 Italian Administrative Elections.</i></td><td>Facebook profile of 104 Italian politicians</td><td>1</td><td>104</td><td></td><td></td><td></td></tr>
</table>

<br>
<h1 id="obtaining">Obtaining Datasets</h1>
<br>
<p>Download and sign the <a href="icwsm_user_agreement_v1.pdf">ICWSM Dataset Usage Agreement</a>.  Please note that this agreement gives you access to all ICWSM-published datasets.  In it, you agree not to redistribute the datasets.  Furthermore, ensure that, when using a dataset in your own work, you abide by the citation requests of the authors of the dataset used.</p>

<p>Email the signed agreement, as a PDF file, to <a href="mailto:dataset-request@icwsm.org">dataset-request@icwsm.org</a>.  In the body of your email,</p>
<ol>
  <li>Be clear that you are requesting access to the ICWSM datasets</li>
  <li>Include your name,</li>
  <li>your email address, and</li>
  <li>the name of your organization.</li>
</ol>

<p>We will respond to your request with a URL, a username, and a password with which you can download the datasets.  <b>Please allow seven business days for a response</b>.</p>

<h1>Contact</h1>
<br>
<p>If you have any questions or concerns regarding the terms of agreement, the datasets available, or need to report infringements on the terms of agreement, please contact <a href="http://www.icwsm.org/2016/organization/organization/fabricio-benevenuto.php">Fabricio Benevenuto</a> or <a href="http://www.icwsm.org/2016/organization/organization/derek-ruths.php">Derek Ruths</a>.</p>

</td>
<td width="5%"></td></tr>
</table>


<p>
We consider both contributed and official ICWSM datasets to provide an empirical basis for excellent research that could appear at ICWSM and related venues.  Please consider using them in work that you will be submitting to ICWSM this year!

</p>

<?php include("../../footer.php"); ?>