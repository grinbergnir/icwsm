<?php $section = "Home"; $subsection = "Media"; include("../../header.php"); ?>

  <h2 class="pageTitle">Media</h2>

  <p>The conference logo is available to download and for use in promoting ICWSM-12.</p>
  
  <div class="image">
    <a href="../../images/media/media_logo_black_big.png"><img style="padding: 10px;" src="../../images/media/media_logo_black_small.png" alt="Black Logo">
  </div>

  <div class="image">
    <a href="../../images/media/media_logo_black_white.png"><img style="padding: 10px;" src="../../images/media/media_logo_white_small.png" alt="White Logo"></a>
  </div>

<?php include("../../footer.php"); ?>