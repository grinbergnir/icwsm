<?php $section = "Submitting"; $subsection = "Workshops"; include("../../header.php"); ?>

  <h2 class="pageTitle">Call for Workshops</h2>
         
  <h4>CALL FOR WORKSHOPS FOR THE TENTH INTERNATIONAL AAAI CONFERENCE ON WEBLOGS AND SOCIAL MEDIA (ICWSM)<br /> STANFORD, CALIFORNIA USA, JUNE 25 - 28, 2018</h4>
  
<ul>
<li>Submission Deadline: <b><a href="https://www.timeanddate.com/countdown/generic?iso=20180116T00&p0=224&msg=ICWSM%2718+Workshop+Proposal+Deadline&font=cursive">January 16, 2018</a></b></li>
<li>Submission Email: <b><a href='mailto:icwsm18ws@googlegroups.com'><b>icwsm16ws@googlegroups.com</a></b></li>
<!--<a href='https://easychair.org/conferences/?conf=icwsm15workshops'>https://easychair.org/conferences/?conf=icwsm15workshops</a>-->
</ul>

<!--<h5>Accepted workshops are now available at: <a href="http://www.icwsm.org/2016/program/workshop/">www.icwsm.org/2016/program/workshop/</a></h5> -->

<p>
The ICWSM-18 Committee invites proposals for Workshops Day at the Twelfth International AAAI Conference on Weblogs and Social Media (ICWSM-18). The Workshops Day will be held on Monday, 25 June, 2018 in Stanford, California USA. Workshop participants will have the opportunity to meet and discuss issues with a selected focus -- providing an informal setting for active exchange among social scientists, computer scientists, and other researchers, developers and PhD students. Workshops are an excellent forum for exploring emerging approaches and task areas, for bridging the gaps between the social science and technology fields or between subfields of social media research, for elucidating the results of exploratory research, or for critiquing existing approaches.
</p>

<p>
Members of all segments of the social media research community are encouraged to submit proposals. To foster interaction and exchange of ideas, the workshops will be kept small, with 20-40 participants. Attendance is limited to active participants only.
</p>

  <p>The format of workshops will be determined by their organizers. The two main criteria for the selection of the workshops will be the following:
  <ul>
    <li>Workshop should not be structured as mini-conferences dominated by long talks and short discussions. Instead, the organizers are encouraged to promote different types of activities including challenges, games, brainstorming and networking sessions. Also, workshops should leave ample time for discussions and interaction between the participants, and should encourage the submission and presentation of position papers that discuss new research ideas.</li>
    <li>The workshop should have the potential to attract the interest of researchers in computer science and social/organizational sciences. Proposals involving people of different backgrounds in the organizing committee and addressing topics at the intersection of different disciplines will have higher chance of acceptance.</li>
  </ul>
</p>


  <p>
  Workshop organizers who want to publish the papers from their workshop (or significant portions of it) will have the opportunity to do so through workshop proceedings by the AAAI Press. For a list of last year's workshops see <a href="http://www.icwsm.org/2017/program/workshop/">http://www.icwsm.org/2017/program/workshop/</a>.
</p>
  
  <h4>Proposal Content</h4>
<p>Proposals for workshops should be no more than five (5) pages in length (10pt, single column, with reasonable margins), written in English, and should contain the following:

  <ul>
    <li>A concise title</li>
    <li>The names, affiliations, and contact information of the organizing committee. A main contact author should be specified. A typical proposal should include no more than four co-chairs.</li>
    <li>An indication as to whether the workshop should be considered for a half-day or full-day meeting.</li>
    <li>A short abstract describing the scope and main objective of the workshop. Identify the specific issues and research questions the workshop will focus on, with a brief discussion of why the topic is of particular interest at this time and for which research communities.</li>
    <li>A two/three paragraph description of the workshop topic and themes.</li>
    <li>A description of the proposed workshop format and a detailed list of proposed activities, with special emphasis on those activities that distinguish it from a mini-conference (e.g., games, brainstorming sessions, challenges, group activities).</li>
    <li>An approximate timeline of the activities.</li>
    <li>A description of how workshop submissions will be evaluated and selected (invited contributions, peer review, etc.). In case a PC is needed, provide a tentative list of the members.</li>
    <li>Historical information about the workshop, when available. Short description of the previous editions reporting highlights and details about the approximate number of attendees and number of submissions.</li>
    <li>A list of other related workshops held previously at related conferences, if any, together with a brief statement on how the proposed workshop differs from or how it follows-up on work presented at previous workshops.</li>
    <li>A short bio for each member of the organizing committee, including a description of their relevant expertise. Strong proposals include organizers who bring differing perspectives to the workshop topic and who are actively connected to the communities of potential participants.</li>
  </ul>
</p>

<p>
  Please email your proposal in a single file to the workshop chairs at <a href='mailto:icwsm18ws@googlegroups.com'>icwsm18ws@googlegroups.com</a> before the <a href='https://www.timeanddate.com/countdown/generic?iso=20180116T00&p0=224&msg=ICWSM%2718+Workshop+Proposal+Deadline&font=cursive'>deadline</a>. For additional information please contact the workshop chairs at the same address.
</p>



  <h4>Important Dates<br />(All deadlines are on 23:59:59)</h4>


  <ul>
<li>Workshop proposal submission deadline: <a href='https://www.timeanddate.com/countdown/generic?iso=20180116T00&p0=224&msg=ICWSM%2718+Workshop+Proposal+Deadline&font=cursive'> January 16, 2018</a></li>
<li>Workshop acceptance notification: January 23, 2018</li>
<li>Workshop papers submission: March 27, 2018</li>
<li>Workshop paper acceptance notification: April 3, 2018</li>
<li>Workshop final camera-ready paper due: April 10, 2018</li>
<li>ICWSM-16 Workshops Day: June 25, 2018</li>
  </ul>

  
   <p><b>Workshop Chairs:</b>
  <ul> 
<b>   
<li><a href="mailto:fdzhu@smu.edu.sg">Feida Zhu</a></li>
<li><a href="mailto:ctoma@wisc.edu">Catalina Toma</a></li>
</b>
  </ul>
 </p>

  
  
  
  
<?php include("../../footer.php"); ?>