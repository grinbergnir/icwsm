<?php $section = "Submitting"; $subsection = "Demos"; include("../../header.php"); ?>
<h2 class="pageTitle">Call for Demos</h2>
<h4>Demos at the Eighth International AAAI Conference on Weblogs and Social Media</h4>
<p>Ann Arbor, MI, USA, June 1-4, 2014<br/>
Sponsored by the Association for the Advancement of Artificial Intelligence</p>
<ul>
  <li>Demo Papers Due on: March 17, 2014</li>
  <li>Notification of Acceptance: March 20, 2014</li>
  <li>Camera Ready Due on: March 25, 2014</li>
  <li>Submission Site: <a href="https://www.easychair.org/conferences/?conf=icwsm14">https://www.easychair.org/conferences/?conf=icwsm14</a></li>
</ul>
<p>Demos give authors the opportunity to present cutting
edge-applications and demonstrate concepts that advance the state of
the art. Submissions can be full implementations or functional
research prototypes. Areas and topics of interest in Social Media
include, but are not limited to the following:
  </p>
<ul>
<li>Interactive visualization and data mining
(specifically addressing Social Media, including generic tools and
case studies)</li>
<li>Artistic works created using social media</li>
<li>Social media Apps (for mobile devices and/or
commercial social networking services)</li>
<li>Simulations and Monitoring</li>
<li>Gaming, storytelling, and entertainment</li>
<li>Open source tools and software in all areas of
social media</li>
<li>Tools for personal data management, quantified
self, and information filtering</li>
<li>Applications demonstrating new interaction
paradigms</li>
<li>Demonstrations of crowdsourcing and collaborative
social media systems</li>
<li>Tools for journalism and digital preservation</li>
</ul>
<p>Submissions should report on implementations of novel social media
applications and social media technologies and concepts. Demos allow
conference participants to view novel and noteworthy social media
systems in action, discuss the systems with those who created them,
and try them out. Appropriate demos include applications,
technologies, and research prototypes. Demos should be interactive and
provide attendees a hands-on experience. Presenters must have been
directly involved with the development of the system and be able to
explain the unique and novel contributions of the system.
  </p>
  <p>
Accepted demos will be showcased during the poster/demo session
reception. The demo submission should include a description of the
demonstrated system (including at least one screenshot) and the novel
approaches or ideas it embodies. In addition, it should provide
concise information about the presenter(s), including their
relationship to the project.</p>
<h4>Paper Submission</h4>
<p>Demo papers are due through the 
  <a href="https://www.easychair.org/conferences/?conf=icwsm14">ICWSM-14 paper submission site</a>
March 17 at 11:59 PM PDT. We cannot accept submissions by e-mail or
fax.</p>
<p>
Papers must be in high-resolution PDF format, formatted for US Letter
(8.5&quot; x 11&quot;) paper, using Type 1 or TrueType fonts. Demo
descriptions must be no longer than 2 pages, and must be submitted by
the deadline given above, and formatted in AAAI two-column
format.</p>
<?php include("../../footer.php"); ?>