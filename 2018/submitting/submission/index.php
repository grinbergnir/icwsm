<?php $section = "Submitting"; $subsection = "Submission"; include("../../header.php"); ?>

  <h2 class="pageTitle">Submission</h2>
         
  <h4>Author Submission Account</h4>

  <p>Authors must set up an account (or add ICWSM-14 to your list of conferences) at the ICWSM-14 web-based technical <a href="http://www.easychair.org/conferences/?conf=icwsm14">paper submission site</a>. Please make a note of your password, as this will allow you to log on to submit an abstract and paper. In order to avoid a rush at the last minute, authors are encouraged to set up their account as soon as possible, and well in advance of the January 15, 2014 abstract deadline.</p>

  <h4>Abstract and Paper Submission</h4>
<p>Electronic abstract submission through the ICWSM-14 paper submission site is required on or preferably before January 15, 2014 at 11:59 PM PST. Full papers are due via the submission site no later than Wednesday, January 22, 2014 at midnight PST.</p>

<p>To submit your abstract, please click on "New Submission" at the ICWSM-14 technical <a href="https://www.easychair.org/conferences/?conf=icwsm14">paper submission site</a>. As part of the registration of a new submission, you will be asked for the following information:</p>
<ul>
 <li>Author Information</li>
<li>Paper Title</li>
<li>Paper Abstract</li>
<li>Paper Category</li>
<li>Keywords</li>
<li>Topics</li>
</ul>
<p>Your abstract, which has no specified limit, should be submitted in the appropriate field on the paper submission page. Abstracts are generally about 100-300 words in length. You can elect to submit your paper at the same time as your abstract (by January 15), or log on later and submit it by the paper deadline (January 22). If you submit your paper prior to the deadline, you can submit updated versions of it until the January 22 deadline. After that time, all submissions will be final, and no further modifications can be made.</p>

<p>Note that papers will be assigned to reviewers based on the abstract, category, keywords and topic information submitted on January 15.  To ensure that papers are assigned to the most appropriate reviewers, it is very important that this information provide an accurate representation of the paper's topic and methodological approach.</p>

<p>We cannot accept submissions by e-mail or fax. Authors will receive confirmation of receipt of their abstracts and papers, including a paper number, shortly after submission. AAAI will contact authors again only if problems are encountered with papers.</p>


  
<?php include("../../footer.php"); ?>
