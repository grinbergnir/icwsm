<?php $section = "Submitting"; $subsection = "Tutorials"; include("../../header.php"); ?>

  <h2 class="pageTitle">Call for Tutorials</h2>

  <h4> TUTORIALS AT THE 12th INTERNATIONAL AAAI CONFERENCE ON WEB AND SOCIAL MEDIA </h4>
  <h6> SPONSORED BY THE ASSOCIATION FOR THE ADVANCEMENT OF ARTIFICIAL INTELLIGENCE </h6>
       
  <ul>
    <li>Submission Deadline: <b>February 4, 2018</b></li>
    <li>Proposal Submission Email: <b><a href="mailto:icwsm18tutorials@googlegroups.com">icwsm18tutorials@googlegroups.com</a></b></li>
    <li>Notification of acceptance: February 26, 2018</li>
  </ul>

  <p>
    The ICWSM-18 Committee invites proposals for Tutorials Day at the 12th International AAAI Conference on Weblogs and Social Media (ICWSM-18). Tutorials will be held on <b>June 25, 2018</b> in Stanford, California.  
  </p>

  <p>
    ICWSM-18 is seeking proposals for advanced tutorials on topics related to the analysis and understanding of social phenomena, particularly as seen on social media. We are looking for contributions from experts in both the social and computational sciences. The tutorials will be an opportunity for cross-disciplinary engagement and a deeper understanding of new or existing tools, techniques, and research methodologies. Each tutorial should provide either an in depth look at emerging techniques or a broad overview of an important direction in the field. For a list of 2016 and 2017 tutorials from last years, visit <a href="http://www.icwsm.org/2016/program/tutorial/">here</a> or <a href="http://www.icwsm.org/2017/program/tutorial/">here</a>. 
  </p>

<h4>Acceptance criteria</h4>

<p>
  The tutorial format will be entirely determined by the tutorial organizers. The proposals will be selected considering the following criteria:
</p>

<ul>
<li><b>Cross-pollination potential.</b> Tutorials that attract an interdisciplinary audience will be given preference. The proposals should highlight, when applicable, the tutorial potential to transfer knowledge from one discipline/area to another.</li>
<li><b>Interactivity.</b> We will favor tutorials that aim to include hands-on experiences, collaborative approaches, and interactivity. </li>
<li><b>Teaching experience of the tutors/organizers.</b></li>
</ul>

<p> 
  Proposals of tutorials presented in past events are allowed, although novelty is a plus.
</p>

<h4>Proposal content and format</h4>

<p>
  Proposals for tutorials should be no more than three (3) pages in length (please use AAAI Author Kit to format your submission; the author kit is available at <a href="http://www.aaai.org/Publications/Templates/AuthorKit17.zip">http://www.aaai.org/Publications/Templates/AuthorKit17.zip</a>). The submissions should also be written in English, and they should include the following:
</p>

<ul>
<li><b>Tutorial title and summary.</b> A short description (300 words) of the main objective of the tutorial to be published on the main ICWSM website.</li>
<li><b>Names, affiliations, emails, and personal websites of the tutorial organizers.</b> A main contact author should be specified. A typical proposal should include no more than four co-organizers. </li>
<li><b>Duration.</b> A typical tutorial would fit in a half-day format, but depending on the type of activities proposed we may accept also shorter (not less than 2 hours) or longer (not more than a full-day schedule) tutorials. The Tutorial Chairs might conditionally accept a proposal and suggest a different duration to best fit the organization of the whole event. </li>
<li><b>Tutorial schedule and activities.</b> A description of the proposed tutorial format, a schedule of the proposed activities (e.g., presentations, interactive sessions) along with a *detailed* description for each of them. </li>
<li><b>Target audience, prerequisites and outcomes.</b> A description of the target audience, the prerequisite skill set for the attendee as well as a brief list of goals for the tutors to accomplish by the end of the tutorial. </li> 
<li><b>Expected number of attendees.</b> This is orientative, and required for logistics planning.</li>
<li><b>Tutorial website and available materials.</b> The organizers of accepted tutorials will be required to set up a web page containing all the information for the tutorial attendees before the tutorial day (roughly 10 days before). The proposal should contain the list of materials that will be made available on the website.</li>
<li><b>Precedent [when available]:</b> A list of other tutorials held previously at related conferences, if any, together with a brief statement on how it follows-up on previous events. If the authors of the proposal have organized other tutorials in the past, pointers to the relevant material (e.g., slides, videos, web pages, code) should be provided.</li>
<li><b>Special requirements [when needed]:</b> A list of equipment and that needs to be made available by conference organizers (other than wifi, a projector, and a regular workshop room setup). </li>

<p>
  Submissions must be in PDF to the submission email (<a href="mailto:icwsm18tutorials@googlegroups.com">icwsm18tutorials@googlegroups.com</a>). Pre-submission questions can be sent to the chair at the same address (<a href="mailto:icwsm18tutorials@googlegroups.com">icwsm18tutorials@googlegroups.com</a>)
</p>

<h4>Important Dates<br />(All deadlines are on 23:59:59 Hawaii Standard Time)</h4>

<ul>
  <li> Tutorials proposal submission deadline: <b>February 4, 2018</b></li>
  <li> Tutorials acceptance notification: February 26, 2018 </li>
  <li> Tutorial sites and materials online: June 15, 2018 </li>
  <li> ICWSM-18 Tutorials Day: June 25, 2018 </li>
</ul>

<br>

<h4> Tutorial Chairs</h4>
<p> 
  <b>Kiran Garimella</b> (Aalto University) 
  <b>Alexandra Olteanu</b> (IBM Research)
</p>

<?php include("../../footer.php"); ?>