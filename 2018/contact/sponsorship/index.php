<?php $section = "Contact"; $subsection = "Sponsorship"; include("../../header.php"); ?>

  <h2 class="pageTitle">Sponsorship</h2>
		Coming soon.		 
<!-- <p><i>Interested in sponsoring the world's premier social media research conference?</i></p>

<p>This will be the 11th annual International Conference on Web and Social Media (ICWSM). One of the premier conferences in computational social science, this year ICWSM will be held in Montreal, Canada in May, 2017. ICWSM has emerged as a unique forum bringing together researchers working at the nexus of computer science and the social sciences, with work drawing upon network science, machine learning, human-computer interaction, psychology, sociology, political science, economics, statistics, multimedia, and communication.  The broad goal of ICWSM is to improve understanding of social media in all its incarnations. ICWSM-16 attracted well over 300 attendees and had an acceptance rate for full papers of about 20%. For more details, you can visit <a href source ="http://www.icwsm.org">http://www.icwsm.org</a>.
The list of past keynote speakers will give you an idea of the caliber and focus of a typical ICWSM conference: Robin Dunbar (University of Oxford), Jon Kleinberg (Cornell University), Eric Horvitz (Microsoft Research), Lada Adamic (Facebook), Vi&eacute;gas and Martin Wattenberg (Google), Duncan Watts (Microsoft Research), danah boyd (Microsoft Research), Jimmy Lin (University of Maryland/Twitter), and more. </p>

<p>Additionally, we have had strong industrial and governmental presence through a variety of invited talks and panels over the years: Dave Sifry (Technorati founder), Brad Fitzpatrick (Live Journal founder), Evan Williams, (Co-founder Twitter), Keith Hampton (Rutgers University), David Lazer (Harvard University), Igor Perisic (LinkedIn), Andrew Tomkins (Google), Connor Murphy (CEO/Co-founder Datahug), Stuart Mcrae (IBM Collaboration Solutions), Keith Griffin (Cisco), Macon Phillips (US White House, Head of New Media), Don Burke (CIA), Haym Hirsh (NSF IIS/Rutgers).</p>

<p>Your participation in the ICWSM-17 Sponsor Program will give your company instant visibility and access to this diverse group of researchers, students, and professionals. This year we are expecting greater participation and involvement at the conference, with over 400 paper submissions.  This will give your company access to recruiting opportunities and high-level sponsors will be able to set up recruiting and/or demo booths during the conference.  Additionally, ICWSM-17 is sponsored by the Association for the Advancement of Artificial Intelligence (AAAI).</p>


<p>The details of the various sponsorship levels and what they encompass are shown below:</p>

<b>Sponsorship Levels:</b>
<ul>
<li>Platinum $12,000</li>
<li>Gold $7,500</li>
<li>Silver $4,000</li>
<li>Bronze $2,000</li>
</ul>


<b>Benefits:</b>
<ul>
<li>All ICWSM-17 Sponsors will receive the following benefits:</li>
<ul>
<li>Company name and/or logo on conference website homepage</li>
<li>Company name and/or logo displayed in conference registration area</li>
<li>Listing on the conference proceedings sponsor page and brochure</li>
<li>Complimentary exhibit tabletop in conference center foyer OR Complimentary one-page insert placement in conference bags</li>
</ul>


<li>Sponsors at the Silver level or higher will also receive:</li>
<ul>
<li>Complimentary technical conference registration(s)/banquet ticket(s)</li>
<li>Logo on conference bag (as appropriate)</li>
<li>Listing in AI Magazine masthead for one year</li>
</ul>


<li>Sponsors at the Gold level or higher will also receive:</li>
<ul>
<li>Banquet/meal sponsorship</li>
<li>Complimentary exhibit tabletop in conference center foyer AND Complimentary one-page insert placement in conference bags</li>
<li>Complimentary quarter-page black & white ad in AI Magazine.</li>
</ul>

<li>The Sponsor at the Platinum level will receive exclusively:</li>
<ul>
<li>Sponsor one of our keynote speakers</li>
<li>Sponsor poster session</li>
</ul>
</ul>
</p>

<p>If you have any questions, please feel free to contact <a href="mailto:bhecht@northwestern.edu">Brent Hecht</a>. We look forward to seeing you in Montreal in May.</p> -->

<?php include("../../footer.php"); ?>