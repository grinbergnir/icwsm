<?php $section = "Contact"; $subsection = "Contact"; include("../../header.php"); ?>

  <h2 class="pageTitle">Contact</h2>
         
<p>Want to get in touch? Need some info? Got some comments, feedback or suggestions? Then we'd like to hear from you... and there's plenty of ways to do that!</p>

<h4>Email</h4>

<p>Drop us an email at <a href="mailto:icwsm18@aaai.org">icwsm18@aaai.org</a></p>

<h4>Twitter</h4>

<p>You can get in touch via Twitter, just send a message to <a href="http://www.twitter.com/icwsm">&#64;icwsm</a> or if you're talking about ICWSM please include the hashtag <a href="http://search.twitter.com/search?q=%23icwsm">#icwsm</a> so we can track all the chatter.</p>

<h4>Facebook</h4>

<p>We're at <a href="http://www.facebook.com/icwsm">facebook.com/icwsm</a>.</p>

<h4>Thanks</h4>

<p>Our thanks to <a href="http://www.compapp.dcu.ie/~cgurrin/">Cathal Gurrin</a>, Daragh Byrne and David Scott from ECIR 2011 for allowing us to reuse their site design.</p>

<?php include("../../footer.php"); ?>