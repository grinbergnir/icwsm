        </div></div>  

        
        <!-- SIDEBAR NAV -->
        <div id="sidebar" class="sidebar">

  
 <div id="importantDates">

  <h4 class="importantDates">WHEN, WHERE, WHO</h4>

<!--To be announced.-->




  <ul>
<li class=""><span class="datePart">January 19</span><span class="description"><a href="http://icwsm.org/2018/submitting/call-for-papers/">Abstract Submission</a></span></li>
<li class=""><span class="datePart">January 25</span><span class="description"><a href="http://icwsm.org/2018/submitting/call-for-papers/">Full Paper Submission</a></span></li>

<li class=""><span class="datePart">March 22</span><span class="description"><a href="http://icwsm.org/2018/submitting/call-for-papers/">Paper & Poster Notification</a></span></li>
<li class=""><span class="datePart">TBD</span><span class="description"><a href="http://icwsm.org/2018/submitting/call-for-papers/">Camera Ready Version Due</a></span></li>

<li class=""><span class="datePart">TBD</span><span class="description"><a href="http://icwsm.org/2018">Tutorials / Workshops</a></span></li>

<li class=""><span class="datePart">May 15-18</span><span class="description"><a href="http://icwsm.org/2018">Main Conference</a></span></li>
    
  </ul>

  <br class="clear">
          </div>


 <div id="importantDates">


<!--
   <h4 class="importantDates">SPONSORS</h4>


<br>

-->
</div>


  
<!-- <div id="twitterMessages">
  <h4 class="twitterMessages">Updates From Twitter</h4>

<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/icwsm" data-widget-id="458431519749455873">Tweets from @ICWSM</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/search?q=%23icwsm" data-widget-id="458431519749455873">Tweets about "#icwsm"</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

<script src="http://widgets.twimg.com/j/2/widget.js"></script>
  <script>
  new TWTR.Widget({
    version: 2,
    type: 'profile',
    rpp: 4,
    interval: 6000,
    width: 250,
    height: 300,
    theme: {
      shell: {
        background: 'transparent',
        color: 'black'
      },
      tweets: {
        background: 'transparent',
        color: 'black',
        links: 'gray'
      }
    },
    features: {
      scrollbar: false,
      loop: false,
      live: false,
      hashtags: true,
      timestamp: true,
      avatars: false,
      behavior: 'all'
    }
  }).render().setUser('icwsm').start();
  </script>  
  <br class="clear">
</div> -->

<!--
<div id="flickrPhotoStream">
  <h4 class="flickrPhotoStream">Flickr Photo Stream</h4>
  <iframe align="center" src="http://www.flickr.com/slideShow/index.gne?tags=icwsm" frameBorder="0" width="240" height="180" scrolling="no"></iframe>
    <br class="clear">
</div>
-->

</div>

          <div class="clear"></div>
      </div>


</div>

      </div>
      
      <div class="container_12 mainContainer">
        <div id="sponsors" >

</div>

      </div>
    </div>
   <div id='footer' class="container_12" >
<div id="footerContent">
  
    <div class="grid_6"><h4 id="footerLogo"><a href="http://www.icwsm.org/2018/" title="ICWSM-18"><span>ICWSM-18<span></a></h4></div>

  <div id="footerPagesMethods" class="grid_3">
    <h5>ICWSM-18</h5>
    <ul>
              
                <li class="home">
        
          <a href="/2018/">Welcome</a>
              
                <li class="submitting">
        
          <a href="/2018/submitting/call-for-papers/">Submitting</a>
        </li>
              
                <li class="program">
        
          <a href="/8/program/program/">Program</a>
        </li>
              
                <li class="organization">
        
          <a href="/2018/organization/organization/">Organization</a>
        </li>
              
                <li class="contact">
        
          <a href="/2018/contact/contact/">Contact</a>
        </li>
      

    </ul>
  </div>
  <div id="footerContactMethods" class="grid_3">
    <h5>Get In Touch</h5>
    <ul>
      <li><span class="label">Email:</span> <a href="mailto:icwsm18&#64;aaai.org">icwsm18&#64;aaai.org</a></li>
<!-- <li><span class="label">RSS:</span> <a href="/2012/feed">News Feed</a></li> -->
      <li><span class="label">Twitter:</span> <a href="http://twitter.com/icwsm">&#64;icwsm</a> <a href="http://twitter.com/#!/search/realtime/icwsm">#icwsm</a></li>
      <li><span class="label">Facebook:</span> <a href="http://facebook.com/icwsm">facebook.com/icwsm</a></li>
      <li><span class="label">Google+:</span> <a href="http://gplus.to/icwsm">gplus.to/icwsm</a></li>
      <li><span class="label">Flickr:</span> <a href="http://www.flickr.com/photos/tags/icwsm">ICWSM Photostream</a></li>
    </ul>
    
    <p><a href="#top">Back To Top</a><p>
  </div>

  <div class="clear">
</div>
    
    <script type="text/javascript" charset="utf-8">
      document.getElementsByTagName('body')[0].className += ' js-enabled';
    </script>
    <script src="./docs/js/jquery-1.3.2.js" type="text/javascript" charset="utf-8"></script>
    <script src="./docs/js/jquery.timers-1.2.js" type="text/javascript" charset="utf-8"></script>
    <script src="./docs/js/gallery.js" type="text/javascript" charset="utf-8"></script>
    <script src="./docs/js/init-gallery.js" type="text/javascript" charset="utf-8"></script>
    
    
  </body>
  
  
</html>